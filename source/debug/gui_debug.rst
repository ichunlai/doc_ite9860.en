﻿.. _gui_debug:

To debug firmware with Eclipse
=================================================

Environment installation
--------------------------

To install Eclipse
^^^^^^^^^^^^^^^^^^^^^^^^

1. First download `Eclipse CDT 2022-09 <https://www.eclipse.org/downloads/download.php?file=/oomph/epp/2022-09/R/eclipse-inst-jre-win64.exe>`_ and install it.

   .. image:: /_static/eclipse_download_page.png

   .. caution::

      Please use Eclipse 2022-03 or later versions. Earlier versions of Eclipse have problems with suspending operations which cause failure to suspend on-going programs.

      The screenshot may still show the 2019-03 version.

2. If the following screen appears during the installation, please download jre 1.8.0 (64 Bit) from https://www.java.com/ and install it first.

   .. image:: /_static/eclipse_prompt_install_jre.png

   .. attention::

      If your Windows is 64-bit, but the 32-bit version of the JRE is installed, the JRE may not be found during installation.

3. After installing jre, come back and install eclipse. remember to choose to install "Eclipse IDE for C/C++ Developers".

   .. image:: /_static/eclipse_select_install_cdt.png

4. Select the installation path. It is recommended that you do not use the default path and change the installation path to :file:`D:/eclipse_cpp_2022_09`. The Eclipse installer will automatically create the :file:`D:/eclipse_cpp_2022_09/eclipse`.

   .. image:: /_static/eclipse_install_path.png

5. After the installation is complete, click the Eclipse icon to execute.

.. raw:: latex

   \begin{samepage}

6. Set the path of workspace. Just use the default path.

   .. image:: /_static/eclipse_workspace_path.png

.. raw:: latex

   \end{samepage}

.. raw:: latex

   \begin{samepage}

7. It is recommended to add the eclipse installation directory to the environment variable of :envvar:`PATH` system. For example:

   .. image:: /_static/eclipse_add_env_variable.png

.. raw:: latex

   \end{samepage}

Debugging Projects
-------------------------

Here we assume that we want to debug the ``my_project`` project; see :ref:`start_a_new_project` for a detailed description of the ``my_project`` project.

To debug the program via GDB debugger, you must make sure that:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

- The EVB has been switched to Co-operative Mode.

  .. note::

     If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

Next, please do the following:

1. To use the GDB debugger, the compilation settings must be modified. So you have to execute ``<sdk_root>/build/openrtos/my_project_all.cmd`` first to open the qconf configuration and build tool.

2. Switch :menuselection:`Develop Environment --> Develop Environment` to ``Develop Mode``.

   .. image:: /_static/qconf_switch_to_develop_mode.png

.. raw:: latex

   \begin{samepage}

3. Uncheck :menuselection:`System --> Watch Dog enable`.

   .. image:: /_static/qconf_uncheck_watchdog.png

.. raw:: latex

   \end{samepage}

4. Uncheck :menuselection:`System --> Internal Settings --> Memory Debug enable`.

   .. image:: /_static/qconf_uncheck_memory_debug.png

5. Uncheck :menuselection:`Screen --> Shows Logo on Bootloader`.

   .. image:: /_static/qconf_uncheck_show_logo_on_bootloader.png

6. Switch :menuselection:`SDK --> Build Type` to ``Debug`` or ``DebugRel``.

   .. image:: /_static/qconf_switch_to_debugrel.png

   Click  |build_icon| button to build a project.

   If you have never burned an image file to NOR flash before, please refer to :ref:`write_image_to_norflash` and burn the generated image file to NOR flash first.

7. Run :file:`<sdk_root>/build/openrtos/my_project/project/my_project/jp2.cmd`.

   .. image:: /_static/gdb_jp2.png

8. Execute :file:`/build/openrtos/my_project/project/my_project/console.exe`. Enter the path to :file:`eclipse.exe` in the console window and run it. If you have previously added the eclipse installation directory to the environment variable of PATH system, here you can just type :file:`eclipse.exe`.

   .. caution::

      Be sure to start eclipse from this console window, otherwise a situation like toolchain or symbol file not being found in eclipse may happen.

   .. image:: /_static/eclipse_execute_eclipse.png

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

9. After launching eclipse, click :menuselection:`file --> new --> Makefile Project with Existing Code`.

   .. image:: /_static/eclipse_new_makefile_project.png

10. Please enter the project name you want in the :guilabel:`Project Name` field, and select the sdk root directory in the :guilabel:`Existing Code Location` field. Here we assume that the sdk root directory is located at :file:`d:/ite_sdk`, and the :guilabel:`Toolchain for Indexer Settings` field is ``<none>`` (it doesn't matter if you don't select the correct because we don't build projects in the eclipse IDE). After setting up, click :guilabel:`Finish`.

   .. image:: /_static/eclipse_new_project_toolchain.png

11. If success, you will see the ``ite_sdk`` project in :guilabel:`Project Explorer`.

   .. image:: /_static/eclipse_project_explorer.png

12. After the project is successfully created, click :menuselection:`Run --> Debug Configurations…`.

   .. image:: /_static/eclipse_run_debug_configuration.png

13. Double click :guilabel:`GDB Hardware Debugging`.

   .. image:: /_static/eclipse_debug_configuration_type.png

14. There are some steps here. First, fill in the :guilabel:`Name` field with any name you want, as long as you can recognize it. Then fill in the :guilabel:`Project` field with the Project Name you filled in step 14. Fill in :guilabel:`C/C++ Application:` field with the path to the `my_project` executable file, such as ``D:\ite_sdk\build\openrtos\my_project\project\my_project\my_project``. Please remember to select the file that does not have a extension name.

   .. note::

      This path is a file path not a directory path.

   .. note::

      You can also enter ``D:\ite_sdk\build\openrtos\${env_var:CFG_PROJECT}\project\${env_var:CFG_PROJECT}\${env_var:CFG_PROJECT}`` in the "C/C++ Application:" field . This will automatically use the value of the environment variable ``CFG_PROJECT`` to determine the executable path. We recommend this approach.

   Finally, please select :guilabel:`Disable auto build`.

   .. image:: /_static/eclipse_debug_configuration_main.png

15. Next, switch to the :guilabel:`Debugger` tab. In the :guilabel:`GDB Command:` field, fill in:

   .. code-block::

      C:\ITEGCC\bin\arm-none-eabi-gdb.exe D:\ite_sdk\build\openrtos\my_project\project\my_project\my_project

   .. note::

      There are three things you need to pay attention to:

      1. If you only fill ``C:\ITEGCC\bin\arm-none-eabi-gdb.exe`` in the field, the message ``No symbol table is loaded. Use the "file" command.`` will appear in the later debug stage.
      2. ``D:\ite_sdk\build\openrtos\my_project\project\my_project\my_project`` is a file path, not a directory path.
      3. ``C:\...\arm-none-eabi-gdb.exe`` and ``D:/...my_project`` should be filled in the same line and separated by a space.

   - Check :guilabel:`Use remote target`.
   - Select ``OpenOCD (via socket)`` in :guilabel:`JTAG Devices:` field.
   - Enter ``3333`` in the :guilabel:`Port number:` field.

   .. image:: /_static/eclipse_debug_configuration_debugger.png

16. Switch to :guilabel:`Startup` tab, uncheck :guilabel:`Load Image` and :guilabel:`Load symbols`.

   .. image:: /_static/eclipse_debug_configuration_startup.png

17. Switch to :guilabel:`Source` tab and click the :guilabel:`Add` button.

   .. image:: /_static/eclipse_debug_configuration_source.png

18. Select ``Path Mapping``. Click :guilabel:`OK`.

   .. image:: /_static/eclipse_debug_configuration_path_mapping.png

19. Enter any name you like in the :guilabel:`Name:` field and click :guilabel:`Add`.

   .. image:: /_static/eclipse_debug_configuration_path_mapping_detail.png

20. Enter :file:`/cygdrive/d` in the :guilabel:`Compilation path:` field and :file:`D:/` in the :guilabel:`Local file system path:` field. It is assumed that your sdk root directory is in slot D. If it is in another slot, please change it yourself. Click :guilabel:`OK` to close the dialog box.

   .. image:: /_static/eclipse_debug_configuration_path_mapping_done.png

21. Click :guilabel:`Apply`, and then :guilabel:`Debug`.

22. Click on the :menuselection:`Windows --> Show View --> Debug Console` and you should see a window like this one, and on the left it shows that the :guilabel:`program counter` is at 0x0, and that it is currently in the ``_start()`` function. Actually, ``_start()`` is located in :file:`<sdk_root>/openrtos/boot/fa626/startup.S`. Because it is written in assembly language, it says :guilabel:`No source available for ...`. If your project was built with the :menuselection:`SDK --> Build Type` compiled with the Debug option, you will see the assembly language code here. The :guilabel:`Debugger Console` below will show the gdb output.

   .. image:: /_static/eclipse_debug_break.png

   You can type ``b BootInit`` or ``c`` in the :guilabel:`Debugger Console` to see if the program will stop at the ``BootInit`` function. if it does, it is successful.

   .. image:: /_static/eclipse_debug_break_at_bootinit.png

.. note::

   To debug a project once again later, just make sure to

   1. First run :file:`jp2.cmd` in the project's directory.
   2. Make sure you open eclipse through :file:`console.cmd` in the project directory.
   3. Select :menuselection:`Run --> Debug History --> ite_sdk` from the eclipse menu to debug again.

.. raw:: latex

    \newpage
