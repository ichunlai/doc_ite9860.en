.. _symbol_table:

Generate Symbol Table
================================================================

After the image is compiled, the :file:`<project_name>.symbol` file will be generated in the build directory. As shown in the figure below:

.. image:: /_static/file_struct_symbol_table.png

The Symbol file states the location of each variable and function, and the range and size assigned to each section. You can refer to this table file to read the corresponding memory location via USB to SPI converter board to check the current status.

