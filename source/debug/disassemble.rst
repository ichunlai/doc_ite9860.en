.. disassemble:

To generate assembly language code
======================================

Please open the qconf setup and build tool and check :menuselection:`Debug --> Output debug files`. click |build_icon| button to re-build the project.

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

.. image:: /_static/qconf_check_output_debug_files.png

When the build is complete, a disassembly code is generated in the ``<sdk_root>/build/openrtos/<project_name>/project/<project_name>/<project_name>.dbg``.

.. image:: /_static/file_struct_disassemble_result.png

