.. _vscode_debug:

To debug firmware with VS Code
================================

VS Code Download and Installation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please download Visual Studio Code from Microsoft and install it: https://code.visualstudio.com/

   .. image:: /_static/vscode_download.png

Install C/C++ Extension Pack
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

On the Extensions page of VS Code, search for the C/C++ extension pack and install it.

   .. image:: /_static/vscode_ccpp_extension_pack.png

..

   .. image:: /_static/vscode_extension_pack_list.png

The Settings before Debugging with VS Code
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Watch dog and Memory debug must be disabled.

.. raw:: latex

   \begin{samepage}

- Uncheck :menuselection:`System --> Watch Dog enable`.

   .. image:: /_static/qconf_uncheck_watchdog.png

.. raw:: latex

   \end{samepage}

.. raw:: latex

   \begin{samepage}

-  Uncheck :menuselection:`System --> Internal Settings --> Memory Debug enable`.

   .. image:: /_static/qconf_uncheck_memory_debug.png

.. raw:: latex

   \end{samepage}

.. raw:: latex

   \begin{samepage}

-  Uncheck :menuselection:`Screen --> Shows Logo on Bootloader`.

   .. image:: /_static/qconf_uncheck_show_logo_on_bootloader.png

.. raw:: latex

   \end{samepage}

- Modify :file:`launch.json`

   Open :file:`<sdk_root>/.vscode/launch.json` and change each field of logging to false to speed up single-step execution.

   .. code-block:: json

      "logging": {
          "moduleLoad": false,
          "trace": false,
          "traceResponse": false,
          "engineLogging": false,
          "programOutput": false,
          "exceptions": false
      },

.. note::

   When using VS Code for debugging, you need to disable Watch dog, Memory debug and Shows Logo on Bootloader.

.. note::

   **Precondition for VS Code Debugging**

   To use VS Code, you need to compile the project through kconfig and burn the ROM to NOR/NAND Flash, and then make sure that you have created a file system before you can use VS Code for debugging.

VS Code Debugging Scenarios
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

There are two scenarios for using VS Code.

The first is a common way to use it, that is, after loading the code into memory, do single-step execution or stop breakpoints to observe the debugging of the Stack or variable. The second is to use VS Code to check the current program counter, stack or variable debugging of the SoC when the system crashes.

These two scenarios are basically the same in VS Code, the difference is that the second scenario needs some modification in :file:`<ite_sdk>/.vscode/task.json`, Just delete "Load init script", "Load bin" in the highlight section below.

.. code-block:: json
   :emphasize-lines: 4

   {
       "label": "Before openocd",
       "dependsOrder": "sequence",
       "dependsOn": ["Load init script", "Load bin", "Switch to jtag"]
   },

Getting Started with VS code Debugging
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- After finishing the above preparation, please run path :file:`<ite_sdk>/build/openrtos/[Project Name]/project/[Project Name]/vscode.cmd` after the compilation is completed, the VS code will be executed automatically and the SDK environment will be loaded.

   .. image:: /_static/vscode_open.png

- Before debugging, ITE SoC needs to set Boot Config to Co-operative mode.

   .. list-table::
      :widths: 30 30 40
      :header-rows: 1

      * - BOOT_CFG1
        - BOOT_CFG0
        -
      * - 0
        - 0
        - SD Card Boot
      * - 0
        - 1
        - NOR Flash Boot
      * - 1
        - 0
        - SPI NAND Boot
      * - 1
        - 1
        - Co-operative mode

- Here is a brief description of the first debugging scenario, click :menuselection:`Run --> Start Debugging` will load the image into the ITE SoC's memory via the usb to spi debugging tool.

   .. image:: /_static/vscode_start_debug.png

- Image will enter startup.s after loading.

   .. image:: /_static/vscode_startup_dot_s.png

- Click :menuselection:`Run --> Continue` or press `F5` to start running, you can view the debug log of UART output simultaneously during the run. Please refer to the following diagram for single-step execution and setup of breakpoints, or look for the operation instructions on the VS Code website.

   .. image:: /_static/vscode_debug_menu.png

.. raw:: latex

    \newpage
