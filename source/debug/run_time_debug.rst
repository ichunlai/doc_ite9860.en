.. _run_time_debug:

Run Time Debug Capability
=======================================

ITE SDK provides several runtime debugging capabilities for developers to debug their programs.

Runtime Statistic Features
-----------------------------------

The main purpose of this capability is to allow the system to periodically output the current system status through the debug message device. The status that can be displayed are

- Heap usage status
- Current DRAM bandwidth usage status
- GPIO mapping settings
- FAT related statistics

The above features can be enabled by checking the corresponding option under the :menuselection:`Debug --> Enable Runtime Statistic` option of the qconf configuration and build tool. Later we will describe the detailed setup procedures and results of each function. Please check the system for any abnormalities based on the output information. Please note that because too many debug functions can seriously affect performance, it is recommended that only :guilabel:`Heap statistic` is left on, and other parts can be used to help with debug if there are any abnormalities, and are recommended to be closed when the problem is solved.


Debug Message
------------------------------

To output debug messages, you must first set the debug message output device. The debug message output device can be selected through the qconf configuration and build tool. :guilabel:`UART interface` is usually the most recommended one, followed by :guilabel:`Print buffer`. if you use :guilabel:`Print buffer`, you need to use :file:`print.cmd` under project's binary output directory to get the output debug message via USB to SPI board.

To set up the debug message output device, run :file:`<sdk_root>/build/openrtos/my_project_all.cmd` to open the qconf setup and build tool.

- Switch :menuselection:`Develop Environment --> Develop Environment` to :guilabel:`Develop Mode`.

   .. image:: /_static/qconf_switch_to_develop_mode.png

- Click :menuselection:`Debug --> Debug Message Device`. You can select the device to which you want to export the debug message.

   .. image:: /_static/qconf_debug_debug_meg_device.png

If you choose to use :guilabel:`Print Buffer` to output the debug message, to see the output debug message, you must run :file:`<sdk_root>/build/openrtos/<project_name>/project/<project_name>/print.cmd` (please note that this program is mutually exclusive with the USB TO SPI utility and cannot be opened at the same time).

.. image:: /_static/file_struct_print_cmd.png

Task list
-----------------------

This capability allows the system to periodically output the status of all current Tasks via the debug message device. To enable this capability, run :file:`<sdk_root>/build/openrtos/my_project_all.cmd` to open the qconf setup and build tool.

- Check :menuselection:`OpenRTOS --> Use Trace Facility`.

   .. image:: /_static/qconf_check_use_trace_facility.png

- And then check :menuselection:`Debug --> Enable Runtime Statistic` and :menuselection:`Debug --> Enable Runtime Task List Statistic`.

   .. image:: /_static/qconf_check_enable_runtime_task_list_statistic.png

The output example is as follows:

.. code-block:: shell

    TASKLIST:
    Tmr Svc         X       5       9795    3
    _raProbeHandler R       1       19805   19
    MainTask        R       1       9488    1
    LinphoneTask    R       1       11522   10
    itv_hw_overlay_ R       1       19905   16
    DriveProbeTask  R       1       19905   6
    IDLE            R       0       19968   2

The first column is :guilabel:`Task Name` and the second column is :guilabel:`Task Status`:

.. code-block::

    X: RUNNING
    B: BLOCKED
    R: READY
    D: DELETED
    S: SUSPENDED

The third column is Proirity (the larger the value the higher the priority). The fourth column is the current maximum usage of Stack in 4 bytes. The fifth column is the Task's serial number. For more details, please refer to https://www.freertos.org/a00021.html#vTaskList.

.. _monitor_memory_usage:

To observe Memory usage
----------------------------------

This function allows the system to periodically output the current Heap usage status through the debug message device. The steps are as follows:

- Check :menuselection:`Debug --> Enable Runtime Statistic` and :menuselection:`Debug --> Enable Runtime Statistic --> Enable Runtime Heap Statistic`.

   .. image:: /_static/qconf_check_enable_runtime_heap_statistic.png

   The output example is as follows:

   .. code-block:: shell

      HEAP newlib : usage = 23395928 / 42745088(54 % ), addr = 0x13B8300

where usage indicates the current heap usage/heap capacity (usage percentage) and addr indicates the starting address of the heap.

- If you check :menuselection:`Debug --> Enable Malloc Debug Library` (you first need to uncheck :menuselection:`System --> Internal Settings --> CPU write-back cache enable`):

   .. image:: /_static/qconf_check_enable_malloc_debug_library.png

   Detailed information can be output for each configuration, examples are as follows:

   .. code-block:: shell

      <MALLOC_STATS>       1 x        4 Bytes in .../port_thread.c : 50, generations : 4433
      <MALLOC_STATS>       1 x        8 Bytes in .../linphonecore.c : 397, generations : 3555
      <MALLOC_STATS>       1 x       19 Bytes in .../linphonecore.c : 3175, generations : 3557
      <MALLOC_STATS>       1 x       22 Bytes in .../linphonecore.c : 1249, generations : 3572
      <ALLOC_STATS>        6 x        8 Bytes in .../lpconfig.c : 72, generations : 3360 3379 3446 ...
      <MALLOC_STATS>       2 x        4 Bytes in .../lpconfig.c : 73, generations : 3466 3473

   The first line indicates that the program is configured with one 4-byte-memory in ``port_thread.c, line. 50``. Generations indicates the configured serial number. For more details, please refer to http://www.hexco.de/rmdebug/.

To observe CPU usage
----------------------------------

This feature allows the system to periodically output the current CPU usage of each Task via the debug message device. The usage statistics are recalculated after each output. To enable this feature, check the :menuselection:`OpenRTOS --> Use Trace Facility` and :menuselection:`OpenRTOS --> Generate Run Time statistics`:


.. image:: /_static/qconf_check_use_trace_facility_and_generate_run_time_statistics.png

And :menuselection:`Debug --> Enable Runtime Statistic` and :menuselection:`Debug --> Enable Runtime Statistic --> Enable Runtime Task Time Statistic`.

.. image:: /_static/qconf_check_enable_runtime_task_time_statistic.png

Output examples are as follows:

.. code-block:: shell

    IDLE            37268           74
    itv_hw_overlay_ 1               <1
    ms_ticker_run   1045            2
    MainTask        11202           22
    ms_ticker_run   0               <1
    USBEX_ThreadFun 1               <1
    UsbHostDetectHa 0               <1
    tcpip_thread    69              <1
    NetworkTask     6               <1

The first column is the Task Name, the second column is the time spent in ms, and the third column is the percentage share of CPU time in the measured time.
