.. _use_spitool_rw_memory.rst:

To use the USB To SPI Tool to read/write registers, read/write memory, and dump ROM
===============================================================================================

ITE USB To SPI Tool can be used to read/write registers or read/write memory to obtain or control the system status, so as to monitor some variables or register changes during runtime, which can be used to help debug.

For detailed steps on how to read/write registers, read/write memory and dump ROMs via the USB To SPI Tool, see :ref:`usb_spi_tool_read_reg`, :ref:`usb_spi_tool_rw_memory`, :ref:`dump_norflash`, :ref:`dump_nandflash`.
