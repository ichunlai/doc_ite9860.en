.. _run_time_error:

Run Time Errors
================================================================

When an abnormality occurs in the system, the following information is often dumped:

.. image:: /_static/teraterm_core_dump.png

You can deduce the location of the possible problems in the software from the XXX Error displayed in the first line of the screen and the address information displayed in the second line. See :ref:`run_time_exception_error` for details of the error message displayed in the first line, and :ref:`back_trace` for how to deduce the location of a possible problem in the software from the address message displayed in the second line.

.. _run_time_exception_error:

Run time exception error
----------------------------------------

The following is a detailed description of the error messages displayed when the system crashes:

- Data Abort Error:

  Usually read/write to an invalid location (e.g. 0xFFFF0000), or there is an alignment problem. (e.g. read/write an Integer pointer but the position given is 0x10003, not on the 4 byte alignment)

- Divide by Zero Error:

  A divide-by-zero problem occurs when the denominator is zero.

- Undefined Error:

  The CPU throws an exception when it finds that it doesn't know the instruction after fetching it from memory and decoding it.

- Prefetch Abort Error:

  This exception will be triggered when the Fetch instruction takes an invalid location.

- Memory Debug Error:

  When write protection is turned on for a segment memory location and the segment is written to by any HW (CPU, Graphic Engine, H264 decoder, etc.), the exception will be triggered. (Please note that write back mode is turned off because the function is not caused by ARM's Exception but by an external interrupt, so the relative backtrace is less accurate in write back mode).

Error judgment criteria
---------------------------------

Usually Data abort, Divde by Zero can be seen through the :file:`backtrace.cmd` to see the complete call stack under normal circumstances, so it is easier to deduce which code is written wrong or logic. However, such as Undefined Error, Prefetch Abort Error, excluding the problem of HW, the following errors often occur in pure software:

- The stack space of the current thread is covered by other threads with wrong logic or overwritten to the relevant memory location.
- Wrong global variable overflow causes overwriting to other global variables (e.g., array size is defined as 4, and the result is written to exceed the 4), causing abnormal behavior in threads that use other variables.
- The global variables shared between Threads are not protected by mutex, which leads to abnormal behavior when a race condition occurs.
- Wrong parameters for HW Engine or illegal use of configuration memory, e.g., memory is still passed to HW use after being released, resulting in writing to another memory segment.
