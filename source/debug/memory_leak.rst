.. _memory_leak:

Memory leak
===================================

Please refer to :ref:`monitor_memory_usage` to turn on the :guilabel:`Runtime Heap statistic` function to see if there is a problem of memory keeping growing under certain operations. First, make sure there is no memory leak problem. Then follow these steps

- Disable the CPU cache write back function by unchecking the :menuselection:`System --> Internal Settings --> CPU write-back cache enable` box in the qconf setup and build tool.

   .. image:: /_static/qconf_uncheck_writeback.png

- Check :menuselection:`Debug --> Enable Malloc Debug Library` to use the output of Rmalloc to view the source of the configured memory to help analyze the possible sources of the leaks, and then use code trace to further locate the part of the error logic.

   .. image:: /_static/qconf_check_enable_malloc_debug_library.png
