.. _back_trace:

Back Trace
===================================

When the system crashes, the message shown below will be displayed. You can fill in the backtrace tool with the address information shown in the second line to deduce the location of the last program that caused the crash.

.. image:: /_static/teraterm_core_dump.png

The steps are as follows:

1. Run :file:`<sdk_root>/build/openrtos/<project_name>/project/<project_name>/backtrace.cmd`.
2. Fill in the address information, as shown in the following figure:

   .. image:: /_static/console_backtrace_fill_address.png

3. Typing Enter will bring up the program to deduce the corresponding source code address based on the address information entered. The upper line of information displayed is closer to the address of the system when it is dropped.

   .. image:: /_static/console_backtrace_result.png
