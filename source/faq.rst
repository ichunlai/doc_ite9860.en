﻿.. _faq:

FAQs
********************************

After a project has been compiled, the entire SDK path has been changed for some reason, and the project cannot be successfully compiled again. How to fix it?
---------------------------------------------------------------------------------------------------------------------------------------------------------------------

Assuming you are currently compiling the ``MY_PROJECT`` project, please first remove all folders and files in the directory :file:`<sdk_root>/build/openrtos/MY_PROJECT` except for:

    * :file:`<sdk_root>/build/openrtos/codec`
    * :file:`<sdk_root>/build/openrtos/codecex`
    * :file:`<sdk_root>/build/openrtos/MY_PROJECT/.config`

Then run :file:`MY_PROJECT_ALL.cmd` (note that there should be no Chinese characters or spaces in the compilation path) to compile correctly.

How do I add a library to the SDK?
---------------------------------------------------------------------

#. Create a folder named ``{lib_name}`` under the path :file:`sdk/share/`.
#. Place all the ``*.c; *.h`` files in the code into the :file:`{lib_name}` folder.
#. Create a :file:`CMakeLists.txt` file in the :file:`{lib_name}` folder with the following contents:

    .. code-block:: shell

	add_library({lib_name} STATIC
	    file1.c
	    file2.c
	    file3.h
	    file4.h
	    …
	)

#. Modify :file:`build.cmake` under path :file:`sdk/` by adding:

    .. code-block:: shell

	ITE_LINK_LIBRARY_IF_DEFINED_CFG_BUILD_LIB({lib_name})

#. In the future, just add the following statement to ``Kconfig`` in the ``project`` so that the library can be linked during compilation:

    .. code-block:: kconfig

	config BUILD_{lib_name}
	    def_bool y


.. note::

	To add library options to ``Kconfig``, in addition to adding a new statement to :file:`sdk/Kconfig`, such as ``config {lib_name}_ENABLE …`` (see :file:`sdk/Kconfig` for other editing examples), you also need to add to :file:`sdk/def.cmake`:

	.. code-block:: kconfig

	    if (DEFINED CFG_{lib_name}_ENABLE)
	        set(CFG_BUILD_{lib_name} 1)
	    endif()

    In the future, you only need to check the box in ``Kconfig`` to use it, no need to add a description to ``Kconfig`` in the ``project``.


.. _multiple_toolchain:

What should I do if I want multiple toolchains co-exist at the same time?
--------------------------------------------------------------------------

If you need to develop software for both IT985X series and IT986X series chips simultaneously, you may encounter the problem of switching toolchain.

Because IT985X series must use :file:`ITEGCC.####.##.##.gcc-4.8.5.7z` toolchain for software development, while the IT986X series uses a newer version of the toolchain. In this case, if the two versions of the toolchain can coexist, the trouble of switching toolchains can be eliminated.

To co-exist multiple toolchains at the same time, follow these steps:

1. First, change the name of the toolchin directory according to the platform where you are building your project

    * If your platform is Windows, rename :file:`c:/ITEGCC`, for example, to :file:`c:/ITEGCC.2020.01.14.gcc-5.4.0`.

    * If your platform is Linux, rename :file:`/opt/ITEGCC`, for example, to :file:`/opt/ITEGCC.2020.01.14.gcc-5.4.0`.

2. Next, you need to modify the contents of the following files in the sdk:

    * ``/build/openrtos/common.cmd``
    * ``/build/linux/common.sh``
    * ``/openrtos/toolchain.cmake``
    * ``/sdk/target/debug/fa626/libcodesize.cmd.in`` (Not necessary to be modified)
    * ``/sdk/target/debug/sm32/libcodesize.cmd.in`` (Not necessary to be modified)

    Search for the ``ITEGCC`` string in these files and modify it to :file:`ITEGCC.2020.01.14.gcc-5.4.0`.

Other versions of toolchain can be modified and installed as described above.
