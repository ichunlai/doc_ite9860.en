..
  負責人: 鴻瑜

.. _touch_panel_develop:

Guidelines for Writing Touch Drivers
=====================================

This chapter provides a guide for users who need to write a driver when using a new touch device and the existing SDK does not support it.

Preparation
------------------------

Take the test_touch project (located at :file:`<sdk_root>/project/test_touch`) as the validation environment.

1. Create a new TP driver (name the driver, for example: ``tp001``)

  - Make a copy of :file:`<sdk_root>/sdk/share/tslib/plugins/ft5316-raw.c` and change the file name to :file:`tp001-raw.c` in lowercase.
  - Replace all ``FT5316`` strings in :file:`thetp001-raw.c` file with ``tp001``.

2. Create a new ``.conf`` file

  - Make a copy of :file:`<sdk_root>/sdk/target/touch/ft5316.conf` and change the file name to :file:`tp001.conf` in lowercase.
  - Replace all ``FT5316`` strings in the :file:`tp001.conf` file with ``tp001``.

3. Modify :file:`<sdk_root>/sdk/share/tslib/plugins/plugins.h`.

  - Add a new line: ``TSLIB_DECLARE_MODULE(tp001);``.

4. Remember to change the setting of :menuselection:`Peripheral --> Touch Enable --> Touch module` to ``tp001`` when you use the qconf setup and build tool to build the project.

Implementation (take GT911 single-finger touch control as an example)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Before PORTING
""""""""""""""""""""""""

1. Assume that the problems on the H/W circuit are solved, i.e., INT is responsive and I2C has ACK.

  - Some INTs are not responding because there is no power-on process, or INTs are not responding until after the program.
  - If I2C has no ACK, it may be due to slave address error or there is still power-on process running (EX: ``GT911``).

2. The so-called implementation is to implement the following six vendor FUNCTION.

  -	``_tpInitSpec_vendor()``
  -	``_tpDoPowerOnSeq_vendor()``
  -	``_tpDoInitProgram_vendor()``
  -	``_tpReadPointBuffer_vendor()``
  -	``_tpParseRawPxy_vendor()``
  -	``_tpParseKey_vendor()``

  Among them, ``_tpDoPowerOnSeq_vendor()``, ``_tpDoInitProgram_vendor()`` and ``_tpParseKey_vendor()`` are non-essential functions, depending on the actual application of TP. Especially ``_tpParseKey_vendor()`` generally TPs do not have the function of TOUCH KEY attached to them, so you can ignore this function. Most TPs operate normally after POWER-ON, so it is not necessary to implement ``_tpDoPowerOnSeq_vendor()`` and ``_tpDoInitProgram_vendor()``, while ``_tpInitSpec_vendor()``, ``_tpReadPointBuffer_vendor()`` and ``_tpParseRawPxy_vendor()`` are the necessary functions to be implemented.

Implement  ``_tpInitSpec_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Fill in the appropriate values according to the definition of the ``TP_SPEC`` structure members. The following is a description of the ``TP_SPEC`` structure members.

``tpIntPin``

  GPIO number of INT pin.

``tpIntActiveState``

  Setting of INT active status. 1: Active High, 0: Active Low.

``tpIntTriggerType``

  How to interrupt trigger. 0: edge trigger, 1: level trigger.

``tpWakeUpPin``

  Wake-Up pin GPIO number. -1 indicates not using Wake-Up pin.

``tpResetPin``

  Reset pin GPIO number. -1 indicates not using reset pin.

``tpIntrType``

  ITE defines three TYPEs of the behavior of INT PIN sends interrupt (as follows):

  .. image:: /_static/diagram_touch_panel_interrupt_type.png

  - 0: indicates TYPE I, keep state when touch down. During finger touching TP, it will keep INT ACTIVE state until finger leaves TP (EX: ``ZT2083``).
  - 1: indicates TYPE II, during finger touching TP, INT will continue to HIGH/LOW change until the finger leaves TP (EX: ``FT5316``).
  - 2: indicates TYPE III, when the finger touches the TP, INT will always be ACTIVE state; even if the finger leaves the TP, it will not be restored, unless the TP send clear command (or after read data), it will be restored to NON-ACTIVE state (EX: ``CYTMA568``).

  Please set the correct type according to the actual condition of TP.

``tpInterface``

  Interface type, 0: I2C, 1: SPI, 2: Other

``tpI2cDeviceId``

  I2C device ID(slave address), if the interface of TP is I2C.

``tpHasTouchKey``

  0: NO touch key, 1: TP comes with touch key (EX: ``CYTMA568``).

``tpIntUseIsr``

  0: polling INT signal, 1: INT use interrupt.

``tpMaxFingerNum``

  The native maximum number of fingers of this TP (not S/W limit maximum number of fingers).

``tpIntActiveMaxIdleTime``

  When the INT ACTIVE time exceeds this setting, the driver will force INT to return to NOT ACTIVE state to avoid the bug that some TPs (EX: TPs with tpIntrType = 2) cannot TOUCH-UP by themselves. default value: 33ms, cytma568: 100ms.

``tpMaxRawX``

  TP resolution of X.

``tpMaxRawY``

  TP resolution of Y.

``tpScreenX``

  Screen resolution of X (LCD WIDTH).

``tpScreenY``

  Screen resolution of Y (LCD HEIGHT).

``tpCvtSwapXY``

  The X-axis and Y-axis of TP coordinates are replaced.

``tpCvtReverseX``

  The X-axis of TP coordinates to be reversed.

``tpCvtReverseY``

  The Y-axis of TP coordinates to be reversed.

``tpCvtScaleX``

  Not currently useful, default value is 0.

``tpCvtScaleY``

  Not currently useful, default value is 0.

``tpEnTchPressure``

  Not currently useful, default value is 0.

``tpSampleNum``

  The maximum number of fingers that the TP driver can output (S/W limits the maximum number of fingers).

  - 0: NO scense.
  - 1: single touch.
  - 2 ~ 10: multi-touch ("tpSampleNum" must be <= "tpMaxFingerNum").

``tpSampleRate``

  Set the sampling rate in Millisecond, and the range is 8~33 ms (120~30 FPS).

``tpIdleTime``

  The default value is 2ms for the time interval of each TP sample update check.

  Because TP driver will create a thread to specifically check whether INT is ACTIVE, and tpIdleTime is the sleep time of this thread. If there is no IDLE 2ms, it will make this thread too busy, resulting in other thread performance being reduced. If the set time is too long, it will make the touch response not immediate enough.

``tpIdleTimeB4Init``

  The default value of IDLE interval before initialization of TP driver is 10ms.

  The reason is the same as tpIdleTime, but before TP initialization, there is no issue of having to respond immediately to the touch.

``tpReadChipRegCnt``

  The number of bytes to be read for reading the TP point buffer.

``tpHasPowerOnSeq``

  To set power-on sequence to be enabled.

``tpNeedProgB4Init``

  To enable the initialization programming process. Some TPs must go through the initialization programming process for the TP to function properly (EX: ``GT911``).

``tpNeedAutoTouchUp``

  To enable the automatic TOUCH-UP function. In order to avoid the problem of TOUCH-UP EVENT being missed by some TPs (TPs with tpIntrType = 1).

  0: off, 1: on.

``tpIntPullEnable``

  To turn on the CHIP internal PULL-UP function of INT PIN. Some INT pins of TP cannot have PULL-UP/PULL-DOWN circuit (e.g. ``INT state`` of ``GT911`` will affect the I2C slave address of TP).

  0: Turn off the PULL function, 1: Turn on the PULL function.

Implement ``_tpDoPowerOnSeq_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Purpose: Power-up initialization. With the correct power-up sequence, in order to execute the TP initialization program, or directly into the touch control state.

Before implementing the power-on sequence, please refer to the power-on sequence description in the datasheet of each TP. Please also understand the control procedure of IT9860 GPIO PIN (please refer to :ref:`gpio`).

Some TP chips do not need power-on sequence (EX: FT5316, IT7260, ILI2118A, ST1633, ZET6221, etc.), and these TP chips can not implement this function. Most of the power-on sequence refers to the signal control of reset-pin, and usually the control flow of reset-pin is as follows:

1. First keep reset-pin in HIGH state A ms.
2. Then make the reset-pin PULL-LOW B ms.
3. Finally, keep the reset-pin in HIGH state C ms.

Some TP chips require to wait for C ms after reset (e.g. ``GT911`` requires 50 ms before HOST can send I2C signal) before the first I2C command can be sent, and achieve the purpose of delaying the sending of I2C signal by "reset-pin KEEP-HIGH for C ms".

Usually through this HIGH-LOW-HIGH format of signal control, most power-on sequence process of such reset pin can be met (EX: CYTMA568, FT6436, SIS9255, ST1727, ZET7235, etc.).

Few of the power-on sequences have more complex signal control, such as GT911, and according to the GT911 datasheet, the power-on process needs to be coordinated with the INT signal to determine the I2C slave address (please refer to the picture below).

.. image:: /_static/diagram_gt911_timing.png

According to the time sequence diagram given by GT911, the power-on sequence is as follows:

1. At the beginning, INT & RESET output as LOW 1 ms at the same time (datasheet requires > 100us).
2. Then set RESET output to HIGH 55ms.
3. Finally, set INT to GPIO input mode for 50ms (datasheet requires INT to be INPUT MODE for 50ms before I2C commands can be executed).

Implement ``_tpDoInitProgram_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Purpose: To initialize the program. To use the touch function, the TP CHIP must be accessed before it can function properly. Some TPs for writing settings, configuration, or even the whole F/W; some need to read configuration (such as resolution, direction, etc.); some are for waking up TPs into working mode.

Most of the TP CHIP can perform the function of touch without PROGRAM as long as POWER-ON; for some TPs, the driver does not even performed I2C WRITE. Only a few TP need to implement this function (e.g. CYTMA568, GSL3680, GSL1691, gt5688, GT911, ILI2118A, etc.).

Take GT911 as an example, you have to read its configuration to know its resolution, direction and INT active HIGH/LOW related information.

Implement ``_tpReadPointBuffer_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Purpose: read TP register and return BUFFER DATA.

Most of the TP chip coordinate information can be completed in one I2C read, only a very few need to read the required register through multiple reads. Please apply the S/W technique to merge these data into a buffer and pass it to the next level of function for analysis (i.e. ``_tpParseRawPxy_vendor()``).

This BUFFER should include

1. whether the data is valid
2. the number of touch fingers (single finger or multi-finger)
3. XY coordinates
4. EVENT information (DOWN/UP, etc.)

Take GT911 as an example, every time when INT pulls LOW, you must first check whether this INT is valid or not. If it is invalid, you should ignore the INT signal this time (and maintain the previous touch status); if it is valid, you should read the coordinate data ("3. Register List" in "GT911 Programming Guide Document") and write the buffer status (0x814E) as 0 at the end (refer to "5. Coordinate Reading" in "GT911 Programming Guide Document").

Implement ``_tpParseRawPxy_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Purpose: receive buffer from ``_tpReadPointBuffer_vendor()``, analyze the information of the buffer, and convert it to ``ts_sample`` format.

The ``ts_sample`` structure member (defined in :file:`<sdk_root>/sdk/include/tslib.h`) is described as follows

- x: X coordinate value, range 0 ~ (X resolution - 1).
- y: Y coordinate value, range 0 ~ (Y resolution - 1).
- pressure: The pressure value of finger touch, currently only support 0 and 1 (0 means no touch, 1 means touch).
- finger: The number of touch points during this INT ACTIVE, and if it is TP with one finger touch, or S/W forces to set it to one finger touch, only 0 or 1 will be reported.
- id: If single-finger touch, the ID is fixed to 0. If multi-finger touch, the ID of the first finger is 0 and the ID of the second finger is 1 according to the datasheet, and so on.

Take GT911 as an example, according to the "3. Register List" in the "GT911 Programming Guide", the number of touch points, X/Y coordinates and ID information (POINT 1 is ID 0) can be analyzed from BUFFER. The reading of TOUCH-UP/TOUCH-DOWN depends on the TP. Take GT911's single finger touch as an example, if the number of touch points is 0, it is TOUCH-UP, if >=1, it is TOUCH-DOWN.

Implement ``_tpParseKey_vendor()``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Purpose: Receive buffer from ``_tpReadPointBuffer_vendor()``, analyze the buffer's button information, and convert it to ``gTpKeypadValue`` format. If TP itself does not contain button function, then this function is not needed.

The format of ``gTpKeypadValue`` is defined as follows:

- gTpKeypadValue is the global variable of unsigned int (32-Bits).
- key0 is pressed if gTpKeypadValue's bit 0 is 1.
- key1 is pressed if gTpKeypadValue's bit 1 is 1.
- NO key event if gTpKeypadValue = 0.
- MAX key number: 32 keys.

If the button information and point information are not in the original point buffer, please apply the S/W technique in ``_tpReadPointBuffer_vendor()`` to bring the button information into the point buffer by yourself (e.g. ``CYTMA568``).

For the KCONFIG setting to support Touch Panel buttons, please refer to the help file 5-b of :file:`<sdk_root>/project/test_touch/test_touch.docx`).

Although GT911 has mentioned the button information in the "GT911 Programming Guide Document", it is not implemented in the driver. For the example of this function, please refer to :file:`<sdk_root>/sdk/share/tslib/plugins/cytma568-raw.c`.

At present, the button function attached to TP can be roughly divided into two forms:

1. Button register (EX: ``CYTMA568`` )

  That which buttons are pressed can be read directly from the TP register.

  .. image:: /_static/reg_map_touch_panel_button_reports.png


2. Part of TP x/y coordination (EX: ``CYTMA448`` )

The information of the button pressed is interpreted from the extension of its own TP coordinates. As shown in the picture below, the red box + yellow box is the whole TP touch range, the red box is defined as the traditional touch range, and the yellow box is defined as the button range (the button pressed is interpreted according to the coordinates).

  .. image:: /_static/e_circuit_touch_panel_frame.png

Debugging process
---------------------------

INT
^^^^^^^^^^^^^^^^^^^

Check if the system has entered ``_tpReadPointBuffer_vendor()``.

- INT ACTIVE STATE setting error.
- INT TRIGGER TYPE setting error.
- INT TYPE setting error.
- INT GPIO setting error.

READ POINT (Assuming SERIES PORT is I2C)
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Whether I2C ACK.

  - I2C SLAVE ADDRESS error.
  - I2C GPIO PIN setting error.

- Whether I2C can read data from REGISTER.

  - DATA are all 0 or 0XFF.
  - REGISTER ADDRESS is not correct.
  - The command is not correct.
  - ADDRESS format is not correct.
  - Process is not correct.

- Whether the read data is in the same format as the DATASHEET.

  - Touch the four corners of the panel to observe whether the read data has changed and whether the location and format of the changed data match.

PARSE X/Y
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- TOUCH-DOWN/TOUCH-UP.

  - Does it report immediately when your finger touches it?
  - Does it stop reporting as soon as the finger leaves the panel?
  - If the register has EVENT information, is the EVENT information of DOWN/UP accurate? If the register does not have EVENT information, is there any other way to determine DOWN/UP? Some TPs can be determined from INT state, while some can be determined by the number of fingers in the register.

- X-coordinate and Y-coordinate

  - Check if the maximum value of the native coordinates is the same as expected (e.g. X_MAX=800, see if the read X value is close to 799).
  - Whether the horizontal direction is the DATA of X coordinate and the vertical direction is the DATA of Y coordinate.
  - Whether the value of coordinates in the middle of the panel is close (X_MAX/2, Y_MAX/2).

- Coordinate direction

  - Confirm that the upper left, upper right, lower left, and lower right coordinates are close to (0,0) (X_MAX,0) (0,Y_MAX) (X_MAX, Y_MAX).

- Multi-finger touch

  - FINGERS

    Whether the number of fingers is correct or not. Whether the number of fingers left on the TP is the same as the number of fingers reported by REGISTER when any multi-finger press is removed.

  - FINGER ID

    Each touch point has a finger ID, carefully observe whether the ID is consistent with the corresponding X/Y coordinates, for example, whether the sequence is consistent (the first point is ID=0, the second point is ID=1, when the first point is TOUCH-UP, whether the second point still remains ID=1), whether the moving points are consistent (the first point is ID=0, the second point is ID=1, the first point moves, the second point does not move, whether only the coordinate with ID=0 have changed, and the coordinate with ID=1 remain in place).

Report to SDL
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- Whether the X/Y coordinates obtained from ``SDL_PollEvent()`` are correct.
- Whether the directions of the coordinates are correct.
- Whether the direction of SLIDE EVENT is correct or not.
