.. _usb_spi_tool:

usb_spi_tool - Read-write register/ read-write memory/ burning Flash
==========================================================================

usb_spi_tool is a multifunctional program that runs in Windows and can be used with USB to SPI board to do the following:

- Read and write the value of the register on chip
- Read and write the contents of the memory on chip
- Burning NOR flash
- Burning NAND flash


Before using usb_spi_tool, you must ensure that:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

- The EVB has been switched to Co-operative Mode.

  .. note::

     If you are not sure if EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

.. _usb_spi_tool_read_reg:

How to read and write the value of the register on the chip
---------------------------------------------------------------

Please ensure that:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

After confirming that the hardware connection is correct, you can start running usb_spi_tool to read and write the register values on the chip. There are two methods:

1. Direct address mode

   In Direct address mode, you must enter the complete address of the register to be read/written.

2. Indirect address mode

   In Indirect address mode, you must select the correct device type for the register to be read/written, just fill in offset in the address field. No need to fill in the full address of the register.

The following is a detailed guide on how to use Direct address mode and Indirect address mode to read and write registers, taking reading the chip id (register address ``0xD8000004``) as an example.

Direct address mode
^^^^^^^^^^^^^^^^^^^^^^^^^

The detailed steps to use Direct address mode to read and write registers are as follows:

1. Excute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2. Uncheck :menuselection:`Register --> Indirect address`.
3. Enter the full register address to be read directly in the :menuselection:`Register --> Address` field. Then, click the :guilabel:`Read` button to see the retrieved value in the :guilabel:`Data` field.

   The following picture shows the value of the IT986x register address ``0xD8000004`` read via :file:`usb_spi_tool.exe`.

   .. image:: /_static/usb2spi_read_write_reg.png

4.	If you want to write a value to the register, fill in :menuselection:`Register --> Address` field with the complete address of the register you want to write the value to. Enter the value to be written in :menuselection:`Register --> Data` field. Then click the :guilabel:`Write` button. There will not be any message if the write is successful. Please try to retrieve the value of the register again to confirm if the write is successful.

Indirect address mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The detailed steps to read and write registers with Indirect address mode are as follows:

1. Excute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2. Check :menuselection:`Register --> Indirect address`, and select the device type to which the registers belong.

   For example, if the chip id register we want to read belongs to General Register device type, then we should select :guilabel:`GeneralReg`. After selecting the device type, a message will pop up indicating that the base address of the General Register device type register is ``0xD8000000``.

   .. image:: /_static/usb2spi_select_dev_type.png

3. Fill in the :menuselection:`Register --> Address` field with the offset address.

   For example, the full address of the chip id register we want to read is ``0xD8000004``, because we have already selected its device type as :guilabel:`General Register` (base address is ``0xD8000000``) in the previous step, so we just need to fill in the :menuselection:`Register --> Address` field with 4.

   .. image:: /_static/usb2spi_read_write_reg_indirect.png

.. _how_to_load_init_script:

How to load the initial script manually
--------------------------------------------

If you want to read/write memory, or perform memory read/write tests that require memory access, you must first boot up the firmware on the chip properly, or manually load the initial script via usb_spi_tool program to correctly initialize the clocks required for the memory.

.. note::

   Initial script contains a bunch of register addresses and register value pairing, and its main function is to correct the clock required to initialize the chip.

.. caution::

   Do not manually load the initial script while the firmware in the chip is activated; this may cause abnormal performance.

To manually load the initial script via usb_spi_tool, first ensure that:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

- The EVB has been switched to Co-operative Mode.

  .. note::

     If you are not sure if the EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

After confirming that the hardware connection is correct, you can start running usb_spi_tool to read/write the contents of the memory on the chip. The steps are as follows:

1. Excucte :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2. Switch to the :guilabel:`Host` tab
3. Click :menuselection:`Script --> Load Script` button, and a dialog indicating opening files will pop up.

   .. image:: /_static/usb2spi_load_script.png

4. Select the initial script you want to load (usually placed under :file:`<sdk_root>/sdk/target/debug` path) and click :guilabel:`Open`.

   .. image:: /_static/usb2spi_load_script_dialog.png

   If it succeeds, the message ``Load Script Finished`` will appear.

How to Perform On-Chip Memory Self-Access Test
-----------------------------------------------------

The IT986x chip has a built-in self-test mechanism for memory access. This function can be used by usb_spi_tool to test whether the CPU in the chip and peripheral modules are accessing memory properly.

To perform this test, first ensure that:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

- The firmware in the chip is not activated and the initial script is loaded.

  .. note::

    If you do not know how to load the initial script, please refer to :ref:`how_to_load_init_script`.

After confirming that the hardware connection is correct, you can start running usb_spi_tool to read/write the contents of the memory on the chip. The steps are as follows:

1. Excute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2.	Fill in the appropriate value in the :menuselection:`Host --> Memory test --> End` field according to the capacity of built-in memory of the chip. The values to be filled in for different memory capacities are listed at the bottom.

   .. list-table::
      :header-rows: 1

      * - Memory Capacity
        - Value to be filled in
      * - 16MB
        - 7FF
      * - 32MB
        - FFF
      * - 64MB
        - 1FFF
      * - 128MB
        - 3FFF

   The formula is:

   .. math::

       \mathtt{Value\ to\ be\ filled\ in} = \frac{\mathtt{Memory\ Capacity}}{\mathtt{0x2000}} - 1

3.	Click :guilabel:`Memory Test` button to start the test

   .. image:: /_static/usb2spi_mem_test.png

   If it passes the test, the message ``Memory test pass`` will pop up. If it fails, ``Memory test fail`` will pop up.

Accessing the GPIO status during runtime
------------------------------------------------

The usb_spi_tool allows access to GPIO status via PC. Here are the functions that can be performed:

1. Switch the alternate function of a GPIO pin.
2. Force a GPIO pin to switch alternate function to GPIO output mode and output high potential.
3. Force a GPIO pin to switch alternate function to GPIO output mode and output low potential.
4. Force a GPIO pin to switch alternate function to GPIO input mode, and get the potential status of the GPIO pin.

The following section will explain how to perform these functions via usb_spi_tool. Before that, please make sure:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

After confirming that all hardware connections are correct, you can start to perform the above four functions. The following explains each one of them separately.

Switching the alternate function of a GPIO pin
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here are the steps:

1. Execute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2. Fill in the :menuselection:`Host --> GPIO --> GPIO` field with the number of the GPIO pin to be controlled.
3. Fill in the :menuselection:`HOST --> GPIO --> MODE` field with the alternate function mode to be switched to.

   .. image:: /_static/usb2spi_switch_gpio.png


.. raw:: latex

    \newpage

Force a GPIO pin to switch alternate function to GPIO output mode and output high potential
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here are the steps:

1. Execute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2. Fill in the :menuselection:`Host --> GPIO --> GPIO` field with the the number of the GPIO pin to be controlled.
3. Click button :menuselection:`HOST --> GPIO --> Output 1`.

   .. image:: /_static/usb2spi_switch_gpio_output_high.png

.. raw:: latex

    \newpage

Force a GPIO pin to switch alternate function to GPIO output mode and output low potential.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here are the steps:

1. Execute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2. Fill in the :menuselection:`Host --> GPIO --> GPIO` field with the the number of the GPIO pin to be controlled.
3. Click button :menuselection:`HOST --> GPIO --> Output 0`.

   .. image:: /_static/usb2spi_switch_gpio_output_low.png

.. raw:: latex

    \newpage

Force a GPIO pin to switch alternate function to GPIO input mode, and get the potential status of the GPIO pin.
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here are the steps:

1. Execute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2. Fill in the :menuselection:`Host --> GPIO --> GPIO` field with the number of the GPIO pin to be controlled.
3. Click button :menuselection:`HOST --> GPIO --> Input Read`.

   .. image:: /_static/usb2spi_switch_gpio_input.png

Controlling the LCD output screen during runtime
---------------------------------------------------

The usb_spi_tool provides the function to change the LCD output screen dynamically when the LCD controller is properly activated. In addition, it also provides the function of capturing the LCD output screen as a picture during runtime. The main purpose of these functions is to allow users to verify if their LCD controller is set up correctly and to capture a complete picture without writing a program.

To perform these functions, first ensure that:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

After confirming that the hardware connection is correct, we will explain how to verify the correctness of the selected lcd script and how to capture the LCD output screen.

How to verify the correctness of the selected lcd script
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Here we will take IT9866 EVB and TL068HWXH08 panel for example. Firstly, we have to prepare the initial script for the chip and the lcd script for the panel.

- The initial script corresponding to IT9866 EVB is :file:`IT9860_360Mhz_DDR2_360Mhz.txt`, located in :file:`<sdk_root>/sdk/target/debug` directory.
- The LCD script corresponding to IT9866 EVB is :file:`IT9860_MIPI_TL068HWXH08_EK79030_480x1280_4LANE_byteclk53.txt`, located in :file:`<sdk_root>/sdk/target/lcd` directory.

  .. note::

      The LCD script here doesn't have the backlight enabled, so you have to add the backlight enabling setting according to your circuit design. If your circuit uses PWM to light the backlight, please add the appropriate GPIO settings to keep the PWM pin at high status when using the LCD tool to generate the LCD script.

1. Click :guilabel:`Host` to switch to :guilabel:`Host` tab.
2.	Refer to ":ref:`how_to_load_init_script`" to load the initial script and LCD script. Always load the initial script first and then load the LCD script.
3.	Click :guilabel:`LCD` to swith to :guilabel:`LCD` tab.
4.	Select the :menuselection:`Function --> Load Image` option, and fill in :menuselection:`Function --> File` field with the path where the picture to be loaded locates.

   .. image:: /_static/usb2spi_lcd_load_img.png

   .. note::

      Width/Height/Pitch will be displayed when loading pictures (the resolution of the picture must be the same as the resolution of the LCD Panel). Please select the correct :guilabel:`LCD Data Format` according to the LCD settings.

5.	Click button :guilabel:`Exec`, and load the picture into the memory corresponding to the LCD frame buffer inside the chip.
6.	When the image is loaded, the ``Load Script Finished`` message window will pop up.
7.	If everything is set up properly, the pictures just loaded should be displayed on the LCD screen of the platform after completion.

How to capture the LCD output screen
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To obtain the screenshot of LCD output, we must first know the five values of LCD, which are as follows

1. LCD width: can be obtained from the 0~11 bits of the register ``0xD0000008``.
2. LCD height: can be obtained from the 16~27 bits of register ``0xD0000008``.
3. LCD pitch: can be obtained from register ``0xD000000C``.
4. LCD frame buffer memory address: can be obtained from registers ``0xD0000010``, ``0xD0000014``, ``0xD0000018``. These three register addresses correspond to the address of frame buffer A, frame buffer B and frame buffer C respectively.
5. The color format of LCD frame buffer can be obtained from bits 12~14 of register ``0xD0000004``. The relation between its value and color format is as follows:

   .. list-table::
      :header-rows: 1

      * - 0xD0000004 [14:12]
        - Color Format
      * - 0
        - RGB565
      * - 1
        - ARGB1555
      * - 2
        - ARGB4444
      * - 3
        - ARGB8888
      * - Others
        - YUV

Please start the system first and obtain the four values as described in :ref:`usb_spi_tool_read_reg` under the condition that you can see the screen from the LCD. Then, please follow these steps:

1. Execute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2. Click :menuselection:`LCD --> Dump Memroy`.
3. Fill in the :menuselection:`LCD --> File` field with the path where the file is to be dumped, and remember to set the extension to ``.bmp``.
4. Fill in the :menuselection:`LCD --> Setting --> Width` field with LCD width.
5. Fill in the :menuselection:`LCD --> Setting --> Height` field with LCD height.
6. Fill in the :menuselection:`LCD --> Setting --> Pitch` field with LCD pitch.
7. Fill in the :menuselection:`LCD --> Setting --> Base` field with frame buffer address (If you don't know which frame buffer address to fill, you can fill the address of frame buffer A directly).
8. The :menuselection:`LCD --> LCD Data Format` field is currently limited to ``RGB565``, so if the color format of the LCD frame buffer you get from the register is not RGB565, there may be a problem with the dumped image file.

   .. image:: /_static/usb2spi_lcd_dump_img.png

9. Click :guilabel:`Exec`.

   .. image:: /_static/usb2spi_lcd_dumping_img.png

   The message ``Dump Image Finished`` will be displayed if the process is completed.

.. _usb_spi_tool_rw_memory:

To read/write the on-chip memory
----------------------------------------------

First please ensure that:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

- The firmware in the chip is activated, or the initial script is loaded.

  .. note::

    If you don't know how to load the initial script, please refer to :ref:`how_to_load_init_script`.

After confirming that the hardware connection is correct, you can start running usb_spi_tool to read and write the contents of the memory on the chip.
Here are the steps:

1. Execute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2. If you only need to read/write within the size of 32 bits:

   - Enter the memory address to be read directly in the :menuselection:`Register --> Address` field. Then click the :guilabel:`Read` button to see the 32-bit value retrieved in the :guilabel:`Data` field.
   - To write a 32-bit value into memory, fill in the :menuselection:`Register --> Address` field with the memory address where the value to be written. Enter the value to be written in the :menuselection:`Register --> Data` field. Then click the :guilabel:`Write` button. If the write is successful, there will be no message, please try to retrieve the value of the memory again to confirm the successful write.

     .. image:: /_static/usb2spi_read_write_mem.png

3. If you need to read/write over the size of 32 bits:

   - If you need to read contents over the size of 32 bits

      1. Click :guilabel:`Memory` to switch to the :guilabel:`Memory` tab.
      2. Enter the starting address of the memory to be read in the :menuselection:`Dump Memory --> Address` field.
      3. Enter the size (in byte) of the memory to be read in the :menuselection:`Dump Memory --> Size` field.
      4. Enter the file path where the retrieved content to be placed in the :menuselection:`Dump Memory --> File` field.
      5. Click :guilabel:`Exec`. If successful, the message ``Dump Memory Finished`` will appear.

         .. image:: /_static/usb2spi_memory_dump.png

      6. You can use any Hex Editor to open the file to view the contents of the memory.

         .. image:: /_static/usb2spi_use_hex_tool_to_view_memory_data.png

   - If you need to write contents over the size of 32 bits

      1. Click :guilabel:`Memory` to switch to the :guilabel:`Memory` tab.
      2. Enter the path to the file containing the data to be written into memory in the :menuselection:`Dump Memory --> File` field.
      3. Enter the starting address of the memory to be written in the :menuselection:`Dump Memory --> Address` field.
      4. Click :guilabel:`Exec`. If successful, the message "Load Memory Finished" will appear.

         .. image:: /_static/usb2spi_memroy_write.png

.. _write_image_to_nandflash:

To burn image files to NAND flash
---------------------------------

Please ensure that:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to  :ref:`install_usb_to_spi_driver`.

- The EVB has been switched to Co-operative Mode.

  .. note::

     If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

After confirming that all hardware connections are correct, you can start to execute the burning software. The steps are as follows:

1. Execute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.

   .. image:: /_static/usb2spi_software_init.png

   .. note::

      If the connection between the USB to SPI board and EVB or PC is not correct, the following screen will appear, indicating that the chip id cannot be read:

      .. image:: /_static/usb2spi_connect_fail.png

2. Switch to the :guilabel:`SPI Writer` tab

   .. image:: /_static/usb2spi_spi_writer_tag.png

3. Switch the options in the :guilabel:`SPI MODE` box from ``NOR`` to ``NAND``.
4.	Click the :guilabel:`GET ID` button to confirm that the NAND flash information can be read and identified properly.

   .. image:: /_static/usb2spi_spiwriter_nandflash.png

5. Select the bootloader file - :file:`kproc.sys` (usually located at :file:`<sdk_root>/openrtos/<project_name>/project/bootloader`) to burn the firmware. Once selected, click :guilabel:`Burn` to start the burning process.
6. ``Spi burn Success!!`` message will be displayed after the burning is completed.

   .. image:: /_static/usb2spi_write_norflash_success.png

7. Turn off the power.
8. Put the PKG file of the firmware to be burned into the USB flash drive and plug it into the USB slot on the device. Turn on the device and wait for the update to complete.

.. _dump_norflash:

To dump the contents of NOR flash
------------------------------------

Please ensure that:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

- The EVB has been switched to Co-operative Mode.

  .. note::

     If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

After confirming that the hardware connection is working properly, you can start running usb_spi_tool to dump the contents of the NOR flash. The steps are as follows:

1. Execute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.

   .. image:: /_static/usb2spi_software_init.png

   .. note::

      If the connection between the USB to SPI board and EVB or PC is not correct, the following screen will appear, indicating that the chip id cannot be read:

      .. image:: /_static/usb2spi_connect_fail.png

2. Switch to the :guilabel:`SPI Writer` tab.

   .. image:: /_static/usb2spi_spi_writer_tag.png

3. Click the :guilabel:`GET ID` button to confirm that NOR flash information can be read and identified properly.

   .. image:: /_static/usb2spi_get_nor_info.png

4. Select the file path where you want to store the dumped content.

5.	Fill in the :guilabel:`Dump Address` field and :guilabel:`Dump Size` field with the starting address and size of the NOR flash to be read. Once selected, click :guilabel:`Dump` to start dumping the contents.

   .. image:: /_static/usb2spi_spiwriter_dump_norflash.png

6. The message ``Dump Finished`` will be displayed after completion.

   .. image:: /_static/usb2spi_spiwriter_dump_norflash_success.png

.. _dump_nandflash:

To dump the contents of the NAND flash
-----------------------------------------------------

Please ensure that:

- The USB to SPI board is properly connected.

  - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

- The EVB has been switched to Co-operative Mode.

  .. note::

     If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

After confirming that the hardware connection is working properly, you can start running usb_spi_tool to dump the contents of the NAND flash. The steps are as follows:

1. Execute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.

   .. image:: /_static/usb2spi_software_init.png

   .. note::

      If the connection between the USB to SPI board and EVB or PC is not correct, the following screen will appear, indicating that the chip id cannot be read:

      .. image:: /_static/usb2spi_connect_fail.png

2. Switch to the :guilabel:`SPI Writer` tab.

   .. image:: /_static/usb2spi_spi_writer_tag.png

3. Switch the options in the :guilabel:`SPI MODE` box from ``NOR`` to ``NAND``.
4. Click the :guilabel:`GET ID` button to confirm that NAND flash information can be read and identified properly.

   .. image:: /_static/usb2spi_spiwriter_nandflash.png

5. Select the file path where you want to store the dumped content.
6. Fill in the :guilabel:`Dump Address` field and :guilabel:`Dump Size` field with the starting address and size of the NAND flash to be read. Once selected, click :guilabel:`Dump` to start dumping the contents.

   .. image:: /_static/usb2spi_spiwriter_dump_nandflash.png

7. The message ``Dump Finished`` will be displayed after completion.

   .. image:: /_static/usb2spi_spiwriter_dump_norflash_success.png
