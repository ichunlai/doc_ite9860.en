.. _video:

Video
=============

Function description
------------------------

IT986x supports video playing functions, including video playing widgets integrated with ITU, boot animation playing, and general multimedia file playing.

- ITU video playing widget:

  You can use GUIDesigner to create video widget for playing videos. We can specify the path of the file to be played in the FilePath field of the video widget. In addition, the Playing and Repeat fields are also provided to users.

  .. image:: /_static/gui_editor_video_widget.png

  - Playing:

	When the user sets Playing to True, the specified video will be played automatically as soon as the user switches to the UI page; if it is set to False, the users can call the API to play it separately according to their demand.

  - Repeat:

	When the user sets Repeat to True, the specified video will be played in loop.

- Boot animation playing:

  users can use GUIDesigner to configure video widget to play boot animation.

- Multimedia playing:

  Users can directly call the API for video playing.

Supported Formats
^^^^^^^^^^^^^^^^^^^^^^

IT986x supports the following compression formats and packaging formats:

Format I
""""""""""""""""""

- Compression formats: Video: H.264, AUDIO: MP3, AAC
- Container formats: MP4, MKV

  .. note::

    The H264 codec supported by IT986x only allows the reference frame = 1 encoding scheme, so the video must go through a specific converter to be played. We provide two types of tools (freemaker, ffmpeg.exe) for users to perform the conversion.

- Freemake:

  GUI interface, can be downloaded from https://www.freemake.com/free_video_converter/

  1. After opening Freemake Video Converter, select the Video option.
  2. After loading the Video to be converted, select the to MP4 option or to MKV option.

	.. image:: /_static/video_converter_to_mp4.png

  3. Click the setting button.

	.. image:: /_static/video_converter_to_mp4_setting.png

  4. After the Video setting window pops up, users can set the frame size, bit rate, and codec according to their demands
  5. Press the :guilabel:`OK` button when finished setting.

	.. image:: /_static/video_converter_to_mp4_setting_ok.png

  6. Press the :guilabel:`Convert` button to start the conversion.

	.. image:: /_static/video_converter_to_mp4_done.png

- ffmpeg.exe:

  CMD interface, example of command parameters used are as follows:

  .. code-block:: shell

	ffmpeg.exe -i "filename" -c:v libx264 -profile:v main -level:v 3.0 -x264opts b-pyramid=0:ref=1:bframes=0 -c:a mp3 output.mp4


Format II
""""""""""""""""""

- Compression formats: Video: MJPEG, AUDIO: MP3, AAC
- Container formats: AVI

.. note::

  JPEG format does not support progressive jpeg

.. note::

  Users can use :file:`ffmpeg.exe` to make videos of mjpeg format from image files (jpg, png, bmp). Examples of command parameters used are as follows.

  .. code-block:: shell

	ffmpeg.exe -i *.jpg -c:v mjpeg -qscale:v 0 output.avi

.. note::

	Because the file size of MJPEG format is relatively large, it will take more time to read the file when playing, so users are recommended to use ffmpeg.exe to convert AVI files to MP4 or MKV files in H264 compressed format.

The video resolutions supported by the IT986x are as follows:

- IT9862, IT9863: The maximum supported resolution is 480 x 272.
- IT9866: The maximum supported resolution is 1280 x 720.
- IT9868, IT9869: The maximum supported resolution is 1920 x 1080.

.. note::

	The video resolution supported by IT986x is different for platforms of different part numbers.

Related KConfig settings
---------------------------------

- :menuselection:`Video --> Video Enable`

  Select Video Enable to allow the IT986x to support video playing.

Related source code files
---------------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - ``sdk/itu/share/itu/itu_video.c``
    - ITU video related API.
  * - | ``sdk/itu/share/itu/itu_framefunc.c``
      | ``sdk/driver/itv/it9860/itv.c``
      | ``sdk/share/ffmpeg/castor3player.c``
    - Multimedia playing related API implementation.
  * - ``project/ctrlboard/layer_videoplayer.c``
    - Multimedia playing examples for reference


Description of ITU video related API
------------------------------------------

ituVideoPlay
^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ituVideoPlay(ITUVideo* video, int percentage)

**Parameters**

``ITUVideo* video``

  Video widget type. See :file:`<sdk_root>/sdk/itu/include/itu.h`. Users can call ``ituSceneFindWidget(ITUScene *scene, const char *name)`` to get the specified video widget type.

``int percentage``

  The value of percentage ranges from 0 to 100, and the file will start playing from the percentage value set by the user; if the percentage is set to 0, it will play from the beginning.

**Description**

This function can be used to play the video set by the video widget. When the user's default Playing field is False, this function must be called before the video starts playing, and then the Playing field will be set to True.

ituVideoStop
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ituVideoStop(ITUVideo* video)

**Parameters**

``ITUVideo* video``

  Video widget type. See :file:`<sdk_root>/sdk/itu/include/itu.h`. Users can call ``ituSceneFindWidget(ITUScene *scene, const char *name)`` to get the specified video widget type.

**Description**

This function can be used to stop the video in the video widget.

ituVideoPause
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ituVideoPause(ITUVideo* video, int percentage)

**Parameters**

``ITUVideo* video``

  Video widget type. See :file:`<sdk_root>/sdk/itu/include/itu.h`. Users can call ``ituSceneFindWidget(ITUScene *scene, const char *name)`` to get the specified video widget type.

**Description**

This function can be used to pause the video in the video widget. After pausing the video, if you want to return to playing status, just call ``ituVideoPlay()`` again to play it.

ituVideoGoto
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ituVideoGoto(ITUVideo* video, int percentage)

**Parameters**

``ITUVideo* video``

  Video widget type. See :file:`<sdk_root>/sdk/itu/include/itu.h`. Users can call ``ituSceneFindWidget(ITUScene *scene, const char *name)`` to get the specified video widget type.

``int percentage``

  The value of percentage ranges from 0 to 100, and the file will start playing from the percentage value set by the user.

**Description**

This function can be used to control the video in the video widget to jump directly to the time point converted by the percentage.

ituVideoSpeedUpDown
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ituVideoSpeedUpDown(ITUVideo* video, float speed)

**Parameters**

``ITUVideo* video``

  Video widget type. See :file:`<sdk_root>/sdk/itu/include/itu.h`. Users can call ``ituSceneFindWidget(ITUScene *scene, const char *name)`` to get the specified video widget type.

``float speed``

  The *speed* value ranges from 0.5 to 3. The file will be played fast or slow according to the speed value set by the user; speed = 1 for normal speed, speed = 0.5 for 1/2 speed, speed = 2 for 2x speed, and so on.

**Description**

This function can be used to control the video in the video widget to be played fast or slow according to the speed value.

ituVideoSetOnStop
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:macro:: ituVideoSetOnStop(video, onStop)

**Parameters**

``ITUVideo* video``

  Video widget type. See :file:`<sdk_root>/sdk/itu/include/itu.h`. Users can call ``ituSceneFindWidget(ITUScene *scene, const char *name)`` to get the specified video widget type.

``onStop``

  onStop is a callback function.

**Description**

This function is used to set the callback function to be called when the file finishes playing; the corresponding callback function will be called when the file finishes playing, so that the user can do the corresponding action after the file finishes playing.

itu_framefuncInit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ituFrameFuncInit(void)

**Description**

This function will perform the initialization of video hardware and display buffer, and must be called before using video-related functions.

itu_framefuncExit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ituFrameFuncExit(void)

**Description**

This function performs the deinitialization of the video hardware and display buffer


.. _video_play_api:

Description of API related to multimedia playing
--------------------------------------------------------

itv_set_video_window
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itv_set_video_window(uint32_t startX, uint32_t startY, uint32_t width, uint32_t height)

**Parameters**

``uint32_t startX``

  To set the x coordinate of the video window.

``uint32_t startY``

  To set the y coordinate of the video window.

``uint32_t width``

  To set the width of the video window.

``uint32_t height``

  To set the height of the video window.

**Description**

This function sets the area of the video window in which the video played by the user will be displayed.

mtal_pb_init
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_init(cb_handler_t callback)

**Parameters**

``cb_handler_t callback``

  Video widget type. See :file:`<sdk_root>/sdk/itu/include/itu.h`. Users can call ``ituSceneFindWidget(ITUScene *scene, const char *name)`` to get the specified video widget type.

**Description**

This function initializes the multimedia playing and registers the callback function, which must be called when the user wants to enter the multimedia playing interface.

mtal_pb_exit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_exit(void)

**Description**

This function deinitializes the multimedia playing, and must be called when the user wants to leave the multimedia playing interface.

mtal_pb_select_file
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_select_file(MTAL_SPEC *spec)

**Parameters**

``MTAL_SPEC *spec``

  ``MTAL_SPEC`` type. See :file:`<sdk_root>/sdk/share/ffmpeg/castor3player.h`. One of the members of this type: ``srcname`` must be entered with the path of the file the user wants to play.

**Description**

The purpose of this function is to allow users to select the file to be played.

mtal_pb_play
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_play(void)

**Description**

The purpose of this function is to allow users to start playing a file.

- After calling the function ``mtal_pb_select_file``, you can call this function to play the file.
- When the function ``mtal_pb_pause`` is called to pause the file, this function can be called to resume playing the file.

mtal_pb_pause
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_pause(void)

**Description**

This function allows the user to pause a file that is playing.

When the file is playing, call this function to pause it.
When the file is paused, call this function to resume playing (this has the same result as calling the ``mtal_pb_play`` function).

mtal_pb_stop
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_stop(void)

**Description**

The purpose of this function is to allow the user to stop the file that is being played.



mtal_pb_get_total_duration
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_get_total_duration(int *totaltime)

**Parameters**

``int *totaltime``

  The total length of time in seconds to return the file.

**Description**

The purpose of this function is to get the total duration of the file being played.



mtal_pb_get_total_duration_ext
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_get_total_duration_ext(int *totaltime, char *filepath)

**Parameters**

``int *totaltime``

  The total length of time in seconds to return the file.

``char *filepath``

  The file path to get the total file duration.

**Description**

The purpose of this function is to get the total duration of a file.

Unlike ``mtal_pb_get_total_duration_ext``, this function allows to get the total duration of the file without playing the file.

mtal_pb_get_current_time
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int mtal_pb_get_current_time(int *currenttime)

**Parameters**

``int *currenttime``

  The current length in seconds of the return file.

**Description**

The purpose of this function is to get the current length of time that the file has been played.


.. raw:: latex

	\newpage
