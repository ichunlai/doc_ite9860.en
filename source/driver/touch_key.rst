.. _touch_key:

Touch Key
==============

Function description
---------------------------

Key is the most basic input device of SoC. According to the input interface, there are GPIO, SPI, I2C, USB, PS/2, RF..., all kinds of types. The discussion in this section refers specifically to the KEY with I2C as the input interface, including I2C interface mechanical keys, touch keys, touch sliders, etc. are applicable to the input devices mentioned in this article. As for the other I/O interface KEY, or the Touch Key attached to the I2C Touch Panel, they are not in the scope of this article (these two types of I2C interface input devices are covered in a separate article). In addition, ITE SoC's Touch Key module mainly uses IT7236, which is a capacitive touch key controller that can provide up to 16 sets of Key Number. At present, in the practical application, 5 & 16 sets (4X4 numeric keys) of Touch Key are the main ones.

For the code of related Touch Key, please refer to :file:`<sdk_root>/sdk/driver/itp/itp_keypad.c` and :file:`<sdk_root>/sdk/driver/itp/keypad/itp_keypad_xxxx.c` (xxxx is the module name of TOUCH KEY ).

Regarding the transmission of Touch Key's event, it is generally the same as the event of GPIO Key, and the upper layer AP will poll the Touch Key's event by calling SDL's API. Please refer to the SDL documentation for SDL's polling event and API calling, or refer to :file:`<sdk_root>/project/doorbell_indoor/scene.c`.

This module can also support input devices of other KEY controllers with a few simple program modifications.

Related KConfig settings
----------------------------------------------------------------

To specify the selected TOUCH KEY module in KCONFIG, you must first check :guilabel:`I2C0/I2C1/I2C2/I2C3 Enable` and :guilabel:`Keypad Enable`, and then enter the corresponding Driver file name (e.g. :file:`itp_keypad_it7236.c`) in the :guilabel:`Keypad Module` field, as shown in the picture:

.. image:: /_static/qconf_peripheral_i2c0_enable.png

.. image:: /_static/qconf_peripheral_keypad_enable.png


- :menuselection:`Peripheral --> Keypad Enable`

  To determine if KEYPAD module should be enabled.

- :menuselection:`Peripheral --> Keypad Enable --> Keypad module`

  To set the name of the KEYPAD module to be used.

- :menuselection:`Peripheral --> Keypad Enable --> Keypad Press Key interval(ms)`

  To set the time interval for sending each KEY Event.

- :menuselection:`Peripheral --> Keypad Enable --> Keypad Repeat Key Enable`

  To enable repeat key function. When the finger presses a KEY without releasing it, the system will send KEY Event repeatedly.

- :menuselection:`Peripheral --> Keypad Enable --> Keypad mapping table`

  To set the MAPPING TABLE file of KEYPAD (placed in :file:`<sdk_root>/sdk/target/keypad` directory). The value reported by Driver to the upper layer is INT 0 ~ N (N refers to the maximum number of KEYs -1), and the upper layer translates according to this Mapping Table into the corresponding Key Code.

- :menuselection:`Peripheral --> Keypad Enable --> Enable the setting of the total keypad number`

  To set the total number of KEY enabled (I2C interface).

- :menuselection:`Peripheral --> Keypad Enable --> Touch Key total number`

  To set the total number of KEY ( keypad applied on I2C interface).

- :menuselection:`Peripheral --> Keypad Enable --> Touch Key use I2C interface`

  To set whether KEYPAD uses the I2C interface.

KConfig settings related to KEY GPIO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Some Touch Keys need to use additional GPIO pins to detect INT signals in order to notify the driver when it is time to read the controller's Register via I2C BUS, so it is necessary to set the Touch Key's GPIO pins in KCONFIG, and the system will use this GPIO as the Touch Key's INT pin.For setting the INT pin of Touch Key, please go to the :menuselection:`GPIO --> Keypad Pins` field of KCONFIG (as shown in the picture below) as well.

.. image:: /_static/qconf_gpio_touch_key.png

Related source code files
----------------------------------------------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - ``<sdk_root>/sdk/driver/itp/itp_keypad.c``
    - | Keypad ITP Driver, the main purpose is to define three APIs, as the specification for the lower layer driver implementation. They are:
      |
      | ``itpKeypadInit();``
      | ``itpKeypadGetMaxLevel();``
      | ``itpKeypadProbe();``
  * - ``<sdk_root>/sdk/driver/itp/keypad/itp_keypad_xxx.c``
    - | ``itp_keypad_xxx.c`` is the underlying driver for Keypad, the main purpose of which is to implement the following three APIs:
      |
      | ``itpKeypadInit();``
      | ``itpKeypadGetMaxLevel();``
      | ``itpKeypadProbe();``

The Touch Key driver that have been implemented so far is ``itp_keypad_it7236.c``.

Description of related API
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The ITP Driver is an API implemented according to the POSIX specification, and can perform I/O DEVICE operations such as reading and writing files using the ``open()``/``read()``/``write()``/``lseek()``/``close()``/``ioctl()`` functions. The following is the Keypad ITP API implemented based on this specification.

The DEVICE ID is ITP_DEVICE_KEYPAD (please refer to :file:`<sdk_root>/sdk/include/ite/itp.h`).

This section describes the APIs used to operate the KEYPAD device. The driver provides a POSIX compliant API that allows the user to operate the KEYPAD device through ``itpKeypadInit()``/``itpKeypadGetMaxLevel()``/``itpKeypadProbe()`` functions.

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  To register a device, i.e. ``itpRegisterDevice(ITP_DEVICE_KEYPAD, &itpDeviceKeypad);``.

**Parameters**

``ITPDeviceType type``

  Device type. For a full list of device types, see :file:`<sdk_root>/sdk/include/ite/itp.h`. The device type associated with the KEYPAD device is ``ITP_DEVICE_KEYPAD``.

``const ITPDevice *device``

  Device identifier. The device identifier associated with the KEYPAD device is ``itpDeviceKeypad``.

**Description**

This function can be used to register the KEYPAD device to the core device list, so that the upper layer AP can operate the KEYPAD functionality through ``ioctl()``/``read()``/``write()`` functions. This function will be called by the ``itpInit()`` function during system initialization (see :file:`<sdk_root>/sdk/driver/itp_init_openrtos.c`), so if ``itpInit()`` has been called in your project, you do not need to run this registration function again.

The following shows how to register the KEYPAD device to the system core and initialize the KEYPAD device.

.. code-block:: c

    itpRegisterDevice(ITP_DEVICE_KEYPAD, &itpDeviceKeypad);
    ioctl(ITP_DEVICE_KEYPAD, ITP_IOCTL_INIT, NULL);

ioctl
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  To control the device.

**Parameters**

``int file``

  Device identifier. Its value is the value in the ``ITPDeviceType`` list. The device identifier associated with the KEYPAD device is ``ITP_DEVICE_KEYPAD``.

``unsigned long request``

  The *request* parameter is used to select the function to be run on the device and will depend on the specified device.

``void *ptr``

  The *ptr* parameter represents the additional information required to perform the requested function on this particular device. The type of *ptr* depends on the particular control request, but it can be an integer or a pointer to a specific data structure of the device.

**Description**

The ``ioctl()`` function is implemented to perform various control functions on the KEYPAD device. *request* parameters and an optional third parameter (with different types) are passed to and interpreted by the corresponding part of the KEYPAD device associated with the file.

The ``ioctl()`` command for KEYPAD devices, the parameters and the error states applicable to each individual command are described below.

- ITP_IOCTL_INIT

  To initialize the KEYPAD device. *ptr* parameter is NULL.

- ITP_IOCTL_PROBE

  To return the total number of keys to the upper layer.

- ITP_IOCTL_GET_MAX_LEVEL

  To detect a key event. If a key input is detected, the event will be inserted into the queue and wait for ``read()`` to fetch it.

read
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __size)

  To read data from KEYPAD. For examples, please refer to :file:`<sdk_root>/sdk/share/sdl/video/castor3/SDL_castor3keyboard.c`.

**Parameters**

``int __fd``

  The KEYPAD Device to be implemented. E.g. ``ITP_DEVICE_KEYPAD``.

``void *__buf``

  The location to write back after reading. The KEYPAD buffer size is ``ITP Event Size(sizeof (ITPKeypadEvent))``, see :file:`<sdk_root>/sdk/include/ite/itp.h` for a complete list of device types.

``size_t __ size``

  The length of the data to be read, i.e. ``ITP Event Size(sizeof (ITPKeypadEvent))``.

**Description**

This function allows users to read back data from the KEYPAD device, and the following shows how to use it.

.. code-block:: c

    ITPKeypadEvent ev;

    ioctl(ITP_DEVICE_KEYPAD, ITP_IOCTL_PROBE, NULL);

    if (read(ITP_DEVICE_KEYPAD, &ev, sizeof(ITPKeypadEvent)) == sizeof(ITPKeypadEvent))
    {
        if (ev.flags & ITP_KEYPAD_DOWN)
        {
            printf("GOT KEYPAD-DOWN\n");
        }
        else if (ev.flags & ITP_KEYPAD_UP)
        {
            printf("GOT KEYPAD-UP\n");
        }
    }

For the code of Touch Key driver, please refer to :file:`<sdk_root>/sdk/driver/itp/keypad/itp_keypad_it7236.c`. This Touch Key controller uses INT and I2C BUS to detect and read the Touch Key Data, and then transmits the Key Number to the upper SDL after interpretation. At present, the Driver of IT7236 can directly support 5 and 16 groups of KEY applications, while other Touch Key applications that are not 5 and 16 groups may need to be modified properly before they can be used.

To implement the DRIVER of TOUCH KEY is to implement the three functions: ``itpKeypadGetMaxLevel()``, ``itpKeypadInit()``, ``itpKeypadProbe()``, where ``itpKeypadGetMaxLevel()`` is to return the total number of keys to be detected, ``itpKeypadInit()`` is to implement the initialization of this Touch Key, and ``itpKeypadProbe()`` is to detect the keypad and return the value corresponding to the keypad mapping table. The value must be an integer between 0 and (KEYPAD number-1). keypad mapping table is defined in the Keypad mapping table option under Keypad Enable in Kconfig's Peripheral (``KEYPAD_MAPPING_TABLE``). The file is placed in the :file:`<sdk_root>/sdk/target/keypad/` directory.This Keypad mapping table file will be read in :file:`<sdk_root>/sdk/share/sdl/video/castor3/SDL_castor3keypad.c` and converted to SDL scancode (refer to :file:`<sdk_root>/sdk/include/SDL/SDL_scancode.h`). The scancode of SDL will finally be converted into the keycode of SDL (refer to :file:`<sdk_root>/sdk/share/sdl/events/SDL_keyboard.c`), which provides the upper layer AP to retrieve the key event through SDL API (please refer to the example of this document). Therefore, the content of the Keypad mapping table is the mapping table between the scancode of SDL and the value returned by ``itpKeypadProbe()``.

Basic example program
---------------------------

1. Read Touch Key Event through SDL
2. The way to build SDL library is to add the following settings in the "Kconfig" file

  .. code-block:: shell

    config BUILD_SDL
 	    def_bool y

  Simple DirectMedia Layer(SDL) is an open source cross-platform multimedia development library that provides several functions for controlling images, sounds, inputs and outputs. For more information about SDL, please refer to [Wikipedia] (https://zh.wikipedia.org/wiki/SDL).

  This example shows that the ITE SDK uses the functions provided by SDL to obtain the EVENT of the underlying KEY.

.. code-block:: c

    #include "SDL/SDL.h"

    int TouchEvent_test(void)
    {
        SDL_Event ev;

	/* SDL initial */
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
            printf("Couldn't initialize SDL: %s\n", SDL_GetError());

        while (SDL_PollEvent(&ev))
        {
            switch (ev.type)
            {
            case SDL_KEYDOWN:
                switch (ev.key.keysym.sym)
                {
                case SDLK_UP:
                    printf("key direction up\n");
                    break;

                case SDLK_DOWN:
                    printf("key direction down\n");
                    break;

                case SDLK_LEFT:
                    printf("key direction left\n");
                    break;

                case SDLK_RIGHT:
                    printf("key direction right\n");
                    break;
        	       }
            }
            SDL_Delay(1);
        }
    }


.. raw:: latex

    \newpage
