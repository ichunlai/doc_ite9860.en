.. _lcd:

LCD
==============

Function description
--------------------

The IT986x series provides an LCD controller that supports the following input interfaces

- RGB interface

  Supports resolutions up to 1920x1080.

  Supports 16/18/24 bpp (RGB565/RGB666/RGB8888) interface.

- CPU interface

  - 80-Type
  - 68-Type

- LVDS

  Supports resolutions up to 1366 x 768 pixel.

- MIPI

  Supports resolutions up to 1920 x 1080 pixel.

ITE provides an :file:`LCD.exe` tool for bringing up lcd. For detailed instructions on the operation of the tool, please refer to:

- ITE AP SoC IT97x_IT986x Panel Tool Guide- RGB_SPI.pdf
- ITE AP SoC IT97x_IT986x Panel Tool Guide- CPU.pdf
- ITE AP SoC IT97x_IT986x Panel Tool Guide- LVDS.pdf
- ITE AP SoC IT97x_IT986x Panel Tool Guide- MIPI.pdf

This section mainly describes the LCD related operation functions provided in the SDK.

Related KConfig settings
----------------------------------------------------------------

- :menuselection:`Screen --> LCD enable`

  Enable LCD device.

- :menuselection:`Screen --> LCD/TV out initial script`

  To select script for the LCD initialization. The file contains a bunch of register addresses and value pairings to initialize the initial register settings of the LCD controller. These initialization script are usually placed under the path :file:`<sdk_root>/sdk/target/lcd/`. The LCD tool can be used to generate this script.

- :menuselection:`Screen --> LCD enable --> LCD width`

  The effective data block in the horizontal direction of the LCD, which indicates the number of horizontal pixels actually displayed on the screen.

- :menuselection:`Screen --> LCD enable --> LCD height`

  The effective data block in the vertical direction of the LCD, which indicates the number of vertical pixels actually displayed on the screen.

- :menuselection:`Screen --> LCD enable --> LCD pitch`

  The number of memory bytes occupied by a horizontal line on the screen. The value to be entered for this setting is LCD width x LCD Byte-Per-Pixel; for example, if LCD width = 480 and LCD Byte-Per-Pixel = 2, then the value to be entered for this setting is 480 x 2 = 960.

- :menuselection:`Screen --> LCD enable --> LCD Byte-Per-Pixel`

  The number of memory bytes occupied by a pixel on the screen. If the format of each pixel on the screen is RGB565, this setting should be filled in as 2. If the format of each pixel on the screen is RGB8888, this setting should be filled in as 4.

Related source code files
----------------------------

.. list-table::
   :header-rows: 1

   * - Path
     - Description
   * - | ``<sdk_root>/sdk/driver/ith/it9860/ith_lcd.c``
       | ``<sdk_root>/sdk/driver/ith/it9860/ith_lcd.h``
     - The uppermost layer function.


Description of related API
---------------------------------------

This section describes the APIs used to operate the LCD controller.

ithLcdDisable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithLcdDisable(void)

**Description**

The function will disable the LCD and it will be called when entering power saving mode (if power saving mode is enabled).


ithLcdEnable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithLcdEnable( void)

**Description**

This function enables the LCD, and is called when you leave the power saving mode (if the power saving mode is enabled).

ithLcdSetRotMode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithLcdSetRotMode(ITHLcdScanType type, ITHLcdRotMode mode)

**Parameters**

``ITHLcdScanType type``

  To set the LCD rotation direction horizontally or vertically.

``ITHLcdRotMode mode``

  LCD rotation mode setting.

**Description**

To rotate the display of the LCD module. IT986x supports Rotate 180, Flip and Mirror modes.

ithLcdGetPanelType
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: ITHLcdPanelType ithLcdGetPanelType(void)

**Description**

To obtain the current LCD panel type.

**Return**

LCD type.

Complete example
----------------------

Let's use Standard EVB to bring up the TL068HWXH08 6.86" MIPI screen as an example. The initial script for the TL068HWXH08 6.86" MIPI screen is located at :file:`<sdk_root>/sdk/target/lcd/IT9860_MIPI_TL068HWXH08_EK79030_480x1280_4LANE_byteclk53.txt`. The screen has an effective display width of 1280 and effective display height of 480. In This example, we will light up the screen and periodically switch between different colors on the entire screen.

Hardware connection
^^^^^^^^^^^^^^^^^^^^^^^^^^

Please connect the screen to the EVB through the cable first. Please connect to the socket indicated by the yellow arrow in the picture below.

  .. image:: /_static/evb_lcd_example.png

KConfig settings
^^^^^^^^^^^^^^^^^

Please run :file:`<sdk_root>/build/openrtos/test_lcd.cmd` and follow these steps to set it up:

- Check :menuselection:`Screen --> LCD Enable`.
- Fill in :menuselection:`Screen --> LCD Enable --> LCD width` with 1280 (i.e. the effective display width of TL068HWXH08 6.86" MIPI screen).
- Fill in :menuselection:`Screen --> LCD Enable --> LCD height` with 480 (i.e. the effective display height of TL068HWXH08 6.86" MIPI screen).
- Fill in :menuselection:`Screen --> LCD Enable --> LCD pitch` with 2560.
- Fill in :menuselection:`Screen --> LCD Enable --> LCD Bytes-Per-Pixel` with 2.
- Double click :menuselection:`Screen --> LCD Enable --> LCD initial script`and choose :file:`<sdk_root>/sdk/target/lcd/IT9860_MIPI_TL068HWXH08_EK79030_480x1280_4LANE_byteclk53.txt`.

  .. image:: /_static/qconf_lcd_example_screen_lcd.png

TL068HWXH08 6.86" MIPI screen uses PWM as backlight. Therefore, it is necessary to set the backlight. The backlight setting procedure is as follows:

- Check :menuselection:`Screen --> Backlight Enable:`.
- Fill in :menuselection:`GPIO --> Backlight PWM pin:` with 63.
- Fill in :menuselection:`GPIO --> Backlight PWM NUMBER:` with 1.
- Fill in :menuselection:`GPIO --> LCD Power Enable Pin:` with 48.
- Uncheck  :menuselection:`GPIO --> LCD Power Enable n --> LCD Power Enable Pin Active Low:`.

  .. image:: /_static/qconf_lcd_example_gpio_lcd.png


To verify the results
^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Click the |build_icon| button to build the project. Please ensure that:

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Reset the power of EVB.
3. When the program is finished building, click the |run_icon|  button to boot up the system via USB to SPI board.
4. Check if the screen is showing the same color on the whole screen.


  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14


.. raw:: latex

    \newpage
