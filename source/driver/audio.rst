﻿.. _audio:

Audio
==========

Function description
---------------------

The IT986x provides basic sound operation consisting of two parts: the upper layer application AUDIO PROCESS FLOW and the bottom layer AUDIO HW CONTROL. The items processed by the bottom layer are packaged in the upper layer application, and users can directly use the API provided by the upper layer to perform functions such as music playing, music recording, speech recognition, intercom, volume adjustment, and pause.

The basic audio formats supported by IT986x are

- sampling rate: 8K ~ 48K
- channels: 1 ~ 2 bit
- size: 16 bit

.. image:: /_static/diagram_audio_flow.png

Audio HW control
^^^^^^^^^^^^^^^^^^^^^^^^^

It includes the interface (I2S) responsible for audio data transfer (read write pointer), digital/analog conversion (DAC/ADC), and audio hardware control (such as pause, volume adjustment, mute/unmute). This part of the software is related to the back-end (bottom) driver hardware control.

.. image:: /_static/diagram_audio_hw_control_flow.png

The following picture is an illustration of the I2S DA buffer data transfer process. The DA buffer is implemented as a ring buffer. The data read from the file is copied into the DA buffer (DA stands for digital-to-analog) and the WP (i.e. write pointer) is updated to indicate that new data has been written. The RP ( i.e. read pointer) is controlled by the hardware and indicates which data has been played.

.. image:: /_static/diagram_audio_da_buffer.png

The following is a diagram of the I2S AD buffer data transfer process. AD buffer is also implemented as a ring buffer. WP (i.e. write pointer) is updated automatically by the hardware and records the location of the data received from the external (mic) and stored in the buffer. RP (i.e. read pointer) is updated by the user and indicates the location of the last data that has been read from the AD buffer.

.. image:: /_static/diagram_audio_ad_buffer.png

Audio process flow
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

For convenience, the underlying driver and data operations are further packaged into a simplified API that allows users to directly call the modularized functions, and users can also add their own audio-related operations according to their demands. For example: playing sound files, recording audio files, mixing, speech recognition, intercom, etc. Depending on the application, there are two types of processes: audio list flow and audio mgr. audio list flow is mainly for intercom applications and can be extended to a wider range of audio processing related applications (including audio file playing). Audio mgr is only for audio file playing applications. The relevant test examples are located in :file:`<sdk_root>/project/test_audio/audioFlow` and :file:`<sdk_root>/project/test_audio/audioMgr`.



audio list flow
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Audio list flow is a process of disassembling the voice processing procedure into filters with different functions and linking them together to form a complete flow. Users can combine the filters to create new functions by themselves. The API provided by Audio list flow includes hardware control, so developers can follow the instructions to operate basic functions such as playing music, recording, and audio processing.

.. image:: /_static/diagram_audio_list_flow.png

audio mgr
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /_static/diagram_audio_manager.png

Related KConfig settings
-----------------------------

- :menuselection:`AUDIO --> Audio Enabel`

  To determine whether AUDIO should be enabled.

- :menuselection:`Peripheral --> I2S Enable --> I2S INTERNAL CODEC`

  Enable and check the I2S Internal codec.

- :menuselection:`Peripheral --> Amplifier available` and :menuselection:`GPIO --> Amplifier Enable/Disable Pin, Amplifier Mute Pin`

  Set the AMP GPIO PIN. If it is designed additionally on the hardware, you need to configure it, but not on the IT986x Standard EVB.

- :menuselection:`AUDIO --> Audio DAC module`

  The IT986x has a built-in internal AD/DA codec and does not require an external one; if you need to use an external AD/DA codec, please replace the default :file:`itp_dac_ite970.c` with a double-click on this setting.

- :menuselection:`AUDIO --> Audio Enable --> Mic single end mode, SPK single ground mode`

  Please check single-end/differential for MIC external circuit (check according to hardware configuration).
  Please check single-ground/differential for SPK external circuit (check according to hardware configuration).

- :menuselection:`AUDIO --> Audio Enable --> Dialogue Parameter Set`

  Intercom system function menu, for Linphone system.

- :menuselection:`AUDIO --> Audio mgr --> Music Codec, Audio Mgr Buffer Size Setting`

  Select the audio decoder (wav, mp3, wma) to be supported. the IT986x has an arm-lite CPU for audio decoding in addition to the main CPU to reduce the burden on the main CPU.

- :menuselection:`AUDIO --> Audio ASR --> Audio ASR LIB module`

  The Speech Recognition function menu, used to select the speech recognition library, and is set to :file:`libasr_ITE01.a` by default.

Related source code files
----------------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - | ``sdk/driver/i2s/it9860/i2s_9860.c``
      | ``sdk/include/i2s/i2s.h``
      | ``sdk/driver/itp/dac/itp_dac_ite970.c``
    - Underlying i2s driver function, providing I2S initialization operation, volume adjustment, AD/DA data receiving and transmitting, AD/DA codec driver.
  * - | ``sdk/share/flower/audio/flower_stream.c``
      | ``sdk/share/flower/audio``
    - List flow architecture, providing the upper layer functions and packaging the bottom layer I2S operations. Users can call directly as the basic API ( playing music, recording music, speech recognition, intercom and other functions ...), and the framework of subsequent development.
  * - ``sdk/share/audio_mgr/audio_mgr.c``
    - Audio mgr architecture, providing the upper layer functions, and packaging the underlying I2S operations, mainly for mp3, wma, aac and other music that needs to be decoded for playing.

Description of related API
-----------------------------

This section describes the relevant APIs used to operate AUDIO, consists of two parts: the bottom layer Audio HW control and the upper layer application Audio process flow. In the development process, the bottom layer I2S control items will mostly be packaged in the upper layer application, and if there is a need, you can add your own or directly call the API provided by the bottom layer for operation.

Please refer to the test example document as below:

- ``project/test_audio/audioHWctrl``
- ``project/test_audio/audioFlow``
- ``project/test_audio/audioMgr``
- ``project/test_audio/test_audio.docx``

Description of API related to Audio HW control
------------------------------------------------------

The first step in AUDIO operation is the initialization of the I2S driver, which includes clock settings, DAC, ADC codec driver settings, etc. The relevant test program is located in :file:`<sdk_root>/project/test_audio/audioHWctrl`. The following is a further description of the API provided by the I2S driver.

i2s_init_DAC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_init_DAC(STRC_I2S_SPEC *i2s_spec)

**Parameters**

``STRC_I2S_SPEC *i2s_spec``

  I2s device init settings. The fields are described as follows:

  - ``unsigned int channels``

    The value of 1 indicates the mono channel and the value of 2 indicates the stereo channel.

  - ``unsigned int sample_rate``

    Sampling rate, in Hz. For example, if 44100Hz, enter 44100.

  - ``unsigned int buffer_size``

    DA buffer size, in bytes.

  - ``unsigned int is_big_endian``

    Whether the data stored in the DA buffer is stored in the big endian format. The value 0 means no, other values mean yes.

  - ``unsigned char *base_i2s``

    DA buffer base address.

  - ``unsigned int sample_size``

    The size of each audio sample stored in the DA buffer, in bit. The value may be 16, 24, or 32, representing 16bits, 24bits, or 32bits respectively.

  - ``unsigned int from_LineIN``

    Whether the input is from line in. The value 0 means no, other values mean yes. Because this function can only be used to process output, this field is filled with 0 at all times.

  - ``unsigned int from_MIC_IN``

    Whether the input is from the microphone. The value 0 means no, other values mean yes. Because this function can only be used to process output, this field is filled with 0 at all times.

  - ``unsigned int num_hdmi_audio_buffer``

    The number of buffers needed to temporarily store the audio data output from hdmi, when the output is from hdmi. If there is no need to output from hdmi, then this field is filled with 0.

  - ``unsigned char *base_hdmi[4]``

    Buffer to temporarily store the audio data from the hdmi output. The number is specified by the num_hdmi_audio_buffer field.

  - ``unsigned int is_dac_spdif_same_buffer``

    Whether the audio data output from spdif shares the same buffer as the audio data output from dac. The value 0 means no, other values mean yes.

  - ``unsigned char *base_spdif``

    The base address of the buffer of the audio data output from spdif.

  - ``unsigned int enable_Speaker``

    Whether to enable the speaker output. The value 0 means no, other values mean yes.

  - ``unsigned int enable_HeadPhone``

    Whether to enable headphone output. The value 0 means no, other values mean yes.

  - ``unsigned int postpone_audio_output``

    If the value is 0, the audio data stored in the buffer will be output immediately after calling this function. If the value is 1, the audio data stored in the buffer will not be output immediately after calling this function.

**Description**

To initialize the DAC.

The sample program for outputting two-channel, 8KHz, 16bits audio data from headphones or speakers is shown below.

.. code-block:: c

    void initDA() {
        /* init DAC */
        dac_buf = (uint8_t*)malloc(DAC_BUFFER_SIZE);
        memset((uint8_t*) dac_buf, 0, DAC_BUFFER_SIZE);
        memset((void*)&spec_daI, 0, sizeof(STRC_I2S_SPEC));
        spec_daI.channels                 = 2;
        spec_daI.sample_rate              = 8000;
        spec_daI.buffer_size              = DAC_BUFFER_SIZE;
        spec_daI.is_big_endian            = 0;
        spec_daI.base_i2s                 = (uint8_t*) dac_buf;
        spec_daI.sample_size              = 16;
        spec_daI.num_hdmi_audio_buffer    = 0;
        spec_daI.is_dac_spdif_same_buffer = 1;
        spec_daI.enable_Speaker           = 1;
        spec_daI.enable_HeadPhone         = 1;
        spec_daI.postpone_audio_output    = 1;
        spec_daI.base_spdif                = (uint8_t*) dac_buf;
        audio_init_DA(&spec_daI);
        i2s_pause_DAC(1);
    }


i2s_init_ADC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_init_ADC(STRC_I2S_SPEC *i2s_spec)

**Parameters**

``STRC_I2S_SPEC *i2s_spec``

  I2s device init settings. The fields are described as follows:

  - ``unsigned int channels``

    The value of 1 indicates the mono channel and the value of 2 indicates the stereo channel.

  - ``unsigned int sample_rate``

    Sampling rate, in Hz. For example, if 44100Hz, enter 44100.

  - ``unsigned int buffer_size``

    DA buffer size, in bytes.

  - ``unsigned int is_big_endian``

    Whether the data stored in the DA buffer is stored in the big endian format. The value 0 means no, other values mean yes.

  - ``unsigned char *base_i2s``

    DA buffer base address.

  - ``unsigned int sample_size``

    The size of each audio sample stored in the DA buffer, in bit. The value may be 16, 24, or 32, representing 16bits, 24bits, or 32bits respectively.

  - ``unsigned int from_LineIN``

    Whether the input is from line in. The value 0 means no, other values mean yes.

  - ``unsigned int from_MIC_IN``

    Whether the input is from the microphone. The value 0 means no, other values mean yes.

  - ``unsigned int num_hdmi_audio_buffer``

    The number of buffers needed to temporarily store the audio data output from hdmi, when the output is from hdmi. If there is no need to output from hdmi, then this field is filled with 0. Because this function can only be used to process input, this field is filled with 0.

  - ``unsigned char *base_hdmi[4]``

    Buffer to temporarily store the audio data from the hdmi output. The number is specified by the num_hdmi_audio_buffer field. Because this function can only be used to process input, this field is filled with 0.

  - ``unsigned int is_dac_spdif_same_buffer``

    Whether the audio data output from spdif shares the same buffer as the audio data output from dac. The value 0 means no, other values mean yes. Because this function can only be used to process input, this field is filled with 0.

  - ``unsigned char *base_spdif``

    The base address of the buffer of the audio data output from spdif. Because this function can only be used to process input, this field is filled with 0.

  - ``unsigned int enable_Speaker``

    Whether to enable the speaker output. The value 0 means no, other values mean yes. Because this function can only be used to process input, this field is filled with 0.

  - ``unsigned int enable_HeadPhone``

    Whether to enable headphone output. The value 0 means no, other values mean yes. Because this function can only be used to process input, this field is filled with 0.

  - ``unsigned int postpone_audio_output``

    If the value is 0, the audio data stored in the buffer will be output immediately after calling this function. If the value is 1, the audio data stored in the buffer will not be output immediately after calling this function. Because this function can only be used to process input, this field is filled with 0.

**Description**

To initialize ADC.

A sample program for inputting mono, 8KHz, 16bits of audio data from a microphone is shown below.

.. code-block:: c

    /* init ADC */
    adc_buf = (uint8_t*)malloc(ADC_BUFFER_SIZE);
    memset((uint8_t*) adc_buf, 0, ADC_BUFFER_SIZE);
    memset((void*)&spec_adI, 0, sizeof(STRC_I2S_SPEC));
    spec_adI.channels                   = 1;
    spec_adI.sample_rate                = 8000;
    spec_adI.buffer_size                = ADC_BUFFER_SIZE;
    spec_adI.is_big_endian              = 0;
    spec_adI.base_i2s                   = (uint8_t*) adc_buf;
    spec_adI.sample_size                = 16;
    spec_adI.record_mode                = 1;
    spec_adI.from_LineIN                = 0;
    spec_adI.from_MIC_IN                = 1;
    //i2s_init_ADC(&spec_adI);
    audio_init_AD(&spec_adI);
    audio_pause_AD(1);


i2s_get_DA_running
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int i2s_get_DA_running(void)

**Description**

To get if the DAC has been initialized.

**Return**

A value of 1 indicates that the DAC has been initialized. A value of 0 indicates that the DAC has not been initialized.

i2s_get_AD_running
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int i2s_get_AD_running(void)

**Description**

To get if the ADC has been initialized.

**Return**

A value of 1 indicates that the ADC has been initialized. A value of 0 indicates that the ADC has not been initialized.


i2s_deinit_DAC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_deinit_DAC(void)

**Description**

To reset DAC.



i2s_deinit_ADC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_deinit_ADC(void)

**Description**

To reset ADC.


i2s_set_direct_volperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_set_direct_volperc(unsigned int volperc)

**Parameters**

``unsigned int volperc``

  The playing volume to be set. The range is 0~99.

**Description**

To set the DAC volume (playing volume).



i2s_ADC_set_rec_volperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_ADC_set_rec_volperc (unsigned int recperc)

**Parameters**

``unsigned int recperc``

  The recording volume to be set. The range is 0~99.

**Description**

To set the ADC volume (recording volume).


i2s_set_direct_RLvolperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_set_direct_RLvolperc(unsigned int volperc, unsigned char RL)

**Parameters**

``unsigned int volperc``

  The playing volume to be set. The range is 0~99.

``unsigned char RL``

  A value of 0 means that only the right channel volume is set, and a value of 1 means that only the left channel volume is set.

**Description**

To set the DAC volume (playing volume) of the left channel or right channel.


i2s_ADC_set_rec_RLvolperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_ADC_set_rec_RLvolperc(unsigned int recperc, unsigned char RL)

**Parameters**

``unsigned int recperc``

  The recording volume to be set. The range is 0~99.

``unsigned char RL``

  A value of 0 means that only the right channel volume is set, and a value of 1 means that only the left channel volume is set.

**Description**

To set the ADC volume (recording volume) for the left channel or right channel.


i2s_get_current_volperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: unsigned int i2s_get_current_volperc(void)

**Description**

To obtain the current volume of the DAC (playing volume).

**Return**

The current volume of the DAC. The range is 0~99.


i2s_ADC_get_current_volstep
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: unsigned int i2s_ADC_get_current_volstep(void)

**Description**

To obtain the current volume of the ADC (recording volume).

**Return**

The current volume of the ADC. The range is 0~99.

i2s_get_current_RLvolperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: unsigned int i2s_get_current_RLvolperc(unsigned char RL)

**Parameters**

``unsigned char RL``

  A value of 0 means that only the volume of the right channel is obtained, and a value of 1 means that only the volume of the left channel is obtained.

**Description**

To get the DAC volume (playing volume) of the left channel or right channel.

**Return**

The volume of the left or right channel. The range is 0~99.

i2s_ADC_get_rec_RLvolperc
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: unsigned int i2s_ADC_get_current_RLvolstep(unsigned char RL)

**Parameters**

``unsigned char RL``

  A value of 0 sets the volume of the right channel only, and a value of 1 sets the volume of the left channel only.

**Description**

To set the ADC volume (recording volume) of the left channel or right channel.

**Return**

The volume of the left or right channel. The range is 0~99.

i2s_pause_DAC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_pause_DAC(int pause)

**Parameters**

``int pause``

  The value of 1 means to pause, and the value of 0 means to start playing.

**Description**

To stop/start I2S transmission (playing control).

i2s_pause_ADC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_pause_ADC(int pause)

**Parameters**

``int pause``

  A value of 1 means to pause recording, and a value of 0 means start recording.

**Description**

To stop/Start I2S transmission (recording control).

i2s_mute_volume
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_mute_volume(int mute)

**Parameters**

``int mute``

  A value of 1 means to mute, a value of 0 means non-mute.

**Description**

To mute/non-mute the audio playing.

i2s_mute_ADC
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void i2s_mute_ADC (int mute)

**Parameters**

``int mute``

  A value of 1 means to mute, a value of 0 means non-mute.

**Description**

To mute/non-mute the audio recording.

I2S_DA32_GET_RP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t I2S_DA32_GET_RP(void)

**Description**

To obtain the read pointer value of the DA buffer.

**Return**

The value of the read pointer of the DA buffer.

I2S_DA32_GET_WP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t I2S_DA32_GET_WP(void)

**Description**

To obtain the write pointer value of the DA buffer.

**Return**

The value of the write pointer of the DA buffer.

I2S_DA32_SET_WP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void I2S_DA32_SET_WP(uint32_t data32)

**Parameters**

``uint32_t data32``

  The write pointer value of the DA buffer.

**Description**

To set the write pointer value of the DA buffer.


I2S_AD32_GET_RP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t I2S_AD32_GET_RP(void)

**Description**

To obtain the read pointer value of the AD buffer.

**Return**

The read pointer value of the AD buffer.

I2S_AD32_SET_RP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void I2S_AD32_SET_RP(uint32_t data32)

**Parameters**

``uint32_t data32``

  The read pointer value of the AD buffer.

**Description**

To set the read pointer value of the AD buffer.



I2S_AD32_GET_WP
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t I2S_AD32_GET_WP(void)

**Description**

To obtain the write pointer value of the AD buffer.

**Return**

The write pointer value of the AD buffer.

Audio list flow related API description
-----------------------------------------------

The API provided by audio list flow is packaged with hardware control, so the software programmer can operate the basic functions such as playing music, recording, and audio processing according to the description. Please refer to the test program :file:`<sdk_root>/project/test_audio/audioFlow`. The following is a further description of the API provided by audio list flow.

.. image:: /_static/diagram_audio_list_flow.png


IteStreamInit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: IteFlower *IteStreamInit(void)

**Description**

To initializes IteFlower.

Before using all the APIs provided by Audio list flow, you need to initialize IteFlower first, otherwise the subsequent functions will not work properly.

**Return**

The pointer of IteFlower.


flow_start_sound_play
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_start_sound_play(IteFlower *f,const char* filepath,PlaySoundCase mode)

**Parameters**

``IteFlower *f``

  The pointer of IteFlower.

``const char* filepath``

  The path to the file to be played.

``PlaySoundCase mode``

  Repeat means to repeat, and Normal Play means to play only once.

**Description**

To start playing a wav file.

.. image:: /_static/diagram_audio_sound_play.png

flow_stop_sound_play
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_stop_sound_play(IteFlower *f)

**Parameters**

``IteFlower *f``

  The pointer of IteFlower

**Description**

To finish playing the wav file.


PlayFlowPause
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void PlayFlowPause(IteFlower *f,bool pause)

**Parameters**

``IteFlower *f``

  The Pointer for IteFlower

``bool pause``

  A value of true means to pause, and a value of 0 means to continue playing.

**Description**

To pause/continue playing wav.


PlayFlowMix
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void PlayFlowMix(IteFlower *f, const char *fileinsert)

**Parameters**

``IteFlower *f``

  The Pointer for IteFlower.

``const char *fileinsert``

  The path to the file to be mixed.

  If you need to mix audio files consecutively, you can use a blank to separate the file paths.

  Ex: ``fileinsert="a:/1.wav a:/2.wav a:/9.wav``.

**Description**

You need to mix in phrases when you play background music with ``flow_start_sound_play``. The background music and phrase format must be wav files with the same format (sampling rate, channels, bitsize).


flow_start_sound_record
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_start_sound_record(IteFlower *f,const char* filepath,int rate)

**Parameters**

``IteFlower *f``

  The Pointer for IteFlower.

``const char* filepath``

  The path to save the recording file.

``int rate``

  To set the sampling rate of the recording.

**Description**

To start recording audio files.

.. image:: /_static/diagram_audio_sound_record.png


flow_stop_sound_record
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_stop_sound_record(IteFlower *f)

**Parameters**

``IteFlower *f``

  The Pointer for IteFlower.

**Description**

To finish recording the audio file.


flow_start_asr
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_start_asr(IteFlower *f,Callback_t func)

**Parameters**

``IteFlower *f``

  The Pointer for IteFlower.

``Callback_t func``

  The upper layer callback function.

**Description**

To start speech recognition.

When speech recognition successfully triggers an event, callback function proceeds to the upper layer operation.

.. image:: /_static/diagram_audio_sound_asr.png

flow_stop_asr
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_stop_asr(IteFlower *f)

**Parameters**

``IteFlower *f``

  The Pointer for IteFlower.

**Description**

To terminate speech recognition.


flow_start_soundrw
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_start_soundrw(IteFlower *f)

**Parameters**

``IteFlower *f``

  The Pointer for IteFlower.

**Description**

To start microphone reception --> Spk playback.

.. image:: /_static/diagram_audio_soundrw.png


flow_stop_soundrw
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_stop_soundrw(IteFlower *f)

**Parameters**

``IteFlower *f``

  The Pointer for IteFlower.

**Description**

To finish microphone reception --> Spk playback.


flow_start_udp_audiostream
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void flow_start_udp_audiostream(IteFlower *f, int rate, AudioCodecType type, const char *rem_ip, unsigned short rem_port)

**Parameters**

``IteFlower *f``

  The Pointer for IteFlower.

``int rate``

  Sampling rate: 8K.

``AudioCodecType type``

  Codec format (ulaw, alaw…).

``const char *rem_ip``

  The address of remote IP.

``unsigned short rem_port``

  Remorte port number.

**Description**

To start two-way transmission of UDP communication.

.. image:: /_static/diagram_audio_udp_audiostream.png


flow_stop_udp_audiostream
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint8_t flow_stop_udp_audiostream(IteFlower *f)

**Parameters**

``IteFlower *f``

  The Pointer for IteFlower.

**Description**

To finish two-way transmission of UDP communication.



Description of API related to Audio mgr
---------------------------------------------

The API provided by audio mgr is packaged with hardware control, so the software programmer can follow the instructions to play music formats that need to be decoded such as mp3, wma, aac, wav, etc. Please refer to the test program :file:`<sdk_root>/project/test_audio/audioMgr`. The API provided by audio mgr will be described further below.

.. image:: /_static/diagram_audio_manager.png

AudioInit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void AudioInit(void)

**Description**

To initialize Audio mgr.

Before using all the APIs provided by Audio mgr, you need to initialize Audiomgr first, otherwise the subsequent functions will not work properly.

AudioPlay
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int AudioPlay(char* filename, AudioPlayCallback func)

**Parameters**

``char* filename``

  The path to the file to be played.

``AudioPlayCallback func``

  The callback function.

**Description**

Play the music format that needs to be decoded. e.g. mp3, aac, wma, wav....

**Return**

-1 means opening file failed, 0 means opening file normally.


AudioStop
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void AudioStop(void)

**Description**

To stop playing.


AudioSetVolume
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void AudioSetVolume(int level)

**Parameters**

``int level``

  The playing volume to be set. The range is 0~99.

**Description**

To set the playing volume.


AudioMute
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void AudioMute(void)

**Description**

To set as mute.


AudioUnMute
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void AudioUnMute (void)

**Description**

To unmute.


Basic example program
-----------------------------

This example demonstrates playing a music file with the file name :file:`Music8k1c.wav` stored on a USB flash drive. The complete code for this example is in :file:`<sdk_root>/project/test_audio/audioFlow/test_flower_play.c`.

.. code-block:: c

    #include <sys/ioctl.h>
    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <pthread.h>
    #include <unistd.h>
    #include "openrtos/FreeRTOS.h"
    #include "flower.h"

    //======================================================//
    IteFlower *flower = NULL;

    /*********************/
    /*   FC 1: A->B->C   */
    /*********************/
    void* TestFunc(void* arg)
    {
        itpInit();
        sleep(2);
        flower = IteStreamInit();

        //IteFChain fc1=s->fc;
        const char *filename="a:/Music8k1c.wav";

        flow_start_sound_play(flower,filename,Normal);

        while(1) {
            sleep(1);
            if(flower->audiostream==NULL) break;
        }

        flow_stop_sound_play(flower);

        printf("Test Task done\n");
    }

Complete example
-----------------------------

:file:`<sdk_root>/project/test_audio/audioHWctr`, :file:`<sdk_root>/project/test_audio/audioFlow`, and :file:`<sdk_root>/project/test_audio/audioMgr` provide commonly used controls for audio, including playing, recording and playing simultaneously. Through these examples, you can preliminarily check whether the audio is working properly, and the basic API application related to audio.

Hardware Connection
^^^^^^^^^^^^^^^^^^^^^^^

1. The left and right channels are connected to MIC and SPK.

   .. image:: /_static/e_circuit_audio_connect_speaker.png

2. Run :file:`<sdk_root>/build/openrtos/test_audio.cmd`.
3. Click the item to be tested.

  .. image:: /_static/qconf_audio_test.png

.. |run_icon| image:: /_static/qconf_run_icon.png
  :alt: run icon
  :width: 14
  :height: 14

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

To verify the results
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Click the |build_icon| button to build the project. Please ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Run Tera Term or any Serial Port software
3. Click the |run_icon| button to boot up the system via USB to SPI board.


The following is a further explanation on the three tests.


test audio HW control
""""""""""""""""""""""""

In Test audio HW control project, there are four sub-test examples. Among them:

.. image:: /_static/qconf_audio_test_all.png

- HW iis play:

  Test the audio playing. The complete test code is located in :file:`<sdk_root>/project/test_audio/audioHWctrl/test_main_simple.c`. Normally, you should be able to hear a long beep followed by a short audio from the speakers after the system starts.

- HW usbwav play:

  To test playing audio files from a USB flash drive (only PCM wav files can be played). The complete test code is located in :file:`<sdk_root>/project/test_audio/audioHWctrl/test_main_usb_play.c`. Please place the audio file named :file:`test.wav` in the USB flash drive and you can hear the sound from the speaker after the system is booted.

- HW usbwav record:

  To test audio recording to a USB flash drive. The complete test code is located in :file:`<sdk_root>/project/test_audio/audioHWctrl/test_main_usb_record.c`. The system will record for about 8 seconds immediately after startup and store the recorded sound on a USB flash drive (in wav file format).

- HW louder:

  To test MIC-IN-SPK-OUT. :file:`<sdk_root>/project/test_audio/audioHWctrl/test_main_louder.c`. After the system is started, the microphone and speaker operate at the same time to create an amplifier effect. Test AD and DA operate at the same time.


test audio FLOW
""""""""""""""""""""""""

In the Test audio FLOW project, there are five sub-test examples. Among them:

.. image:: /_static/qconf_audio_test_audio_flow.png

- FLOWER play:

  To test the audio file played from a USB flash drive (only available for PCM wav files). The complete test code is located in :file:`<sdk_root>/project/test_audio/audioFlow/test_flower_play.c`. Please place the test audio file named :file:`Music8k1c.wav` in the USB flash drive, and you can hear the sound from the speakers after the system starts.

- FLOWER usbrec:

  To test audio recording to a USB flash drive. The complete test code is located in :file:`<sdk_root>/project/test_audio/audioFlow/test_flower_usbrec`. Plug in the USB flash drive, the system will record the sound for about 8 seconds immediately after the system starts, and store the recorded sound in the USB flash drive (in wav file format, the file path is :file:`a:/rec.wav`).

- FLOWER mix:

  Mixes multiple audio files from a USB flash drive. The complete test code is located in :file:`<sdk_root>/project/test_audio/audioFlow/test_flower_mix.c`.

  Put the following audio files into the USB flash drive, insert the USB flash drive, and you can hear the sound after the system starts.

  - The main audio file: ``a:/Music8k1c.wav`` (background music).
  - Audio files to be mixed: ``a:/0.wav``, ``a:/1.wav``, ``a:/2.wav``, ``a:/3.wav`` (The content is short audio).

- FLOWER asr:

  This is a sample program for speech recognition. The full test code is located in :file:`<sdk_root>/project/test_audio/audioFlow/test_flower_asr.c`, and currently only Chinese version is available. To run this program, please read the following Chinese command after system boot:

  .. 底下表格會造成無法轉換成 pdf，所以將每個中文字加上 {} 來 workaround

  .. list-table::

    * - {小陽管家}
      - {打開窗簾}
      - {打開空調}
    * - {電影模式}
      - {關閉窗簾}
      - {關閉空調}
    * - {在家模式}
      - {停止窗簾}
      - {最小風量}
    * - {工作模式}
      - {打開燈光}
      - {中等風量}
    * - {離家模式}
      - {關閉燈光}
      - {最大風量}
    * - {打開網絡}
      - {調亮一點}
      - {自動風量}
    * - {關閉網絡}
      - {調暗一點}
      - {調高風量}
    * -
      -
      - {調低風量}
    * -
      -
      - {調低溫度}
    * -
      -
      - {調高溫度}

  If successful, the recognized command will be displayed in the output message.

  .. image:: /_static/teraterm_audio_test_log.png


- FLOWER louder:

  To test MIC-IN-SPK-OUT. The complete test code is located in :file:`<sdk_root>/project/test_audio/audioFlow/test_main_louder.c`. When the system is started, it will receive audio from the microphone and transmit sound from the speakers, creating a amplifier effect.

test audio MGR
""""""""""""""""""""""""

In the Test audio FLOW project, only one sub-test example is included:

.. image:: /_static/qconf_audio_test_audio_mgr.png

- AUDIOMGR play:

  To test the audio file played from a USB flash drive. Here we demonstrate the playing of an MP3 file. The complete test code is located in :file:`<sdk_root>/project/test_audio/audioHWctrl/test_main_louder.c`. Please put :file:`test.mp3` in the USB flash drive and you can hear the sound after the system boot up.

.. raw:: latex

  \newpage
