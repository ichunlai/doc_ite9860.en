.. _sd:

SD/SDIO
===========

The ITE platform provides two SD controllers, both of which can support SD cards, eMMC cards and SDIO wifi modules. Depending on the product application, the two SD controllers may share some pins or may have completely independent pins.

.. note::

    The SD module only provides the library and not the source code.

SD/eMMC cards are accessed through the file system and are not open to direct API control.

The SDIO interface currently supports wifi applications. Please refer to :file:`<sdk_root>/sdk/include/ite/ite_sdio.h` for the API provided by the SDIO interface.

Software Configuration Instructions
---------------------------------------

SD/eMMC card
^^^^^^^^^^^^^^^^^^^^^^^^^

- Depending on the system application, you can turn on SD0/SD1 individually or both.
- If all GPIO pins of SD0 are not shared with other devices, please check the :menuselection:`SD0 --> SD0 Device --> SD0 no pin share` option; the same for SD1.
- If SD0 has no card detect pin, please check :menuselection:`SD0 --> SD0 Device --> SD0 as static device` option; the same for SD1.
- :guilabel:`SD0 card 1-bit mode` is for testing, and usually do not need to check; SD1 the same.
- :guilabel:`SD New Driver` is checked by default.

  .. image:: /_static/qconf_storage_sd.png


Depending on the design of the platform, the following GPIO settings should be completed.

- If the platform you are using does not provide power enable gpio, please set :guilabel:`SD0 Power Enable Pin` to -1; same for write protect.
- SD I/O pins in order are: CLK, CMD, D0, D1, D2, D3, D4, D5, D6, D7
- If eMMC is used and 8-bit mode is provided, then D4~D7 should be entered with the corresponding GPIO settings, otherwise they are all set to -1.

  .. image:: /_static/qconf_gpio_sd.png


SDIO Configuration
^^^^^^^^^^^^^^^^^^^^^^^^^

- To support SDIO function, you need to check SDIO Enable. If the SDIO device is connected to SD0 controller, you need to check :guilabel:`SDIO 0 as static device` option; if the SDIO device is connected to SD1 controller, you need to check :guilabel:`SDIO 1 as static device` option.
- Initially, if the circuit is not good, you can uncheck the :guilabel:`Enable SDIO 4-bit mode` to experiment, then it will operate in 1-bit mode.
- If you are using ESP32 module, you need to confirm whether the module supports 4-bit mode, if not, please uncheck the :guilabel:`Enable SDIO 4-bit mode` option.

.. image:: /_static/qconf_peripheral_sd.png

Timing Settings
^^^^^^^^^^^^^^^^^^^^^^^^^

If a newly designed board is suspected to be poorly routed, you can first run the scan window function provided by the test program, which is only available for SD cards but not SDIOs. Before execution, you need to check if the relevant GPIOs are configured according to the board design, and check :guilabel:`Scan window for SD` in Kconfig.

.. image:: /_static/qconf_sdtest_scan_window_for_sd.png

The following table will be presented at the end of the execution and it is recommended to provide the results to RD for analysis.

.. image:: /_static/console_sdtest_scan_window_for_sd.png


Timing related settings can be found in ``ithSdDelay()`` in :file:`<sdk_root>/sdk/driver/ith/ith_card.c`.


.. raw:: latex

    \newpage
