.. _wiegand:

Wiegand
===========

Function description
---------------------

Wiegand interface is a common communication interface for non-contact readers in access control or security systems. The wiegand of IT986x provide a standard way to read the wiegand code from the induction card. IT986x supports up to two wiegand interfaces, each providing the standard Wiegand 26, Wiegand 34 and Wiegand 37 protocols. Users can also write their own protocol lengths to match specific systems.

Wiegand input format
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

IT986x supports three input formats.

1. Wiegand 26

  Wiegand 26 protocol has a total of 26 binary digits, among which

  - bit0 is the even parity check bit of bit1~bit12.
  - The bits 1 to 8 are the Facility code, 8 bits in total.
  - bit9~bit24 are User Data, total 16 bits.
  - Bit25 is the odd parity check bit of bit13~bit24.

  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | 0 |	1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | F | F | F | F | F | F | F | F | D | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | E | E | E | E | E | E | E | E | E | E  | E  | E  |    |    |    |    |    |    |    |    |    |    |    |    |    |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  |   |   |   |   |   |   |   |   |   |   |    |    |    | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

  - E: ven parity
  - O: dd parity
  - F: acility Code
  - D: User Data
  - P: Parity bit

2. Wiegand 34

  The Wiegand 34 protocol has a total of 34 binary digits, among which

  -	bit0 is the even parity check bit of bit1~bit16.
  -	The bits 1 to 8 are the Facility code, 8 bits in total.
  -	The bit9~bit32 bits are User Data, total 24 bits.
  -	The bits 1 to 8 are the Facility code, 8 bits in total.

  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | F | F | F | F | F | F | F | F | D | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | E | E | E | E | E | E | E | E | E | E  | E  | E  | E  | E  | E  | E  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  |   |   |   |   |   |   |   |   |   |   |    |    |    |    |    |    |    | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

  - E: Even parity
  - O: Odd parity
  - F: Facility Code
  - D: User Data
  - P: Parity bit

3. Wiegand 37

  The Wiegand 37 protocol has a total of 37 binary digits, among which

  -	bit0 is the even parity check bit of bit1~bit18.
  -	Bit1~bit35 are User Data, total 35 bits.
  -	The bit36 is the odd parity check bit of bit18~bit35.

  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11 | 12 | 13 | 14 | 15 | 16 | 17 | 18 | 19 | 20 | 21 | 22 | 23 | 24 | 25 | 26 | 27 | 28 | 29 | 30 | 31 | 32 | 33 | 34 | 35 | 36 |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | D | D | D | D | D | D | D | D | D | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | D  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  | P | E | E | E | E | E | E | E | E | E | E  | E  | E  | E  | E  | E  | E  | E  | E  |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |    |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+
  |   |   |   |   |   |   |   |   |   |   |    |    |    |    |    |    |    |    | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | O  | P  |
  +---+---+---+---+---+---+---+---+---+---+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+----+

  - E: Even parity
  - O: Odd parity
  - D: Data
  - P: Parity bit

Related KConfig settings
----------------------------------------------------------------

- :menuselection:`Peripheral --> Wiegand Enable`

  To determine if Wiegand should be enabled. This option must be enabled if Wiegand-related features are to be enabled.

- :menuselection:`Peripheral --> Wiegand Enable --> Wiegand#`

  Two Wiegand interfaces are provided, which can be activated at the same time or one at a time.

- :menuselection:`Peripheral --> Wiegand Enable --> Wiegand# --> Bit Count`

  Set the input format to Wiegand 26, Wiegand 34, and Wiegand 37, or customize the format by specifying the number of bits.

- :menuselection:`Peripheral --> Wiegand Enable --> Wiegand# --> Bit Count --> Customize --> Customize Module`

  To set up the replacement of customized drive files.

- :menuselection:`Peripheral --> Wiegand Enable --> Wiegand# --> WIEGAND0_GPIO0`

  To set the GPIO pin number of Data0.

- :menuselection:`Peripheral --> Wiegand Enable --> Wiegand# --> WIEGAND0_GPIO1`

  To set the GPIO pin number of Data1.

Related source code files
----------------------------------------------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - ``sdk/driver/itp/itp_wiegand.c``
    - API prototype for reference


Description of related API
---------------------------------------

This section describes the APIs for operating Wiegand devices. Wiegand driver provides a POSIX compliant API that allows users to perform operations like reading and writing on Wiegand devices via the ``open()``/``read()``/``write()``/``ioctl()``.

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  To register the device.

**Parameters**

``ITPDeviceType type``

  Device Types. The device types associated with Wiegand devices are  ``ITP_DEVICE_WIEGAND0`` ~ ``ITP_DEVICE_WIEGAND1``.

``const ITPDevice *device``

  The device identifiers. The device identifiers associated with Wiegand devices are  ``itpDeviceWiegand0`` ~ ``itpDeviceWiegand1``.

**Description**

This function can be used to register a Wiegand device to the core device list.


ioctl
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

**Parameters**

``int file``

  Device identifier. Its value is the value in the ``ITPDeviceType`` list. The device identifiers associated with Wiegand devices are ``ITP_DEVICE_ WIEGAND0`` ~ ``ITP_DEVICE_ WIEGAND1``.

``unsigned long request``

  The *request* parameter is used to select the function to be run on the device and will depend on the specified device.

``void *ptr``

  The *ptr* parameter represents the additional information required to perform the requested function on this particular device. The type of *ptr* depends on the particular control request, but it can be an integer or a pointer to a specific data structure of the device.

**Description**

The ``ioct()`` function is used to perform various control functions on the Wiegand device. The ``ioctl()`` commands for Wiegand devices are described below.

- ITP_IOCTL_INIT

  To initialize the Wiegand device.

- ITP_IOCTL_ENABLE

  To enable the Wiegand device.

- ITP_IOCTL_SET_BIT_COUNT

  To set the mode of the Wiegand protocol.

- ITP_IOCTL_SET_GPIO_PIN

  GPIO pin number used to set Wiegand Data.

read
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __nbyte)

**Parameters**

``int __fd``

  The Wiegand device to be executed. E.g. ``ITP_DEVICE_ WIEGAND0``.

``void *__buf``

  The location to write back after reading.

``size_t __nbyte``

  The value is 0.

**Description**

This function allows the user to read the Wiegand code in the induction card via the Wiegand device.

Example program
---------------------

.. code-block:: c

    #include "ite/itp.h"    // for ITP_DEVICE_WIEGAND0 & ITP_DEVICE_WIEGAND1

    int main(void)
    {
        int bit_count;
        int wiegand_gpio[2];

        // wiegand 0
        itpRegisterDevice(ITP_DEVICE_WIEGAND0, &itpDeviceWiegand0);
        ioctl(ITP_DEVICE_WIEGAND0, ITP_IOCTL_INIT, NULL);
        bit_count = WIEGAND0_BIT_COUNT;
        ioctl(ITP_DEVICE_WIEGAND0, ITP_IOCTL_SET_BIT_COUNT, &bit_count);
        wiegand_gpio[0] = CFG_WIEGAND0_GPIO0;
        wiegand_gpio[1] = CFG_WIEGAND0_GPIO1;
        ioctl(ITP_DEVICE_WIEGAND0, ITP_IOCTL_SET_GPIO_PIN, wiegand_gpio);
        ioctl(ITP_DEVICE_WIEGAND0, ITP_IOCTL_ENABLE, NULL);

        // wiegand 1
        itpRegisterDevice(ITP_DEVICE_WIEGAND1, &itpDeviceWiegand1);
        ioctl(ITP_DEVICE_WIEGAND1, ITP_IOCTL_INIT, NULL);
        bit_count = WIEGAND1_BIT_COUNT;
        ioctl(ITP_DEVICE_WIEGAND1, ITP_IOCTL_SET_BIT_COUNT, &bit_count);
        wiegand_gpio[0] = CFG_WIEGAND1_GPIO0;
        wiegand_gpio[1] = CFG_WIEGAND1_GPIO1;
        ioctl(ITP_DEVICE_WIEGAND1, ITP_IOCTL_SET_GPIO_PIN, wiegand_gpio);
        ioctl(ITP_DEVICE_WIEGAND1, ITP_IOCTL_ENABLE, NULL);

        while(1)
        {
            char* card_id0, card_id1;

            read(ITP_DEVICE_WIEGAND0, &card_id0, 0);
            read(ITP_DEVICE_WIEGAND1, &card_id1, 0)

            usleep(10000);
        }
    }

.. raw:: latex

    \newpage
