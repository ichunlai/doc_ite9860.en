.. _gpio:

GPIO
==========

Function description
--------------------------------

The IT986x GPIO controller (ITGPIO) is a user-programmable general-purpose I/O controller. It is used to input/output data from systems and devices. Each GPIO pin can be programmed as an input or output.

Each GPIO pin can also be used as an interrupt input. It supports rising edge, falling edge, double edge and high/low potential interrupt detection types.

Related KConfig settings
-----------------------------

None.

Related source code files
-----------------------------

.. list-table::
   :header-rows: 1

   * - Path
     - Description
   * - :file:`<sdk_root>/sdk/include/ith/ith_gpio.h`
     - API prototype definition.

Description of related API
-----------------------------

This section describes the APIs used to operate the GPIO pins.

ithGpioEnable
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioEnable(unsigned int pin)

   To switch a pin to be used as a GPIO.

**Parameter**

``unsigned int pin``

   GPIO pin number.

**Description**

Switch the GPIO pin corresponding to the pin to be used as GPIO.


ithGpioSetIn
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioSetIn(unsigned int pin)

   To set the GPIO pin to input mode.

**Parameter**

``unsigned int pin``

   GPIO pin number.

**Description**

To set the GPIO pin corresponding to the pin to input mode.


ithGpioSetOut
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioSetOut(unsigned int pin)

   Set the GPIO pin to output mode.

**Parameter**

``unsigned int pin``

   GPIO pin number.

**Description**

Set the GPIO pin corresponding to the pin to output mode.


ithGpioSet
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioSet(unsigned int pin)

   To set the GPIO pin output high potential.

**Parameter**

``unsigned int pin``

   GPIO pin number.

**Description**

To set the GPIO pin corresponding to the pin to output a high potential. This is only valid when the GPIO pin is in the output state.

ithGpioClear
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioClear(unsigned int pin)

   To set the GPIO pin output to a low potential.

**Parameter**

``unsigned int pin``

   GPIO pin number.

**Description**

Set the GPIO pin corresponding to the pin to output a low potential. This is only valid when the GPIO pin is in the output state.


ithGpioGet
^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t ithGpioGet(unsigned int pin)

   To obtain the input of GPIO pin as high (1) or low (0) potential.

**Parameter**

``unsigned int pin``

   GPIO pin number.

**Description**

Get the input of the GPIO pin corresponding to the pin as high (1) or low (0). If the GPIO pin is set to output mode, call this function to get the current output of the GPIO pin as high (1) or low (0).

**Return**

- 0: Low potential
- 1: High potential

ithGpioCtrlEnable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioCtrlEnable(unsigned int pin, ITHGpioCtrl ctrl)

   To set the control mode of GPIO pin.

**Parameter**

``unsigned int pin``

   GPIO pin number.

``ITHGpioCtrl ctrl``

   The control mode to be enabled or set.

**Description**

To enables various control modes for the GPIO pin corresponding to the pin. ctrl parameter represents the control mode to be enabled and can be one of the following values:

- ITH_GPIO_PULL_ENABLE

  To enable the pull up/down function of the GPIO pin corresponding to the pin.

- ITH_GPIO_PULL_UP

  When the pin corresponding GPIO pin has the pull up/down function enabled, set the pin corresponding GPIO pin to pull up mode.

- ITH_GPIO_INTR_LEVELTRIGGER

  To set the GPIO pin corresponding to the pin to be interrupt input and adopt level trigger mode.

- ITH_GPIO_INTR_BOTHEDGE

  To enable Both edge trigger mode when the GPIO pin corresponding to the pin is set to interrupt input and edge trigger mode is adopted.

- ITH_GPIO_INTR_TRIGGERFALLING

  To set the pin to the falling edge trigger mode when the GPIO pin corresponding to the pin is set to interrupt input and the edge trigger mode is adopted.

- ITH_GPIO_INTR_TRIGGERLOW

  To set the pin to low level trigger when the GPIO pin corresponding to the pin is set to interrupt input and the level trigger mode is adopted.

ithGpioCtrlDisable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioCtrlDisable(unsigned int pin, ITHGpioCtrl ctrl)

   Set the control mode of GPIO pin.

**Parameter**

``unsigned int pin``

   GPIO pin number.

``ITHGpioCtrl ctrl``

   The control mode to disable or clear.

**Description**

To disable or clear the various control modes of the GPIO pin corresponding to the pin. ctrl Parameter represents the control mode to be disabled and can be one of the following values:

- ITH_GPIO_PULL_ENABLE

   Disable the pull up/down function of the GPIO pin corresponding to the pin.

- ITH_GPIO_PULL_UP

   When the pin corresponding GPIO pin has the pull up/down function enabled, set the pin corresponding GPIO pin to pull down mode.

- ITH_GPIO_INTR_LEVELTRIGGER

   To set the GPIO pin corresponding to the pin to interrupt the input and adopt the edge trigger mode.

- ITH_GPIO_INTR_BOTHEDGE

   Both edge trigger mode is disabled when the GPIO pin corresponding to the pin is set to interrupt input and edge trigger mode is adopted.

- ITH_GPIO_INTR_TRIGGERFALLING

   To set the pin to rising edge trigger when the GPIO pin corresponding to the pin is set to interrupt input and the edge trigger mode is used.

- ITH_GPIO_INTR_TRIGGERLOW

   To set the pin to high level trigger when the GPIO pin corresponding to the pin is set to interrupt input and the level trigger mode is adopted.

ithGpioRegisterIntrHandler
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioRegisterIntrHandler(unsigned int pin, ITHGpioIntrHandler handler, void *arg)

**Parameter**

``unsigned int pin``

   GPIO pin number.

``ITHGpioIntrHandler handler``

   The ISR function pointer to be registered.

``void *arg``

   The Parameter to be passed into the ISR function.

**Description**

To register the ISR of GPIO.

ithGpioEnableIntr
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioEnableIntr(unsigned int pin)

**Parameter**

``unsigned int pin``

   GPIO pin number.

**Description**

To enable GPIO interrupt function.

ithGpioDisableIntr
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioDisableIntr(unsigned int pin)

**Parameter**

``unsigned int pin``

   GPIO pin number.

**Description**

To disable GPIO interrupt function.

ithGpioClearIntr
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioClearIntr(unsigned int pin)

**Parameter**

``unsigned int pin``

   GPIO pin number.

**Description**

Clear the interrupt flag of GPIO.

The following describes the interrupt setting process:

- It is recommended to use ``ithEnterCritical()`` to protect the whole interrupt setting block from other interrupts before setting.
- To clear the previous interrupt signal. Use ``ithGpioClearIntr(pin)`` to clear the interrupt signal of this GPIO pin.
- To register ISR. Use ``ithGpioRegisterIntrHandler()`` to register ISR.
- To set the interrupt mode. Use ``ithGpioCtrlEnable()`` and ``ithGpioCtrlDisable()`` to set the interrupt mode (such as edge trigger/level trigger, both edge/single edge, ...).
- To enable the IRQ of GPIO interrupt. Use ``ithIntrEnableIrq(ITH_INTR_GPIO)`` to enable the IRQ of GPIO.
- To enable interrupt function for this GPIO pin. Use ``ithGpioEnableIntr()`` to enable interrupt function for this GPIO pin.
- Use ``ithExitCritical()`` to leave the critical section.


ithGpioSetMode
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithGpioSetMode(unsigned int pin, ITHGpioMode mode)

**Parameter**

``unsigned int pin``

   GPIO pin number.

``ITHGpioMode mode``

   To set the GPIO multiplexed function mode, the following parameter values can be used.

   - ITH_GPIO_MODE0
   - ITH_GPIO_MODE1
   - ITH_GPIO_MODE2
   - ITH_GPIO_MODE3
   - ITH_GPIO_MODE4

**Description**

Set the multiplex function mode of GPIO PIN. Because of the limited number of IT986x series pins, each pin can also be switched to other functions, which is called multiplexed functions). The following table lists the multiplexed function mode corresponding to each GPIO pin.

  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | Pin          | Mode 0       | Mode 1       | Mode 2          | Mode 3       | Mode 4       |
  +==============+==============+==============+=================+==============+==============+
  | GPIO0        | GPIO0        |                                                              |
  +--------------+--------------+                                                              +
  | GPIO1        | GPIO1        |                                                              |
  +--------------+--------------+                      USB2SPI_Debug                           +
  | GPIO2        | GPIO2        |                                                              |
  +--------------+--------------+                                                              +
  | GPIO3        | GPIO3        |                                                              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO4        | GPIO4        |              Read_Log (Default use UART0_Tx)                 |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO5        | GPIO5        | AXISPI_CS0   |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO6        | GPIO6        | AXISPI_D0    | SD0_D0          |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO7        | GPIO7        | AXISPI_D1    | SD0_D1          |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO8        | GPIO8        | AXISPI_D2    | SD0_D2          |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO9        | GPIO9        | AXISPI_D3    | SD0_D3          |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO10       | GPIO10       | AXISPI_CLK   | SD0_D3          |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO11       | GPIO11       | AXISPI_CS1   | SD1_CLK         |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO13       | GPIO13       |              | SD0_CMD         |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO14       | GPIO14       |              | SD0_CLK         |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO15       | GPIO15       |              | SD0_D0          | SPI1_CLK     |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO16       | GPIO16       |              | SD0_D1          | SPI1_CS0     |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO17       | GPIO17       |              | SD0_D2          | SPI1_DO      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO18       | GPIO18       |              | SD0_D3          | SPI1_DI      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO19/XAIN0 | GPIO19       | WO1_D0       | SD1_D1          | SPI0_CLK     | RX_ZD1       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO20/XAIN1 | GPIO20       | WO1_D1       | SD1_CMD         | SPI0_CS0     | RX_ZD2       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO21/XAIN2 | GPIO21       | IrDA_RXL     | WI0_Out(Debug)  | SPI0_DO      | RX_ZD3       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO22/XAIN3 | GPIO22       | IrDA_TX      | WI1_Out(Debug)  | SPI0_DI      | IIS_AMCLK    |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO23/XAIN4 | GPIO23       | WO0_D0       |                 | SPI0_CS1     | IIS_ZCLK     |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO24/XAIN5 | GPIO24       | WO0_D1       |                 | SPI0_CLK     | IIS_ZWS      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO25/XAIN6 | GPIO25       | WO1_D0       |                 | SPI0_CS0     | IIS_ZDO      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO26/XAIN7 | GPIO26       | WO1_D1       |                 |              | IIS_ZDI      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO27       | GPIO27       |  MDIO        | SD0_D1          | SPI0_CLK     |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO28       | GPIO28       |  MDC         | SD0_CMD         | SPI0_CS0     |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO29       | GPIO29       |  TXD1        | SD0_CLK         | SPI0_DO      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO30       | GPIO30       |  TXD0        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO31       | GPIO31       |  TXEN        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO32       | GPIO32       |  TXC         | SD1_CLK         |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO33       | GPIO33       |  RX_CRS_DV   | SD1_CMD         | SPI0_DI      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO34       | GPIO34       |  RXD0        | SD1_D0          | SPI1_CLK     | WO0_D0       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO35       | GPIO35       |  RXD1        | SD1_D1          | SPI1_CS0     | WO0_D1       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO36       | GPIO36       |  RXER        | SD1_D2          | SPI1_DO      | WO1_D0       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO37       | GPIO37       |  INTB        | SD1_D3          | SPI1_DI      | WO1_D1       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO38       | GPIO38       |  LDCLK       | VD0             | SPI1_CLK     |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO40       | GPIO40       |  LD1         | VD2             | SPI1_DO      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO41       | GPIO41       |  LD2         | VD3             | SPI1_DI      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO42       | GPIO42       |  LD3         | VD4             | SPI1_CS1     | SD1_CLK      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO43       | GPIO43       |  LD4         | VD5             | SD1_CMD      |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO44       | GPIO44       |  LD5         | VD6             | SD1_D0       |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO45       | GPIO45       |  LD6         | VD7             | SD1_D1       |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO46       | GPIO46       |  LD7         | VD8             | SD1_D2       |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO47       | GPIO47       |  LD8         | VD9             | WO0_D0       | SD1_D3       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO48       | GPIO48       |  LD9         | VD10            | WO0_D1       | SD1_D4       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO49       | GPIO49       |  LD10        | VD11            | WO1_D0       | SD1_D5       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO50       | GPIO50       |  LD11        | VD12            | WO1_D1       | LDCLK        |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO51       | GPIO51       |  LD12        | VD13            | SD0_CLK      | SD1_D6       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO52       | GPIO52       |  LD13        | VD14            | SD0_CMD      | SD1_D7       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO53       | GPIO53       |  LD14        | VD15            | SD0_D0       |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO54       | GPIO54       |  LD15        | VD16            | SD0_D1       |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO55       | GPIO55       |  LD16        | VD17            | SD0_D2       | RX_ZCLK      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO56       | GPIO56       |  LD17        | VD18            | SD0_D3       | RX_ZWS       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO57       | GPIO57       |  LD18        | VD19            | SD0_D4       | RX_ZD0       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO58       | GPIO58       |  LD19        | VD20            | SD0_D5       | RX_AMCLK     |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO59       | GPIO59       |  LD20        | VD21            | SD0_D6       | LDCLK        |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO60       | GPIO60       |  LD21        | VD22            | SD0_D7       | RX_ZCLK      |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO61       | GPIO61       |  LD22        | VD23            | SD1_D1       | RX_ZWS       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO62       | GPIO62       |  LD23        | VD24            | SD1_CMD      | RX_ZD0       |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO63       | GPIO63       |  LD24        | VD25            | SD1_CLK      | RX_AMCLK     |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO64       | GPIO64       |  LD25        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO65       | GPIO65       |  LD26        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO66       | GPIO66       |  LD27        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+
  | GPIO67       | GPIO67       |  LD28        |                 |              |              |
  +--------------+--------------+--------------+-----------------+--------------+--------------+

Basic example program
-----------------------------

The following is the GPIO example code.

GPIO output
^^^^^^^^^^^^^^^^^^^^^^^^

The following display setting the pin corresponding to :envvar:`CFG_GPIO_TEST_OUTPUT_PIN` to output mode, and switching the output high/low status every second.

.. code-block:: c

   #include "ite/itp.h" // for all ith driver (include GPIO) & MACRO

   void main(void)
   {
       int gpioPin = CFG_GPIO_TEST_OUTPUT_PIN;
       int i = 0;

       itpInit();

       //initial GPIO
       ithGpioSetOut(gpioPin);
       ithGpioSetMode(gpioPin, ITH_GPIO_MODE0);

       //
       while(1)
       {
           if (i++ & 0x1)
           {
               ithGpioClear(gpioPin);
           }
           else
           {
               ithGpioSet(gpioPin);
           }
           printf("current GPIO[%d] state=%x, index=%d\n”, gpioPin, ithGpioGet(gpioPin), i);
           usleep(1000 * 1000);  // wait for 1 second
       }

       return NULL;
   }

GPIO input
^^^^^^^^^^^^^^^^^^^^^^^^

The following shows setting the pin corresponding to :envvar:`CFG_GPIO_TEST_OUTPUT_PIN` to input mode, and constantly polling the high/low status of the pin.

.. code-block:: c

    #include "ite/itp.h"    // for all ith driver (include GPIO) & MACRO

    static void _gpioPinInit(void)
    {
        ithGpioSetMode(gpioPin, ITH_GPIO_MODE0);
        ithGpioSetIn(gpioPin);
        ithGpioEnable(gpioPin);
    }

    void main(void)
    {
        int gpioPin = CFG_GPIO_TEST_INPUT_PIN;
        int lastPinStatus = 0;

        itpInit();
        // initial GPIO
        _gpioPinInit();

        while(1)
        {
            // polling gpio pin
            if (ithGpioGet(gpioPin) != lastPinStatus)
            {
                // GPIO status has changed
                lastPinStatus = ithGpioGet(gpioPin);
                if (lastPinStatus)
                {
                    printf("The lasted GPIO state is HIGH\n”);
                }
                else
                {
                    printf("The lasted GPIO state is LOW\n”);
                }
            }
        }
        return NULL;
    }

GPIO interrupt
^^^^^^^^^^^^^^^^^^^^^^^^

The following shows setting the pin corresponding to :envvar:`CFG_GPIO_TEST_OUTPUT_PIN`` to interrupt input mode and using both edge trigger.

.. code-block:: c

    #include "ite/itp.h"    // for ITH_GPIO_MODE0 & GPIO ith driver

    static char g_GPIO_INTR = 0;

    void _gpio_isr(void* data)
    {
        g_GPIO_INTR = 1;
    }

    void _initGpioPin(int pin)
    {
        ithGpioSetMode(pin, ITH_GPIO_MODE0);    // Set the GPIO pin as mode 0.
        ithGpioSetIn(pin);  // Set the GPIO pin as input pin.
        ithGpioCtrlEnable(pin, ITH_GPIO_PULL_ENABLE);   // Enable the GPIO pin's pull function
        ithGpioCtrlEnable(pin, ITH_GPIO_PULL_UP);       // Enable the GPIO pin's pull up function
        ithGpioEnable(pin); // Enable GPIO pin
    }

    void _initGpioIntr(int pin)
    {
        ithEnterCritical();     // To prevent from being interrupted
        ithGpioClearIntr(pin);  // Clear the interrupt
        ithGpioRegisterIntrHandler(pin, _gpio_isr, (void*)pin); // Register the GPIO pin's interrupt handler
        ithGpioCtrlDisable(pin, ITH_GPIO_INTR_LEVELTRIGGER);    // Use edge trigger interrupt mode
        ithGpioCtrlEnable(pin, ITH_GPIO_INTR_BOTHEDGE);         // Use Both edge trigger
        ithIntrEnableIrq(ITH_INTR_GPIO);    // Enable the GPIO pin's interrupt (to the interrupt controller)
        ithGpioEnableIntr(pin);             // Enable the GPIO pin's interrupt
        ithExitCritical();      // unlock spinlock
    }

    void _initGpio(int pin)
    {
        _initGpioPin(pin);
        _initGpioIntr(pin);
    }

    void main(void)
    {
        int gpioPin = CFG_GPIO_TEST_INTR_PIN;
        int i = 0;

        itpInit();

        _initGpio(gpioPin);

        while(1)
        {
            // polling "g_GPIO_INTR"
            if (g_GPIO_INTR)
            {
                unsigned int gpioState = ithGpioGet(gpioPin) ? 1 : 0;
                printf("current GPIO[%d] state = %d \n”, gpioPin, gpioState);
                g_GPIO_INTR = 0;
                ithGpioClearIntr(gpioPin);
            }
            usleep(1000);   // sleep for 1 ms
        }

        return NULL;
    }

.. raw:: latex

    \newpage
