﻿.. _i2c:

I2C
=======

Function description
--------------------

The IT986x series supports four I2C modules, all of which can be used as I2C Master/Slave. Each I2C module supports standard (100 kbit/s) and fast modes (400 kbits/s), and 7 bit addressing mode.

Related KConfig settings
----------------------------------------------------------------

- :menuselection:`Peripheral --> I2C# Enable`

  To determine if the I2C# device should be enabled or not. When this option is checked, the system will register the corresponding I2C# device via the ``itpRegisterDevice()`` function in the ``itpInit()`` function and perform the initialization.

- :menuselection:`Peripheral --> I2C# Enable --> I2C# Clock Rate`

  To determine the clock rate of the I2C# device. 400kHz is the default.

- :menuselection:`GPIO --> IIC# CLK GPIO Pin`

  To specify which GPIO pin is to be used as the Clock pin for this I2C module.

- :menuselection:`GPIO --> IIC# DATA GPIO Pin`

  To specify which GPIO pin to be used as the Data pin for this I2C module.


Related source code files
----------------------------


.. list-table::
   :header-rows: 1

   * - Path
     - Description
   * - ``<sdk_root>/sdk/driver/itp/itp_i2c.c``
     - The uppermost layer function. It mainly provides the API that POSIX standards specified to be implemented, for the upper-level AP to operate the device through ``open()``, ``close()``, ``ioctl()`` and other functions.
   * - | ``<sdk_root>/sdk/driver/iic/*``
       | ``<sdk_root>/sdk/include/iic/*``
     - The underlying function, which may change in the future.


Description of related API
---------------------------------------

This section describes the APIs used to operate the I2C device. I2C driver provides a POSIX compliant API that allows users to perform operations like reading and writing files to the I2C device through functions such as ``open()``/``read()``/``write()``/``ioctl()``.

itpRegisterDevice
^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  To register a device.

**Parameters**

``ITPDeviceType type``

  Device type. See :file:`<sdk_root>/sdk/include/ite/itp.h` for a full list of device types. The device types associated with I2C devices are ``ITP_DEVICE_I2C0`` ~ ``ITP_DEVICE_I2C3``.

``const ITPDevice *device``

  Device Identifier. The device identifiers associated with I2C devices are ``itpDeviceI2c0`` ~ ``itpDeviceI2c3``.

**Description**

This function can be used to register the I2C# device to the core device list so that the upper AP can operate the I2C functionality via ``ioctl()``/``read()``/``write()`` functions. This function is called by the ``itpInit()`` function during system initialization (see :file:`<sdk_root>/sdk/driver/itp_init_openrtos.c`), so if ``itpInit()`` is called in your project, there is no need to run this registration function again.

.. note::

  It is recommended to use the ``ioctl()``/``read()``/``write()`` as much as possible when calling the I2C API and not to call the I2C underlying driver directly.

The following shows how to register the I2C2 device to the system core:

.. code-block:: c

    itpRegisterDevice(ITP_DEVICE_I2C0, &itpDeviceI2c2);


ioctl
^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  To control the device.

**Parameters**

``int file``

  Device identifier. Its value is the value in the ``ITPDeviceType`` list. The device identifiers associated with I2C devices are ``ITP_DEVICE_I2C0`` ~ ``ITP_DEVICE_I2C3``.

``unsigned long request``

  The *request* parameter is used to select the function to be run on the device and will depend on the specified device.

``void *ptr``

  The *ptr* parameter represents the additional information needed to perform the requested function for this particular device. *ptr*'s type depends on the particular control request, but it can be an integer or a pointer to a specific data structure of the device.

**Description**

The ``ioct()`` function is used to perform various control on the I2C device. *request* parameter and an optional third parameter (with different types) will be passed to and interpreted by the corresponding part of the I2C device associated with the *file*.

- ITP_IOCTL_INIT

  This is used to initialize the I2C device. The I2C group must be set to Master or Slave mode by passing in the *ptr* parameter.

  The following shows how to initialize the I2C2 device by default setting Master mode and the I2C3 device by Slave mode.

  .. code-block:: c

    IIC_MODE iic_port2_mode = MASTER_MODE;
    ioctl(ITP_DEVICE_I2C2, ITP_IOCTL_INIT, (void *)iic_port2_mode);
    IIC_MODE iic_port3_mode = IIC_SLAVE_MODE;
    ioctl(ITP_DEVICE_I2C3, ITP_IOCTL_INIT, (void*)iic_port3_mode);

- ITP_IOCTL_RESET

  Same as ``ITP_IOCTL_INIT``, except that ``ITP_IOCTL_RESET`` will first Terminate the I2C device and then reinitialize it. Therefore, the type and usage of the *ptr* parameter is exactly the same as that of ``ITP_IOCTL_INIT``.

  The following shows how to reset the I220 device by default setting Master mode and using the ``ioctl()`` function.

  .. code-block:: c

    IIC_MODE iic_port2_mode = MASTER_MODE;
    ioctl(ITP_DEVICE_I2C2, ITP_IOCTL_RESET, (void *)iic_port2_mode);

open
^^^^^^^^^^^^^^^^^

.. c:function:: int open(const char * name, int value)

  To turn on the device.

**Parameters**

``const char name``

  The name of the device to be turned on. EX: ``ITP_DEVICE_I2C2``.

``int value``

  To be specified.

**Description**

Users can open the I2C port through this function.

Here shows how to use the function:

.. code-block:: c

  int gMasterDev = 0;
  gMasterDev = open(":i2c2", 0);

read
^^^^^^^^^^^^^^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __nbyte)

  To read data from the Slave end.

**Parameters**

``int __fd``

  I2C port to be implemented. E.g. ``ITP_DEVICE_I2C2``.

``void *__buf``

  The location of the structure used to read the data. E.g.

  .. code-block:: c

    typedef struct
    {
        uint8_t     slaveAddress; //slave address
        uint8_t*    cmdBuffer; 	  //send command buffer
        uint32_t    cmdBufferSize; //send command size
        uint8_t*    dataBuffer;   //receive data buffer
        uint32_t    dataBufferSize; //receive data size
    } ITPI2cInfo;

``size_t __nbyte``

  To be specified.

**Description**

  This function allows users to read back data from a specific I2C port.

The following shows how to use the function:

.. code-block:: c

    ITPI2cInfo evt;
    evt.slaveAddress   = 0x77;
    evt.cmdBuffer      = 0;
    evt.cmdBufferSize  = 0;
    evt.dataBuffer     = recvBuffer;
    evt.dataBufferSize = 256;
    read(gMasterDev, &evt, 1);


write
^^^^^^^^^^^^^^^^^

.. c:function:: int write(int __fd, const void *__buf, size_t __nbyte)

  To send data from the Master end.

**Parameters**

``int __fd``

  I2C port to be implemented. E.g. ``ITP_DEVICE_I2C2``.

``void *__buf``

  The location of the structure used to send the data. E.g.

  .. code-block:: c

    typedef struct
    {
        uint8_t     slaveAddress; //slave address
        uint8_t*    cmdBuffer; 	  //send command buffer
        uint32_t    cmdBufferSize; //send command size
        uint8_t*    dataBuffer;   //receive data buffer
        uint32_t    dataBufferSize; //receive data size
    } ITPI2cInfo;

``size_t __nbyte``

  To be specified.

**Description**

This function allows users to send data from a specific I2C port.

The following shows how to use the function.

.. code-block:: c

    ITPI2cInfo evt;
    IicMasterWriteBuffer[0] = 0xAD;
    IicMasterWriteBuffer[1] = 0xB8;
    IicMasterWriteBuffer[2] = 0xB7;
    IicMasterWriteBuffer[3] = 0xB6;
    IicMasterWriteBuffer[4] = 0xB5;

    evt.slaveAddress   = 0x77;
    evt.cmdBuffer      = IicMasterWriteBuffer;
    evt.cmdBufferSize  = 5;
    evt.dataBuffer     = 0;
    evt.dataBufferSize = 0;
    write(gMasterDev, &evt, 1);


Complete example
---------------------------------------

An I2C example program is attached in the :file:`<sdk_root>/project/test_iic` directory. The program shows an mutual read/write function of I2C2 and I2C3. By default, I2C2 is set to Master mode and I2C3 is set to Slave mode, and a loop is used to repeatedly send 5-byte commands from I2C2 to the I2C3 device with Slave address 0x77 and read back 256 bytes of return data.

I2C communication is mainly through two pins to read and write data, respectively CLOCK and DATA pin, so on the board, I2C2 GPIO_SDA must connect to I2C3 GPIO_SDA docking, and I2C2 GPIO_SCL to I2C3 GPIO_SCL docking.

The I2C clock rate is set to 400kHz by default, and it should be adjusted according to the frequency supported by the connected device.

Hardware Connection
^^^^^^^^^^^^^^^^^^^^^^^

Connect CLK GPIO61 to GPIO41 and DATA GPIO62 to GPIO42. All the GPIOs used must be PULL HIGH.

.. image:: /_static/e_circuit_i2c_left.png

.. image:: /_static/e_circuit_i2c_right.png

KConfig settings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please run :file:`<sdk_root>/build/openrtos/test_iic.cmd` and follow these steps to set it up:

- Check :menuselection:`Peripheral --> I2C2 Enable`.
- Check :menuselection:`Peripheral --> I2C3 Enable`.

  .. image:: /_static/qconf_check_i2c_enable.png

Next, set the pins used by IIC2 and IIC3.

- Set :menuselection:`GPIO --> IIC2 CLK Pin` to 61.
- Set :menuselection:`GPIO --> IIC2 DATA Pin` to 62.
- Set :menuselection:`GPIO --> IIC3 CLK Pin` to 41.
- Set :menuselection:`GPIO --> IIC3 DATA Pin` to 42.

  .. image:: /_static/qconf_gpio_i2c.png

To verify the results
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

1. Click the |build_icon| button to build the project. Please ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Reset the power of EVB.
3. When the program is finished building, click the |run_icon|  button to boot up the system via USB to SPI board.
4. Run Tera Term, click :menuselection:`Setup --> Serial Port` in the main menu of Tera Term. Select the com port corresponding to the USB to TTL board for the Port, and select 115200 for the Speed.
5. Test the result as follows, the sender sends 5 bytes of data and receives 256 bytes of data to compare the success, and repeat the process.

  .. image:: /_static/console_i2c_test_result.png

.. raw:: latex

    \newpage
