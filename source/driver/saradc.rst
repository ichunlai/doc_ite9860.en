.. _saradc :

SAR ADC
========


Function description
--------------------

The SAR ADC provides analog-to-digital signal conversion and is capable of detecting different sensors through 8 channels. In general applications, users can store the digital signal output in memory, and it can also detect and collect information about a specific output range by calculating the average of the digital outputs. Theoretically, each digital output is expressed in 12 bits, and this output varies in proportion to the input voltage, ranging from 0V to 3.3V. The relevant details are described below.

- Sample rate

  - ADC Clock (Hz): WCLK / ADC Divider
  - ADC Conversion Time (sec): 12 / ADC Clock
  - ADC Sample Rate (Hz): 1 / ADC Conversion Time

  .. note::

    The actual WCLK frequency used can be obtained by ``ithGetBusClock()``.

- Specification

  - Input Channel:

    Configurable 8 channels share the ADC.

  - Analog Range:

    0.3V to 3.0V.

  - Sample Rate:

    Up to 1 MHz of sample rate.

  - 12-bit Resolution:

    | Effective 5-bit resolution without calibration.
    | Effective 6-bit resolution with external calibration.
    | Effective 6-bit resolution with internal calibration.

- Memory Output Format

  .. flat-table::
    :widths: 1 1 6

    * - :cspan:`2` 8 byte
    * - 1 byte
      - 1 byte
      - 6 byte
    * - raw_1_low (0 ~ 7 bit)
      - raw_1_high (0 ~ 7 bit)
      - raw data


Related KConfig settings
----------------------------------------------------------------

-	:menuselection:`Peripheral --> SARADC Enable`

  To determine if the SAR ADC device is to be enabled and set a valid XAIN value between 0x0 and 0xff, from low to high bits for the corresponding GPIO pin.

-	:menuselection:`Peripheral --> SARADC Enable --> SARADC Calibration Enable`

  To determine if the SAR ADC calibration function should be enabled.

-	:menuselection:`Peripheral --> SARADC Enable --> SARADC Calibration Enable --> Calibration Reference Type`

  To determine the calibration mode of the SAR ADC. The calibration modes are external voltage calibration mode and internal partial voltage calibration mode.

  -	External voltage calibration mode:

    The calibration function is implemented by referring to the external voltage, and the calibration XAIN and the input voltage to be referenced during calibration must be set. The input voltage must be between 0 and 3.3, corresponding to calibration GPIO pin from the low index to the high index.

  - Internal partial voltage calibration mode:

    The calibration function is implemented by referring to the internal partial voltage, and the calibration XAIN and the input voltage to be referenced during calibration must be set. XAIN must be between 0x0 and 0x1f, with the low to high bits representing the corresponding reference voltages of 0.0V, 0.825V, 1.8V, 2.475V, and 3.3V respectively; and the input voltage must be between 0 and 3.3, with the low to high indexes representing the corresponding calibration control signals.

-	:menuselection:`GPIO --> XAIN#`

  To set the SAR ADC GPIO. The current HW design is fixed PIN number and cannot be changed arbitrarily. The unused SAR ADC GPIO will not conflict with other GPIO functions, and cannot be set to "-1".


Related source code files
----------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - | :file:`<sdk_root>/sdk/include/saradc/*`
      | :file:`<sdk_root>/sdk/driver/saradc/*`
      | :file:`<sdk_root>/sdk/driver/ith/<chip_family_id>/ith_saradc.c`
    - Underlying function with SAR ADC reset related behavior and driver.

Description of related API
---------------------------------------

This section describes the relevant APIs used to operate the SAR ADC device.

mmpSARInitialize
^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: SARADC_RESULT mmpSARInitialize(SARADC_MODE_AVG modeAVG, SARADC_MODE_STORE modeStore, SARADC_AMPLIFY_GAIN amplifyDriving, SARADC_CLK_DIV divider);

  SAR ADC initialization API.

**Parameters**

``SARADC_MODE_AVG modeAVG``

  modeAVG means whether to turn on the engine's AVG and INTR detection modules, usually set to on to get the full functionality. For a complete list of types, see :file:`<sdk_root>/sdk/include/saradc/<chip family id>/saradc_type.h`.

``SARADC_MODE_STORE modeStore``

  modeStore stands for storing RAW or AVG digital signals in memory (AVG is preset to average of 32 RAW data). For a complete list of types, see :file:`<sdk_root>/sdk/include/saradc/<chip family id>/saradc_type.h`.

``SARADC_AMPLIFY_GAIN amplifyDriving``

  amplifyDriving stands for I/O driving, which can be adjusted according to the impedance of the analog circuit, and is normally set to double. For a complete list of types, see :file:`<sdk_root>/sdk/include/saradc/<chip family id>/saradc_type.h`.

``SARADC_CLK_DIV divider``

  The divider represents the control of the engine's operating clock, which can be used to change the sampling rate. For a complete list of types, see see :file:`<sdk_root>/sdk/include/saradc/<chip family id>/saradc_type.h`.


**Return**

Function return value. See :file:`<sdk_root>/sdk/include/saradc/<chip family id>/saradc_error.h` for a complete list of return types. If initialization succeeds, ``SARADC_SUCCESS`` will be returned.


mmpSARTerminate
^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: SARADC_RESULT mmpSARTerminate(void)

  SAR ADC destructuring API. To reset engine and turn off operating clock.

**Return**

Function return value. See :file:`<sdk_root>/sdk/include/saradc/<chip family id>/saradc_error.h` for a complete list of return types. If initialization succeeds, ``SARADC_SUCCESS`` will be returned.


mmpSARConvert
^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: SARADC_RESULT mmpSARConvert(SARADC_PORT hwPort, uint16_t wbSize, uint16_t *avg);

  SAR ADC sampling API. After this API is called, the engine will stop sampling after writing the specified memory block.

**Return**

Function return value. See :file:`<sdk_root>/sdk/include/saradc/<chip family id>/saradc_error.h` for a complete list of return types. If initialization succeeds, ``SARADC_SUCCESS`` will be returned.

**Parameters**

``SARADC_PORT hwPort``

  hwPort represents the channel (GPIO) from which the sampling function is performed. For a complete list of types, see :file:`<sdk_root>/sdk/include/saradc/<chip family id>/saradc_type.h`.

``uint16_t wbSize``

  wbSize represents the total amount of memory sampled, in Byte, and must be 512B or more.

``uint16_t *avg``

  avg can return the average of all digital outputs in wbSize after finishing sampling.


Basic example program
----------------------

.. code-block:: c

    #include "ite/itp.h"
    #include "saradc/saradc.h"

    void main(void)
    {
        SARADC_RESULT result = SARADC_SUCCESS; // SAR ADC error number
        int channel = 0; // channel number
        uint16_t writeBuffer_len = 512, calibrationOutput = 0; // SAR ADC average buffer and average output

        // setting of clock, average, store type on memory, amplifier level and initialization
        result = mmpSARInitialize(
          SARADC_MODE_AVG_ENABLE,
          SARADC_MODE_STORE_RAW_ENABLE,
          SARADC_AMPLIFY_1X,
          SARADC_CLK_DIV_9);
        if (result) printf("mmpSARInitialize() error (0x%x) !!\n", result);

        while (1) // conversion loop of SAR ADC
        {
            if (result = mmpSARConvert(channel, writeBuffer_len, &calibrationOutput))
                printf("mmpSARConvert() error (0x%x) !!\n", result);
            else
                printf("Calibration Output:%d\n", calibrationOutput);
        }

        result = mmpSARTerminate(); // reset engine and disable clock
        if (result) printf("mmpSARTerminate() error (0x%x) !!\n", result);
    }


Complete example
----------------------

An SAR ADC example program is attached to the :file:`<sdk_root>/project/test_saradc` directory. The program shows a continuous sequential sampling function from XAIN_0 to XAIN_7. The default is to return the average of 256 RAW data for each XAIN, and continue sampling to the next XAIN.

Hardware Connection
^^^^^^^^^^^^^^^^^^^^^^^^

Here we use a 16-Keypad external divider to simulate the analog signal and determine whether the SAR ADC can correctly achieve analog-to-digital conversion based on the average of the digital output corresponding to the input voltage.

- Connect the ground wire of the voltage divider to the ground terminal of EVB.
- Connect the power cable of the divider to the 3.3V supply side of the EVB.
- Connect the voltage divider's output line to the XAIN# of the EVB. In this example it is connected to EVB XAIN_3 (GPIO_22).

  .. image:: /_static/e_circuit_saradc.png

KConfig settings
^^^^^^^^^^^^^^^^^^^

Please run :file:`<sdk_root>/build/openrtos/test_saradc.cmd` and follow the steps below to set it up.

- Check :menuselection:`Peripheral --> SARADC Enable`. The valid XAIN is set to 0xff.
- Check :menuselection:`Peripheral --> SARADC Enable --> SARADC Calibration Enable --> Internal Voltage Reference`. The calibration XAIN is set to 0xe (0.825V, 1.8V, 2.475V) and the calibration reference voltages are the best statistical values (0.0V, 0.847V, 1.822V, 2.395V, 3.3V).

  .. image:: /_static/qconf_peripheral_saradc.png

- Set :menuselection:`GPIO --> XAIN#` to 19 ~ 26 in order.

  .. image:: /_static/qconf_gpio_xain.png

To verify the results
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

1. Click the |build_icon| button to build the project. Please ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Reset the power of EVB.
3. Run Tera Term and monitor the board log.
4. When the program is finished building, click the |run_icon|  button to boot up the system via USB to SPI board.
5. 5.	To test the results, press a different button before each XAIN_3 sample.

 .. image:: /_static/teraterm_xain_example_log.png
