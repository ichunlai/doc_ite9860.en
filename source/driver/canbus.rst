.. _canbus:

CanBus Controller
============================

Function description
--------------------

The IT986x series contains two CAN modules. Both CAN modules are compliant with ISO 11898-1:2015 (CAN-specification 2.0B). This document will refer to these two modules as can0 and can1.

The Controller Area Network (CAN) allows multiple microcontrollers or devices on a network to communicate directly with each other without the need for a Host to control the communication. Canbus has a highly scalable, reliable and low-cost feature where devices on the same line must have the same baud rate in order to communicate, and its basic network architecture is shown below.

.. image:: /_static/diagram_canbus.png

Canbus Signals Introduction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

When CANBUS transmits a dominant (0) signal, it raises the CANH terminal to 5V high potential and pulls CANL to 0V low potential. When transmitting a recessive (1) signal, it does not drive the CANH or CANL side, as shown below.

.. image:: /_static/diagram_canbus_signal.png

Canbus frame Introduction
^^^^^^^^^^^^^^^^^^^^^^^^^^^

Canbus communication is based on one frame as a unit, while CAN 2.0B has four frame types.

.. list-table::
  :header-rows: 1

  * - Type
    - Purpose
  * - Data frame
    - The frame of the transferred node data.
  * - Remote frame(RTR)
    - The frame for transferring a specific identifier.
  * - Error frame
    - Frame that is sent by the node that detected the error.
  * - Overload frame
    - Insert a delayed frame between data frames or remote frames.

The data frame is the most user-defined type, and it has two types of information structures:

.. list-table::
  :header-rows: 1

  * - Type (IDE)
    - ID bits supproted
  * - Basic frame format (STD frame)
    - 11
  * - Extended frame format (EXT frame)
    - 29

.. image:: /_static/diagram_canbus_frame_format.png

Basic frame format description(CAN 2.0 standard frame):

  .. list-table::
    :header-rows: 1

    * - Field
      - Bits
      - Description
    * - SOF
      - 1
      - Indicates the start point of the frame transfer
    * - BASE ID
      - 11
      - Identifier (ID), which also has a meaning of transmission priority
    * - RTR
      - 1
      - Remote request frame setting. data frame setting (0), remote request frame setting (1)
    * - IDE
      - 1
      - Basic frame setting (0)
    * - R0
      - 1
      - Reserved space
    * - DLC
      - 4
      - Number of data bytes
    * - Data
      - 0 - 64
      - Data to be transferred (length specified by data length code DLC)
    * - CRC
      - 16
      - CRC error check
    * - ACK
      - 2
      - The sender sends (1) but any receiver can set (0)
    * - EOF
      - 7
      - means the end of the frame must be (1)


Extended frame format (CAN 2.0 extended frame):


  .. list-table::
    :header-rows: 1

    * - Field
      - Bits
      - Description
    * - SOF
      - 1
      - Indicates the start point of the frame transfer
    * - BASE ID
      - 11
      - Identifier (ID), which also has a meaning of transmission priority
    * - SRR
      - 1
      - Alternative remote request, must be (1)
    * - IDE
      - 1
      - Extended frame setting (1)
    * - Extended ID
      - 18
      - The second part of the Identifier (ID), with the meaning of transmission priority, same as the BASE ID
    * - RTR
      - 1
      - Remote request frame setting. data frame setting (0), remote request frame setting (1)
    * - R0
      - 1
      - Reserved space
    * - DLC
      - 4
      - Number of data bytes
    * - Data
      - 0 - 64
      - Data to be transferred (length specified by data length code DLC)
    * - CRC
      - 16
      - CRC error check
    * - ACK
      - 2
      - The sender sends (1) but any receiver can set (0)
    * - EOF
      - 7
      - means the end of the frame must be (1)

DLC(Data Length Code) conversion table:


  .. list-table::
    :header-rows: 1

    * - DLC
      - bytes
    * - 0x0000
      - 0
    * - 0x0001
      - 1
    * - 0x0010
      - 2
    * - 0x0011
      - 3
    * - 0x0100
      - 4
    * - 0x0101
      - 5
    * - 0x0110
      - 6
    * - 0x0111
      - 7
    * - 0x1000
      - 8


Introduction to Canbus Message buffers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The structure diagram of Message buffers is as follows:

.. image:: /_static/diagram_canbus_msg_buffer.png

IT986x supports 16 RBs, and the data format is as follows.

Standard frame:

.. table::

  +--------------+---------------------------------------------------------------+
  | Address      |                       Bit Position                            |
  +--------------+-------+-------+-------+-------+-------+-------+-------+-------+
  |              | 7     | 6     | 5     | 4     | 3     | 2     | 1     | 0     |
  +==============+=======+=======+=======+=======+=======+=======+=======+=======+
  | RBUF         |                       ID(7:0)                                 |
  +--------------+---------------------------------------+-----------------------+
  | RBUF+1       |                   \-                  |        ID(10:8)       |
  +--------------+---------------------------------------+-----------------------+
  | RBUF+2       |                        \-                                     |
  +--------------+-------+-------------------------------+-----------------------+
  | RBUF+3       | ESI   |                         \-                            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | RBUF+4       | IDE=0 | RTR   | EDL   | BRS   |           DLC(3:0)            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | RBUF+5       |         KOER          | TX    |           \-                  |
  +--------------+-----------------------+-------+-------------------------------+
  | RBUF+6       |                       CYCLE_TIMIE(7:0)                        |
  +--------------+---------------------------------------------------------------+
  | RBUF+7       |                      CYCLE_TIMIE(15:8)                        |
  +--------------+---------------------------------------------------------------+
  | RBUF+8       |                          d1(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | RBUF+9       |                          d2(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | RBUF+71      |                          d64(7:0)                             |
  +--------------+---------------------------------------------------------------+
  | RBUF+72      |                          RTS(7:0)                             |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | RBUF+79      |                          RTS(63:56)                           |
  +--------------+---------------------------------------------------------------+

Extended frame:


.. table::

  +--------------+---------------------------------------------------------------+
  | Address      |                       Bit Position                            |
  +--------------+-------+-------+-------+-------+-------+-------+-------+-------+
  |              | 7     | 6     | 5     | 4     | 3     | 2     | 1     | 0     |
  +==============+=======+=======+=======+=======+=======+=======+=======+=======+
  | RBUF         |                       ID(7:0)                                 |
  +--------------+---------------------------------------------------------------+
  | RBUF+1       |                       ID(15:8)                                |
  +--------------+---------------------------------------------------------------+
  | RBUF+2       |                       ID(23:16)                               |
  +--------------+-------+---------------+---------------------------------------+
  | RBUF+3       | ESI   |     \-        |         ID(28:24)                     |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | RBUF+4       | IDE=1 | RTR   | EDL   | BRS   |           DLC(3:0)            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | RBUF+5       |         KOER          | TX    |           \-                  |
  +--------------+-----------------------+-------+-------------------------------+
  | RBUF+6       |                       CYCLE_TIMIE(7:0)                        |
  +--------------+---------------------------------------------------------------+
  | RBUF+7       |                      CYCLE_TIMIE(15:8)                        |
  +--------------+---------------------------------------------------------------+
  | RBUF+8       |                          d1(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | RBUF+9       |                          d2(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | RBUF+71      |                          d64(7:0)                             |
  +--------------+---------------------------------------------------------------+
  | RBUF+72      |                          RTS(7:0)                             |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | RBUF+79      |                          RTS(63:56)                           |
  +--------------+---------------------------------------------------------------+


The IT986x supports two types of TBs: PTB and STB, where PTB has higher priority than STB, and STB can send multiple frames at once, and the transmission order is first-in-first-out, with the following data format.

Standard frame:


.. table::

  +--------------+---------------------------------------------------------------+
  | Address      |                       Bit Position                            |
  +--------------+-------+-------+-------+-------+-------+-------+-------+-------+
  |              | 7     | 6     | 5     | 4     | 3     | 2     | 1     | 0     |
  +==============+=======+=======+=======+=======+=======+=======+=======+=======+
  | TBUF         |                       ID(7:0)                                 |
  +--------------+---------------------------------------+-----------------------+
  | TBUF+1       |                   \-                  |        ID(10:8)       |
  +--------------+---------------------------------------+-----------------------+
  | TBUF+2       |                        \-                                     |
  +--------------+-------+-------------------------------+-----------------------+
  | TBUF+3       | TTSEN |                         \-                            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | TBUF+4       | IDE=0 | RTR   | EDL   | BRS   |           DLC(3:0)            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | TBUF+8       |                          d1(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | TBUF+9       |                          d2(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | TBUF+71      |                          d64(7:0)                             |
  +--------------+---------------------------------------------------------------+


Extended frame:

.. table::

  +--------------+---------------------------------------------------------------+
  | Address      |                       Bit Position                            |
  +--------------+-------+-------+-------+-------+-------+-------+-------+-------+
  |              | 7     | 6     | 5     | 4     | 3     | 2     | 1     | 0     |
  +==============+=======+=======+=======+=======+=======+=======+=======+=======+
  | TBUF         |                       ID(7:0)                                 |
  +--------------+---------------------------------------------------------------+
  | TBUF+1       |                       ID(15:8)                                |
  +--------------+---------------------------------------------------------------+
  | TBUF+2       |                       ID(23:16)                               |
  +--------------+-------+---------------+---------------------------------------+
  | TBUF+3       | TTSEN |     \-        |         ID(28:24)                     |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | TBUF+4       | IDE=1 | RTR   | EDL   | BRS   |           DLC(3:0)            |
  +--------------+-------+-------+-------+-------+-------------------------------+
  | TBUF+8       |                          d1(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | TBUF+9       |                          d2(7:0)                              |
  +--------------+---------------------------------------------------------------+
  | ...          |                          ...                                  |
  +--------------+---------------------------------------------------------------+
  | TBUF+71      |                          d64(7:0)                             |
  +--------------+---------------------------------------------------------------+


Introduction to Canbus Acceptance filtering
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Canbus is similar to broadcasting in the process of sending data, so when there are many devices, a single device will receive all kinds of data from all devices, but in fact, it only needs to receive the data related to it. Therefore, ID filter is needed at the receiving end to filter the data, which has the advantage of reducing the burden of the device to process the data. The following figure is a schematic diagram of the use of the filter.

.. image:: /_static/diagram_canbus_filter.png

.. list-table::
  :header-rows: 1

  * - Control field
    - Description
  * - AE_x
    - Filter enable. When AE_x is set to 1, this group filter enable will be turned on, and the following fields will be effective. There are 16 groups in IT986x series, as long as one of them is accepted, then the data will be accepted.
  * - | ACODE_x
      | AMASK_x
    - | ACODE & AMASK are used together. ACODE can specify a restricted ID, and only the ID is the same as ACODE will be accepted by HW. AMASK can achieve the masking effect, for example:
      |
      | Example1:
      |     Acode = 0x000000011
      |     Amask = 0x000000000
      | This group means that ID 0x11 can be accepted
      |
      | Example2:
      |     Acode = 0x000000011
      |     Amask = 0x00000000F
      | This group means that ID 0x10 ~0x1F can be accepted
      |
      | Example3:
      |     Acode = 0x000000000
      |     Amask = 0x1FFFFFFF
      | This group means that all IDs are accepted
  * - AIDEE
    - | AIDEE
      | 0 - accept both standard or extended frame
      | 1 - defined by AIDE
  * - AIDE
    - | AIDE
      | 0 - accept only standard frames
      | 1 - only extended frames

Canbus Debug mode introduction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

IT986x supports two Loop Back Modes: internal (LBMI) and external (LBME). Both modes result in reception of the own t ransmitted frame which can be useful for self tests.

In LBMI is disconnected from the CAN bus and the txd output is set to recessive. The output data stream is internally fedback to the input.

In LBME stays connected to the transceiver and a transmitted frame will be visible on the bus. Therefore in LBME with SACK=0 there are two possible results upon a frame transmission:


.. list-table::

  * - Success
    - Another node receives the frame too and generates an ACK. This will result in a successful transmission and reception.
  * - Fail
    - No other node is connected to the bus and this results in an ACK error.

.. image:: /_static/diagram_canbus_lbmi_lbme.png

Canbus Interrupt
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
It is recommended to enable 2, 3, 5, 7, and other interrupt types to monitor the status can be enabled depending on the user's needs.

1. Error_INT(Error interrupt)

  - To monitor Error Count & Bus off to see if the status changes.

    Errors during reception / transmission are counted by RECNT and TECNT. A programmable error warning limit EWL, located in register LIMIT, can be used by the host controller for flexible reaction on those events. The limit values can be chosen in steps of 8 errors from 8 to 128:

    Limit of count of errors = (EWL + 1) * 8

    The interrupt EIF will be set if enabled by EIE under the following conditions:

      - the border of the error warning limit has been crossed in either direction by RECNT or TECNT or
      - the BUSOFF bit has been changed in either direction.

    .. note::

       EWL default value= 0xB

2. TP_INT(PTB Transmission interrupt)

  - Triggers an interrupt after the user calls ithCANWrite and the CAN controller completes the transmission.

3. TS_INT(STB Transmission interrupt)

  - Triggers an interrupt after the user calls ithCANFIFOWrite and the CAN controller completes the transmission.

4. RB_Almost_Full_INT(RB Almost Full interrupt)

  - To monitor the current status of the Recvice Buffer.

    - RB Almost Full Interrupt Flag = 1

      When the number of filled buffers >= AFWL

    - RB Almost Full Interrupt Flag = 0

      When the number of filled buffers < AFWL


    .. note::

      receive buffer Almost Full Warning Limit

      AFWL defines the internal warning limit AFWL_i with :math:`n_{RB}` being the number of available RB slots.

      .. math::
        AFWL_i =
        \begin{cases}
        AFWL & \text{if } n_{RB} < 16 \\
        2 \cdot AFWL & \text{if } 16 \leq n_{RB} < 32 \\
        4 \cdot AFWL & \text{if } 32 \leq n_{RB} < 64 \\
        \ \vdots & \ \vdots \\
        \end{cases}

      - AFWL_i is compared to the number of filled RB slots and triggers RAFIF if equal. The valid range of :math:`AFWL\_i=[1...n_{RB}]`.

      - :math:`AFWL = 0` is meaningless and automatically treated as 0x1.
        (Note that AFML is meant in this rule not AFML_i.)
      - :math:`AWFL\_i > n_{RB}` is meaningless and automatically treated as :math:`n_{RB}`.
      - :math:`AWFL\_i = n_{RB}` is a valid value, but note that RFIF also exists.

    .. note::

       AFWL default value = 0x1

5. RB_Full_INT(RB Full Interrupt)

  - To monitor whether the Recvice Buffer is full or not.

6. RB_Overrun_INT(RB Overrun Interrupt Flag)

  - To monitor whether the Recvice Buffer has been overwritten or not.

    - RB Overrun Interrupt Flag = 1

      At least one received message has been overwritten in the Recvice Buffer

    - RB Overrun Interrupt Flag = 0

      No Recvice Buffer overwritten

7. Receive_INT(Receive interrupt)

  - To let the user know whether the current Recvice Buffer contains data or not.

8. Bus_Error_INT(BUS Error interrupt)

  - To inform the user that the current CAN controller has detected an error.

9. Arbitration_Lost_INT(Arbitration_Lost interrupt)

  - To let the user know if an Arbitration Lost has occurred. Arbitration Lost occurs when the CAN bus is busy processing higher priority messages.

10. Error_Passive_INT(Error Passive interrupt)

  - To monitor the current status of Node Error Status

    - Error Passive Interrupt Flag = 1

      Error status changes from error active to error passive or vice versa and if this interrupt in enabled.

    - Error Passive Interrupt Flag = 0

      No changes

11. Abort Interrupt Flag

  - This interrupt is mainly for user to confirm whether the force stop transmission command is completed or not.

    This interrupt has no switch, and will only be triggered when the following APIs are called.

    .. code-block:: c

      ithCANSecondarySendCtrl(base, false);  // Force to stop STB transmission
      ithCANPrimarySendCtrl(base, false);    // Force to stop PTB transmission


Related KConfig settings
----------------------------------------------------------------

- :menuselection:`Peripheral --> Canbus Enable`

  To determine if a Canbus device should be enabled. This option must be enabled for any device between Canbus 0 and 1, otherwise the associated API will not be available.



Related source code files
----------------------------

Code files related to Canbus are provided in library.

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - ``<sdk_root>/sdk/include/can_bus/it9860/can_api.h``
    - The functions to be called by the upper level AP are listed here.
  * - ``<sdk_root>/sdk/include/can_bus/it9860/can_hw.h``
    - Underlying control functions.
  * - ``<sdk_root>/sdk/include/can_bus/it9860/can_reg.h``
    - Definition of hardware registers.


Description of related API
---------------------------------------

ithCANSetGPIO
^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCANSetGPIO(uint32_t Instance, uint32_t rxpin, uint32_t txpin)

**Parameters**

``uint32_t instance``

  Canbus device (0) or Canbus device 1 (1)


``uint32_t rxpin``

  Set RX pin by target board

``uint32_t txpin``

  Set TX pin by target board


**Description**

Set the PIN Number of GPIO_RX and GPIO_TX used by CAN Controller, and switch the GPIO pin mux to Canbus function for the signal to come in and out properly.

ithCANOpen
^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCANOpen(CAN_HANDLE* base, void* rx_callback, void* tx_callback)

**Parameters**

``CAN_HANDLE* Base``

  .. list-table::
    :header-rows: 1
    :class: longtable

    * - CAN_HANDLE struct
      - Option
    * - Instance
      - | 0 (Canbus device 0)
        | 1 (Canbus device 1)
    * - ADDR
      - | CAN0_BASE_ADDRESS
        | CAN1_BASE_ADDRESS
    * - BaudRate
      - | CAN_USER_DEFINED
        | CAN_20K
        | CAN_50K
        | CAN_100K
        | CAN_125K_500K
        | CAN_125K_833K
        | CAN_250K_1M
        | CAN_250K_1M5
        | CAN_250K_2M
        | CAN_250K_3M
        | CAN_250K_4M
        | CAN_500K_1M
        | CAN_500K_2M
        | CAN_500K_3M
        | CAN_500K_4M
        | CAN_500K_5M
        | CAN_1000K_4M
    * - SourceClock
      - | CAN_SRCCLK_40M
        | CAN_SRCCLK_60M
        | CAN_SRCCLK_80M
    * - ProtocolType
      - protocol_CAN_2_0B
    * - ExternalLoopBackMode
      - | 0 - off
        | 1 - on
        | IT9860 supports two Loop Back Modes: internal and external. Both modes result in reception of the own transmitted frame which can be useful for self-tests. In external mode, IT9860 stays connected to the transceiver and a transmitted frame will be visible on the bus.
    * - InternalLoopBackMode
      - | 0 - off
        | 1 - on
        | In internal mode, IT9860 is disconnected from the CAN bus and the tx output is set to recessive. The output data stream is internally feedback to the input.
    * - ListenOnlyMode
      - | 0 - off
        | 1 - on
        | LOM provides the ability of monitoring the CAN bus without any influence to the bus.
    * - Tptr
      - Filter table pointer.
    * - SlowBitRate
      - If BaudRate is CAN_USER_DEFINED, user can set bit rate on here.
    * - Interrupt table
      - Enable interrupt or not.
    * - InterruptHD
      - Interrupt callback function.

  The Source Clock is set according to the RAM script selected by the Project, and the corresponding list is as follows:

  .. list-table::
    :header-rows: 1

    * - Script
      - Source CLK
    * - IT9860_264Mhz_DDR2_264Mhz
      - 60Mhz
    * - IT9860_324Mhz_DDR2_324Mhz
      - 60Mhz
    * - IT9860_360Mhz_DDR2_360Mhz
      - 40Mhz
    * - IT9860_396Mhz_DDR2_396Mhz
      - 60Mhz
    * - IT9860_720Mhz_DDR2_396Mhz
      - 80Mhz
    * - IT9860_780Mhz_DDR2_396Mhz
      - 60Mhz
    * - IT9860_396Mhz_DDR3_792Mhz
      - 60Mhz
    * - IT9860_780Mhz_DDR3_792Mhz
      - 60Mhz
    * - IT9860_792Mhz_DDR2_396Mhz
      - No support Can
    * - IT9860_792Mhz_DDR3_792Mhz
      - No support Can


``void *rx_callback``

  When controller receive a frame, this function will be called.

``void *tx_callback``

  When controller send frame finished, this function will be called.

**Description**

  According to the user input parameters, initialize the Canbus Controller, and CAN0 and CAN1 are completely independent. Source Clock & Baud Rate are the most important and must be set correctly to work properly.


ithCANRead
^^^^^^^^^^^^^^^^^^^

.. c:function:: int ithCANRead(CAN_HANDLE* base, CAN_RXOBJ* info)

**Parameters**

``CAN_HANDLE* Base``

  See ithCANOpen for details.

``CAN_RXOBJ* info``

  .. list-table::
    :header-rows: 1

    * - CAN_RXOBJ struct
      - Option
    * - Identifier
      - ID
    * - Control
      - | - DLC (Data len code)
        | - RTR (Remote Transmission Request)
        | - IDE (0 - STD format, 1 - EXT format)
    * - RXData[DLC_MAX]
      - receive buffers, size = 8
    * - RXRTS[2]
      - timestamp

**Description**

This function reads the data of a frame out of the hardware's SRAM, returns it via info, and tells the hardware that the memory block has been read before the hardware write the newly received frame again.


ithCANWrite
^^^^^^^^^^^^^^^^^^^

.. c:function:: int ithCANWrite(CAN_HANDLE *base, CAN_TXOBJ *info, uint8_t *dataptr)

**Parameters**

``CAN_HANDLE* Base``

  See ithCANOpen for details.

``CAN_TXOBJ* info``

  .. list-table::
    :header-rows: 1

    * - CAN_TXOBJ struct
      - Option
    * - Identifier
      - ID
    * - Control
      - | - DLC (Data len code)
        | - RTR (Remote Transmission Request)
        | - IDE (0 - STD format, 1 - EXT format)
    * - SingleShot
      - | 0 - off
        | 1 - on
    * - TTSENSEL
      - | 0 - off (disable enable CiA 603 time stamping)
        | 1 - on (enable CiA 603 time stamping)

``uint8_t *dataptr``

  data array pointer.

**Description**

A frame is sent to the network according to a user-defined format, the length of which is determined by the DLC, and the data array must be larger than or equal to the size defined by the DLC.

ithCANFIFOUpdate
^^^^^^^^^^^^^^^^^^^

.. c:function:: int ithCANFIFOUpdate(CAN_HANDLE *base, CAN_TXOBJ *info, uint8_t *dataptr)

**Parameters**

``CAN_HANDLE* Base``

  See ithCANOpen for details.

``CAN_TXOBJ* info``

  See ithCANWrite for details.

``uint8_t *dataptr``

  data array pointer.

**Return**

0 means update success, and 1 means fifo full.


**Description**

Allowing users to write data to STB buffers.


ithCANFIFOWrite
^^^^^^^^^^^^^^^^^^^

.. c:function:: int ithCANFIFOWrite(CAN_HANDLE *base, CAN_TXOBJ *info)


``CAN_HANDLE* Base``

  See ithCANOpen for details.

``CAN_TXOBJ* info``

  See ithCANWrite for details.


**Description**

Allowing users to send the data in STB buffers to the network.



ithCANClose
^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCANClose(CAN_HANDLE *base)

**Parameters**

``CAN_HANDLE* Base``

  See ithCANOpen for details.

**Description**

  To close the Controller. Open it again if you want to use it.


ithCANDlcToBytes
^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t ithCANDlcToBytes(CAN_DLC dlc)

**Parameters**

CAN_DLC dlc

  Indication of the number of transmitted data specified by Canbus 2.0B

**Description**

Function for converting DLC to Bytes. It is convenient for users to get the correct number of data bytes.


**Return**

Number of bytes converted from DLCs

ithCANGetTTS
^^^^^^^^^^^^^^^^^^^

.. c:function:: int ithCANGetTTS(CAN_HANDLE *base)

**Parameters**

``CAN_HANDLE* Base``

  See ithCANOpen for details.

**Description**

Provides the user with a confirmation of the last frame delivery completion time.

**Return**

The timestamp of the completion of the last frame transmission.




ithCANReset
^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithCANReset(CAN_HANDLE *base)

**Parameters**

``CAN_HANDLE* Base``

  See ithCANOpen for details.

**Description**

Controller sw reset.


ithCANGetErrorState
^^^^^^^^^^^^^^^^^^^

.. c:function:: CAN_ERROR_STATE ithCANGetErrorState(CAN_HANDLE *base)

**Parameters**

``CAN_HANDLE* Base``

  See ithCANOpen for details.


**Description**

To obtain Controller error status.



**Return**

- 0x00: ERROR ACTIVE
- 0x01: ERROR PASSIVE
- 0x02: BUS OFF



ithCANGetREC
^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t ithCANGetREC(CAN_HANDLE *base)

**Parameters**

``CAN_HANDLE* Base``

  See ithCANOpen for details.

**Description**

Allowing users to obtain the counts of error receiving frames.



**Return**

0 ~ 255

ithCANGetTEC
^^^^^^^^^^^^^^^^^^^

.. c:function:: uint32_t ithCANGetTEC(CAN_HANDLE *base)


**Parameters**

``CAN_HANDLE* Base``

  See ithCANOpen for details.

**Description**

Allowing users to obtain the counts of error receiving frames.



**Return**

0 ~ 255


ithCANGetKOER
^^^^^^^^^^^^^^^^^^^

.. c:function:: CAN_ERROR_TYPE ithCANGetKOER(CAN_HANDLE *base)

**Parameters**

``CAN_HANDLE* Base``

  See ithCANOpen for details.


**Description**

Allowing users to obtain the latest error types





**Return**

- 0x0: CAN_NO_ERROR
- 0x1: CAN_BIT_ERROR
- 0x2: CAN_FORM_ERROR
- 0x3: CAN_STUFF_ERROR
- 0x4: CAN_ACKNOWLEDGEMENT_ERROR
- 0x5: CAN_CRC_ERROR
- 0x6: CAN_OTHER_ERROR


Complete example
--------------------------------------------

Hardware connection
^^^^^^^^^^^^^^^^^^^^^^^^^^^

1.	Connect the DB_CANBUS daughter board to the Standard EVB according to the above picture.

  .. image:: /_static/e_circuit_canbus.png

  .. list-table::
    :header-rows: 1

    * - DB_CANBUS
      - IT9866 Standard EVB
    * - TXD
      - GPIO 50
    * - RXD
      - GPIO 51
    * - Enable
      - GPIO 52
    * - 5V
      - EVB 5V
    * - 3.3V
      - EVB 3.3V
    * - GND
      - EVB GND

2. The reference picture of the completed connection

  .. image:: /_static/e_circuit_canbus_connect.png

3. CAN H & CAN L of DB_CANBUS daughter board connects to the network

  .. attention::

    The network should have a terminal resistor of 120ohm

  .. image:: /_static/diagram_canbus_connect.png


Software
^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. Open file:`<sdk_root>/project/test_canbus/test_canbus9860.c`
2. set baud rate (Note: must be the same as other devices on the network)
3. Control the define option and select the function you want to test.

   - RX_DEBUG_PRINTF

     To show information received by CAN.

     .. note::

        ithPrintf is time consuming so it is recommended to be enabled only when DEBUG

   - TX_PTB_TEST

     To send data with PTB for testing.

   - TX_STB_TSET

     To send data with STB for testing.

   .. code-block:: c
      :linenos:
      :emphasize-lines: 7-9

      #include <stdio.h>
      #include <malloc.h>
      #include "ite/itp.h"    //for all ith driver
      #include "ite/ith.h"
      #include "can_bus/it9860/can_api.h"

      #define  RX_DEBUG_PRINTF 0
      #define  TX_PTB_TEST     1
      #define  TX_STB_TSET     0

4. Please run :file:`<sdk_root>/build/openrtos/test_canbus.cmd`.

5. Click |build_icon| to build.

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

To verify the results
^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. After the build is complete, please confirm that the following have been completed:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Reset the power of EVB.
3. Run Tera Term or any Serial Port software.
4. Click the |run_icon| button to boot up the system via USB to SPI board.


5. When the Serial Port software displays the following message, you can use other devices to send signals to the IT986x.

   .. code-block:: shell

      booting time: 0ms
      CLK: cpu=396000000 hz,mem=396000000 hz,bus=975000000 hz, risc:195000000 hz
      test 9860 canbus 2.0B!
      [MSG]CAN [S]Prescaler = 2, Bit_Time = 40, Seg1 = 30, Seg2 = 7, SJW = 7


6. If "Invalid Chip" is displayed, it means the Chip part number is wrong; please replace it with the correct one.

   .. code-block:: shell

      booting time: 0ms
      CLK: cpu=396000000 hz,mem=396000000 hz,bus=975000000 hz, risc:195000000 hz
      test 9860 canbus 2.0B!
      [MSG]CAN [S]Prescaler = 2, Bit_Time = 40, Seg1 = 30, Seg2 = 7, SJW = 7
      [MSG]Invalid CHIP!!!





.. raw:: latex

    \newpage
