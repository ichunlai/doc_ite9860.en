.. _norflash:

NOR Flash
================

Function description
---------------------------

SPI NOR flash is one of the peripheral storage devices of IT986x, mainly used for storing bootloader, Image and FAT file system.The communication protocol of the physical layer uses SPI, and the user can decide which SPI module is the transmission medium according to the KConfig, and the underlying layer NOR flash driver accesses the NOR flash by calling the SPI driver; please refer to section :ref:`spi` for the introduction of SPI. In addition, IT986x supports high-speed AXISPI and low-speed SPI (SPI0 and SPI1), and SPI NOR flash can be connected to either AXISPI or SPI, but only AXISPI supports NOR booting. Because of the different specifications of NOR flash, before using, it is necessary to check whether the electrical characteristics of AC/DC CHARACTERISTICS match and whether the NOR CMD is consistent with the definition of Mask ROM and NOR flash drive. For NOR devices verified on IT986x, please refer to :ref:`norflash_support_list`. The related SPI modules and storage partitions are described below.

- AXISPI

  Higher speed SPI controller with SPI clock up to 66 Mhz to 80 Mhz and support QUAD operation mode. The maximum theoretical transfer rate is (80 Mhz / 8) * 4 = 40 MB/s, and the IT986x only supports AXISPI NOR booting.

- SPI

  For the lower speed SPI controller, the IT986x provides two SPI controllers (SPI0 and SPI1) with SPI clock up to 20 Mhz, but only supports Single operation mode. The maximum theoretical transfer rate is (20 Mhz / 8) = 2.5 MB/s, and the SPI controllers do not support NOR booting.

- Reserved Area

  This area is mainly used to store the bootloader, Image, backup-pkg, MAC address, and user-defined data.

- Disk Partition

  This area is controlled by the file system.

Related KConfig settings
----------------------------------------------------------------

- :menuselection:`Storage --> NOR Device`

  To determine if the SPI NOR flash device should be enabled.

- :menuselection:`Storage --> NOR Device --> NOR device use axispi`

  To select the SPI module to be used for NOR. Checking the box means AXISPI is selected, while the opposite means SPI0 and SPI1 are selected. For details of SPI0 and SPI1 settings, please refer to :ref:`spi`.

- :menuselection:`Storage --> NOR Device --> NOR device use axispi --> NOR enable QUADMODE`

  If the SPI NOR flash supports QUAD mode, you can check it to enable AXISPI QUAD mode, and the opposite means AXISPI Single mode is selected. the AXISPI divider setting for Single and QUAD mode is 2.

- :menuselection:`Storage --> NOR Device --> NOR device use axispi --> NOR enable QUADMODE --> NOR enable DTRMODE`

  If the SPI NOR flash supports DTR mode, you can check to enable AXISPI DTR mode, and otherwise it means AXISPI QUAD mode is selected. the AXISPI divider setting for DTR mode is 4.

- :menuselection:`Peripheral --> AXISPI_Enable`

  To determine if the AXISPI device should be enabled.

- :menuselection:`GPIO --> AXISPI#`

  To set up the AXISPI GPIO. In the current HW design, there is a specific PIN number, and the selectable PIN number can be found in the notes of the AXISPI pin.

Related source code files
----------------------------------------------------------------

.. list-table::
   :header-rows: 1

   * - Path
     - Description
   * - ``<sdk_root>/sdk/driver/itp/itp_nor.c``
     - The uppermost layer function. It mainly provides POSIX compliant API for the upper-layer AP to operate the device through ``open()``, ``lseek()``, ``read()``, ``write()``, ``close()``, ``ioctl()``  and other functions.

   * - | ``<sdk_root>/sdk/include/nor``
       | ``<sdk_root>/sdk/driver/nor``
     - Underlying function with SPI NOR flash driver.

Description of related API
---------------------------------------

This section describes the APIs for operating SPI NOR flash devices. NOR flash driver provides a POSIX compliant API that allows users to perform operations like reading and writing on NOR devices via the ``open()``/``read()``/``write()``/``ioctl()`` functions.

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device);

  To register the device.

**Parameters**

``ITPDeviceType type``

  Device type. For a full list of device types, see :file:`<sdk_root>/sdk/include/ite/itp.h`. The device type associated with the NOR device is ``ITP_DEVICE_NOR``.

``const ITPDevice *device``

  Device Identifier. The device identifier associated with the NOR device is ``itpDeviceNor``.

**Description**

This function can be used to register the NOR flash device to the core device list, so that the upper layer AP can operate the NOR functionality through ``ioctl()``/``read()``/``write()`` functions. This function will be called by the ``itpInit()`` function during system initialization (see :file:`<sdk_root>/sdk/driver/itp_init_openrtos.c`), so if ``itpInit()`` has been called in your project, you do not need to run this registration function again. 

ioctl
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr);

  To control the device.

**Parameters**

``int file``

  The device identifier. Its value is the value in the ``ITPDeviceType`` list. The device identifier associated with a NOR device is ``ITP_DEVICE_NOR``.

``unsigned long request``

  The *request* parameter is used to select the function to be run on the device and will depend on the specified device.

``void *ptr``

  The *ptr* parameter represents the additional information required to perform the requested function on this particular device. The type of *ptr* depends on the particular control request, but it can be an integer or a pointer to a specific data structure of the device.

**Description**

The ``ioctl()`` functions used on NOR devices are currently INIT, GET_BLOCK, GET_GAP, and FLUSH.

- ITP_IOCTL_INIT

  To initialize the NOR device. The following shows how to initialize the NOR device with the default settings.

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_INIT, (void *)0);

- ITP_IOCTL_GET_BLOCK_SIZE

  To obtain the block size of the NOR device, and the following shows how to use the API.

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize);

- ITP_IOCTL_GET_GAP_SIZE

  To obtain the Gap size of the NOR device, and the following shows how to use the API.

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_GET_GAP_SIZE, &gapsize);

- ITP_IOCTL_FLUSH

  If the FAT NOR cache is turned on, you can write the modification data of the cache to NOR through this API.

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_FLUSH, NULL);

read
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int read(int file, char *ptr, int len);

  To read data from RX.

**Parameters**

``int file``

  NOR ports to be executed. E.g. ``ITP_DEVICE_NOR``.

``char *ptr``

  Memory address to be written after data is read from NOR.

``int len``

  The length read by NOR in Block.

**Description**

This function allows users to read back data from the NOR port and the following shows how to use the function.

.. code-block:: c

    // initialize NOR, the method below could be found in itpInit();
    // Register NOR device
    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);

    // Do initialization
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_INIT, NULL);

    // Do NOR Read operation
    fd = open(":nor", O_RDONLY);
    ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize);
    temp = malloc(blocksize);
    read(fd, temp, 1);
    close(fd);
    free(temp);

write
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int write(int file, char*ptr, int len);

  To send data from TX.

**Parameters**

``int file``

  NOR ports to be executed. E.g. ``ITP_DEVICE_NOR``.

``char *ptr``

  The memory address to be read before writing to NOR.

``int len``

  The length to write to NOR in blocks.

**Description**

This function allows users to write data through NOR ports and the following shows how to use the function.

.. code-block:: c

    // initialize NOR, the method below could be found in itpInit();
    // Register NOR device
    itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);

    // Do initialization
    ioctl(ITP_DEVICE_NOR, ITP_IOCTL_INIT, NULL);

    // Do NOR Write operation
    fd = open(":nor", O_WRONLY);
    ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize);
    temp = malloc(blocksize);
    write(fd, temp, 1);
    close(fd);
    free(temp);

Basic example program
---------------------------------------

.. code-block:: c

    #include "ite/itp.h"

    void main(void)
    {
        int fd = 0;
        uint32_t blocksize = 0;
        uint8_t *temp = 0;

        itpRegisterDevice(ITP_DEVICE_NOR, &itpDeviceNor);
        ioctl(ITP_DEVICE_NOR, ITP_IOCTL_INIT, NULL);

        printf("Start NOR test!\n");

        fd = open(":nor", O_RDONLY);
        if (!fd) printf("--- open device NOR fail ---\n");
        else printf("fd = %d\n", fd);

        if (ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize))
        printf("get block size error\n");

        temp = malloc(blocksize);

        while (1)
        {
            read(fd, temp, 1);
            printf("NorInfo 0x%x, 0x%x, 0x%x\n", temp[0], temp[1], temp[2]);
        }

        close(fd);
        free(temp);
    }

Complete example
---------------------------------------

A verification program for SPI NOR flash is attached to the :file:`<sdk_root>/project/test_nor` directory. The program shows three examples, including NOR full read/write check via AXISPI, NOR full read/write check via SPI, and test on SPI Slave's simple protocol.

Hardware Connection
^^^^^^^^^^^^^^^^^^^^^

Below shows the connection between IT986x and SPI NOR flash, and also the connection between SPI0(Master) and SPI1(Slave) on IT986x.

- The connection of NOR on AXISPI

  .. image:: /_static/e_circuit_norflash_axispi.png

- The connection of NOR on SPI

  .. image:: /_static/e_circuit_norflash_spi.png

- The connection between SPI0(Master) and SPI1(Slave)

  .. image:: /_static/e_circuit_spi_master_spi_slave.png

KConfig settings
^^^^^^^^^^^^^^^^^^^^^

Please run :file:`<sdk_root>/build/openrtos/test_nor.cmd` and follow the steps below to set up.

- Verification of NOR on AXISPI

  .. image:: /_static/qconf_storage_nor.png

  .. image:: /_static/qconf_peripheral_axispi.png

  .. image:: /_static/qconf_gpio_axispi.png

- Verification of NOR on SPI

  .. image:: /_static/qconf_storage_spi_nor.png

  .. image:: /_static/qconf_peripheral_spi.png

  .. image:: /_static/qconf_gpio_spi_nor.png

- Verification of SPI0(Master) and SPI1(Slave)

  .. image:: /_static/qconf_storage_spi_nor.png

  .. image:: /_static/qconf_peripheral_spi.png

  .. image:: /_static/qconf_gpio_spi_master_spi_slave.png

  .. image:: /_static/qconf_spi_slave_test_spi_bist.png

To verify the results
^^^^^^^^^^^^^^^^^^^^^

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

1. Click the |build_icon| button to build the project. Please ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Reset the power of EVB.
3. Run Tera Term and monitor the board log.
4. When the program is finished building, click the |run_icon|  button to boot up the system via USB to SPI board.
5. Verification results of NOR on AXISPI.

  .. image:: /_static/teraterm_nor_axispi.png

6. Verification results of NOR on SPI

  .. image:: /_static/teraterm_nor_spi.png

7. Verification results of SPI0(Master) and SPI1(Slave)

  .. image:: /_static/teraterm_spi_master_spi_slave.png



.. raw:: latex

    \newpage
