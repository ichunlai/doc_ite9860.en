.. _remote_ir:

Remote IR
==============

Function description
--------------------

Remote IR (Infrared) is a remote infrared control technology with an effective transmission distance of 8 meters, a transmission rate of 2MBps, and a transmitting angle of 120 degrees, which can be used in desktop computers, notebooks, digital cameras, mobile phones, and other products.

At present, there are many kinds of infrared remote control protocols, such as NEC, Phlips RC5, RC6, RC-MM, Toshiba, Sharp, JVC, Sony SIRC, etc., but NEC is the majority. The NEC protocol is used in the test_ir example. For more information about the NEC protocol, please refer to `the website <http://coopermaa2nd.blogspot.com/2010/01/nec-ir-protocol.html>`_.

.. image:: /_static/_remote_ir_protocol.png

IT986x supports up to 4 sets of IR (IR0 ~ 3) and 3 operation modes (Interrupt, DMA, FIFO).

During initialization, users can change the sampling conditions by modifying the prescale value written to the ``ITH_IR_RX_PRESCALE_REG`` registers. Here is the prescale formula:

.. math::

  prescale = sampleRate * busClk / precision - 1;

And

- **precision** can be adjusted according to user's requirements. It is meant to adjust the sampling time.
- **sampleRate** can be adjusted according to user's requirement, and is meant to sample once for certain clocks.

The current Default setting is sampleRate = 10 and precision = 1000000, which means to sample once every 10us.

Starting from it986x, the IR frequency detection function is added. The operation principle of this function is that when the IR signal is reversed, it records the number of clock cycles from the last time to this point, and deduces the frequency received by this Rx with the clock frequency.

Related KConfig settings
----------------------------------------------------------------

- :menuselection:`Peripheral --> IrDA Enable`

  To determine if the IrDA device should be enabled. This option must be enabled for any device between IR0 and 3 to be enabled, otherwise the detailed setup options for the individual IrDA device will not appear.

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable`

  To determine if the IrDA# device should be enabled. When this option is checked, the system will register the corresponding IrDA# device via the ``itpRegisterDevice()`` function in the ``itpInit()`` function and perform the initialization.

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable --> IR# mode`

  To determine the operation mode of the IrDA# device.There are FIFO, Interrupt and DMA modes.

  - FIFO mode: Also known as Polling mode. The IrDA driver running in this mode sends or receives data by continuously polling the device's status. It is usually used when the sending and receiving speed is not fast. When sending data in the case of interrupt shutdown, only this mode can be used.
  - Interrupt mode: When the IrDA module is used as a receiver, it can receive an interrupt every time it receives a character of data or if there is an error in the received data. As a transmitter, it can receive an interrupt notification when the IrDA FIFO is empty. Usually used in case of medium transmission speed.
  - DMA mode: When the IrDA module is used as a receiver, an interrupt can be triggered only when a certain amount of data is received. When used as a transmitter, it is similar to Interrupt mode, but can receive an interrupt only after sending a larger amount of data. Usually used when high transmission speed is required.

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable --> IR# Rx Modulation Enable`

  When Rx receive IR wave with carrier, then you need to check this option. Currently Default filter range is 20k~60k.

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable --> IR# Rx sample Enable`

  Through the sampling function, you can get the highest, lowest, average and latest frequencies of the current Rx signal (available only after it9860).

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable --> IR# Tx Modulation Enable`

  When the Tx transmits IR wave with carrier, then this function needs to be checked. Currently the Default filter range is 39k.

- :menuselection:`Peripheral --> IrDA Enable --> IR# Enable --> IR# protocol`

  Communication code selection. Default is the common NEC protocol.

- :menuselection:`GPIO --> IR# TX Pin`

  To specify which GPIO pin to use as TX pin for this IrDA module.

- :menuselection:`GPIO --> IR# RX Pin`

  To specify which GPIO pin to use as RX pin for this IrDA module.

Related source code files
----------------------------------------------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - ``<sdk_root>/sdk/driver/itp/itp_ir.c``
    - The uppermost layer function. It mainly provides POSIX compliant API for the upper-layer AP to operate the device through ``open()``, ``close()``, ``ioctl()`` and other functions.
  * - | ``<sdk_root>/sdk/driver/irda/{chip}/irda.c``
      | ``<sdk_root>/sdk/include/irda/irda.h``
      | ``<sdk_root>/sdk/chip/{chip}/include/ith/ith_ir.h``
      | ``<sdk_root>/sdk/driver/ith/ith_ir.c``
    - The underlying function, which may change in the future.


Description of related API
---------------------------------------

This section describes the APIs used to operate IrDA devices. IrDA driver provides a POSIX compliant API that allows users to perform operations like reading and writing on IrDA devices by using ``open()``/``read()``/``write()``/``ioctl()`` functions.

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  To register a device.

**Parameters**

``ITPDeviceType type``

  Device type. For a complete list of device types, see :file:`<sdk_root>/sdk/include/ite/itp.h`. The device types associated with IrDA devices are ``ITP_DEVICE_IR0`` ~ ``ITP_DEVICE_IR3``.

``const ITPDevice *device``

  Device Identifier. The device identifiers associated with IR devices are ``itpDeviceIr0`` ~ ``itpDeviceIr3``.

**Description**

This function can be used to register the IrDA# device to the core device list, so that the upper layer AP can operate the IrDA functionality through ``ioctl()``/``read()``/``write()`` functions. This function will be called by the ``itpInit()`` function during system initialization (see :file:`<sdk_root>/sdk/driver/itp_init_openrtos.c`), so if ``itpInit()`` has been called in your project before, there is no need to run this registration function again.


ioctl
^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  To control a device

**Parameters**

``int file``

  Device identifier. Its value is the value in the ``ITPDeviceType`` list. The device identifiers associated with IrDA devices are ``ITP_DEVICE_IR0`` ~ ``ITP_DEVICE_IR3``.

``unsigned long request``

  *request* parameter is used to select the function to be run on the device and will depend on the specified device.

``void *ptr``

  The *ptr* parameter represents the additional information required to perform the requested function on this particular device. The type of *ptr* depends on the particular control request, but it can be an integer or a pointer to a specific data structure of the device.

**Description**

Only INIT is currently available for the ``ioct()`` function on IrDA devices.

- ITP_IOCTL_INIT

  It is used to initialize the IrDA device, and the relevant parameters are set in the Kconfig setting before the build.

  The following shows how to initialize the IR0 device through the default settings.

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_IR0, &itpDeviceIr0);
    ioctl(ITP_DEVICE_IR0, ITP_IOCTL_INIT, (void *)0);


read
^^^^^^^^^^

.. c:function:: int read(int file, char *ptr, int len)

  To reads data from the RX.

**Parameters**

``int file``

  IR ports to be executed. E.g. ``ITP_DEVICE_IR0``.

``char *ptr``

  The location to write back after reading the data.

``int len``

  Reserved for development.

**Description**

Users can use this function to read back data from a specific IR port.

The following shows how to use the function.

.. code-block:: c

    // initialize IrDA, the method below could be found in itpInit();
    ITPKeypadEvent ev;
    // Register device Remote control
    itpRegisterDevice(TEST_PORT, &TEST_DEVICE);

    // Do initialization
    ioctl(TEST_PORT, ITP_IOCTL_INIT, (void *) 0);
    // initialize IrDA end.
    for (;;)
    {
    	if (read(TEST_PORT, &ev, sizeof (ITPKeypadEvent)) == sizeof (ITPKeypadEvent))
    		printf("key: time=%lld.%ld,code=%d,down=%d,up=%d,repeat=%d,flags=0x%X/r\n",
    		ev.time.tv_sec,
    		ev.time.tv_usec / 1000,
    		ev.code,
    		(ev.flags & ITP_KEYPAD_DOWN) ? 1 : 0,
    		(ev.flags & ITP_KEYPAD_UP) ? 1 : 0,
    		(ev.flags & ITP_KEYPAD_REPEAT) ? 1 : 0,
    		ev.flags);
    	usleep(33000);
    }

The underlying layer will send the received IR code to the IR's own RxQueue, and the following function will retrieve the value from the RxQueue.

write
^^^^^^^^^^^

.. c:function:: int write(int file, char*ptr, int len)

  To send data from TX.

**Parameters**

``int file``

  IR ports to be executed. E.g. ``ITP_DEVICE_IR0``.

``char *ptr``

  Location of the data when it's sent out.

``int len``

  Reserved for development.

**Description**

Users can use this function to read back data from a specific IR port.

The following shows how to use the function.

.. code-block:: c

  ITPKeypadEvent ev;
  ev.code = 0;
  ev.code = TxBuf[cnt++];
  write(TEST_PORT, &ev, sizeof (ITPKeypadEvent));


iteIrClearClkSample
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrClearClkSample(ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to clear the sampling data from a specific IR port and restart the sampling.


iteIrGetFreqFast
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetFreqFast (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the fastest frequency from a specific IR port.


iteIrGetFreqSlow
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetFreqSlow (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the slowest frequency from a specific IR port.


iteIrGetFreqAvg
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetFreqAvg (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the average frequency from a specific IR port.


iteIrGetFreqNew
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetFreqNew (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the latest frequency from a specific IR port.


iteIrGetHighDCFast
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetHighDCFast(ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the high potential period of the fastest frequency waveform from a specific IR port.


iteIrGetLowDCFast
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetLowDCFast (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the low potential period of the fastest frequency waveform from a specific IR port.


iteIrGetHighDCSlow
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetHighDCSlow (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the high potential period of the slowest frequency waveform from a specific IR port.


iteIrGetLowDCSlow
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetLowDCSlow (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the low potential period of the slowest frequency waveform from a specific IR port.


iteIrGetHighDCAvg
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetHighDCAvg (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the high potential period of the average frequency waveform from a specific IR port.


iteIrGetLowDCAvg
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetLowDCAvg (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the low potential period of the average frequency waveform from a specific IR port.


iteIrGetHighDCNew
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetHighDCNew (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the high potential period of the latest frequency waveform from a specific IR port.


iteIrGetLowDCNew
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void iteIrGetLowDCNew (ITHIrPort port)

**Parameters**

``ITHIrPort port``

  IR ports to be executed. E.g. ``ITH_IR0_BASE``.

**Description**

This function allows the user to obtain the low potential period of the latest frequency waveform from a specific IR port.

Example
----------------------------------

There are three Ir example programs in the :file:`<sdk_root>/project/test_ir` directory.

This sample program is divided into three sub-tests:

1. Test Rx to receive the signal from the remote control of NEC Ir communication specification.
2. Test the Tx to simulate the signal of NEC Ir communication specification for Rx to receive.
3. Test the function of detecting frequency.

Hardware Connection
^^^^^^^^^^^^^^^^^^^^^^

Here we use a USB to TTL converter board to connect with PC and see the test result through UART1.

- Test 1: Connect the IR receiver to GPIO24 and power it up, and test it with the NEC remote control.

  .. image:: /_static/e_circuit_ir_test1.png

- Test 2: Connect the IR receiver to GPIO24 and supply power; then connect the IR transmitter to GPIO14 and supply power.

  .. image:: /_static/e_circuit_ir_test2.png

- Test 3: Connect GPIO24 to GPIO14 via DuPont cable.

  .. image:: /_static/e_circuit_ir_test3.png

KConfig settings
^^^^^^^^^^^^^^^^^^

Please run :file:`<sdk_root>/build/openrtos/test_ir.cmd` and follow these steps to set it up:

- Check :menuselection:`Peripheral --> IrDA Enable`.
- Check :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable`.
- Select :guilabel:`IR0 Interrupt Enable` via :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 mode`.

  .. image:: /_static/qconf_peripheral_ir.png

Test 1:

- Check :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 RX Modulation Enable`.
- Select :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 Protocol: ir_nec.inc`.

  .. image:: /_static/qconf_peripheral_ir_test1.png

- Select :menuselection:`Test IR --> Test IR --> test IR Rx`.

  .. image:: /_static/qconf_testir_test_ir_rx.png

Test 2:

- Check :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 RX Modulation Enable`.
- Check :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 TX Modulation Enable`.
- Select :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 Protocol: ir_nec.inc`.

  .. image:: /_static/qconf_peripheral_ir_test2.png

- Select :menuselection:`Test IR --> Test IR --> test IR Tx sig to Rx`.

  .. image:: /_static/qconf_testir_test_ir_tx.png

Test 3:

- Check :menuselection:`Peripheral --> IrDA Enable --> IR0 Enable --> IR0 RX sample Enable`.

  .. image:: /_static/qconf_peripheral_ir_test3.png


Next, set the pin used by IR0.

- Set ``GPIO --> IR0 RX Pin`` as 24.
- Set ``GPIO --> IR0 TX Pin`` as 14.

  .. image:: /_static/qconf_gpio_ir.png

- Select :menuselection:`Test IR --> Test IR --> test IR sampling function`.

  .. image:: /_static/qconf_testir_test_ir_sample.png

To verify the results
^^^^^^^^^^^^^^^^^^^^^^^

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

1. Click the |build_icon| button to build the project. Please ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Reset the power of EVB.
3. When the program is finished building, click the |run_icon|  button to boot up the system via USB to SPI board.
4. Execute Tera Term.

Test 1:
""""""""""""""""""""""

IR Rx: Remote control is required ( in the case of NEC protocol).

When you press the button "1" of the remote control, you will see the message ``IR code: 0x11`` in the Console. If pressing the button "2" of the remote control, you will see 0x12.

.. image:: /_static/teraterm_ir_test1.png

Test 2:
""""""""""""""""""""""

IR Tx sig to Rx: Send data from its own TX to its own RX.

A value equal to or close to the ``TxBuf[]`` in :file:`test_ir.c` should be seen in the Console.

In this example, currently the value in ``TxBuf[]`` is 0x11 of the NEC protocol and will be sent every 500ms.


.. image:: /_static/teraterm_ir_test2.png

Test 3:
""""""""""""""""""""""

IR sampling function: The data is sent to its own Rx through PWM at 38khz, and the frequency is detected through the sample function. In the Console, you should see the frequency equal to or close to 38khz. The result is not accurate enough because the sampling rate is not high enough, so there are errors, which can be improved by increasing the sample rate.

.. image:: /_static/teraterm_ir_test3.png





.. raw:: latex

    \newpage
