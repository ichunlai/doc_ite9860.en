﻿.. _spi:

SPI
=======

Function description
--------------------

SPI stands for Serial Peripheral Interface. It is a synchronous serial interface designed and developed by Motorola and is commonly used to communicate with peripheral chips such as Storage, Sensor and Bridge.It is a master-slave architecture with one (or more) master (master device) and one (or more) slave (slave device), and the devices communicate with each other using full duplex mode. Details of the internal hardware architecture are described below.

.. figure:: /_static/diagram_spi.png

  Structure Diagram

Pin name and meaning
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- SCLK: Clock signal, generated and controlled by the Master.
- SS: Slave selection signal, controlled by the Master. A Slave will only respond to the Master's operation command when the SS signal is at a low potential.
- MOSI: Master data output, Slave data input.
- MISO: Master data input, Slave data output.

.. figure:: /_static/diagram_spi_master_to_multi_slaves.png

  Master side connects to multiple Slave sides

.. figure:: /_static/diagram_spi_timing.png

  Sequence diagram and working mode


Related KConfig settings
----------------------------------------------------------------

- :menuselection:`Peripheral --> SPI_Enable`

  To determine if the SPI device should be enabled and select the SPI module you want to turn on.

  .. image:: /_static/qconf_peripheral_check_spi_enable.png


- :menuselection:`Peripheral --> SPI_Enable --> SPI# Enable`

  Select the SPI module to be turned on

  .. image:: /_static/qconf_peripheral_check_spi0_enable.png

- :menuselection:`GPIO --> SPI#`

  To set the SPI GPIO. In current HW design, there are specific PIN numbers, and the selectable PIN number can be found in the notes of the SPI pins.

  .. image:: /_static/qconf_gpio_spi.png


Related source code files
----------------------------


.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - :file:`<sdk_root>/sdk/driver/itp/itp_spi.c`
    - The uppermost layer function. It mainly provides POSIX compliant API for the upper-layer AP to operate the device through ``open()``, ``close()``, ``ioctl()`` and other functions.
  * - | :file:`<sdk_root>/sdk/include/ssp/*`
      | :file:`<sdk_root>/sdk/driver/spi/*`
    - The underlying function, which may change in the future.


Description of related API
---------------------------------------

This section describes the APIs used to operate the SPI device, the SPI driver provides a POSIX compliant API that allows the users to perform operations like reading and writing on the SPI device through the ``open()``/``read()``/``write()``/``close()``/``ioctl()`` functions. If you need to refer to the SPI slave implementation flow, a simple example is provided in :ref:`spi_simple_example`. It should be noted that the software SPI slave connection requires a customized protocol from the customer.

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  To register a device.

**Parameters**

``ITPDeviceType type ITP_DEVICE_SPI``

  Device type. For a complete list of device types, see :file:`<sdk_root>/sdk/include/ite/itp.h`. The device types associated with SPI devices are ``ITP_DEVICE_SPI`` and ``ITP_DEVICE_SPI1``.

``const ITPDevice *device``

  Device Identifier. The device identifiers associated with SPI devices are ``itpDeviceSpi0`` and ``itpDeviceSpi1``.

**Description**

This function can be used to register the SPI# device to the core device list so that the upper level AP can operate the SPI functionality through ``ioctl()``/``read()``/``write()`` functions. This function will be called by the ``itpInit()`` function during system initialization (see :file:`<sdk_root>/sdk/driver/itp_init_openrtos.c`), so if ``itpInit()`` has been called in your project, you do not need to run this registration function again.


ioctl
^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  To control the device.

**Parameters**

``int file``

  The device identifier. Its value is the value in the ITPDeviceType list. The device identifiers associated with the SPI device are ``ITP_DEVICE_SPI`` and ``ITP_DEVICE_SPI1``.

``unsigned long request``

  The *request* parameter is for selecting the operation function to run on the device and will depend on the specified device.

``void *ptr``

  The *ptr* parameter represents the additional information needed to perform the requested function for this specific device. *ptr*'s type depends on the specific control request, but it can be a quantified integer or a pointer to a specific data structure of the device.

**Description**

Currently the ``ioctl()`` function used on SPI devices is only the INIT function.

- ITP_IOCTL_INIT

  To initialize the SPI device. The following shows how to initialize the SPI0 device with default settings.

  .. code-block:: c

    itpRegisterDevice(ITP_DEVICE_SPI, &itpDeviceSpi0);
    ioctl(ITP_DEVICE_SPI, ITP_IOCTL_INIT, (void *)0);

read
^^^^^^^^^^^^

.. c:function:: int read(int file, char *ptr, int len)

  To read data from RX.

**Parameters**

``int file``

  SPI port to be implemented. E.g. ``ITP_DEVICE_SPI``.

``char *ptr``

  To read the array header address of the structure, including read/write mode, instruction content, and actual memory delivery information.

``int len``

  To read the size of the structure array. It represents the number of reads done.

**Description**

This function allows users to read back data from the SPI0 port and the way to use it is shown below.

.. code-block:: c

    // initialize SPI, the method below could be found in itpInit();
    // Register SPI device
    itpRegisterDevice(ITP_DEVICE_SPI, &itpDeviceSpi0);

    // Do initialization
    ioctl(ITP_DEVICE_SPI, ITP_IOCTL_INIT, NULL);

    // Do SPI Read operation
    fd = open(":spi0", O_RDONLY);
    SpiInfo.readWriteFunc = ITP_SPI_PIO_READ;
    SpiInfo.cmdBuffer = &command;
    SpiInfo.cmdBufferSize = 1;
    SpiInfo.dataBuffer = &recv_buf;
    SpiInfo.dataBufferSize = 1;
    read(fd, &SpiInfo, 1);
    close(fd);

write
^^^^^

.. c:function:: int write(int file, char*ptr, int len)

  To send data from TX.

**Parameters**

``int file``

  SPI port to be implemented. E.g. ``ITP_DEVICE_SPI``.

``char *ptr``

  To write the array header address of the structure, including read/write mode, instruction content, and actual memory delivery information.

``int len``

  To write the size of the structure array. It represents the number of writes done.

**Description**

This function allows users to write data from the SPI0 port and the way to use it is shown below.

.. code-block:: c

    // initialize SPI, the method below could be found in itpInit();
    // Register SPI device
    itpRegisterDevice(ITP_DEVICE_SPI, &itpDeviceSpi0);

    // Do initialization
    ioctl(ITP_DEVICE_SPI, ITP_IOCTL_INIT, NULL);

    // Do SPI Write operation
    fd = open(":spi0", O_WRONLY);
    SpiInfo.readWriteFunc = ITP_SPI_PIO_WRITE;
    SpiInfo.cmdBuffer = &command;
    SpiInfo.cmdBufferSize = 1;
    SpiInfo.dataBuffer = &send_buf;
    SpiInfo.dataBufferSize = 1;
    write(fd, &SpiInfo, 1);
    close(fd);



.. _spi_simple_example:

Basic example program
----------------------------------------------------------------

.. code-block:: c

    #include "ite/itp.h"

    void main(void)
    {
        int fd = 0;
        ITPSpiInfo SpiInfo = {0};
        uint8_t command[5] = {0};
        uint8_t id2[3]  = {0};

        itpRegisterDevice(ITP_DEVICE_SPI, &itpDeviceSpi0);
        ioctl(ITP_DEVICE_SPI, ITP_IOCTL_INIT, NULL);

        printf("Start SPI test!\n");

        fd = open(":spi0", O_RDONLY);
        if (!fd)
            printf("--- open device spi0 fail ---\n");
        else
            printf("fd = %d\n", fd);

        command[0] = 0x9F;

        SpiInfo.readWriteFunc = ITP_SPI_PIO_READ;
        SpiInfo.cmdBuffer = &command;
        SpiInfo.cmdBufferSize = 1;
        SpiInfo.dataBuffer = &id2;
        SpiInfo.dataBufferSize = 3;

        while (1)
        {
            read(fd, &SpiInfo, 1);
            printf("SpiInfo 0x%x, 0x%x, 0x%x\n", id2[0],id2[1], id2[2]);
        }

        close(fd);
    }

.. raw:: latex

    \newpage
