.. _wifi:

Wifi
================================================================

Function description
-------------------------------------

The IT986x supports several WiFi modules with SDIO interface, the module commonly used is RTL 8189FTV from Realtek, which supports 2.4GHz band and Non-GPL license. Each WiFi module supports two modes.

- STA mode: Set the device as Client, and select the existing wireless hotspots (Access Point) and connect.

  .. note::

    Some applications require hotspots to have Internet capability.

- HostAP mode: set the device as a wireless hotspot (Access Point), which can be connected by other client devices

  The two modes can be switched between via the API, but they cannot co-exist.

.. _wifi_kconfig:

Related KConfig settings
----------------------------------------------------------------

- :menuselection:`Network --> Enable WiFi module`

  To determine if the WiFi device should be enabled, and if you decide to enable it, sub-options will appear.

- :menuselection:`WiFi Interface --> SDIO WiFi device`

  This is a sub-option after enabling WiFi. Select the interface first, and then select the module corresponding to the interface (WiFi with USB interface is not available for IT986x series). Please note that when the "SDIO WiFi device" option appears, in addition to the "WiFi Interface", you must also check some preceding options, which are usually the interface options in Peripheral, as explained below.

- :menuselection:`Storage --> SD1 Device --> Peripheral --> SDIO Enable --> SDIO 1 as static device`

  This is the preceding option of RTL 8189FTV. Since the module is SDIO interface, the SDIO device must be turned on to mount the driver, otherwise the "SDIO WiFi device" option is empty.

- :menuselection:`Network --> Enable WiFi SDIO Power OnOff User Defined`

  The IT986x has a specified power pin for SDIO interface WiFi, which can be customized when to power on when it is enabled, or powered on immediately when it is not.

- :menuselection:`GPIO --> SD1 WiFi Card Power Enable --> SD1 WiFi Power Pin`

  Fill in the power GPIO number of the WiFi module.

- :menuselection:`GPIO --> SD1 WiFi Card Power Enable --> SD1 WiFi Detect Pin Active High`

  The module on board is preset to high level, just check the box if you don't have special requirement.

- :menuselection:`GPIO --> SD1 I/O Pin`

  Fill in the GPIO number of the WiFi module, in order of CLK, CMD, SD_D0, SD_D1, SD_D2, SD_D3.

Related source code files
---------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - ``sdk/driver/itp/itp_wifi.c``
    - Intermediary function for LWIP network interface settings, binding operations, and for upper-layer APs such as WifiMgr to operate the device through ``ioctl()`` (customer developers generally do not use it directly).
  * - ``sdk/driver/non_gpl_wifi``
    - RTL 8189FTV Driver
  * - ``sdk/share/lwip``
    - TCP/IP Stack
  * - ``sdk/share/Wifi_Mgr``
    - WiFi operation manager and API to implement various WiFi functions and operations. If you want to develop an application that requires WiFi operation, you don't need to call ioctl directly. WifiMgr has been packaged as an API, so just use the API provided here, as defined in :file:`WifiMgr.h`.

Description of related API
---------------------------

This section describes the common APIs used to operate WiFi devices. WifiMgr has various WiFi basic control APIs for developers to operate, such as ``WifiMgr_Init()``/``WifiMgr_Sta_Connect()``/``WifiMgr_Get_Scan_Info()`` and other functions to perform operations such as setting mode, connecting, scanning, etc. on WiFi devices. The following are some of the commonly used APIs.

WifiMgr_Init
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int WifiMgr_Init(WIFIMGR_MODE_E init_mode, int mp_mode, WIFI_MGR_SETTING wifiSetting)

  To initialize the WiFi device.

**Parameters**

``WIFIMGR_MODE_E Init_Mode``

  Initializing WiFi mode. 0 for STA mode, 1 for HostAP mode.

``Int Mp_Mode``

  Developer mode. 0 to turn it off, 1 to turn it on. It's usually set to 0 directly. No need to enter this mode.

``WIFI_MGR_SETTING WifiSetting``

  The WiFi data structure at the application layer, to which the data stored in INI will write the corresponding information such as SSID and PW.

**Description**

This function manages the initialization of the program WifiMgr and WiFi operation mode. After running this function, you can use the API provided by WifiMgr to operate the device.

**Usage Example**

.. code-block:: c

    static WIFI_MGR_SETTING gWifiSetting;

    void NetworkWifiInit(void)
    {
        int ret;
        ret = WifiMgr_Init(WIFIMGR_MODE_CLIENT, 0, gWifiSetting);
    }

WifiMgr_Get_Scan_AP_Info
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int WifiMgr_Get_Scan_AP_Info(WIFI_MGR_SCANAP_LIST* pList)

  To scan and obtain a list of SSIDs.

**Parameters**

``WIFI_MGR_SCANAP_LIST  *pList``

  Hotspot information structure. The scanned information will be stored in the structure, with a maximum of 64 hotspots.

**Description**

Use this API to scan and save the information. the return value is the number of hotspots obtained so far.

The information includes SSID, MAC, Security, Signal Power, etc., up to 64 groups. If you want to display this information in Log, please open ``WIFIMGR_SHOW_SCAN_LIST`` in :file:`WifiMgr.h`.

**Example**

.. code-block:: c

    static WIFI_MGR_SCANAP_LIST  pList[64];

    void scan_ap (void)
    {
        int get_scan_count = 0;
        get_scan_count = WifiMgr_Get_Scan_AP_Info(pList);
    }


WifiMgr_Sta_Connect
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int WifiMgr_Sta_Connect(char* ssid, char* password, char* secumode)

  Connecting hotspots.

**Parameters**

``char* ssid, char* password, char* secumode``

  SSID, password and security encryption code required to connect to the hotspot.

**Description**

Connect to the hotspot with this API. Enter the SSID, password and encryption code of the hotspot. The encryption code is defined in :file:`WifiMgr.h`, and the prefix is ``ITE_WIFI_SEC_XXX``. If you don't know the encryption type of the hotspot, usually the encryption of the router is WPA2, and you can just fill in "7." Even if you fill in the wrong one, it doesn't matter, the API will update itself.

**Example**

.. code-block:: c

    /* Connect Info */
    #define SSID "ITE_GUEST"
    #define PW   "12345678"
    #define SEC  "7"

    void link_ap (void)
    {
        int ret = -1;
        ret = WifiMgr_Sta_Connect(SSID, PW, SEC);
    }

WifiMgr_Sta_HostAP_Switch
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int WifiMgr_Sta_HostAP_Switch(WIFI_MGR_SETTING wifiSetting)

  To switch between STA mode and HostAP mode.

**Parameters**

``WIFI_MGR_SETTING  WifiSetting``

  The WiFi data structure at the application layer, to which the data stored in INI will write the corresponding information such as SSID and PW.

**Description**

To switch between the two modes without rebooting

**Example**

.. code-block:: c

    void NetworkWifiModeSwitch(void)
    {
        int ret;
        ret = WifiMgr_Sta_HostAP_Switch(gWifiSetting);
    }

Callback Functions
-------------------

Prototype
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: c

    typedef enum tagWIFIMGR_STATE_CALLBACK_E
    {
        WIFIMGR_STATE_CALLBACK_CONNECTION_FINISH = 0,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_DISCONNECT_30S,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_RECONNECTION,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_TEMP_DISCONNECT,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_CONNECTING_FAIL,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_CONNECTING_CANCEL,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_SLEEP_SAVE_INFO,
        WIFIMGR_STATE_CALLBACK_CLIENT_MODE_SLEEP_CLEAN_INFO,
        WIFIMGR_STATE_CALLBACK_SWITCH_CLIENT_SOFTAP_FINISH,
        WIFIMGR_STATE_CALLBACK_MAX,
    } WIFIMGR_STATE_CALLBACK_E;

Description
^^^^^^^^^^^^^^^^^^^^^^^^^^^

- WIFIMGR_STATE_CALLBACK_CONNECTION_FINISH

  The meaning and status suggestion are as follows

  - Meaning: Sta/AP mode connection completed. Sta mode connection completed means the router is connected, and AP mode connection completed means the AP is turned on.
  - Status suggestion: If there is a function that needs a response when the connection is completed, you can add a code here.

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_DISCONNECT_30S

  The meaning and status suggestion are as follows

  - Meaning: This callback is triggered when a router connected in Sta mode is disconnected for some reason (e.g., shutdown) and the connection is lost for more than 30 seconds.
  - Status suggestion: You can disconnect or wait for a while before reconnecting.

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_TEMP_DISCONNECT

  The meaning and status suggestion are as follows

  - Meaning: This callback is triggered when a router connected in Sta mode is disconnected for some reason (e.g., shutdown) and the disconnection does not exceed 30 seconds.
  - Status suggestion: NA.

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_RECONNECTION

  The meaning and status suggestion are as follows

  - Meaning: This callback is triggered when the router connected in Sta mode is disconnected for some reason (e.g., shutdown) and the disconnection does not exceed 30 seconds, and the router is reconnected.
  - Status suggestion: NA.

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_RECONNECTION

  The meaning and status suggestion are as follows

  - Meaning: This callback is triggered when the router connected in Sta mode is disconnected for some reason (e.g., shutdown) and the disconnection does not exceed 30 seconds, and the router is reconnected.
  - Status suggestion: NA.

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_CONNECTING_FAIL

  The meaning and status suggestion are as follows

  - Meaning: This callback is triggered when a router connected in Sta mode fails to connect for some reason.
  - Status suggestion: Prompt connection errors.

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_CONNECTING_CANCEL

  The meaning and status suggestion are as follows

  - Meaning: This callback is triggered when the device actively disconnects during a router connection in Sta mode.
  - Status suggestion: NA.

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_SLEEP_SAVE_INFO

  The meaning and status suggestion are as follows

  - Meaning: The board will store the currently connected SSID/PW/SEC when it enters sleep mode.
  - Status suggestion: Prompt information.

- WIFIMGR_STATE_CALLBACK_CLIENT_MODE_SLEEP_CLEAN_INFO

  The meaning and status suggestion are as follows

  - Meaning: Clear all the ``gWifiSetting`` information when calling ``WifiMgr_Terminat``.
  - Status suggestion: Prompt information.

- WIFIMGR_STATE_CALLBACK_SWITCH_CLIENT_SOFTAP_FINISH

  The meaning and status suggestion are as follows

  - Meaning: Notification on the end of Sta/AP mode switching.
  - Status suggestion: Code judgment after the switch.

Basic example program
----------------------------

.. code-block:: c

    #include <pthread.h>
    #include <sys/ioctl.h>
    #include "openrtos/FreeRTOS.h"
    #include "openrtos/task.h"

    void* TestFunc(void* arg)
    {
        pthread_t task_ap;
        pthread_attr_t attr_ap;

        printf("====>NGPL: init itp\n");
        itpInit();
        usleep(5*100*1000);

        gWifiSetting.wifiCallback = CallbackFucntion;
        PreSettingWifi();

    #ifdef CFG_NET_WIFI_SDIO_POWER_ON_OFF_USER_DEFINED
        WifiPowerOn();
    #endif

        WifiMgr_Sta_Switch(1);
        WifiMgr_Init(WIFIMGR_MODE_CLIENT, 0, gWifiSetting);

        for (;;)
        {
            sleep(1);
        }
    }

    int main(void)
    {
        pthread_t task;
        pthread_attr_t attr;

        pthread_attr_init(&attr);
        pthread_create(&task, &attr, TestFunc, NULL);

        /* Now all the tasks have been started - start the scheduler. */
        vTaskStartScheduler();

        /* Should never reach here! */
        return 0;
    }

.. _wifi_complete_example:

Complete example
----------------------------

An example main program of WiFi, ``test_wifi_ngpl_lwip``, is attached to the :file:`<sdk_root>/project/test_wifi_sdio` directory. this program shows a simple WiFi initialization process and has some subroutines for testing, such as PING, Socket Client testing, DNS resolution, etc. Just enable the corresponding function in the directory :file:`test_config.h`. Customers can also add their own code to the main program and complete the experiments they want to perform.

For some modifications of specifications, please focus on :file:`test_config.h`.

Connection Information
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: c

    /* Connect Info */
    #define SSID "ITE_GUEST"
    #define PW   "12345678"
    #define SEC  "7"

Switch for test function
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: c

    #define test_iperf          0 // Make sure you have iperf lib and open HTTP
    #define tcp_client_test     0
    #define test_connect        0 // Don't OPEN while test iperf or socket
    #define test_scan_ap        0
    #define test_dns            0
    #define test_ping           0

Hardware Connection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

If you use Daughter board to connect external module, please refer to the file :file:`IT986x_MINI USER GUIDE_SDIO.pdf` for GPIO connection, and also please recheck the GPIO number setting in Kconfig.

KConfig settings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please run :file:`<sdk_root>/build/openrtos/test_wifi_sdio.cmd`. All settings have been done. For more details, please refer to Kconfig or :ref:`wifi_kconfig`. If you have no special requirements, you can compile it directly:

To verify the results
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

1. Click the |build_icon| button to build the project. Please ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. The antenna is installed
3. Reset the power of EVB.
4. When the program is finished building, click the |run_icon|  button to boot up the system via USB to SPI board.
5. Run Tera Term. If the SSID and password are correct, the test program will automatically connect to the hotspot and eventually obtain the IP.
6. Note that most of the test functions need to obtain the IP before being carried out, such as TEST_PING, TEST_DNS

  .. image:: /_static/console_wifi_log.png

.. note::

  1. From time to time, Kconfig options or functions may be added or modified, not necessarily exactly the same as the picture above.
  2. If WiFi Driver initialization fails, you can first check if there is any setting error in GPIO, or you can observe if SD ERR is reported by CMD53 in Log.
  3. If there is an unknown error that cannot be handled, please contact ITE's FAE to forward to the responsible RD.


FAQ
------------------------------------------------

We have collected some frequently asked questions from our customers, which can help in the initial judgment when errors or strange cases occur

1. Q: 8189FTV initialization failed

  A: Please check the Kconfig settings such as GPIO, SD1 WIFI Card Power Enable needs to be turned on for IT986x.

2. Q: What is the way to know that SDIO is successfully connected to the module?

  A: You can observe the CMD53 value read at the beginning, as shown below

  .. image:: /_static/console_wifi_cmd_log.png

  Or you can write any value in a certain address into the register and then read it to test if it is correct.

  .. code-block:: c

    struct sdio_func *wifi_sdio_func = NULL;
    void cmd_test(void)
    {
        wifi_writeb(wifi_sdio_func, 0x1, 0x2a);
        printf("test read 0x1 is 0x%x \n", wifi_readb(func, 0x1)); // If the value is 0x2a, it is correct
    }

3. Q: The SSID obtained after scanning is very little, or the signal strength is very weak (mostly below 30%)

  A: Please connect the antenna, with and without the antenna will be much different

4. Q: Can you turn on Ethernet at the same time?

  A: Yes, the Kconfig setting of dual MAC is written in Pre-Setting, the file name is :file:`_config_xxx_indoor_ngpl_wifi_ethernet`. Read and compile it directly.

5. Q: How to troubleshoot the unknown problem?

  A: Please use the test program test_wifi_sdio to identify the problem. Please see :ref:`wifi_complete_example` for the way to use it, and collect the log information.

6. Q: Is STA+HostAP mode supported?

   A: STA+HostAP mode is not supported at the same time.


.. raw:: latex

    \newpage
