.. _pwm:

PWM
===

Function description
--------------------

PWM is a way to simulate analog signals with digital signals. Usually we can use it to adjust the brightness of the lights, the rotation speed of the motor, the color matching of the RGB LEDs, the brightness control of the screen, the loudness/sound frequency of the speakers, etc.

The IT986x provides PWM1~PWM8 for users to use, and the use of PWM1~8 corresponds the same number as the internal TIMER1~8. For example, if TIMER8 has been used by OS first, then you cannot select the corresponding PWM8. The following is the general TIMER configuration of IT986x. If you want to use PWM, you should pay attention to whether TIMER has been used first and avoid using the same number.

.. list-table::

    * - timer 1
      - free
    * - timer 2
      - free
    * - timer 3
      - :file:`backlight/sdk/driver/itp/itp_backlight.c`
    * - timer 4
      - RTC
    * - timer 5
      - DEBUG
    * - timer 6
      - Remote UART Wakeup
    * - timer 7
      - free
    * - timer 8
      - OS


Related KConfig settings
----------------------------------------------------------------

- :menuselection:`Screen --> Backlight# Enable`

  To determine if Backlight should be enabled. Check this box to register the corresponding Backlight device via the ``itpRegisterDevice()`` function in the ``itpInit()`` function and perform the initialization action.

- :menuselection:`GPIO --> Backlight PWM Pin`

  Specify which GPIO pin to use as PWM pin for this Backlight.

- :menuselection:`GPIO --> Backlight PWM Number`

  Specifies which set of PWMs to use for this Backlight.


Related source code files
----------------------------

.. list-table::
   :header-rows: 1

   * - Path
     - Description
   * - ``<sdk_root>/sdk/driver/ith/ith_pwm.c``
     - The uppermost layer function.


Description of related API
---------------------------------------

ithPwmInit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithPwmInit(ITHPwm pwm, unsigned int freq, unsigned int duty)

**Parameters**

``ITHPwm pwm``

    Device Identifier. The device identifiers associated with PWM device are ``ITH_PWM1`` ~ ``ITH_PWM8``.

``unsigned int freq``

    Frequency.

``unsigned int duty``

    Duty cycle, the value range is 0 ~ 100.

**Description**

To initialize PWM device.

ithPwmReset
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithPwmReset(ITHPwm pwm, unsigned int pin, unsigned int gpio_mode)

**Parameters**

``ITHPwm pwm``

    Device Identifier. The device identifiers associated with PWM device are ``ITH_PWM1`` ~ ``ITH_PWM8``.

``unsigned int pin``

    GPIO number.

``unsigned int gpio_mode``

    GPIO MODE: 0~4. This parameter is for compatibility and will be ignored on IT986x platforms.

**Description**

To reset PWM device.

ithPwmSetDutyCycle
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithPwmSetDutyCycle(ITHPwm pwm, unsigned int duty)

**Parameters**

``ITHPwm pwm``

    Device Identifier. The device identifiers associated with PWM device are ``ITH_PWM1`` ~ ``ITH_PWM8``.

``unsigned int duty``

    Duty cycle, the value range is 0 ~ 100.

**Description**

To set the PWM duty cycle.

ithPwmEnable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithPwmEnable(ITHPwm pwm, unsigned int pin, unsigned int gpio_mode)

**Parameters**

``ITHPwm pwm``

    Device Identifier. The device identifiers associated with PWM device are ``ITH_PWM1`` ~ ``ITH_PWM8``.

``unsigned int pin``

    GPIO number.

``unsigned int gpio_mode``

    GPIO MODE: 0 ~ 4. This parameter is for compatibility and will be ignored on IT986x platforms.

**Description**

To enable the device.

ithPwmDisable
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void ithPwmDisable(ITHPwm pwm, unsigned int pin)

**Parameters**

``ITHPwm pwm``

    Device Identifier. The device identifiers associated with PWM device are ``ITH_PWM1`` ~ ``ITH_PWM8``.

``unsigned int pin``

    GPIO number.

**Description**

To disable the device.
