.. _key:

Key
=======

Function description
----------------------

Key is the most basic input device of SoC, and there are various types according to the input interface, such as GPIO, SPI, I2C, USB, PS/2, RF...etc.This section only applies to KEYs using GPIO as the input interface, other I/O interface KEYs will be described in a separate article. ITE SOC provides three types of Key modules, supporting up to 64 KEYs.

ITE SDK can support three types of Key modules:

- The first is the general GPIO KEY, featuring a simple KEY circuit, i.e., one GPIO PIN controlling a group of KEYs, with the disadvantage of taking up more GPIO PINs.
- The second type is the N*N HEX KEY, featuring 2N sets of GPIOs to control :math:`N^2` sets of KEYs. For information about the HEX KEY, please refer to [Hex Keypad Datasheet](http://www.winpicprog.co.uk/pic_tutorial9.htm).
- The third type is ITE's self-developed Castor3 KEY, which features the use of N sets of GPIOs to control N2 sets of KEYs, but the wiring is also the most complicated among the three types of KEYs.

Assuming the same 8 GPIOs are used, the first GPIO KEY can only control 8 sets of KEYs, the second HEX KEY can control 16 sets of KEYs, and the third Castor3 KEY can support up to 64 sets of KEYs, which is the maximum number of KEYs supported by the SDK at present.

The Driver corresponding to these three types of KEY modules are all placed in the :file:`<sdk_root>/sdk/driver/itp/keypad/` directory, where the Driver corresponding to the first type, GPIO KEY, is :file:`itp_keypad_gpio.c`, the one to the second type, HEX KEY, is :file:`itp_keypad_hex.c`, and the one to the third type, Castor3 KEY, is :file:`itp_keypad_castor3.c`.



Related KConfig settings
----------------------------------------------------------------

To specify the selected KEY module in KCONFIG, you must first check the :guilabel:`Keypad Enable` box, then enter the name of the Driver (e.g. :file:`itp_keypad_castro3.c`) corresponding to the KEY module in the :guilabel:`Keypad Module` field. And set the relevant KEYPAD parameters properly according to the project requirements (as shown in the picture). Here is the description of each KCONFIG setting:

  .. image:: /_static/qconf_peripheral_keypad.png

- :menuselection:`Peripheral --> Keypad Enable`

  To determine if KEYPAD module should be enabled.

- :menuselection:`Peripheral --> Keypad Enable -->  Keypad module`

  To set the name of the KEYPAD module to be used.

- :menuselection:`Peripheral --> Keypad Enable --> Keypad Press Key interval(ms)`

  To set the time interval for sending each KEY Event.

- :menuselection:`Peripheral --> Keypad Enable --> Keypad Repeat Key Enable`

  To enable repeat key function. When the finger presses a KEY without retraction, the system will send KEY Event repeatedly.

- :menuselection:`Peripheral --> Keypad Enable --> Keypad mapping table`

  To set the MAPPING TABLE file of KEYPAD (placed in :file:`<sdk_root>/sdk/target/keypad` directory). The value reported by Driver to the upper layer is INT 0 ~ N (N refers to the maximum number of KEYs -1), and the upper layer translates according to this Mapping Table into the corresponding Key Code.



KConfig settings related to KEY GPIO
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

To set the GPIO PIN of KEY, you should go to theG :menuselection:`GPIO --> Keypad Pins` field (as shown below).

.. image:: /_static/qconf_gpio_keypad.png

1. GPIO KEY

  Fill in the GPIO order according to the order of KEY. E.g. KEY0 uses GPIO 51, KEY1 uses GPIO 58, KEY2 uses GPIO 31, then "Keypad Pins: 51, 58, 31"

2. HEX KEY

  Fill in the GPIO order according to the order of COLUMN first and then RAW. e.g. column_1 uses GPIO 31, column_2 uses GPIO 30, column_3 uses GPIO 29, column_4 uses GPIO 28, raw_1 uses GPIO 40, raw _2 uses GPIO 41, raw _3 uses GPIO 42, raw _4 uses GPIO 43, then "Keypad Pins: 31, 30, 29, 28, 40, 41, 42, 43"

3. Castor3 KEY

  Fill in the GPIO order according to the GPIO SCAN order. E.g. The first SCAN, no GPIO set OUTPUT 0; the second SCAN, GPIO22 set OUTPUT 0; the third SCAN, GPIO21 set OUTPUT 0; the fourth SCAN, GPIO33 set OUTPUT 0; the fifth SCAN, GPIO16 set OUTPUT 0; then set "Keypad Pins: 22, 21, 33, 16".

Related source code files
----------------------------------------------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - ``<sdk_root>/sdk/driver/itp/itp_keypad.c``
    - | Keypad ITP Driver, the main purpose is to define three APIs, as the specification for the lower layer driver implementation. They are:
      | ``itpKeypadInit();``
      | ``itpKeypadGetMaxLevel();``
      | ``itpKeypadProbe();``
  * - ``<sdk_root>/sdk/driver/itp/keypad/itp_keypad_xxx.c``
    - | ``itp_keypad_xxx.c`` is the underlying driver for Keypad, the main purpose of which is to implement the following three APIs:
      | ``itpKeypadInit();``
      | ``itpKeypadGetMaxLevel();``
      | ``itpKeypadProbe();``

Description of related API
---------------------------------------

DEVICE ID : ITP_DEVICE_KEYPAD (See ``<sdk_root>/sdk/include/ite/itp.h`` )

This section describes the APIs used to operate the KEYPAD device. The driver provides a POSIX compliant API that allows the user to perform operations like reading and writing on the KEYPAD device through ``itpKeypadInit()``/``itpKeypadGetMaxLevel()``/``itpKeypadProbe()`` functions.

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  To register the device, i.e. ``itpRegisterDevice(ITP_DEVICE_ KEYPAD, &itpDeviceKeypad);``.

**Parameters**

``ITPDeviceType type``

  Device type. For a complete list of device types, see :file:`<sdk_root>/sdk/include/ite/itp.h`. The device type associated with the KEYPAD device is ``ITP_DEVICE_KEYPAD``.

``const ITPDevice *device``

  Device identifier. The device identifier associated with the KEYPAD device is ``itpDeviceKeypad``.

**Description**

This function can be used to register the KEYPAD device to the core device list, so that the upper layer AP can operate the KEYPAD functionality through ``ioctl()``/``read()``/``write()`` functions. This function will be called by the ``itpInit()`` function during system initialization (see :file:`<sdk_root>/sdk/driver/itp_init_openrtos.c`), so if ``itpInit()`` has been called in your project, you do not need to run this registration function again.

The following shows how to register the KEYPAD device to the system core and initialize the KEYPAD device.

.. code-block:: c

    itpRegisterDevice(ITP_DEVICE_KEYPAD, &itpDeviceKeypad);
    ioctl(ITP_DEVICE_KEYPAD, ITP_IOCTL_INIT, NULL);

ioctl
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  To control the device.

**Parameters**

``int file``

  Device identifier. Its value is the value in the ``ITPDeviceType`` list. The device identifier associated with the KEYPAD device is ``ITP_DEVICE_KEYPAD``.

``unsigned long request``

  The *request* parameter is used to select the function to be run on the device and will depend on the specified device.

``void *ptr``

  The *ptr* parameter represents the additional information required to perform the requested function on this particular device. The type of *ptr* depends on the particular control request, but it can be an integer or a pointer to a specific data structure of the device.

**Description**

The ``ioctl()`` function is implemented to perform various control functions on the KEYPAD device. request parameters and an optional third parameter (with different types) are passed to and interpreted by the corresponding part of the KEYPAD device associated with the file.

The ``ioctl()`` command for KEYPAD devices, the parameters and the error states applicable to each individual command are described below.

- ITP_IOCTL_INIT

  To initialize the KEYPAD device. *ptr* parameter is NULL.

- ITP_IOCTL_PROBE

  To return the total number of keys to the upper layer.

- ITP_IOCTL_GET_MAX_LEVEL

  To detect a key event. If a key input is detected, the event will be inserted into the queue and wait for ``read()`` to fetch it.

read
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __size)

  To read data from KEYPAD. For examples, please refer to :file:`<sdk_root>/sdk/share/sdl/video/castor3/SDL_castor3keyboard.c`.

**Parameters**

``int __fd``

  The KEYPAD Device to be implemented. E.g. ``ITP_DEVICE_KEYPAD``.

``void *__buf``

  The location to write back after reading. The KEYPAD buffer size is ``ITP Event Size(sizeof (ITPKeypadEvent))``, see :file:`<sdk_root>/sdk/include/ite/itp.h` for a complete list of device types.

``size_t __ size``

  The length of the data to be read, i.e. ``ITP Event Size(sizeof (ITPKeypadEvent))``.

**Description**

This function allows users to read back data from the KEYPAD device, and the following shows how to use it.

.. code-block:: c

    ITPKeypadEvent ev;

    if (read(ITP_DEVICE_KEYPAD, &ev, sizeof(ITPKeypadEvent)) == sizeof(ITPKeypadEvent))
    {
        if (ev.flags & ITP_KEYPAD_DOWN)
        {
            printf("GOT KEYPAD-DOWN\n");
        }
        else if (ev.flags & ITP_KEYPAD_UP)
        {
            printf("GOT KEYPAD-UP\n");
        }
    }

Basic example program
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block:: c

    /*
    Add the following statements in the "Kconfig" file to build the SDL library.

    config BUILD_SDL
        def_bool y

    Simple DirectMedia Layer (SDL) is a cross-platform software development library designed to provide a hardware abstraction layer for computer multimedia hardware components. Software developers can use it to write high-performance computer games and other multimedia applications that can run on many operating systems such as Android, iOS, Linux, macOS, and Windows. https://en.wikipedia.org/wiki/Simple_DirectMedia_Layer.

    This example demos how ITE SDK uses SDL library to get the KEY EVENT.
    */

    #include "SDL/SDL.h"

    int TouchEvent_test(void)
    {
        SDL_Event ev;

        /* SDL initial */
        if (SDL_Init(SDL_INIT_VIDEO) < 0)
            printf("Couldn't initialize SDL: %s\n", SDL_GetError());

        while (SDL_PollEvent(&ev))
        {
            switch (ev.type)
            {
            case SDL_KEYDOWN:
                switch (ev.key.keysym.sym)
                {
                case SDLK_UP:
                    printf("key direction up\n");
                    break;

                case SDLK_DOWN:
                    printf("key direction down\n");
                    break;

                case SDLK_LEFT:
                    printf("key direction left\n");
                    break;

                case SDLK_RIGHT:
                    printf("key direction right\n");
                    break;
        	       }
            }
            SDL_Delay(1);
        }
    }


.. raw:: latex

    \newpage
