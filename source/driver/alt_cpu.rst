.. _alt_cpu:

Alternative Processor
===========================

Function description
----------------------

Alternative Processor, abbreviated as ALT CPU, is a 32-bit RISC architecture CPU that provides developers with the ability to develop software IO protocols, regular programs, and other applications that require real-time processing to reduce main CPU usage and improve load balancing.

.. image:: /_static/diagram_alt_cpu.png

Application scope:

- Used to handle simple and low speed IO protocols, such as: I2C, RSL, UART, etc.
- Waveform generators, e.g. PWM, Heartbeat, etc.
- Customized protocols.
- As an MCU to handle regular programs, such as real-time monitoring and responding to certain events.

Limitations of use:

- No OS.
- Big Endian.
- 16kb sram; DRAM memory space available for reading and writing, but poor performance.
- Cannot use APIs related to dynamic memory, such as ``malloc()`` function.

ALT CPU Directory Structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /_static/file_struct_alt_cpu.png

The control of ALT CPU requires the cooperation of the main CPU, so the complete code contains parts for both the main CPU side (ARM) and the ALT CPU side (RISC). The following is an overview of the code related to the RISC segment, the ARM segment, and the ARM/RISC common segment.

- RISC segment

  This segment of the code runs on the ALT CPU and its source code is contained in the following two directories.

  - :file:`<sdk_root>/project/alt_cpu`: Provides functions for GPIO control, tick timer related tools dedicated to ALT CPU.
  - :file:`<sdk_root>/sdk/alt_cpu/xxx`: implementation of various ALT CPU applications.

  The code running on the ALT CPU will be pre-built for subsequent ARM related projects.

- ARM segment

  This segment of code runs on the main CPU and its source code is contained in the following two directories:

  - :file:`<sdk_root>/sdk/driver/itp/itp_alt_cpu.c`: upper layer ALT CPU driver function implementation. Provides POSIX-type functions for the AP layer to load the firmware of the ALT CPU, reset the ALT CPU, and other operations.
  - :file:`<sdk_root>/sdk/driver/alt_cpu/xxx`: The underlying ALT CPU driver function implementation and is subject to change in the future, so please do not call this part of the function directly.

- ARM/RISC Shared segment

  This segment contains the raw code that will be used by both ARM and RISC, mainly in the :file:`<sdk_root>/sdk/include/alt_cpu/xxx` directory, which contains

  - The declaration of the message structure used by ARM/RISC to communicate with each other.
  - The specification of the command ID.
  - Shared memory address offsets.

Communication between ARM/RISC is mainly through a shared memory mechanism and uses read/write queues implemented by multiple unused IO registers to exchange commands and data.

How to add a new device module
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

By writing the firmware on the ALT CPU, the ALT CPU can be used as a device module that is actually emulated by the software. The following describes what ITE has modified to use the ALT CPU as a pattern generator device:

- Added alt_cpu firmware implementation program under :file:`<sdk_root>/sdk/alt_cpu`.
- Put the ARM/RISC shared include file in :file:`<sdk_root>/include/alt_cpu`.
- Placed the ARM part of device driver in :file:`<sdk_root>/driver/alt_cpu`.
- Added a new test example under :file:`<sdk_root>/project/test_pattern_gen`.
- Added the new driver to the upper alt cpu device driver located at :file:`<sdk_root>/sdk/driver/itp_alt_cpu.c`. As the bolded part of the following equation:

  .. code-block:: c

    ...
    #ifdef CFG_PATTERN_GEN
    extern ITPDevice itpDevicePatternGen;
    #endif
    ...
    static ALT_CPU_ENGINE gptAltCpuEngineArray[] =
    {
       ...

    #ifdef CFG_PATTERN_GEN
        { ALT_CPU_PATTERN_GEN, &itpDevicePatternGen },
    #endif
       ...
    }
    ...

- Added compilation options to :file:`<sdk_root>/sdk/def.cmake`.

  .. code-block:: shell

    ...
    if (DEFINED CFG_ALT_CPU_ENABLE)
        set(CFG_BUILD_RISC 1)
        set(CFG_BUILD_ALT_CPU 1)
        add_definitions(
            -DCFG_RISC_ENABLE
            -DCFG_ALT_CPU_ENABLE
        )
        ITE_ADD_DEFINITIONS_IF_DEFINED(CFG_RSL_MASTER)
        ITE_ADD_DEFINITIONS_IF_DEFINED(CFG_RSL_SLAVE)
        ITE_ADD_DEFINITIONS_IF_DEFINED(CFG_PATTERN_GEN)
        ITE_ADD_DEFINITIONS_IF_DEFINED(CFG_OLED_CTRL)
        ...

- Added library linking options to :file:`<sdk_root>/sdk/build.cmake`.

  .. code-block:: shell

    ...
        # move alt cpu libraray here to solve linking order issue
        # ALT CPU
        if (DEFINED CFG_ALT_CPU_ENABLE)
            target_link_libraries(${CMAKE_PROJECT_NAME}
                risc
            )

            ITE_LINK_LIBRARY_IF_DEFINED(CFG_RSL_MASTER rslMaster)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_RSL_SLAVE rslSlave)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_SW_PWM swPwm)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_PATTERN_GEN patternGen)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_SW_UART swUart)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_SW_SERIAL_PORT swSerialPort)
            ITE_LINK_LIBRARY_IF_DEFINED(CFG_OLED_CTRL oledCtrl)
    ...

- Added Kconfig option for general-purpose devices in :file:`<sdk_root>/sdk/Kconfig`. For customized devices, it is recommended to enable the device only through the project's default Kconfig settings.

  .. code-block:: shell

    ...
    config PATTERN_GEN
        bool "Use ALT CPU to generate certain data pattern"
        default n
        depends on ALT_CPU_ENABLE
        help
            Use ALT CPU to generate certain data pattern.
        help_cht
            使用 ALT CPU 模擬自訂數據波形模式
        help_chs
            使用 ALT CPU 仿真自定义数据波形模式
    ...

- Added the Kconfig setting under :file:`<sdk_root>/project/test_pattern_gen`.

  .. code-block:: shell

    ...
    config ALT_CPU_ENABLE
        def_bool y

    config PATTERN_GEN
        def_bool y
    ...

- Added build and build_all script under :file:`<sdk_root>/build/openrtos`, i.e. file :file:`<sdk_root>/build/openrtos/test_pattern_gen.cmd` and :file:`<sdk_root>/build/openrtos/test_pattern_gen_all.cmd`.

- Added the alt cpu pre-build option to the build_all script (i.e. :file:`<sdk_root>/build/openrtos/test_pattern_gen_all.cmd`).

  .. code-block:: shell

    @echo off

    set CFG_PROJECT=%~n0

    set ALT_CPU=1
    call test_pattern_gen.cmd

    @if not defined NO_PAUSE pause

How to debug
^^^^^^^^^^^^^^^^^^^

- Firmware on RISC can use an unused IO register from RISC to store information and read these registers to check status via ARM test code or the usb_to_spi tool. Another thing to remind firmware developers is that the unused registers listed below may be used by the audio processor in the actual project.

  .. list-table::
    :header-rows: 1

    * - Address
      - Type
      - Field
      - Description
    * - 0xD800 0200
      - R/W
      - USER1
      - User Defined Register
    * - 0xD800 0204
      - R/W
      - USER2
      - User Defined Register
    * - 0xD800 0208
      - R/W
      - USER3
      - User Defined Register
    * - ...
      - ...
      - ...
      - ...
    * - 0xD800 027C
      - R/W
      - USER160
      - User Defined Register

- Write a debug message to an unused shared memory block and display that message via ARM test code. The usb_to_spi tool does not have direct access to the SRAM, so the ARM test code is the only way to access the debug message.

  For example:

  - In the code of the RISC side, e.g. :file:`<sdk_root>/sdk/alt_cpu/patternGen/main.c`, add

    .. code-block:: c

        ...
        int main(int argc, char **argv)
        {
            //Set GPIO and Clock Setting
            int inputCmd = 0;

            //Start Timer
            startTimer(0);

            {
                uint32_t* pDebugAddr = (uint32_t*)(15 * 1024);
                *pDebugAddr = ENDIAN_SWAP32(0x12345678);
            }
        ...

- In the ARM side test project code, e.g. :file:`<sdk_root>/project/test_pattern_gen/test_pattern_gen.c`, add

  .. code-block:: c

    ...
    void* TestFunc(void* arg)
    {
        #ifdef CFG_DBG_TRACE
        uiTraceStart();
        #endif

        itpInit();
        //_SimpleTest();
        _MultiPleDeviceTest();

        {
            uint32_t* pDebugAddress =  (uint32_t*)(iteRiscGetTargetMemAddress(ALT_CPU_IMAGE_MEM_TARGET) + 15 * 1024);
            printf("debug msg: 0x%X\n”, *pDebugAddress");
        }
        ...
    }

Rebuild the project and run it, you can see the message 0x12345678 from RISC in the debug message output from the main CPU.

.. image:: /_static/teraterm_alt_cpu_debug_msg.png

Application example
^^^^^^^^^^^^^^^^^^^^^^^

ITE currently has provided some firmware for ALT CPUs to accomplish the following applications.

- PWM :

  It is usually used to control the power supplied to the electrical equipment through different duty cycle. For example: LCD backlight control, LED brightness control.

  .. image:: /_static/wave_pwm.png

- Pattern Generator :

  To generate a specific waveform pattern.

  .. image:: /_static/wave_custom.png

Related KConfig settings
---------------------------------------

- :menuselection:`Peripheral --> ALT CPU Enable`

  To determine if the ALT CPU emulation function should be enabled. This option must be selected if you want to enable ALT CPU related functions.

- :menuselection:`Peripheral --> ALT CPU Enable --> Use ALT CPU to generate certain data pattern`

  Enable this option if you want to use the ALT CPU to simulate the customized data waveform mode.

Related source code files
----------------------------------------------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - ``sdk/driver/alt_cpu/xxx``
    - API prototype for reference


Description of related API
---------------------------------------

This section describes the APIs for operating ALT CPU device emulator. There is a POSIX compliant API that allows users to perform operations like reading and writing on ALT CPU device emulator via the ``open()``/``read()``/``write()``/``ioctl()`` functions.

ioctl
^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  Control the device.

**Parameters**

``int file``

  The device identifier. Its value is ``ITP_DEVICE_ALT_CPU``.

``unsigned long request``

  The *request* parameter is used to select the operation to be run on the device and will depend on the specified device.

``void *ptr``

  The *ptr* parameter represents the additional information required to perform the requested function on this particular device. The type of *ptr* depends on the particular control request, but it can be an integer or a pointer to a specific data structure of the device.

**Description**

The ``ioct()`` function is used to perform various control functions on the ALT CPU device emulator. Each emulation interface has its own defined commands, the more important of which are described below.

- ITP_IOCTL_ALT_CPU_SWITCH_EN

  To switch the ALT CPU to a certain emulation interface. If you want to emulate a pattern generator, the parameter is ``ALT_CPU_PATTERN_GEN``.

- ITP_IOCTL_INIT

  To initialize the ALT CPU device emulator.

Example program
----------------------

.. code-block:: c

    // pattern generator
    #include "ite/itp.h"        // for ITP_DEVICE_WIEGAND0 & ITP_DEVICE_WIEGAND1
    #include "alt_cpu/alt_cpu_device.h"
    #include "alt_cpu/patternGen/patternGen.h"

    PATTERN_GEN_WRITE_CMD_DATA gtPatternData[] =
    {
        {PATTERN_GEN0, 6, INIFINITE_SEND_COUNT, {{0, 2000}, {1, 600}, {0, 200}, {1, 1000},
        {0, 500}, {1, 10000}}}
    };

    int main(void)
    {
        int altCpuEngineType = ALT_CPU_PATTERN_GEN;
        PATTERN_GEN_INIT_DATA tInitData = { 0 };

        // Load Pattern Generator Engine on ALT CPU
        printf("Load Pattern Generator Engine\n”);
        ioctl(ITP_DEVICE_ALT_CPU, ITP_IOCTL_ALT_CPU_SWITCH_ENG, &altCpuEngineType);

        ioctl(ITP_DEVICE_ALT_CPU, ITP_IOCTL_INIT, NULL);

        printf("Init Pattern Generator Parameters\n”);
        tInitData.patternGenId = PATTERN_GEN0;
        tInitData.cpuClock = ithGetRiscCpuClock();
        tInitData.patternGenGpio = 86;
        tInitData.defaultGpioValue = GPIO_PULL_HIGH;
        tInitData.timeUnitInUs = 1;
        ioctl(ITP_DEVICE_ALT_CPU, ITP_IOCTL_INIT_PATTERN_GEN_PARAM, &tInitData);

        printf("Start Pattern Generator Write\n”);
        ioctl(ITP_DEVICE_ALT_CPU, ITP_IOCTL_PATTERN_GEN_WRITE_DATA, &gtPatternData[0]);
    }


.. raw:: latex

    \newpage
