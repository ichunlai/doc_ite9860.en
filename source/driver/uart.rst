﻿.. _uart:

Uart
=======

.. _uart_feature:

Function description
-----------------------------

The IT986x supports up to six UARTs (UART0 ~ 5) and three operation modes (Interrupt, DMA, FIFO). Each UART supports full duplex asynchronous communication and the following options :

- **baud rate:** The maximum supported rate is related to WCLK frequency. For example, if the WCLK is 99MHz (if you choose :file:`IT9860_396Mhz_DDR2_396Mhz.txt` initial script), then the maximum supported baud rate will be 99MHz/16= 6,187,500 bps.

  .. note::

      The actual WCLK frequency used can be obtained by ``ithGetBusClock()``.

- **word length:** See the following table for the combination of word length and number of stop bits supported.

  .. list-table::
     :header-rows: 1

     * - Character Length (Bits)
       - Number of Stop Bits
     * - 5
       - 1
     * - 6
       - 1
     * - 7
       - 1
     * - 8
       - 1
     * - 5
       - 1.5
     * - 6
       - 2
     * - 7
       - 2
     * - 8
       - 2

- **parity bit mode:** Supports No Parity, Odd Parity, Even Parity, Mark, and Space.

  Except for PIN19~26 and PIN38, any of the GPIO pins on the chip can be used as UART TX/RX. See the following table for details.


  .. list-table::
    :header-rows: 1

    * - Pin#
      - UART * 6
      - Pin#
      - UART * 6
      - Pin#
      - UART * 6
      - Pin#
      - UART * 6
    * - GPIO0
      -
      - GPIO17
      - Rx/Tx
      - GPIO34
      - Rx/Tx
      - GPIO51
      - Rx/Tx
    * - GPIO1
      -
      - GPIO18
      - Rx/Tx
      - GPIO35
      - Rx/Tx
      - GPIO52
      - Rx/Tx
    * - GPIO2
      -
      - GPIO19
      - Rx
      - GPIO36
      - Rx/Tx
      - GPIO53
      - Rx/Tx
    * - GPIO3
      -
      - GPIO20
      - Rx
      - GPIO37
      - Rx/Tx
      - GPIO54
      - Rx/Tx
    * - GPIO4
      - Rx/Tx
      - GPIO21
      - Tx
      - GPIO38
      -
      - GPIO55
      - Rx/Tx
    * - GPIO5
      - Rx/Tx
      - GPIO22
      - Tx
      - GPIO39
      - Rx/Tx
      - GPIO56
      - Rx/Tx
    * - GPIO6
      - Rx/Tx
      - GPIO23
      - Tx
      - GPIO40
      - Rx/Tx
      - GPIO57
      - Rx/Tx
    * - GPIO7
      - Rx/Tx
      - GPIO24
      - Tx
      - GPIO41
      - Rx/Tx
      - GPIO58
      - Rx/Tx
    * - GPIO8
      - Rx/Tx
      - GPIO25
      - Rx
      - GPIO42
      - Rx/Tx
      - GPIO59
      - Rx/Tx
    * - GPIO9
      - Rx/Tx
      - GPIO26
      - Rx
      - GPIO43
      - Rx/Tx
      - GPIO60
      - Rx/Tx
    * - GPIO10
      - Rx/Tx
      - GPIO27
      - Rx/Tx
      - GPIO44
      - Rx/Tx
      - GPIO61
      - Rx/Tx
    * - GPIO11
      - Rx/Tx
      - GPIO28
      - Rx/Tx
      - GPIO45
      - Rx/Tx
      - GPIO62
      - Rx/Tx
    * - GPIO12
      - Rx/Tx
      - GPIO29
      - Rx/Tx
      - GPIO46
      - Rx/Tx
      - GPIO63
      - Rx/Tx
    * - GPIO13
      - Rx/Tx
      - GPIO30
      - Rx/Tx
      - GPIO47
      - Rx/Tx
      - GPIO64
      - Rx/Tx
    * - GPIO14
      - Rx/Tx
      - GPIO31
      - Rx/Tx
      - GPIO48
      - Rx/Tx
      - GPIO65
      - Rx/Tx
    * - GPIO15
      - Rx/Tx
      - GPIO32
      - Rx/Tx
      - GPIO49
      - Rx/Tx
      - GPIO66
      - Rx/Tx
    * - GPIO16
      - Rx/Tx
      - GPIO33
      - Rx/Tx
      - GPIO50
      - Rx/Tx
      - GPIO67
      - Rx/Tx

  In addition, RS485 serial bus, which is a kind of UART interface, is often used in smart home application, so the API of UART is called directly by the bottom layer of the driver.

.. _uart_kconfig:

Related KConfig settings
-----------------------------

- :menuselection:`Peripheral --> UART Enable`

  Determines whether or not to enable the UART device. This option must be enabled for any device among UART0 ~ 5, otherwise the detailed configuration options for the individual UART device will not appear.

- :menuselection:`Peripheral --> UART Enable --> UART# Enable`

  Determines whether the UART# device to be enabled or not. After checking this option, the system will register the corresponding ``UART#`` device in the ``itpInit()`` function through the ``itpRegisterDevice()`` function and perform the initialization.

- :menuselection:`Peripheral --> UART Enable --> UART# Enable --> UART# mode`

  Determines the operation mode of the UART# device. The modes include FIFO, Interrupt, and DMA Mode.

  - FIFO mode: Also known as Polling mode. The UART driver running in this mode sends or receives data by continuously polling the status of the device. It is usually used when the sending and receiving speed is not fast. This mode is the only mode that can be used to send data during interrupt shutdown.
  - Interrupt mode: When UART module is the receiver, it can receive an interrupt every time it receives a character data or there is an error in the received data. As a transmitter, it can receive an interrupt notification when the UART FIFO is empty. This mode is usually used in the case of medium transmission/receiving speed.
  - DMA mode: When UART module is the receiver, it will attain the ring buffer function by using DMA LLP (linked list pointer) mode, so all received data will be temporarily stored in this ring buffer, and the size is currently set to 8KB. User can add :envvar:`CFG_UART_DMA_BUF_SIZE` size in the project's Kconfig to adjust the size. The user can decide when to take out the data by ``read()``, but it should be noted that when the data retrieval speed is lower than the write speed, the data at the front of the ring buffer may be overwritten. This mode is usually used when the speed of transmission and receipt is high.

- :menuselection:`Peripheral --> UART Enable --> UART# Enable --> UART# Baud Rate`

  Specifies the rate of transmission and reception, in bps.

- :menuselection:`Peripheral --> UART Enable --> UART# Enable --> RS485_# interface enable`

  Determines if the UART module is to be used as RS485.

- :menuselection:`GPIO --> UART# TX Pin`

  Specify which GPIO pin to be used as TX pin for this UART module.

- :menuselection:`GPIO --> UART# RX Pin`

  Specify which GPIO pin to be used as RX pin for this UART module.

- :menuselection:`Debug --> UART#`

  Specifies a Debug message to be output from a particular UART device. debug messages are messages output by calling ``printf()`` or ``ithPrintf()`` by the program. ``printf()`` will be registered to ``ITP_DEVICE_STD`` at bootloader time with the UART# selected in Kconfig, so using ``printf()`` will execute the write of ``ITP_DEVICE_STD``, which is the same as using the wirte of UART#. ``ithPrintf()`` is defined in :file:`<sdk_root>/sdk/driver/ith/ith_printf.c`, where you can see that the most important function ``ithPutcharFunc()`` will hook the send of the corresponding UART# mode in ``iteUartInit()``.

Related source code files
----------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - ``<sdk_root>/sdk/driver/itp/itp_uart.c``
    - The uppermost function mainly provides the POSIX specification implementation API for the upper AP to operate the device through ``open()``, ``close()``, ``ioctl()`` and other functions.
  * - ``<sdk_root>/sdk/driver/itp/itp_uart_api.c``
    - When building the Win32 simulator project, it is used to simulate the RS232 function on the actual board via the PC's COM port.
  * - | ``<sdk_root>/sdk/driver/uart/{chip}/*``
      | ``<sdk_root>/sdk/include/uart/uart.h``
      | ``<sdk_root>/sdk/include/uart/uart_*.h``
      | ``<sdk_root>/sdk/include/uart/interface/rs485.h``
      | ``<sdk_root>/sdk/include/ith/ith_uart.h``
      | ``<sdk_root>/sdk/driver/ith/ith_uart.c``

    - The underlying function, which may change in the future.

Description of related API
---------------------------------------

This section describes the APIs for operating UART devices. UART driver provides a POSIX compliant API that allows users to read/write UART devices via the ``open()``/``read()``/``write()``/``ioctl()`` functions.

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

   To register the device.

**Parameters**

``ITPDeviceType type``

   Device type. See :file:`<sdk_root>/sdk/include/ite/itp.h` for the full list of device types. The device types related to UART devices are ``ITP_DEVICE_UART0`` ~ ``ITP_DEVICE_UART5``.

``const ITPDevice *device``

   Device identifier. The device identifiers associated with UART devices are ``itpDeviceUart0`` ~ ``itpDeviceUart5``.

**Description**

This function is used to register the UART# device to the core device list, so that the upper AP can perform UART functionality via ``ioctl()``/``read()``/``write()`` functions. This function will be called by ``itpInit()`` function during system initialization (see :file:`<sdk_root>/sdk/driver/itp_init_openrtos.c`), so if ``itpInit()`` function has been called in your project, you do not need to run this registration function.

The following shows how to register the UART0 device to the system core and to initialize the UART0 device with the default settings (baud rate: 115200, character length: 8 bits, Parity: none, Stop bits: 1 bit).

.. code-block:: c

   itpRegisterDevice(ITP_DEVICE_UART0, &itpDeviceUart0);
   ioctl(ITP_DEVICE_UART0, ITP_IOCTL_INIT, NULL);

ioctl
^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

   To control the device.

**Parameters**

``int file``

   The device identifier. Its value is the value in the ITPDeviceType list. The device identifiers associated with UART devices are ``ITP_DEVICE_UART0`` ~ ``ITP_DEVICE_UART5``.

``unsigned long request``

   The *request* parameter is used to select the function to be run on the device and will depend on the assigned device.

``void *ptr``

   The *ptr* parameter represents the additional information needed to perform the requested function for this particular device. *ptr*'s type depends on the particular control request, but it can be an integer or a pointer to a specific data structure of the device.

**Description**

The ``ioct()`` function is used to perform various control functions on the UART device. *request* parameters and an optional third parameter (with different types) are passed to and interpreted by the corresponding part of the UART device associated with the *file* parameter.

The ``ioctl()`` command for the UART device, its parameters and the error status applicable to each individual command are described below.

The following ``ioctl()`` commands are applicable to all UART devices:

- **ITP_IOCTL_INIT**

   It is used to initialize the UART device. The pointer of UART_OBJ type must be passed through the ptr parameter to point to the UART device initialization parameter structure. the fields of UART_OBJ type are described as follows:

   - ITHUartPort  port

      The device handle. value should be one of ``ITH_UART0`` ~ ``ITH_UART5``. Note that this field cannot be filled with values from the ITPDeviceType list.

   - ITHUartParity parity

      The type of parity code. The value can be ``ITH_UART_NONE``, ``ITH_UART_ODD``, ``ITH_UART_EVEN``, ``ITH_UART_MARK``, or ``ITH_UART_SPACE``.

   - txPin

      The GPIO pin number corresponding to UART TX. Filling in -1 means UART TX is not enabled, while filling in a number greater than 0 means GPIO# is used as UART TX pin. For example, fill in 5 means GPIO5 will be used as UART TX pin.

   - rxPin

      The GPIO pin number corresponding to UART RX. Filling in -1 means UART RX is not enabled, while filling in a number greater than 0 means GPIO# is used as UART RX pin. For example, fill in 5 means GPIO5 will be used as UART RX pin.

   - baud

      baud rate is used to set the transmission rate in bps. The maximum rate supported is related to the WCLK frequency. See :ref:`uart_feature`.

   - timeout

      Timeout value in CPU tick. 0 means timeout mechanism is not enabled. It is used when the user wants the UART to have extra buffer time to wait for the system to send out correctly when it is busy.

   - mode

      Operation mode. Its value can be ``UART_INTR_MODE``, ``UART_DMA_MODE``, or ``UART_FIFO_MODE``. Please refer to :ref:`uart_kconfig` for the difference of different operation modes.

   - forDbg

      It specifies whether a UART device is a debug message output device or not. If its value is non-zero, it means this UART device will be debug message output device, so when there is a program calling ``printf()`` or ``ithPrintf()`` function, the message will be output through this device. If the value is 0, the UART device will not be used as debug message output device.

      If ``CFG_DBG_UART#`` macro is defined by Kconfig, when the system to initialize the corresponding UART# device by calling this function during the startup phase, it will internally set the ``forDbg`` field of the ``UART_OBJ`` type structure of the corresponding UART# device to 1 and initializing the device. For details, please refer to the ``INIT_UART_OBJ_DEFAULT`` macro in :file:`<sdk_root>/sdk/include/uart/uart.h` file and the ``iteUartInit()`` function in :file:`<sdk_root>/sdk/driver/uart/it9860/uart.c`` file.

      If multiple UART devices are set as debug message output devices, only the device lastly set in the time sequence will be used as the debug message output device. It has not been verified whether there are any problems with setting multiple UART devices as debug message output devices, so this is not recommended.

   The following shows how to initialize the UART0 device by filling the ``UART_OBJ`` type variable and using the ``ioctl()`` function.

   .. code-block:: c

      UART_OBJ    UartInitParam = {};
      UartInitParam.port      = ITH_UART0;
      UartInitParam.parity    = ITH_UART_NONE;
      UartInitParam.txPin     = 5;
      UartInitParam.rxPin     = 6;
      UartInitParam.baud      = 115200;
      UartInitParam.timeout   = 0;
      UartInitParam.mode      = UART_DMA_MODE;
      UartInitParam.forDbg    = false;

      itpRegisterDevice(ITP_DEVICE_UART0, &TEST_DEVICE);
      ioctl(ITP_DEVICE_UART0, ITP_IOCTL_INIT, (void *)&UartInitParam);


   If the *ptr* parameter value is NULL, the UART device will be initialized with the default settings. The default setting is the value that the user fills in Kconfig.

   The following shows how to initialize the UART0 device by default settings.

   .. code-block:: c

      itpRegisterDevice(ITP_DEVICE_UART0, &TEST_DEVICE);
      ioctl(ITP_DEVICE_UART0, ITP_IOCTL_INIT, NULL);


- **ITP_IOCTL_RESET**

     The same as ``ITP_IOCTL_INIT``, except that ``ITP_IOCTL_RESET`` will first Terminate the UART device and then reinitialize it. Therefore, the type and usage of the *ptr* parameter are exactly the same as those of ``ITP_IOCTL_INIT``.

     The following shows how to reset the UART0 device by filling the UART_OBJ type variable and using the ``ioctl()`` function.

     .. code-block:: c

        UART_OBJ UartInitParam  = {};
        UartInitParam.port      = ITH_UART0;
        UartInitParam.parity    = ITH_UART_NONE;
        UartInitParam.txPin     = 5;
        UartInitParam.rxPin     = 6;
        UartInitParam.baud      = 115200;
        UartInitParam.timeout   = 0;
        UartInitParam.mode      = UART_DMA_MODE;
        UartInitParam.forDbg    = false;

        ioctl(ITP_DEVICE_UART0, ITP_IOCTL_RESET, (void *)&UartInitParam);


     The following shows how to reset the UART0 device by default settings.

     .. code-block:: c

         ioctl(ITP_DEVICE_UART0, ITP_IOCTL_RESET, NULL);

- **ITP_IOCTL_REG_UART_CB**

     It is used to mount the UART RX Interrupt Service Routine(ISR). This function is only available if the operation mode of the UART device is Interrupt mode. The ISR mounted here will be called every time a character is received. The *ptr* parameter is the ISR to be mounted.

     The following shows how to mount the ISR and what actions are usually performed within the UART RX ISR.

     .. code-block:: c

        static sem_t UartSemIntr;

        void UartRxCallback(void)
        {
            sem_post(&UartSemIntr);
        }

        int main(void)
        {
            sem_init(&UartSemIntr, 0, 0);
            ...
            ioctl(TEST_PORT, ITP_IOCTL_REG_UART_CB, (void*)UartRxCallback);

            while(1)
            {
               ...
               sem_wait(&UartSemIntr);
               ...
            }
        }

read
^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __nbyte)

   To read data from UART RX.

**Parameters**

``int __fd``

  The device identifier. Its value is the value in the ITPDeviceType list. The device identifiers associated with UART devices are ``ITP_DEVICE_UART0`` ~ ``ITP_DEVICE_UART5``.

``void *__buf``

  The indicator pointing to the receive data buffer.

``size_t __nbyte``

  The number of maximum bytes of data to be received.

**Description**

This function is used to read up to *__nbyte* characters from the UART RX into the buffer that *__buf* points to, and return the actual number of bytes received.

The following shows how to use the function:

.. code-block:: c

   char getstr[256] = "";
   int len = 0, count = 0;
   len = read(ITP_DEVICE_UART0, getstr + count, 256);

write
^^^^^

.. c:function:: int write(int __fd, const void *__buf, size_t __nbyte)

  To send data from UART TX.

**Parameters**

``int __fd``

  The device identifier. Its value is the value in the ITPDeviceType list. The device identifiers associated with UART devices are ``ITP_DEVICE_UART0`` ~ ``ITP_DEVICE_UART5``.

``void *__buf``

  The indicator pointing to the send data buffer.

``size_t __nbyte``

  The number of bytes of data to be sent.

**Description**

This function is used to send the data in the buffer that *__buf* points to via UART TX up to *__nbyte* characters, and return the actual bytes sent.

The following shows how to use the function:

.. code-block:: c

   char sendstr[256] = "iTE";
   int len = 0;
   len = write(ITP_DEVICE_UART0, sendstr, 3);

Basic example program
----------------------------------------------------------------

.. code-block:: c

    #include "ite/itp.h"

    static sem_t    UartSemIntr;

    void UartCallback(void)
    {
        sem_post(&UartSemIntr);
    }

    int main(void)
    {
        int i;
        char getstr[256];
        char sendtr[256];
        int len = 0;

        /* if in rs485 interface then use RS485_OBJ instead */
        UART_OBJ UartInitParam	= {};
        UartInitParam.port	= ITH_UART0;
        UartInitParam.parity	= ITH_UART_NONE;
        UartInitParam.txPin	= 5;
        UartInitParam.rxPin	= 6;
        UartInitParam.baud	= 115200;
        UartInitParam.timeout	= 0;
        UartInitParam.mode	= UART_INTR_MODE;
        UartInitParam.forDbg	= false;

        printf("Start uart test!\n”);
        sem_init(&UartSemIntr, 0, 0);
        ioctl(TEST_PORT, ITP_IOCTL_RESET, (void*)&UartInitParam);     // use UART structure and reset
        ioctl(TEST_PORT, ITP_IOCTL_REG_UART_CB, (void*)UartCallback); // set callback function

        while(1)
        {
            sem_wait(&UartSemIntr);

            len = read(ITP_DEVICE_UART1,getstr,256);
            if (len > 0)
            {
                printf("uart read: %s\n”,getstr);
                sprintf(sendtr,"uart write: read str = %s",getstr);
                write(ITP_DEVICE_UART1,sendtr,256);
            }
        }
    }


Complete Example – TTL
--------------------------------------------

A sample program of UART is attached to the directory :file:`<sdk_root>/project/test_uart`. This program shows a UART echo function. By default, for every 9 characters of data received from UART0 RX, a ``\n`` character is added to the end of each data and sent out via UART0 TX. In addition, UART1 TX is also used to output debug message.

We want to send data out via UART0 TX for every character received from UART0 RX, so please modify the UartCommandLen macro specified in the :file:`<sdk_root>/project/test_uart/test_uart.c` file, and change

.. code-block:: c

  #define UartCommandLen 9

to

.. code-block:: c

  #define UartCommandLen 1


We also want the program not to actively add a ``\n`` character to the end of each data entry, so please change the following statements in the ``TestFuncUseINTR()``, ``TestFuncUseDMA()``, ``TestFuncUseFIFO()`` functions

.. code-block:: c

  sendstr[count] = '\n';


to

.. code-block:: c

  // sendstr[count] = '\n';


Hardware Connection
^^^^^^^^^^^^^^^^^^^^

Here we use a USB to TTL board to connect to the PC, and

- Connect the TX of the board (the pin connected by the yellow DuPont cable on the upper left of the picture below) to the GPIO5 of the EVB (the pin connected by the yellow DuPont cable on the right of the picture below, which we will set as UART0 RX of the EVB)
- Connect the RX of the converter board (the pin connected by the blue DuPont wire at the upper left of the picture below) to GPIO6 of the EVB (the pin connected by the blue DuPont wire at the right of the picture below, which we will set as UART0 RX of the EVB)
- Connect the GND of the converter board (the pin connected by the black DuPont wire at the upper left of the picture below) to the GPIO6 of the EVB (the pin connected by the black DuPont wire at the upper right of the picture below)

  .. image:: /_static/evb_ttl_example.png

KConfig settings
^^^^^^^^^^^^^^^^^^^^^^^^^

Please execute :file:`<sdk_root>/build/openrtos/test_uart.cmd` and follow these steps to set it up:

- Check :menuselection:`Peripheral --> UART Enable`.
- Check :menuselection:`Peripheral --> UART Enable --> UART0 Enable`.
- Select the operation mode of UART0 at :menuselection:`Peripheral --> UART0 Enable --> UART0 mode`. Here we select ``UART0 DMA Enable``.
- Set :menuselection:`Peripheral --> UART Enable --> UART0 Baud Rate` to 9600.
- Check :menuselection:`Peripheral --> UART Enable --> UART1 Enable`.
- •	Select the operation mode of UART1 at :menuselection:`Peripheral --> UART1 Enable --> UART1 mode`. Here we select ``UART1 Interrupt Enable``.
- Set :menuselection:`Peripheral --> UART Enable --> UART1 Baud Rate` to 115200.

  .. image:: /_static/qconf_peripheral_uart.png

Next, set the pins used by UART0 and UART1.

- Set :menuselection:`GPIO --> UART0 RX Pin` to 5.
- Set :menuselection:`GPIO --> UART0 TX Pin` to 6.
- Set :menuselection:`GPIO --> UART1 TX Pin` to 4.

  .. image:: /_static/qconf_gpio_uart.png

  .. note::

    The reason why UART1 TX pin is set to GPIO4 is that in the EVB circuit diagram, GPIO4 of the IT986x is connected to the RX (labeled as DBG_TX in the circuit diagram) of the USB to SPI board.

    .. image:: /_static/e_circuit_uart.png

Next, set UART1 as the Debug message output device.

- Set :menuselection:`Debug --> Debug Message Device` to UART1.

  .. image:: /_static/qconf_check_debug_uart1.png

To verify the results
^^^^^^^^^^^^^^^^^^^^^^^^^

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

1. Click the |build_icon| button to build the project. Ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Reset the power of EVB.
3. When the program is finished building, click the |run_icon| button to boot up the system via USB to SPI board.
4. Execute Tera Term, and click :menuselection:`Setup --> Serial Port` in the main menu of Tera Term. For Port, please select the com port corresponding to USB to TTL board. For Speed, please select 9600.

  .. image:: /_static/teraterm_set_speed_9600.png

5. Click New setting and type any text in the black area of the Tera Term window and you will see that it will feed back whatever you type.

  .. image:: /_static/teraterm_uart_echo_example.png

.. attention::

  1. If the window is still blank after you enter something, please repeat the whole procedure and check if the wiring is correct.
  2. Here we choose the operation mode of UART0 as DMA mode, so we will run ``TestFuncUseDMA()`` in :file:`<sdk_root>/project/test_uart/test_uart.c` to perform the whole test. If the operation mode you choose is Interrupt mode, ``TestFuncUseINTR()`` of :file:`<sdk_root>/project/test_uart/test_uart.c` will be run. If the operation mode you choose is FIFO mode, then ``TestFuncUseFIFO()`` of :file:`<sdk_root>/project/test_uart/test_uart.c` will be run.



Complete example – RS232
--------------------------------------------

A sample program of UART is attached to the :file:`<sdk_root>/project/test_uart` directory. This program shows a UART echo function. By default, for every 9 characters of data received from UART0 RX, a ``\n`` character is added to the end of each data entry and sent out via UART0 TX. In addition, UART1 TX is used to output debug message.

We want to send data out via UART0 TX for every character received from UART0 RX, so please modify the ``UartCommandLen`` macro specification in the :file:`<sdk_root>/project/test_uart/test_uart.c` file, and change

.. code-block:: c

  #define UartCommandLen 9


to

.. code-block:: c

  #define UartCommandLen 1


We also want the program not to automatically add a ``\n`` character to the end of each data entry, so please change the following statements in the ``TestFuncUseINTR()``, ``TestFuncUseDMA()``, ``TestFuncUseFIFO()`` functions

.. code-block:: c

  sendstr[count] = '\n';


to

.. code-block:: c

  // sendstr[count] = '\n';


Hardware Connection
^^^^^^^^^^^^^^^^^^^^^^^

Here we use an RS232 converter board to connect to the PC, and

- Connect the RX and TX of the DB9 from the PC to the converter board.
- Connect the TX of the board to GPIO5 of the EVB.
- Connect the RX of the board to GPIO6 of the EVB.

  .. image:: /_static/evb_rs232_example.png

  .. image:: /_static/evb_rs232_connect_example.png


KConfig Setting
^^^^^^^^^^^^^^^^^^^^^^^^^

Please execute :file:`<sdk_root>/build/openrtos/test_uart.cmd` and follow the steps below to set it up:

- Check :menuselection:`Peripheral --> UART Enable`.
- Check :menuselection:`Peripheral --> UART Enable --> UART0 Enable`.
- Select the operation mode of UART0 at :menuselection:`Peripheral --> UART0 Enable --> UART0 mode`, here we select ``UART0 DMA Enable``.
- Set :menuselection:`Peripheral --> UART Enable --> UART0 Baud Rate` to 9600.
- Check :menuselection:`Peripheral --> UART Enable --> UART1 Enable`.
- Select the operation mode of UART1 at :menuselection:`Peripheral --> UART1 Enable --> UART1 mode`, here we select ``UART1 Interrupt Enable``.
- Set :menuselection:`Peripheral --> UART Enable --> UART1 Baud Rate` to 115200.

  .. image:: /_static/qconf_peripheral_uart.png

Next, set the pins used by UART0 and UART1.

- Set :menuselection:`GPIO --> UART0 RX Pin` to 5.
- Set :menuselection:`GPIO --> UART0 TX Pin` to 6.
- Set :menuselection:`GPIO --> UART1 TX Pin` to 4.

  .. image:: /_static/qconf_gpio_uart.png

  .. note::

    The reason why UART1 TX pin is set to GPIO4 is that in the EVB circuit diagram, GPIO4 of the IT986x is connected to the RX (labeled as DBG_TX in the circuit diagram) of the USB to SPI adapter.

    .. image:: /_static/e_circuit_uart.png

Next, set UART1 as the Debug message output device.

- Set :menuselection:`Debug --> Debug Message Device` to UART1.

  .. image:: /_static/qconf_check_debug_uart1.png

To verify the results
^^^^^^^^^^^^^^^^^^^^^^^^^

1. Click the |build_icon| button to build the project. Please ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Reset the power of EVB.
3. When the program is finished building, click the |run_icon|  button to boot up the system via USB to SPI board.
4. Execute Tera Term, and click :menuselection:`Setup --> Serial Port` in the main menu of Tera Term. For Port, please select the com port corresponding to USB to TTL board. For Speed, please select 9600.

  .. image:: /_static/teraterm_set_speed_9600.png

5. Click :guilabel:`New setting` and type any text in the black area of the Tera Term window and you will see that it will feed back whatever you type.

  .. image:: /_static/teraterm_uart_echo_example.png

.. attention::

  1. If the window is still blank after you enter something, please repeat the whole procedure and check if the wiring is correct.
  2. Here we choose the operation mode of UART0 as DMA mode, so we will run ``TestFuncUseDMA()`` in :file:`<sdk_root>/project/test_uart/test_uart.c` to perform the whole test. If the operation mode you choose is Interrupt mode, ``TestFuncUseINTR()`` of :file:`<sdk_root>/project/test_uart/test_uart.c` will be run. If the operation mode you choose is FIFO mode, then ``TestFuncUseFIFO()`` of :file:`<sdk_root>/project/test_uart/test_uart.c` will be run.


Complete example – RS485
--------------------------------------------

A sample program of UART is attached to the :file:`<sdk_root>/project/test_uart` directory. This program shows a UART echo function. By default, for every 9 characters of data received from UART0 RX, a ``\n`` character is added to the end of each data entry and sent out via UART0 TX. In addition, UART1 TX is used to output debug message.

We want to send data out via UART0 TX for every character received from UART0 RX, so please modify the UartCommandLen macro specification in the :file:`<sdk_root>/project/test_uart/test_uart.c` file, and change

.. code-block:: c

  #define UartCommandLen 9


to

.. code-block:: c

  #define UartCommandLen 1


We also want the program not to automatically add a ``\n`` character to the end of each data entry, so please change the following statements in the ``TestFuncUseINTR()``, ``TestFuncUseDMA()``, ``TestFuncUseFIFO()`` functions

.. code-block:: c

  sendstr[count] = '\n';


to

.. code-block:: c

  // sendstr[count] = '\n';


Hardware Connection
^^^^^^^^^^^^^^^^^^^^^^^

Here we use an RS485 converter board to connect with PC, and

- Connect the DB9 from the PC to the RS232toRS485 converter, and then connect the DA and DB to the converter board.
- Connect the TX of the converter board to the GPIO40 of the EVB.
- Connect the RX of the converter board to GPIO41 of the EVB.

Since the signal will change back to RS232 after the conversion by RS485 converter, the ENABLE pin is not needed.

  .. image:: /_static/evb_rs485_example.png

  .. image:: /_static/evb_rs485_connect_example.png

KConfig setting
^^^^^^^^^^^^^^^^^^^^^^^^^

Please run :file:`<sdk_root>/build/openrtos/test_uart.cmd` and follow these steps to set it up:

- Check :menuselection:`Peripheral --> UART Enable`.
- Check :menuselection:`Peripheral --> UART Enable --> UART0 Enable`.
- Select the operation mode of UART0 at :menuselection:`Peripheral --> UART0 Enable --> UART0 mode`, here we select ``UART0 DMA Enable``.
- Check :menuselection:`Peripheral --> UART Enable --> RS485_0_interface enable`.
- Set :menuselection:`Peripheral --> UART Enable --> UART0 Baud Rate` to 115200.
- Check :menuselection:`Peripheral --> UART Enable --> UART1 Enable`.
- Select the operation mode of UART1 at :menuselection:`Peripheral --> UART1 Enable --> UART1 mode`, here we select ``UART1 Interrupt Enable``.
- Set :menuselection:`Peripheral --> UART Enable --> UART1 Baud Rate` to 115200.


  .. image:: /_static/qconf_peripheral_uart_rs485_example.png

Next, set the pins used by UART0 and UART1.

- Set :menuselection:`GPIO --> UART0 RX Pin` to 40.
- Set :menuselection:`GPIO --> UART0 TX Pin` to 41.
- Set :menuselection:`GPIO --> UART1 TX Pin` to 4.

  .. image:: /_static/qconf_gpio_uart_rs485_example.png

Next, set UART1 as the Debug message output device.

- Set :menuselection:`Debug --> Debug Message Device` to UART1.

  .. image:: /_static/qconf_check_debug_uart1.png

To verify the results
^^^^^^^^^^^^^^^^^^^^^^^^^

1. Click the |build_icon| button to build the project. Please ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Reset the power of EVB.
3. When the program is finished building, click the |run_icon|  button to boot up the system via USB to SPI board.
4. Execute Tera Term, and click :menuselection:`Setup --> Serial Port` in the main menu of Tera Term. For Port, please select the com port corresponding to USB to TTL board. For Speed, please select 9600.

  .. image:: /_static/teraterm_set_speed_9600.png

5. Click :guilabel:`New setting` and type any text in the black area of the Tera Term window and you will see that it will feed back whatever you type.

  .. image:: /_static/teraterm_uart_echo_example.png

.. attention::

  1. If the window is still blank after you enter something, please repeat the whole procedure and check if the wiring is correct.
  2. Here we choose the operation mode of UART0 as DMA mode, so we will run ``TestFuncUseDMA()`` in :file:`<sdk_root>/project/test_uart/test_uart.c` to perform the whole test. If the operation mode you choose is Interrupt mode, ``TestFuncUseINTR()`` of :file:`<sdk_root>/project/test_uart/test_uart.c` will be run. If the operation mode you choose is FIFO mode, then ``TestFuncUseFIFO()`` of :file:`<sdk_root>/project/test_uart/test_uart.c` will be run.


.. raw:: latex

    \newpage
