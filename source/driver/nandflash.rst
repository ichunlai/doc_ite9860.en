.. _nandflash:

NAND Flash
================

Function description
---------------------------

NAND as a storage device has AXISPI and SPI as I/O interfaces on the hardware (IT986X does not support NAND with Parallel interface), Reserved Area for raw data such as Boot-Loader and boot-Image, and Disk Area with file system on the software. Since NAND specifications vary, before using, it is important to check whether the electrical characteristics of AC/DC Characteristics match and whether the NAND CMD is consistent with the definition of Mask ROM and NAND driver. For NAND devices verified on IT986x, refer to :ref:`nandflash_support_list`.

The software architecture of NAND module is basically built on file system and FTL (Flash Translation Layer), the file system calls the NAND module driver through FTL, and the NAND driver is written according to the API developed on FTL. However, the SDK does not provide the FTL source code, only the Library. NAND is usually used for accessing non-frequent reads and writes such as Bootloader and Boot-Image, because reading and writing NAND Flash through the ITP interface is not protected by the FTL's Wear leveling algorithm. If the read and write is too frequent, it may cause bad blocks in NAND Flash.

Since NAND Flash uses SPI as the interface, the underlying Driver calls SPI API to access NAND Flash. IT986x chip supports high-speed AXISPI and low-speed SPI (SPI0/SPI1). Depending on the project application, SPI NAND can be connected to either AXISPI or general SPI0/SPI1, but only AXISPI supports NAND boot function.

The following is an introduction to AXISPI, SPI, and data partition:

- AXISPI:

  Higher speed SPI controller with SPI clock up to 66 Mhz to 80 Mhz and support QUAD operation mode. The maximum theoretical transfer rate is (80 Mhz / 8) * 4 = 40 MB/s, and the actual speed is about 6~10MB/s. IT986x only supports AXISPI NOR booting.

- SPI0/SPI1:

  For the lower speed SPI controller, the IT986x provides two SPI controllers (SPI0 and SPI1) with SPI clock up to 20 Mhz, Single I/O operation mode. The maximum theoretical transfer rate is (20 Mhz / 8) = 2.5 MB/s, and the actual speed is about 1.8~2.2MB/s. SPI controller does not support boot function.

- Reserved Area:

  The data area is divided into "Reserved Area" and "Disk Area." "Reserved Area" is not controlled by the File System, and this area is mainly used to store Bootloader, Boot-Image, backup-pkg, MAC address, or other USER-defined data. This area has a simple bad block management mechanism, so there is no problem of bad block misuse. However, this area is not protected by the wear leveling algorithm, so it should be avoided to read and write too often, which is suitable for data like Bootloader or Boot-Image that is only read once per boot or rarely updated.

  There is a limitation of Reserved Area, that is, the size of Reserved Area can not be adjusted dynamically. For example, if a NAND is planned to have 16MB Reserved Area, this NAND can only maintain 16MB all the time, and cannot be changed to 12MB or 24MB in the middle. If you want to change the Reserved Area Size, you have to replace the NAND with a new one. So please plan a larger Reserved Area Size or calculate the required Reserved Area Size at the beginning of the development stage to avoid frequent replacement of a new NAND, which will affect the development progress.

  - Bootloader: Bootloader is related to NAND boot, the starting position of the bootloader is fixed in Page0 of NAND Block0 (i.e. position = 0x000000). IT986x will decide the boot DEVICE according to BOOT-CONFIG. If during boot process it is found that there is no Bootloader stored in NAND, then boot failure will be reported.

  - Boot-Image: This is the application program of the project, which is fixed in a specified location in the Reserved Area, and the USER should provide enough space to store the Boot-Image according to the project content. Because the NAND has the problem of Bad Block, it is usually recommended to plan twice the Boot-Image size to store the Boot-Image (e.g. Boot-Image size=2MB, BOOTLOADER=350KB, then RESERVED AREA is recommended to be 4MB+1MB and round up to 8MB, and BOOT-IMAGE POSITION to be set to 1MB (0x100000) location, so that BOOT-IMAGE can still be used when it expands to 6MB).

- Storage(disk/partition):

  This area is controlled by FILE SYSTEM, which contains the bad block management and WEAR LEVELING function of NAND to avoid premature bad block problem.

Related KConfig settings
----------------------------------------------------------------

Regarding the application plans for NAND, there are single NAND applications (i.e., using AXISPI to connect to SPI NAND) and NOR+NAND applications (there are three combinations, i.e., "AXISPI-NOR + AXISPI-NAND", "AXISPI-NOR + SPI0-NAND", and "AXISPI-NOR + SPI1-NAND").

For general application configuration of single SPI NAND, you need to enable AXISPI and set the GPIO PIN of AXISPI, and then check the NAND Device. This section will focus on NAND configuration.

.. image:: /_static/qconf_storage_nand.png

- :menuselection:`Storage --> NAND Device`

  To determine if the NAND device is to be enabled. This option must be enabled for any device between UART0 ~ 5, otherwise the detailed configuration options for the individual UART device will not appear.

- :menuselection:`Storage --> NAND Device --> NAND Interface Type`

  Please ignore this option first. Currently only SPI NAND is available in the SDK.

- :menuselection:`Storage --> NAND Device --> SPI NAND Choise SPI Bus`

  To determine the SPI module to be used with SPI NAND. Please select AXISPI for single SPI NAND.

- :menuselection:`Storage --> NAND Device --> NAND Page Size`

  To set the Page Size (in Byte) of the NAND.

- :menuselection:`Storage --> NAND Device --> NAND Block Size`

  To set the Block Size (in page) of the NAND.

- :menuselection:`Storage --> NAND Device --> Enable SPI NAND Boot`

  To set whether to turn on the boot function of AXISPI NAND.

- :menuselection:`Storage --> NAND Device --> Enable SPI NAND Boot --> Select SPI NAND QUAD Mode Address Type`

  To set the SPI address format used for SPI NAND in Quad Mode, one is to use three bytes (GD5FxGQ4xB/GD5FxGQ4xE for GigaDevice, and W25N01GVxxIG for Winbond, and SPI NAND for Micron, Macron, Winbond, Toshiba, and Core), and the other is to use four bytes (GD5FxGQ4xC/GD5FxGQ4xF for GigaDevice, and W25N01GVxxIT for Winbond). This setting will determine whether the NAND BOOT can successfully boot in Quad Mode. However, it will not cause boot failure, but only affect the boot speed.

The above settings can be configured once through the file in :file:`<sdk_root>/build/_presettings` directory. You can set it directly by clicking the :guilabel:`LOAD` icon (see below)

.. image:: /_static/qconf_load.png

Or click on :guilabel:`LOAD` from the :guilabel:`FILE` menu (as shown below)

.. image:: /_static/qconf_file_load.png

Download the file :file:`_config_spi_nand_boot` from the :file:`<sdk_root>/build/_presettings` directory. After downloading, it will automatically configure the AXISPI and SPI NAND related settings and enable the NAND BOOT function.

NOR + NAND configuration planning
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

- AXISPI-NOR + SPI0-NAND

  Please enable both AXISPI and SPI0 (see AXISPI Settings and SPI0 Settings), and then configure NOR and NAND. Please refer to :ref:`norflash` for NOR configuration. For NAND configuration, you need to uncheck Enable SPI NAND Boot and click :menuselection:`Storage --> NAND Device --> SPI NAND Choice SPI Bus`. To quickly load the _PRESETTINGS function, select the file :file:`_config_spi_nand` (the default file without the NAND BOOTING function).  Then select :guilabel:`SPI NAND SPI0` in the :guilabel:`Enable SPI NAND Boot` option.

- AXISPI-NOR + SPI1-NAND

  Please enable both AXISPI and SPI0 (see AXISPI Settings and SPI0 Settings), and then configure NOR and NAND. Please refer to :ref:`norflash` for NOR configuration. For NAND configuration, you need to uncheck :guilabel:`Enable SPI NAND Boot` and click :menuselection:`Storage --> NAND Device --> SPI NAND Choise SPI Bus`. To quickly load the _PRESETTINGS function, select the file _config_spi_nand (the default file without the NAND BOOTING function).  Then select :guilabel:`SPI NAND SPI1` in the :guilabel:`Enable SPI NAND Boot` option.

Partitioning of File System and NAND
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. image:: /_static/qconf_file_system_nand.png

The above picture shows the general SPI NAND partition planning, where NAND reserved Size: 0x1000000 means that the first 16MB of NAND space is planned as :guilabel:`Reserved Area`, and this 16MB of space is not controlled by the File System. This area can only be accessed correctly through the NAND Driver of ITP. There are at least two kinds of data in this area, one is Bootloader and the other is boot-Image. The location of Bootloader is fixed at 0, while the location of boot-Image is set at :menuselection:`Upgrade --> Upgrade Image --> Image position`, which is generally set to 3MB (0x300000) by default.

.. image:: /_static/qconf_upgrade_upgrade_image.png

All the partitions after the Reserved Area area of NAND are all within the scope of NAND Disk Partition. The above picture shows that this area is divided into Partition0~Partition3, a total of 4 partitions, which is to correspond to the 4 The drive of xxxxx disk partitions: A ~ D, partition0 corresponds to A, partition1 to B, partition2 to C and partition3 to D. The size of each partition has a corresponding size value, and of course the total size cannot exceed the capacity of NAND Disk Partition (i.e., the total capacity of NAND minus the size of reserved area). Each partition size should be planned in Block Size (i.e. 0X20000 or 0x40000, the correct number depends on the specification of NAND used by USER). Once the size is set wrongly, it will cause the File System to fail to partition because the sum is larger than the actual size provided by NAND, or waste space because the size is smaller than the size provided by NAND.

Related source code files
---------------------------

.. list-table::
   :header-rows: 1

   * - Path
     - Description
   * - ``<sdk_root>/sdk/driver/itp/itp_nand.c``
     - The uppermost layer function. It mainly provides POSIX compliant API for the upper-layer AP to operate the device through ``open()``, ``close()``, ``ioctl()`` and other functions.
   * - ``<sdk_root>/sdk/driver/nand/*.*``
     - Programs related to NAND driver
   * - ``<sdk_root>/sdk/driver/nand/nf_spi_nand.c``
     - The DRIVER of SPI NAND bottom layer, which takes over the API of FTL layer to call :file:`spinfdrv.c` downwards.
   * - ``<sdk_root>/sdk/driver/ith/spinfdrv.c``
     - The driver of the SPI NAND bottom layer, which takes over the API of :file:`nf_spi_nand.c` from the upward side, and calls the SPI API downward to implement the Read/Write/Erase/Read ID and other commands of the SPI NAND.
   * - ``<sdk_root>/sdk/driver/spi/it9860/axispi/mmp_axispi.c``
     - Programs related to AXISPI driver
   * - ``<sdk_root>/sdk/driver/spi/it9860/spi/mmp_spi.c``
     - Programs related to SPI driver


Description of related API
---------------------------------------

DEVICE ID: ITP_DEVICE_NAND (refer to :file:`<sdk_root>/sdk/include/ite/itp.h`)

This section describes the APIs for operating NAND devices. NAND driver provides a POSIX compliant API that allows users to to perform operations like reading and writing on NAND devices via the ``open()``/``read()``/``write()``/``ioctl()``.

itpRegisterDevice
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: void itpRegisterDevice(ITPDeviceType type, const ITPDevice *device)

  To register the device, i.e. ``itpRegisterDevice(ITP_DEVICE_NAND, &itpDeviceNand)``.

**Parameters**

``ITPDeviceType type``

  Device type. For a full list of device types, see :file:`<sdk_root>/sdk/include/ite/itp.h`. The device type associated with NAND devices is ``ITP_DEVICE_NAND``.

``const ITPDevice *device``

  Device identifier. The device identifier associated with a NAND device is ``itpDeviceNand``.

**Description**

This function can be used to register the NAND device to the core device list, so that the upper layer AP can operate the NAND functionality through ``ioctl()``/``read()``/``write()`` functions. This function will be called by the ``itpInit()`` function during system initialization (see :file:`<sdk_root>/sdk/driver/itp_init_openrtos.c`), so if ``itpInit()`` has been called in your project, you do not need to run this registration function again.

The following shows how to register a NAND device to the system core and initialize the NAND device.

.. code-block:: c

    itpRegisterDevice(ITP_DEVICE_NAND, &itpDeviceNand);
    ioctl(ITP_DEVICE_NAND, ITP_IOCTL_INIT, NULL);

ioctl
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int ioctl(int file, unsigned long request, void *ptr)

  To control the device

**Parameters**

``int file``

  Device identifier. Its value is the value in the ``ITPDeviceType`` list. The device identifier associated with the UART device is ``ITP_DEVICE_NAND``.

``unsigned long request``

  The *request* parameter is used to select the function to be run on the device and will depend on the specified device.

``void *ptr``

  The *ptr* parameter represents the additional information required to perform the requested function on this particular device. The type of *ptr* depends on the particular control request, but it can be an integer or a pointer to a specific data structure of the device.

**Description**

The ``ioctl()`` function is used to perform various controls on the NAND device. *request* parameters and an optional third parameter (with different types) are passed to and interpreted by the corresponding part of the UART device associated with the file.

The ``ioctl()`` command for NAND devices, the parameters and the error states applicable to each individual command are described below.

- ITP_IOCTL_INIT

  To initialize the NAND device. *ptr* parameter is NULL.

- ITP_IOCTL_GET_BLOCK_SIZE

  Call this function to get the Block Size defined by the NAND in the driver, and the ptr parameter is passed to the variable location (e.g. &BlockSize).

- ITP_IOCTL_FLUSH

  If KCONFIG enables the FAT file system function, it will force the data in the FAT cache to be written back to NAND. The current SDK has integrated the NAND Flush mechanism into File Close, so whenever the File System's File Close is executed, the NAND Flush action is completed at the same time.

read
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int read(int __fd, void *__buf, size_t __nBlk)

  To read data from RX.

**Parameters**

``int __fd``

  The NAND device to be executed. E.g. ``ITP_DEVICE_NAND``.

``void *__buf``

  The location to write back after reading. The minimum unit of NAND buffer size is Block Size, that is, Buffer Size should be an integral multiple of Block Size. Block Size should be obtained from ``ITP_IOCTL_GET_BLOCK_SIZE`` of ``ioctl()``, and this size will not be exactly an integral multiple of 0X20000, but similar to 0X1FFFFC or 0X3FFFFC. Please use this size to configure the buffer size, and refer to ``LoadImage()`` of  :file:`<sdk_root>/project/bootloader/boot.c` or the ITP NAND Read/Write example at the end of this section for programs.

``size_t __nBlk``

  The length of the data to be read is in Block Size (see the description of ``void *__buf`` at previous section).

**Description**

This function allows users to read back data from SPI NAND.

The following shows how to use the function.

.. code-block:: c

    int fd = -1;
    uint8_t *buffer;
    uint32_t blocksize = 0;
    uint32_t pos = 0;
    int RdLen = 1;  // read one block

    fd = open(":nand", O_RDONLY, 0);    // open NAND device
    ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize);    // get NAND block size
    buffer = (uint8_t *)malloc(blocksize);  // locate a buffer with “blocksize”
    lseek(fd, pos, SEEK_SET);   // seek position to block 0
    read(fd, buffer, RdLen);	// read one block data(one block has “blocksize” bytes)
    close(fd);	// close NAND device

write
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. c:function:: int write(int __fd, const void *__buf, size_t __nbyte)

  To write data to SPI NAND.

**Parameters**

``int __fd``

  The NAND device to be executed. E.g. ``ITP_DEVICE_NAND``.

``void *__buf``

  The location where the data to be sent. The minimum unit of NAND buffer size is Block Size, which means that the Buffer Size should be an integral multiple of the Block Size. The Block Size should be obtained from ``ITP_IOCTL_GET_BLOCK_SIZE`` of ``ioctl()``. This size will not be an integral multiple of 0X20000, but something like 0X1FFFFC or 0X3FFFFC, so please use this size to configure the buffer size, and please refer to ``LoadImage()`` in :file:`<sdk_boot>/project/bootloader/boot.c` or the ITP NAND NAND example attached at the end of this section for the program.

``size_t __nbyte``

  The length of the data to be sent, in Block Size (see the description of ``void *__buf`` in the previous section).

**Description**

Users can use this function to read back data from a specific UART port

The following shows how to use the function.

.. code-block:: c

    int fd = -1;
    uint8_t *buffer;
    uint32_t blocksize = 0;
    uint32_t pos = 0;
    int WtLen = 1;	// read one block

    fd = open(":nand", O_RDONLY, 0);    // open NAND device
    ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize);	// get NAND block size
    buffer = (uint8_t *)malloc(blocksize);	// locate a buffer with “blocksize”
    memset(buffer, 0x00, blocksize);		// set write data
    lseek(fd, pos, SEEK_SET);	// seek position to block 0
    write(fd, buffer, WtLen);	// read one block data(one block has “blocksize” bytes)
    close(fd);  // close NAND device

Basic example program
--------------------------------

.. code-block:: c

    #include "ite/itp.h"

    void main(void)
    {
        int fd = 0;
        uint32_t blocksize = 0;
        uint8_t *temp = 0;

        itpRegisterDevice(ITP_DEVICE_NAMD, &itpDeviceNand);
        ioctl(ITP_DEVICE_NAND, ITP_IOCTL_INIT, NULL);

        printf("Start NAND test!\n");

        fd = open(":nand", O_RDONLY);
        if (!fd)
            printf("--- open device NAND fail ---\n");
        else
            printf("fd = %d\n”, fd);

        if (ioctl(fd, ITP_IOCTL_GET_BLOCK_SIZE, &blocksize))
            printf("get block size error\n”);

        temp = malloc(blocksize);

        while (1)
        {
            read(fd, temp, 1);
            printf("SpiInfo 0x%x, 0x%x, 0x%x\n”, temp[0], temp[1], temp[2]);
        }

        close(fd);
        free(temp);
    }

.. raw:: latex

    \newpage
