.. _nandflash_support_list:

NAND flash support list
===============================

NAND flash is an external storage device for IT986x. Since there are different specifications nowadays, it is important to check the electrical characteristics of the AC/DC CHARACTERISTICS before use, and to make sure that the NAND CMD is consistent with the definition of Mask ROM and NOR driver.

Usage limitations
----------------------------------------------------------------

- Using AXISPI protocol, the NAND clock frequency needs to be supported above 60Mhz.
- Generally SPI0/SPI1 is used as I/O bridge interface, its frequency should be supported above 20MHZ.

Support List
----------------------------------------------------------------

GIGADEVICE
^^^^^^^^^^^^^

- GD_5F1GQ4UB (Out of production)
- GD_5F2GQ4UB (Out of production)
- GD_5F1GQ4UC (Out of production)
- GD_5F2GQ4UC (Out of production)
- GD_5F4GQ4UC (Out of production)
- GD_5F1GQ4UE
- GD_5F2GQ4UE
- GD_5F1GQ4UF
- GD_5F2GQ4UF
- GD_5F4GQ4UF

MXIC
^^^^^^^^^^^^^

- MX35LF1GE4AB
- MX35LF2GE4AB

PARAGON
^^^^^^^^^^^^^

- PN26G01AWSIUG

XTX
^^^^^^^^^^^^^

- XT26G01A
- XT26G02A
- XT26G04A

WINBOND
^^^^^^^^^^^^^

- W25N01GV

ATO
^^^^^^^^^^^^^

- ATO25D1GA

TOSHIBA
^^^^^^^^^^^^^

- TC58CVG2S0HRAIG

Dosilicon
^^^^^^^^^^^^^

- DS35Q1GA


.. raw:: latex

    \newpage
