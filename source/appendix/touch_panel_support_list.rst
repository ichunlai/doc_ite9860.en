.. _touch_panel_support_list:

Touch Panel support list
===============================

Touch panels with I2C and SPI interfaces, capacitive and resistive touch panels, pure touch panels and touch panels with their own touch buttons are all supported.

Usage limitations
----------------------------------------------------------------

- Try to use touch panel with sample rate of 60~120 FPS to avoid frequent report sampling which affects the execution efficiency of other applications.
- I2C clock 100kHz ~ 1MHz

Support List
----------------------------------------------------------------

- ALLTOUCH
- CYTMA568
- FT5216
- FT5306
- FT5316
- FT5406
- FT6336
- FT6436
- GSL1691
- GSL3680
- GT911
- GT5688
- HY4635
- ILI2118A
- IT7260
- SIS9252
- SIS9255
- ST1633
- ST1727
- ZET6221
- ZET6231
- ZET7235
- ZT2083


.. raw:: latex

    \newpage
