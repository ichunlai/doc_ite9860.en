.. _norflash_support_list:

NOR flash support list
===============================

NOR flash is an external storage device for IT986x. Since there are different specifications nowadays, it is important to check the electrical characteristics of the AC/DC CHARACTERISTICS before use, and to make sure that the NOR CMD is consistent with the definition of Mask ROM and NOR driver.

Usage limitations
----------------------------------------------------------------

- Using AXISPI protocol, NOR clock frequency needs to be supported above 60Mhz.
- EX4B CMD needs to be 0xE9 as defined by Mask ROM
- Only NORs with Exit 4-byte command as 0xE9 can be supported.

Support List
----------------------------------------------------------------

We have made a NOR support list with both serial mode and QUAD mode both verified to be correct in the AXISPI_80Mhz environment, and these NORs can support the related functions defined by IT986x.

Macronix
^^^^^^^^^^^^^

- MX_25L25645G

Boya
^^^^^^^^^^^^^

- BY_25Q64AS
- BY_25Q128AS

Zbit
^^^^^^^^^^^^^

- ZB_25VQ64
- ZB_25VQ128

Dosilicon
^^^^^^^^^^^^^

- DS_25Q64M
- DS_25Q4AM

Winbond
^^^^^^^^^^^^^

- WIN_W25Q32JV
- WIN_W25Q64JV
- WIN_W25Q128JV

GigaDevice
^^^^^^^^^^^^^

- GD_GD25Q128C
- GD_GD25Q256D

FORESEE
^^^^^^^^^^^^^

- FS_25Q128F2TFI


.. raw:: latex

    \newpage
