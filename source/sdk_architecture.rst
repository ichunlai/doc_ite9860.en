﻿.. _sdk_architecture:

Introduction to the ITE SDK framework
****************************************************

Introduction to the SDK directory
==========================================

/build
--------------------------------

This directory contains the batch files and shell scripts needed to build projects, and the final output will be placed in this directory. When you need to add a new project, you must add several new batch files and shell scripts for the new project in this directory , which contains three subdirectories: :file:`_presettings`, :file:`openrtos`` and :file:`win32`.

_presettings
^^^^^^^^^^^^^^^^^^^^

It stores the Kconfig default values. For more information about Kconfig, please refer to :ref:`kconfig`.

openrtos
^^^^^^^^^^^^^^^^^^^^

It contains the batch files needed to build projects that can run on OpenRTOS™ (https://www.freertos.org/openrtos.html). The final build output is also placed in this directory. There are six main types of batch files in it:

- :file:`<project_name>.cmd`

  Each project has a corresponding :file:`<project_name>.cmd`. Clicking on this file will launch the qconf build and setup tool so you can start configuring and building a project.

  The underlying mechanism of this file is to set the value of :envvar:`CFG_PROJECT` environment variable to ``<project_name>`` and then call :file:`build.cmd`, and :envvar:`CFG_PROJECT` environment variable will be used to generate the build directory and find subdirectories of the same name in :file:`<sdk_root>/project` directory. So it is important to name ``<project_name>`` correctly.

  For example, :file:`my_project.cmd` sets the value of :envvar:`CFG_PROJECT` environment variable to ``my_project``. When building, the system looks for files in the :file:`<sdk_root>/project/my_project` directory and creates the :file:`<sdk_root>/build/openrtos/my_project` directory.

  To manually create a new project yourself, you can make a copy of :file:`build_projects.cmd` and rename it to :file:`<project_name>.cmd`.

- :file:`<project_name>_all.cmd`

  :file:`<project_name>_all.cmd` only serves to set two additional environment variables, :envvar:`BOOTLOADER` and :envvar:`ARMLITECODEC`, compared with :file:`<project_name>.cmd`, so that :file:`<project_name>_all.cmd` has additional bootloader and codec projects compared with :file:`<project_name>.cmd`.

- :file:`build.cmd`

  This file sets the :envvar:`BUILD_CMD` environment variable to the batch file that stores the actual build commands, i.e. :file:`<sdk_root>/build/openrtos/post_build.cmd`, and sets the :envvar:`RUN_CMD` environment variable to the batch file that stores the commands for actual running the project, i.e. :file:`<sdk_root>/build/openrtos/<project_name>/project/<project_name>/exec.cmd`. Finally, execute qconf or mconf to start the configuration and build interface.

  qconf reads the values of :envvar:`BUILD_CMD` environment variable and :envvar:`RUN_CMD` environment variable at startup and executes the batch file referenced by :envvar:`BUILD_CMD` environment variable when you click the |build_icon| button, and the batch file referenced by :envvar:`RUN_CMD` environment variable when you click the |run_icon| button.

.. |run_icon| image:: /_static/qconf_run_icon.png
  :alt: run icon
  :width: 14
  :height: 14

- :file:`common.cmd`

  The main purpose is to set :envvar:`PATH` environment variables so that the required toolchain, tools and related dll can be found without entering the full path during the build process.

- :file:`post_build.cmd`

  The actual batch file used to build the project. This file first uses :file:`<sdk_root>/tool/bin/cmake.exe` to generate the makefile, and then uses :file:`<sdk_root>/tool/bin/make.exe` to build the project.

- :file:`console.cmd`

  The tools batch file is useful when running toolchain or tools in command line mode. This batch file first sets :envvar:`PATH` environment variable with :file:`common.cmd`, and then displays the Windows command line window.

  The reason why you need to set :envvar:`PATH` environment variable with common.cmd is that some tools programs are compiled in cygwin environment and these software need to load cygwin related dll files when running. These dll files are located in the :file:`<sdk_root>/tool/bin` directory. If you do not add the :file:`<sdk_root>/tool/bin` directory to :envvar:`PATH` environment variable, you will not be able to run the software compiled in cygwin. common.cmd provides a solution to add the :file:`<sdk_root>/tool/bin` directory to the :envvar:`PATH` environment variable.

win32
^^^^^^^^^^^^^^^^^^^^

It contains the batch files required to build projects that can run on Windows™. The final build outcome is also placed in this directory.

/data
--------------------------------

It contains UI resource files shared by all projects.

/doc
--------------------------------

Documents related to SDK.

/openrtos
--------------------------------

Includes openrtos source code, as well as boot code and CPU-related code.

/project
--------------------------------

It contains project-specific code for each project.

/sdk
-------------------------------

Includnig drivers, shared libraries, third-party program.

/tool
-------------------------------

It contains auxiliary tools needed for the compilation process.

/win32
-------------------------------

It contains the underlying library required to build Win32.

CMake
================================

ITE SDK uses CMake as the generator for its build system. For information on how to use CMake, please see the documentation on CMake website (https://cmake.org/documentation/).

.. _kconfig:

Kconfig
==================

ITE SDK adopts Kconfig to build the configuration options. For more details on the Kconfig syntax, please see https://www.kernel.org/doc/Documentation/kbuild/kconfig-language.txt.

.. _build_flow:

Build Process
==================

Here describes what actually happens at the ground layer when you click the |build_icon| button to build a project through the qconf build and configure tool:

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

1. Generate makefile through CMake. See :file:`<sdk_root>/build/openrtos/post_build.cmd`.

   .. code-block:: shell

      if exist CMakeCache.txt (
          cmake.exe -G"Unix Makefiles" "%CMAKE_SOURCE_DIR%"
      ) else (
          cmake.exe -G"Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE="%CFG_TOOLCHAIN_FILE%" "%CMAKE_SOURCE_DIR%"
      )

   When you need to use CMake to cross compile, you will need to define the toolchain information through ``-DCMAKE_TOOLCHAIN_FILE="%CFG_TOOLCHAIN_FILE%"``. Once CMake has been executed once and :file:`CMakeCache.txt` file has been generated, CMake can retrieve toolchain-related information from the File, so there is no need to set the ``-DCMAKE_TOOLCHAIN_FILE="%CFG_TOOLCHAIN_FILE%"`` option.

   The makefile generated by CMake is located at :file:`<sdk_root>/build/openrtos/<project_name>/Makefile`.

2. Generate elf format binary code - ``<project_name>`` with make. Please see :file:`<sdk_root>/build/openrtos/post_build.cmd`. The generated elf format binary code is located at :file:`<sdk_root>/build/openrtos/<project_name>/project/<project_name>/<project_name>`.

3. Remove the elf header of elf format binary code with objcopy to generate raw binary code without any header. See :file:`<sdk_root>/sdk/build.cmake`.

   .. code-block:: shell

      add_custom_command(
          TARGET ${CMAKE_PROJECT_NAME}
          POST_BUILD
          COMMAND ${OBJCOPY}
          ARGS -O binary ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME} ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}.bin
      )

   The output file will locate at :file:`<sdk_root>/build/openrtos/<project_name>/project/<project_name>/<project_name>.bin`.

4. Generate ROM file with :file:`<sdk_root>/tool/bin/mkrom.exe`. See :file:`<sdk_root>/sdk/build.cmake`.

   .. code-block:: shell

      add_custom_command(
          TARGET ${CMAKE_PROJECT_NAME} POST_BUILD
          COMMAND mkrom
          ARGS ${args} ${PROJECT_SOURCE_DIR}/sdk/target/ram/${CFG_RAM_INIT_SCRIPT} ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_PROJECT_NAME}.bin ${CMAKE_CURRENT_BINARY_DIR}/kproc.sys
      )

   Example of the use of mkrom is as follows:

   .. code-block::

       mkrom -z -b 512K init.scr input.bin output.rom

   The above command inputs :file:`init.scr` and :file:`input.bin` to mkrom and asks mkrom to output :file:`output.rom`. The :option:`-z` is to specify that :file:`input.bin` should be compressed. The structure of the output ROM file is as follows:

   .. image:: /_static/file_struct_rom.png

   The resulting file is located at :file:`<sdk_root>/build/openrtos/<project_name>/project/<project_name>/kproc.sys`. This file can be burned into NOR flash and booted via NOR Boot Mode.

   .. note::

      In NOR boot mode, the Mask Rom code in ITE chip will look for the ITE ROM header from the 0x0 location of NOR flash and verify if the format of the header is correct. Then, it finds the address of the initial script from the header, loads the initial script and writes a value to the proper register according to the initial script. Finally, find the address of the compressed binary code in NOR flash according to the header, decompress it and load it to the memory address 0x0. Then the CPU will start to retrieve instructions from the memory address 0x0 and execute them.

      If the correct ITE ROM header is missing, the Mask Rom code will not be able to find the address of the binary code in NOR flash and load it into memory. Therefore, the system will not boot up either.

   .. note::

      Initial script is written in C like script language, which contains a bunch of register addresses and register value pairing, used to set the initial clock of the Chip, and initialize the memory controller settings.

   The initial script used to build the ROM can be set in the field of :menuselection:`System --> RAM initial script` in qconf configuration and build tool.

   .. image:: /_static/qconf_init_script.png

5. Use :file:`<sdk_root>/tool/bin/pkgtool.exe` to create a pkg file. The command is as follows:

   .. code-block::

       D:/ite_sdk/tool/bin/pkgtool.exe
           -o ITEPKG03.PKG
           --nor-unformatted-data2 D:/ite_sdk/build/openrtos/my_project/project/my_project/kproc.sys
           --nor-unformatted-data2-pos 0x80000
           --nor-unformatted-size 0x2D0000
           --nor-partiton0-dir D:/ite_sdk/build/openrtos/my_project/data/private
           --nor-partiton1-dir D:/ite_sdk/build/openrtos/my_project/data/public
           --nor-partiton2-dir D:/ite_sdk/build/openrtos/my_project/data/temp
           --nor-partiton0-size 0xDC0000
           --nor-partiton1-size 0x40000
           --nor-partiton2-size 0xF0000
           --nor-partiton3-size 0
           --partition
           --version 1.0
           --nor-unformatted-data1 D:/ite_sdk/build/openrtos/my_project/project/bootloader/kproc.sys
           --key 0

   :option:`-o ITEPKG03.PKG` indicates outputting a file named :file:`ITEPKG03.PKG`. :option:`--nor-unformatted-data1 ../bootloader/kproc.sys --nor-unformatted-data2 ../my_project/kproc.sys --nor-unformatted-data2-pos 0x80000` indicates placing :file:`../bootloader/kproc.sys` at the address of 0x0, and placing :file:`../my_project/kproc.sys` at 0x80000. :option:`--nor-partiton0-dir D:\\ite_sdk\\build\\openrtos\\my_project\\data\\private --nor-partiton0-size 0xDC0000` indicates that the Reserved Area is 0x2D0000 bytes in size, and also that the File System Area starts from 0x2D0000. :option:`--nor-partiton0-dir D:\\ite_sdk\\build\\openrtos\\my_project\\data\\private --nor-partiton0-size 0xDC0000` specifies that the content of the first partition in the File System Area comes from :file:`D:\\ite_sdk\\build\\openrtos\\my_project\\data\\private`, and the size is 0xxDC0000.

   .. note::

      See :ref:`norflash_space_layout` for detailed introduction to Reserved Area and File System Area.

6. Convert the pkg file to rom file with :file:`<sdk_root>/tool/bin/pkgtool.exe`. The command is as follows:

   .. code-block::

       D:\ite_sdk\tool\bin\pkgtool.exe -s 0x1200000 -o ITE_NOR.ROM -n ITEPKG03.PKG

   :option:`-s 0x1200000 -o ITE_NOR.ROM` specifies that the output file name is :file:`ITE_NOR.ROM` and size is 0x1200000 bytes. :option:`-n ITEPKG03.PKG` specifies that the input file name is :file:`ITEPKG03.PKG`.

.. _norflash_space_layout:

Layout of NOR flash storage space
=======================================

Complete space layout of NOR flash
-------------------------------------

By default, this SDK uses NOR flash as the system storage space, which is divided into two main sections: the reserved area and the file system. The reserved area is used to store the Bootloader, MAC Address and Firmware Image, while the remaining space other than the reserved area is dedicated to the file system and can be divided into up to four partitions. :numref:`file_struct_rom_full` shows the NOR flash storage layout of an indoor machine, packaged with the following :file:`pkgtool.exe` command. When planning, attention should be paid to the size and location of each area so that they do not overlap with each other. Due to NOR flash address restrictions, each location must be aligned with 64KBytes.

.. code-block::

    D:\ite_sdk\tool\bin\pkgtool.exe
        -o ITEPKG03.PKG
        --nor-unformatted-data2 D:\ite_sdk\build\openrtos\my_project\project\my_project\kproc.sys
        --nor-unformatted-data2-pos 0x80000
        --nor-unformatted-size 0x2D0000
        --nor-partiton0-dir D:\ite_sdk\build\openrtos\my_project\data\private
        --nor-partiton1-dir D:\ite_sdk\build\openrtos\my_project\data\public
        --nor-partiton2-dir D:\ite_sdk\build\openrtos\my_project\data\temp
        --nor-partiton0-size 0xDC0000
        --nor-partiton1-size 0x40000
        --nor-partiton2-size 0xF0000
        --nor-partiton3-size 0
        --partition
        --version 1.0
        --nor-unformatted-data1 D:\ite_sdk\build\openrtos\my_project\project\bootloader\kproc.sys
        --key 0

.. note::

    The location of the MAC address is not specified via the PKG tool.

.. figure:: /_static/file_struct_rom_full.png
   :name: file_struct_rom_full

   NOR flash storage layout

The reserved area stores data that is not suitable for the file system, currently including Bootloader, MAC Address and Firmware Image. The size of the reserved area is determined by the value of :menuselection:`File System --> NOR reserved size` in the qconf configuration and build tool. As shown in :numref:`qconf_reserved_area_size`.

.. figure:: /_static/qconf_reserved_area_size.png
   :name: qconf_reserved_area_size

   size of the reserved area

Bootloader
^^^^^^^^^^^^^^^^^^^^^^

The bootloader must be placed at location 0x0 for booting. the size of the bootloader can be acquired from the file size of :file:`<sdk_root>/build/openrtos/<project_name>/project/bootloader/kproc.sys`.

MAC Address
^^^^^^^^^^^^^^^^^^^^^^

The MAC address is usually placed between the Bootloader and Firmware Image, and the storage location is specified by :menuselection:`Network --> Enable Network --> Enable Ethernet module --> MAC address data is stored in static storage --> Position of MAC address data` in the qconf configuration and build tool, as shown in :numref:`qconf_mac_addr_store_pos`. The MAC address takes up 8 bytes.

.. figure:: /_static/qconf_mac_addr_store_pos.png
   :name: qconf_mac_addr_store_pos

   Setting of the storage location of MAC Address

Firmware Image
^^^^^^^^^^^^^^^^^^^^^^

The Firmware Image location is specified by :menuselection:`Upgrade --> Upgrade Image --> Image position` in the qconf configuration and build tool, as shown in :numref:`qconf_fw_img_pos`. The size of the Firmware Image can be acquired from the file size of :file:`<sdk_root>/build/openrtos/<project_name>/project/<project_name>/kproc.sys`.

.. figure:: /_static/qconf_fw_img_pos.png
   :name: qconf_fw_img_pos

   Setting of the storage location of Firmware Image

File system partition
----------------------------

The file system in the file system partition is in FAT format. The first partition is located immediately after the last byte of the reserved area. There must be no gaps between each partition and they must be adjacent to each other.The location of each partition is specified by :menuselection:`File System --> NOR disk partition # --> The partition # size of NOR disk` in the qconf setup and build tool, as shown in :numref:`qconf_nor_disk_partition`. In the last partition, you can specify size 0 to use all the remaining space.

.. figure:: /_static/qconf_nor_disk_partition.png
   :name: qconf_nor_disk_partition

   Setting of the internal file system partition

The order of partitions is the order in which drives are mounted, and we can specify the purpose of each partition using a predefined drive letter, as shown in :numref:`qconf_disk_parition_id`.

.. figure:: /_static/qconf_disk_parition_id.png
    :name: qconf_disk_parition_id

    Disk Drive Letter Setting

To generate NOR flash ISO files
---------------------------------------

If you need to generate a NOR flash ISO file, specify the size of the ISO file in :menuselection:`Upgrade --> Generate NOR image --> NOR image size` field of the qconf setup and build tool, and specify the name of the resulting ISO file in :menuselection:`Upgrade --> Generate NOR image --> Upgrade NOR image filename` field of the qconf setup and build tool. As shown in :numref:`qconf_nor_image_size`.

.. figure:: /_static/qconf_nor_image_size.png
   :name: qconf_nor_image_size

   Setting the size of NOR flash ISO file

Boot process
=======================

The boot process will be different, depending on whether you adopt NOR Boot Mode or SPI Boot Mode. Here explains the boot process for these modes

.. note::

   If you are not sure about the difference between NOR Boot Mode and SPI Boot Mode, please see :ref:`usb2spi_switch2cooperative`.

The boot process in NOR Boot Mode
-----------------------------------------

If the system adopts NOR Boot Mode, a mask rom in the chip will be mapped to the address 0x0 in the memory space. This mask rom has a built-in bootloader. Upon power up, the CPU will first go to the mask rom to retrieve the first instruction and execute it. This process is called first-stage boot.

The program in the mask rom retrieves a piece of data from the 0x0 address in NOR Flash into memory and checks if the content matches the correct ITE ROM Header format (see :numref:`file_struct_rom_full`).  If it is correct, the initial script next to the ITE ROM Header will be executed, and the Compressed Binary Code will be decompressed and placed at the front of the RAM. After decompression, the RAM is remapped to the 0x0 address in the memory space and the CPU's Program Counter is modified to 0x0. Therefore, the CPU will fetch instructions from the very beginning of the RAM and execute them. Next, the second stage of boot up begins.

The code that is executed in the second stage of booting is the actual project code, usually starting from the program in :file:`<sdk_root>/openrtos/boot/fa626/startup.S`. This file is written in assembly language. In this code, the initial interrupt vector table is set up, as well as the stacks needed for each ARM mode. Then, execute the ``BootInit()`` function (located in :file:`<sdk_root>/openrtos/boot/init.c`). After ``BootInit()`` function, execute ``main()`` function.

The boot process in SPI Boot Mode
--------------------------------------

If the system is booted in SPI Boot Mode, the RAM will be mapped to the 0x0 address in the memory space. When you click the |run_icon| button on the qconf setup and build tool, the :file:`run.cmd` batch file in the project's dedicated build directory will be executed. For example, if your project name is ``my_project``, then :file:`<sdk_root>/build/openrtos/my_project/project/my_project/run.cmd` will be run. The batch file will first run the initial script under the ``glamomem -t glamomem.dat -i –q`` command, and the path to the initial script file is specified in :file:`<sdk_root>/build/openrtos/my_project/project/my_project/glamomem.dat`. You can change the initial script to be run here by modifying the :menuselection:`Debug --> Debug initial script` option in the qconf setup and build tool.

.. attention::

   The initial script here is not the same as the initial script run in NOR Boot Mode. If you want to modify the initial script run by NOR Boot Mode, please see :ref:`build_flow`.

.. image:: /_static/qconf_dbg_init_script.png

After running the initial script, ``glamomem -t glamomem.dat -l my_project.bin`` command will be executed to load the contents of :file:`my_project.bin` file to the top of the memory. Next, ``glamomem -t glamomem.dat -R 0x00000001 -a 0xd8300000`` command will be executed to write the value of register 0 to 1 to start the CPU, and the CPU will retrieve the instruction from the memory 0x0 address. The subsequent steps are the same as the second stage of NOR Boot Mode.
