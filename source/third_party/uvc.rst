.. _uvc:

UVC
=============

Function description
-------------------------------------
The IT986x SDK is pre-ported with the libuvc library (https://github.com/libuvc/libuvc) to support the use of cameras compliant with the USB device class specification. The Library supports the specification of the latest UVC version 1.5 as defined by the USB-IF (USB Developers Forum) .

The supported camera output formats for the Library are as follows:

.. list-table::
  :header-rows: 1

  * - Format
    - Driver
    - Example
  * - YUV
    - Y
    - Y
  * - MJPEG
    - Y
    - Y
  * - H264
    - Y
    - Y
  * - RGB
    - Y
    - Y

The "Driver" field is labeled `Y` to indicate that the chip has built-in hardware support to convert the format to RGB565 for LCD output. The "Example" field is marked as `Y` to indicate that there is a sample code in the test_uvc for reference.


Related KConfig settings
----------------------------------------------------------------

- :menuselection:`Peripheral --> USB Enable`

  Enable USB Module.

- :menuselection:`Peripheral --> USBHCC Disable`

  Disable HCC Module.

- :menuselection:`Video --> USB Video Class Enable`

  Enable USB Video Class


Related source code files
----------------------------

.. list-table::
  :header-rows: 1

  * - Path
    - Description
  * - ``<sdk_root>/sdk/driver/uvc``
    - Pre-ported libuvc library
  * - ``<sdk_root>/project/test_uvc``
    - UVC test program. Only for users to quickly test if a camera is supported, and there is no UI. For H264 output format, it does not support real-time display.
  * - | ``<sdk_root>/sdk/share/flower/video/filter_uvc.c``
    - A complete sample code for player that supports YUV or MJPEG or H264 UVC input formats. It can be freely expanded to support video recording or photo capture. For actual projects, it is recommended to refer to this sample code and then make modifications.



Description of Key functions
---------------------------------------
The UVC library built into the IT986x SDK is ported from the libuvc library (https://github.com/libuvc/libuvc). For full API description, please refer to https://ken.tossell.net/libuvc/doc/modules.html. A few key functions are described below:


uvc_init
^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: uvc_error_t uvc_init(uvc_context_t **pctx)

   Initializes the UVC context.


uvc_open
^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: uvc_error_t uvc_open(uvc_device_t *dev, uvc_device_handle_t **devh)

   Open a UVC device.


uvc_start_streaming
^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: uvc_error_t uvc_start_streaming(uvc_device_handle_t *devh, uvc_stream_ctrl_t *ctrl, uvc_frame_callback_t *cb, void *user_ptr, uint8_t flags)

   Begin streaming video from the camera into the callback function.


uvc_get_stream_ctrl_format_size
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: uvc_error_t uvc_get_stream_ctrl_format_size(uvc_device_handle_t *devh, uvc_stream_ctrl_t *ctrl, enum uvc_frame_format cf, int width, int height, int fps)

   Get a negotiated streaming control block for some common parameters.


uvc_set_ae_mode
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: uvc_error_t uvc_set_ae_mode(uvc_device_handle_t *devh, uint8_t mode)

   Sets camera's auto-exposure mode.


uvc_stop_streaming
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: void uvc_stop_streaming(uvc_device_handle_t *devh)

   Closes all streams, ends threads and cancels pollers.


uvc_close
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: void uvc_close(uvc_device_handle_t *devh)

   Closes stream, frees handle and all streaming resources.


uvc_exit
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. c:function:: void uvc_exit(uvc_context_t *ctx)

   Closes the UVC context, shutting down any active cameras.


The Instruction of Test projects
---------------------------------------

The IT986x SDK contains two test projects related to UVC: ``test_uvc`` and ``test_uvc_player``. ``test_uvc`` is smaller in size and is mainly used when you want to quickly check if a new camera output can be supported for the display. To quickly understand how to use the libuvc library, you can also start by tracing this test project.

``test_uvc_player`` provides a complete example and supports YUV, MJPEG, and H264 input formats, and can combine the input content with UI to be displayed on the LCD display. It is recommended to refer to ``test_uvc_player`` for practical projects. The two test projecrs are described in details below.


test_uvc
---------------------------------------

The test project ``test_uvc`` is located at :file:`<sdk_root>/project/test_uvc`. It demonstrates how the input image format of a UVC camera are properly converted and loaded into the LCD frame buffer for display.

The test project is designed to quickly check if a camera output can be supported. Therefore modifications are required when the target board is connected to a different UVC camera. The following section describes how to run the test project and how to modify it to make it work correctly.


Hardware Setup
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please connect the LCD Panel, UVC Camera, USB to SPI board and power cable according to the following picture.

.. image:: /_static/e_circuit_uvc.png

KConfig Settings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please run  :file:`<sdk_root>/build/openrtos/test_uvc.cmd`, and make configuration according to the following steps:

- Check :menuselection:`Screen --> LCD Enable`.

  Enable the LCD device. Refer to the :ref:`lcd` chapter for the rest of the LCD-related configuration. It is recommended that you run the test project in the :ref:`lcd` chapter before running this project to make sure your LCD is configured correctly.

- Check :menuselection:`Graphics --> JPEG H/W enable or not`.

  If the input format of the UVC Camera is MJPEG (Motion JPEG), be sure to check this option to enable hardware acceleration.

- Check :menuselection:`Peripheral --> UART Enable`.
- Check :menuselection:`Peripheral --> UART Enable --> UART0 Enable`.
- :menuselection:`Peripheral --> UART0 Enable --> UART0 mode`
  Select the operation mode of UART0, and check UART0 Interrupt Enable.
- Set :menuselection:`Peripheral --> UART Enable --> UART0 Baud Rate` as 115200.
- Set :menuselection:`GPIO --> UART0 TX Pin` as 4.
- Select UART0 at :menuselection:`Debug --> Debug Message Device` as Debug output.

  .. note::

    Note: The reason why the UART0 TX pin is set to GPIO4 is because in the EVB circuit diagram, GPIO4 of the IT986x is connected to the RX (labeled as DBG_TX in the circuit diagram) of the USB to SPI board.

    .. image:: /_static/e_circuit_uart.png

- Uncheck :menuselection:`Peripheral --> USBHCC Enable`.
- Check :menuselection:`Peripheral --> USB0 Enable`.
- Uncheck :menuselection:`Peripheral --> USB0 Enable --> USB Device Mode`.
- Check :menuselection:`Video --> USB Video Class Enable`.

Implementation Example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

  .. |run_icon| image:: /_static/qconf_run_icon.png
    :alt: run icon
    :width: 14
    :height: 14

  .. |build_icon| image:: /_static/qconf_build_icon.png
    :alt: build icon
    :width: 14
    :height: 14

1. Click the |build_icon| button to build the project. Please ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Reset the power of EVB.
3. When the program is finished building, click the |run_icon|  button to boot up the system via USB to SPI board.
4. Run Tera Term, and click :menuselection:`Setup --> Serial Port` on the main menu. For the Port, please follow the Serial Port displayed when the USB to SPI board is connected to the PC, and for the Speed, please enter 115200, and check if the Device Manufacturer is displayed as "FTDI".

   .. image:: /_static/teraterm_setup_serial_port_dialog.png

5. 5.	If the system successfully detects the USB Camera device, ``Device found`` -> ``Device opened`` will be shown on Tera Term in order. The message is as follows.

   .. code-block:: shell

      Found uvc_driver usb driver!
       find driver: (config #1, interface 1)

       Found uvc_driver usb driver!
      Chip: 0x960, gChipRevision: 0x0
      Init memory status-----------------------
      max system bytes =     496544
      system bytes     =     496544
      in use bytes     =     495256
      -----------------------------------------
      UVC initialized
      Device found
      unsupported descriptor subtype: 3
      unsupported descriptor subtype: 13
      unsupported descriptor subtype: 3
      unsupported descriptor subtype: 13
      Device opened
      DEVICE CONFIGURATION (0c45:64ab/[none]) ---
      Status: idle
      VideoControl:
              bcdUVC: 0x0100

6. If the system successfully detects that the Camera supports UVC protocol, the following message will be displayed. Lines 6 and 8 indicate that the Camera supports motion jpeg as video output format, and lines 45 and 47 indicate that the Camera supports the video output format of RAW YUY2.

   .. code-block:: shell
      :linenos:
      :emphasize-lines: 6,8,15,19,20,23,27,28,31,35,36,39,43-45,47,54,58,61,65,66,69,73,74,77,81-83

      VideoControl:
              bcdUVC: 0x0100
      VideoStreaming(1):
              bEndpointAddress: 129
              Formats:
              MJPEGFormat(1)
                        bits per pixel: 0
                        GUID: 4d4a5047000000000000000000000000 (MJPG)
                        default frame: 1
                        aspect ratio: 0x0
                        interlace flags: 00
                        copy protect: 00
                              FrameDescriptor(1)
                                capabilities: 00
                                size: 1600x1200
                                bit rate: 153623560-614494240
                                max frame size: 3840589
                                default interval: 1/20
                                interval[0]: 1/20
                                interval[1]: 1/5
                              FrameDescriptor(2)
                                capabilities: 00
                                size: 1280x1024
                                bit rate: 104881160-419524640
                                max frame size: 2622029
                                default interval: 1/20
                                interval[0]: 1/20
                                interval[1]: 1/5
                              FrameDescriptor(3)
                                capabilities: 00
                                size: 1280x960
                                bit rate: 98327560-393310240
                                max frame size: 2458189
                                default interval: 1/20
                                interval[0]: 1/20
                                interval[1]: 1/5
                              FrameDescriptor(4)
                                capabilities: 00
                                size: 640x480
                                bit rate: 24599560-147597360
                                max frame size: 614989
                                default interval: 1/30
                                interval[0]: 1/30
                                interval[1]: 1/5
              UncompressedFormat(2)
                        bits per pixel: 16
                        GUID: 5955593200001000800000aa00389b71 (YUY2)
                        default frame: 1
                        aspect ratio: 0x0
                        interlace flags: 00
                        copy protect: 00
                              FrameDescriptor(1)
                                capabilities: 00
                                size: 1600x1200
                                bit rate: 122880000-122880000
                                max frame size: 3840000
                                default interval: 1/4
                                interval[0]: 1/4
                              FrameDescriptor(2)
                                capabilities: 00
                                size: 1280x1024
                                bit rate: 104857600-188743680
                                max frame size: 2621440
                                default interval: 1/9
                                interval[0]: 1/9
                                interval[1]: 1/5
                              FrameDescriptor(3)
                                capabilities: 00
                                size: 1280x960
                                bit rate: 98304000-176947200
                                max frame size: 2457600
                                default interval: 1/9
                                interval[0]: 1/9
                                interval[1]: 1/5
                              FrameDescriptor(4)
                                capabilities: 00
                                size: 640x480
                                bit rate: 24576000-147456000
                                max frame size: 614400
                                default interval: 1/30
                                interval[0]: 1/30
                                interval[1]: 1/20
                                interval[2]: 1/5
      END DEVICE CONFIGURATION

   The fields of size and interval indicate the output resolution and frame rate supported by the camera. The output formats supported by the camera, as shown in the above information, are listed in the table below.


   .. list-table::
     :header-rows: 1

     * - Color Format
       - Width
       - Height
       - Frame Rate
     * - MJPEG
       - 1600
       - 1200
       - 20
     * -
       -
       -
       - 5
     * -
       - 1280
       - 1024
       - 20
     * -
       -
       -
       - 5
     * -
       - 1280
       - 960
       - 20
     * -
       -
       -
       - 5
     * -
       - 640
       - 480
       - 30
     * -
       -
       -
       - 5
     * - YUY2
       - 1600
       - 1200
       - 4
     * -
       - 1280
       - 1024
       - 9
     * -
       -
       -
       - 5
     * -
       - 1280
       - 960
       - 9
     * -
       -
       -
       - 5
     * -
       - 640
       - 480
       - 30
     * -
       -
       -
       - 20
     * -
       -
       -
       - 5

7. Based on the above information, use ``the uvc_get_stream_ctrl_format_size()`` function to set the color format, width, height and frame rate. For example, in the code below (which is retrieved from :file:`<sdk_root>/project/test_uvc/example.c`), the highlighted section shows how to set the output size of the camera to 640x480, the frame rate of 30 fps, and color format of Motion JPEG for video streaming.

   .. code-block:: c
      :linenos:
      :emphasize-lines: 43-46

      int uvc_main()
      {
          uvc_context_t       *ctx;
          uvc_device_t        *dev;
          uvc_device_handle_t *devh;
          uvc_stream_ctrl_t   ctrl;
          uvc_error_t         res;
          pthread_t           pJpegDecodeThread, pDisplayThread;

          ...

          res = uvc_init(&ctx);

          ...

          /* Locates the first attached UVC device, stores in dev */
          res = uvc_find_device(ctx, &dev, 0, 0, NULL); /* filter devices: vendor_id, product_id, "serial_num"        */

          if (res < 0)
          {
              uvc_perror(res, "uvc_find_device"); /* no devices found */
          }
          else
          {
              puts("Device found");

              /* Try to open the device: requires exclusive access */
              res = uvc_open(dev, &devh);
              if (res < 0)
              {
                  uvc_perror(res, "uvc_open"); /* unable to open device */
              }
              else
              {
                  puts("Device opened");
                  /* Print out a message containing all the information that libuvc
                   * knows about the device */
                  uvc_print_diag(devh, stderr);

                  ...

                  /* Try to negotiate a 640x480 30 fps YUYV stream profile */
                  res = uvc_get_stream_ctrl_format_size(
                      devh, &ctrl,            /* result stored in ctrl */
                      UVC_FRAME_FORMAT_MJPEG, /* Motion JPEG. try _COMPRESSED */
                      640, 480, 30);          /* width, height, fps */

                  //res = uvc_get_stream_ctrl_format_size(
                  //    devh, &ctrl,           /* result stored in ctrl */
                  //    UVC_FRAME_FORMAT_YUYV, /* YUV 422, aka YUV 4:2:2. try _COMPRESSED */
                  //    640, 480, 25);         /* width, height, fps */

                  //res = uvc_get_stream_ctrl_format_size(
                  //    devh, &ctrl,            /* result stored in ctrl */
                  //    UVC_FRAME_FORMAT_H264,  /* YUV 422, aka YUV 4:2:2. try _COMPRESSED */
                  //    1920, 1080, 30);        /* width, height, fps */

8. If the settings from step 7 are correct, the callback function passed in when ``uvc_start_streaming()`` is called will be called every time the system receives a frame from the camera. For example, the callback function used in the following code is ``_uvcFrameCb``. You can use printf in ``_uvcFrameCb`` to output any messages to tell if the settings from step 7 are correct.

   .. code-block:: c

      res = uvc_start_streaming(devh, &ctrl, _uvcFrameCb, NULL, 0);

9. 9.	Based on the color format set in the above steps, modify :file:`<sdk_root>/project/test_uvc/example.c` to select the proper decoding and display process.

   .. note::

      The ``test_uvc`` project does not include the H.264 decoding flow. If the camera outputs as H.264 video streaming, please refer to the ``test_uvc_player`` project.

   - Select initialization queues

     .. list-table::
       :header-rows: 1

       * - Color Format
         - Code
       * - UVC_FRAME_FORMAT_YUYV
         - ``packetQueueInit(&gtYuyvQueue, _yuyvPktRelease, 4, sizeof(YuyvPkt));``
       * - UVC_FRAME_FORMAT_MJPEG
         - | ``packetQueueInit(&gtJpegInputQueue, _jpegInputPktRelease, 4, sizeof(JpegInputPkt));``
           | ``packetQueueInit(&gtJpegOutputQueue, _jpegOutputPktRelease, 4, sizeof(JpegOutputPkt));``


   - Select initialization threads

     .. list-table::
       :header-rows: 1

       * - Color Format
         - Code
       * - UVC_FRAME_FORMAT_YUYV
         -
       * - UVC_FRAME_FORMAT_MJPEG
         - | ``pthread_create(&pJpegDecodeThread, NULL, _jpegThread, 0);``
           | ``pthread_create(&pDisplayThread, NULL, _displayThread, 0);``

   - Select close functions

     .. list-table::
       :header-rows: 1

       * - Color Format
         - Code
       * - UVC_FRAME_FORMAT_YUYV
         - ``packetQueueEnd(&gtYuyvQueue);``
       * - UVC_FRAME_FORMAT_MJPEG
         - | ``pthread_join(pJpegDecodeThread, NULL);``
           | ``pthread_join(pDisplayThread, NULL);``
           | ``packetQueueEnd(&gtJpegInputQueue);``
           | ``packetQueueEnd(&gtJpegOutputQueue);``

10. Click the |build_icon| button to re-build the project. When finished, click the |run_icon| button to boot up the system via the USB to SPI board. If everything is correctly configured, you should be able to see the live output image from the camera on the LCD display. The preview will stop automatically after 2 minutes.


test_uvc_player
---------------------------------------

The ``test_uvc_player`` project is located at :file:`<sdk_root>/project/test_uvc_player`. It shows how to use the Flower player framework, developed by ITE, to play the output video from the UVC Camera as well as how to integrate it with UI. See :file:`<sdk_root>/project/test_flower` for a detailed description of the Flower player framework. This section only focuses on the uvc player.

The key code for ``test_uvc_player`` is located at :file:`<sdk_root>/project/test_uvc_player/layer_uvc_player.c`. This project supports three input formats: YUV, MJPEG, and H264. The Flower player is composed of multiple filters. Different playback formats will contain different filters, and the filters are connected to each other by pins. The following is a description of the filters for the three input formats and the connection status between the filters.


YUV
^^^^^

The ``test_uvc_player`` project provides a streaming example of display for the YUV input format, and the structure diagram is as follows.

.. graphviz::

   digraph yuv {
     rankdir=LR
     node [shape=rect]
     "UVC filter" -> "Display filter" [label= "YUYV Stream"]
   }
　
And here are the related APIs:

flow_start_uvc_yuv
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_start_uvc_yuv(IteFlower *f, int width, int height, int fps)

   Build YUV format filter chain.


flow_stop_uvc_yuv
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_stop_uvc_yuv(IteFlower *f)

   Destroy YUV format filter chain.


MJPEG
^^^^^

The ``test_uvc_player`` project provides a streaming example of display and recording for the MJPEG input format, and the structure diagram is as follows.

.. graphviz::

   digraph mjpeg {
     rankdir=LR
     node [shape=rect]
     "UVC filter" -> "MJPEG DEC filter" [label= "MJPEG Stream"]
     "UVC filter" -> "AVI filter" [label= "MJPEG Stream"]
     "MJPEG DEC filter" -> "Display filter"
   }

And here are the related APIs:

flow_start_uvc_mjpeg
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_start_uvc_mjpeg(IteFlower *f, int width, int height, int fps);

   Build Motion JPEG format filter chain.


flow_stop_uvc_mjpeg
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_stop_uvc_mjpeg(IteFlower *f)

   Destroy Motion JPEG format filter chain.

flow_start_uvc_mjpeg_record
"""""""""""""""""""""""""""
.. c:function:: void flow_start_uvc_mjpeg_record(IteFlower *f, char* file_path, int width, int height, int fps)

   Save all the incoming mjpeg streams into a file.

flow_stop_uvc_mjpeg_record
"""""""""""""""""""""""""""
.. c:function:: void flow_stop_uvc_mjpeg_record(IteFlower *f, char* file_path, int width, int height, int fps)

   Stop recording and close file.

H264
^^^^^

The ``test_uvc_player`` project provides a streaming example of display and recording for the H264 input format, and the structure diagram is as follows.

.. graphviz::

   digraph h264 {
     rankdir=LR
     node [shape=rect]
     "UVC filter" -> "H264 DEC filter" [label= "H264 Stream"]
     "UVC filter" -> "AVI filter" [label= "H264 Stream"]
     "H264 DEC filter" -> "Display filter"
   }
　
And here are the related APIs:

flow_start_uvc_h264
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_start_uvc_h264(IteFlower *f, int width, int height, int fps);

   Build h264 format filter chain.

flow_stop_uvc_h264
"""""""""""""""""""""""""""
.. c:function:: uint8_t flow_stop_uvc_h264(IteFlower *f)

   Destroy h264 format filter chain.

flow_start_uvc_h264_record
"""""""""""""""""""""""""""
.. c:function:: void flow_start_uvc_h264_record(IteFlower *f, char* file_path, int width, int height, int fps)

   Save all the incoming h264 streams into a file.

flow_stop_uvc_h264_record
"""""""""""""""""""""""""""
.. c:function:: void flow_stop_uvc_h264_record(IteFlower *f, char* file_path, int width, int height, int fps)

   Stop recording and close file.



Hardware Connection
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please connect the LCD Panel, UVC Camera, USB to SPI board and power cable according to the following picture.

.. image:: /_static/e_circuit_uvc.png

If you need to test the recording feature, you also need to insert the SD Card daughter board(COM3 by default).

.. image:: /_static/e_circuit_uvc_sd.png

KConfig Settings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Please run :file:`<sdk_root>/build/openrtos/test_uvc_player_all.cmd`, and make configuration according to the following steps:

- Check :menuselection:`Screen --> LCD Enable`.

  Enable the LCD device. Refer to the :ref:`lcd` chapter for the rest of the LCD-related configuration. It is recommended that you run the test project in the :ref:`lcd` chapter before running this project to make sure your LCD is configured correctly.

- Check :menuselection:`Graphics --> JPEG H/W enable or not`.

  If the input format of the UVC Camera is MJPEG (Motion JPEG), be sure to check this option to enable hardware acceleration.

- Check :menuselection:`Peripheral --> UART Enable`.
- Check :menuselection:`Peripheral --> UART Enable --> UART0 Enable`.
- :menuselection:`Peripheral --> UART0 Enable --> UART0 mode`

  Select the operation mode of UART0, and check UART0 Interrupt Enable.

- Set :menuselection:`Peripheral --> UART Enable --> UART0 Baud Rate` as 115200.
- Set :menuselection:`GPIO --> UART0 TX Pin` as 4.
- Select UART0 at :menuselection:`Debug --> Debug Message Device` as Debug output.

  .. note::

     The reason why the UART0 TX pin is set to GPIO4 is because in the EVB circuit diagram, GPIO4 of the IT986x is connected to the RX (labeled as DBG_TX in the circuit diagram) of the USB to SPI board.

    .. image:: /_static/e_circuit_uart.png

- Uncheck :menuselection:`Peripheral --> USBHCC Enable`.
- Check :menuselection:`Peripheral --> USB0 Enable`.
- Uncheck :menuselection:`Peripheral --> USB0 Enable --> USB Device Mode`.
- Check :menuselection:`Video --> USB Video Class Enable`.
- Check :menuselection:`Video --> Video Enable`.

The rest of the options are related to the UI, so just follow the project defaults. Note that the following statement must be added to :file:`<sdk_root>/project/test_uvc_player/Kconfig`, and it must be before the ``source $CMAKE_SOURCE_DIR/sdk/Kconfig`` statement:

.. code-block:: kconfig

   config BUILD_FLOWER
       def_bool y


Implementation Example
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Before running the ``test_uvc_player`` project, please modify the :file:`<sdk_root>/project/test_uvc_player/layer_uvc_player.c`. Make proper configuration at the highlighted section of the following code according to the format supported by the connected UVC Camera.

.. code-block:: c
   :emphasize-lines: 15-18

   #include <assert.h>
   #include <stdio.h>
   #include <stdlib.h>
   #include <string.h>
   #include "scene.h"
   #include "ctrlboard.h"
   #ifdef __OPENRTOS__
   #include "flower.h"
   #endif
   #include "ite/itv.h"

   ...

   //User define uvc camera
   #define UVC_WIDTH     640
   #define UVC_HEIGHT    480
   #define UVC_FPS       30
   #define UVC_FORMAT    2  //  0 : H264 1:MJPEG 2:YUV

And then follow the steps below:

1. Click the |build_icon| button to build the project. Please ensure that:

  - The USB to SPI board is properly connected.

    - If you have not connected the USB to SPI board to your PC, please refer to :ref:`usb2spi_connect2pc`.
    - If you have not connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
    - If you have not installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

  - The EVB has been switched to Co-operative Mode.

    .. note::

       If you are not sure whether EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. 2.	Please follow the instructions in ref:`write_image_to_norflash` section to burn ``<sdk_root>/build/openrtos/test_uvc_player/project/test_uvc_player/ITE_NOR.ROM`` to NOR Flash.

3. Reset the power of EVB.
4. You will see the following screen on the LCD display after the system starts.

   .. image:: /_static/ui_uvc_main.png

5. Click on the USB Camera icon on the screen and enter the uvc_player layer.

   - Click "Play/Pause" to start/end playback.
   - Click "Save/Stop" to start/end recording.
   - Click "Back" to go back to the previous page.

   .. image:: /_static/ui_uvc_player.png
