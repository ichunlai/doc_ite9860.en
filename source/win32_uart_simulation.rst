.. _win32_uart_simulation:

Win32 Uart emulation
=============================

Function description
-------------------------

When developing with Win32 emulation, you can emulate the Uart operation on the sub-control product from the Win32 PC side through this API, which means you can develop the Uart communication between the ITE board side and the sub-control product at the stage of Win32 emulation development.

Related source code files
-------------------------

.. list-table::
    :header-rows: 1

    * - Path
      - Description
    * - :file:`sdk/driver/itp/itp_uart_api.c`
      - The main API function file that provides operations on Uart.

Description of related API
----------------------------------------

ComInit
^^^^^^^^^^^^^^^^

.. c:function:: int ComInit(int ComNum, int Baudrate)

  Initialization of Serial Port on Win32.

**Parameters**

``ComNum``

  The number of ComPort where the sub-control product connected to Win32. e.g. COM13, ComNum=13.

``Baudrate``

  Baudrate is the transfer rate of Uart.

**Return**

0 is success, -1 is failure.

ComExit
^^^^^^^^^^^^^^^^

.. c:function:: void ComExit(int ComNum)

  To turn off Serial Port operation on Win32.

**Parameters**

``ComNum``

  The number of ComPort where the sub-control product connected to Win32. e.g. COM13, ComNum=13.


UartReceive
^^^^^^^^^^^^^^^^

.. c:function:: int UartReceive(int ComNum, char *str, int len)

  The same as the ``read()`` function in Uart.

**Parameters**

``ComNum``

  On Win32, ComNum is the same as above; on openrtos, it is device id(e.g. ``ITP_DEVICE_UART0``).

``str``

  The string to be read.

``len``

  The length of the string to be read.

**Return**

If there is a string received, then return the str length, otherwise return 0.

UartSend
^^^^^^^^^^^^^^^^

.. c:function:: int UartSend(int ComNum, char *str, int len)

  The same as the ``write()`` function in Uart.

**Parameters**

``ComNum``

  On Win32, ComNum is the same as above; on openrtos, it is device id(e.g. ``ITP_DEVICE_UART0``).

``str``

  The string to be written.

``len``

  The length of the string to be written.

**Return**

The length of the written string.

Note
-------------------------

The following points should be noted when using Win32 Uart API in Win32 emulation development and actual development in openrots:

- Emulation development process on Win32

  1. Call ``ComInit()`` to initialize first.
  2. Call ``UartSend()`` or ``UartReceive()`` at the place where you need to send or receive data respectively. the length of the ``UartReceive()`` function return needs to confirm whether the buffer has data.
  3. Call ``ComExit()`` when you don't need to use it.

- If you move to develop on openrots

  1. You can still use ioctl(e.g. ``read()``, ``write()``) at the bottom layer of Uart driver.
  2. If you want to continue to use Win32 Uart API, you should separate ``ComInit()`` and ``ComExit()`` with Win32 compiler flag, and the ComNum parameter of ``UartReceive()`` and ``UartSend()`` should be changed to Uart Device id(e.g. ``ITP_DEVICE_UART1``).
