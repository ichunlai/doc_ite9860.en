.. gui_designer:

Build Option Settings
***************************

If you would like to start compiling a project, execute :file:`<sdk_root>/build/openrtos/<project_name>.cmd` or :file:`<sdk_root>/build/openrtos/<project_name>_all.cmd`, and a qconf setup and build tool will be opened. From this window, you can change the project build options and compile the project.

Basic Operations
=================

The following sections are commonly used in qconf setup and build tool.

.. image:: /_static/qconf_intro.png

1.	Storage Settings (Save compiler options settings)
2.	Build Project (Build and compile software)
3.	Setting categories (categories for each setting)
4.	Setting menu (menu for each category)
5.	Setting description (description of each category)
6.	Output screen when building project (output message of the compilation process).

Under :guilabel:`Develop Environment`, the first item of setting category, there are two options as follows

1.	Develop mode (if you select this item, all categories and menus will be displayed)
2.	Release mode (if you select this item, only the common menus will be displayed)

As shown below: Select the development mode and more categories will be displayed.

.. image:: /_static/qconf_devel_mode_setting.png

Select Easy Mode to display, and only the frequently used parts will be displayed.

.. image:: /_static/qconf_release_mode_setting.png

Some settings are related to certain options and will only be displayed when those options are enabled.

For example, in the image below, when :menuselection:`System --> Watch Dog enable` option is checked, the Watch Dog related settings will appear below :guilabel:`Watch Dog enable` option.

.. image:: /_static/qconf_watchdog_enable.png

When :menuselection:`System --> Watch Dog enable` option is unchecked, the Watch Dog-related settings will be hidden.

.. image:: /_static/qconf_watchdog_hidden.png

To save complete setup descriptions
=========================================

The previous section briefly explains how the setup and build tool works. If you would like to save the complete setup description, you can click :menuselection:`File --> Save XML As…`, which will output :file:`kconfig.xml` file.

.. image:: /_static/qconf_save_xml.png

You can open :file:`kconfig.xml` file with Excel to see the tree structure of all the configuration options with a description of each option.

.. image:: /_static/qconf_kconfig_xml.png

Search Config
=================

Click |search icon| on the toolbar or press :kbd:`Ctrl-F` key combination to open a window. In this window, enter the keyword you want to search for, such as ``IIC``, and you will be able to search for settings related to this keyword in the Setting menu.

.. |search icon| image:: /_static/qconf_search_icon.png
  :alt: search icon
  :width: 14
  :height: 14

.. image:: /_static/qconf_search_dialog.png

To search the output window
=========================================

When you click |build_icon| button to start building a project, messages about the building process will be displayed in the output box. Right-click the mouse in this window to open the menu, and click :guilabel:`Search String`.

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

.. image:: /_static/qconf_search_string.png

Enter the string you want to search for in the :guilabel:`Search string` field.

.. image:: /_static/qconf_search_string_dialog.png

You can use the key combination :kbd:`Ctrl-N` to search for the next matching string.

Correspondence between config and CMake variables
========================================================

The setting of each option will result in a corresponding CMake variable definition. In the case of RTC, for example, :menuselection:`Peripheral --> RTC Enable` option is associated with the RTC_ENABLE keyword as seen in the configuration description box. When the |save_icon| button is clicked to save the settings, qconf writes the set(CFG_RTC_ENABLE y) statement in the resulting :file:`<sdk_root>/build/openrtos/<project_name>/config.cmake` file. Note that the original RTC_ENABLE keyword has been prefixed with the string ``CFG_``. So just include the :file:`config.cmake` file in the appropriate CMake script file to get the value. Currently the ITE SDK includes the :file:`config.cmake` file in the :file:`<sdk_root>/CMakeLists.txt` file using the ``include(build/$ENV{CFG_BUILDPLATFORM}/${CMAKE_PROJECT_NAME}/config.cmake)`` clause.

.. |save_icon| image:: /_static/qconf_save_icon.png
  :alt: save icon
  :width: 14
  :height: 14

.. image:: /_static/qconf_rtc_setting.png

.. note::

   The following commands can be found in ``<sdk_root>/build/openrtos/build.cmd``:

   .. code-block:: shell

      qconf --fontsize 11 --prefix "CFG_" --cmakefile %CFG_PROJECT%/config.cmake --cfgfile %CFG_PROJECT%/.config %PRESETTINGS% "%CMAKE_SOURCE_DIR%/project/%CFG_PROJECT%/Kconfig"

   where :option:`--prefix "CFG_"` means that all the keywords of the output configuration items should be prefixed with the ``CFG_`` string. :option:`--cmakefile %CFG_PROJECT%/config.cmake` means qconf will output the file written in CMake script language to :file:`%CFG_PROJECT%/config.cmake` when saving the configuration. :option:`--cfgfile %CFG_PROJECT%/.config` means that previous settings will be read from :file:`%CFG_PROJECT%/.config` and stored in the :file:`%CFG_PROJECT%/.config` file when the configuration is saved. :file:`"%CMAKE_SOURCE_DIR%/project/%CFG_PROJECT%/Kconfig"` means that all options displayed by qconf are from the :file:`%CMAKE_SOURCE_DIR%/project/%CFG_PROJECT%/Kconfig` file.

Menu Specifications
==========================

Please refer to the detailed description of each driver's test project.

