.. _third_party:

Introduction to third-party libraries
********************************************

This chapter provides a detailed description of the use of the third-party libraries that have been ported to the IT986x SDKs, and gives examples of test programs.

.. toctree::
   :glob:

   third_party/uvc.rst
