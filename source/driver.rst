﻿.. _driver:

Introduction to peripherals and related APIs
********************************************************

This chapter introduces the peripherals and the operation functions supported by IT986x series, and gives examples of test programs.

.. toctree::
   :glob:

   driver/gpio.rst
   driver/uart.rst
   driver/lcd.rst
   driver/pwm.rst
   driver/i2c.rst
   driver/spi.rst
   driver/canbus.rst
   driver/saradc.rst
   driver/remote_ir.rst
   driver/key.rst
   driver/touch_key.rst
   driver/norflash.rst
   driver/nandflash.rst
   driver/sd.rst
   driver/wifi.rst
   driver/4g.rst
   driver/wiegand.rst
   driver/alt_cpu.rst
   driver/video.rst
   driver/capture.rst
   driver/audio.rst