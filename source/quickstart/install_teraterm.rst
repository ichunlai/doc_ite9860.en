﻿.. _install_teraterm:

To install the debugging tool software
=============================================

Tera Term is a well-known tool for endpoint devices that can be used to display messages that PC receives from UART. So we use it to receive and display debug messages from EVB.

The steps to install Tera Term are as follows:

1. Download and execute the latest version of this program (e.g. :file:`teraterm-4.105.exe`) from the official Tera Term website (https://osdn.net/projects/ttssh2/releases/).

   .. note:: The following installation screenshots are captured from version 4.105 of Tera Term, those of new versions may be slightly different, please adjust accordingly.

2. Click :guilabel:`Run`.

   .. image:: /_static/teraterm_install_init.png

.. raw:: latex

   \begin{samepage}

3.	Click :guilabel:`Yes`.

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/teraterm_user_access_control.png

.. raw:: latex

   \end{samepage}

4. Choose :guilabel:`I accept the agreement` and click :guilabel:`Next >`.

   .. image:: /_static/teraterm_install_license.png

.. raw:: latex

   \begin{samepage}

5. After selecting the installation folder, click :guilabel:`Next >`.

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/teraterm_install_path.png

.. raw:: latex

   \end{samepage}

6. Click :guilabel:`Next >`.

   .. image:: /_static/teraterm_install_select_component.png

.. raw:: latex

   \begin{samepage}

7. Select a language and click :guilabel:`Next >`.

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/teraterm_install_language.png

.. raw:: latex

   \end{samepage}

8. Adopt the default setting and click :guilabel:`Next >`.

   .. image:: /_static/teraterm_install_menu_name.png

.. raw:: latex

   \begin{samepage}

9. Adopt the default options and click :guilabel:`Next >`.

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/teraterm_install_select_tasks.png

.. raw:: latex

   \end{samepage}

10. Click :guilabel:`Install` to start the installation.

   .. image:: /_static/teraterm_install_ready.png

.. raw:: latex

   \begin{samepage}

11. When the installation is complete, check :guilabel:`Launch Tera Term` box, and click :guilabel:`Finish` to execute Tera Term.

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/teraterm_install_finish.png

.. raw:: latex

   \end{samepage}

12. Select Serial and Port. Port is based on the Serial Port displayed when the USB to SPI board is connected to the PC. Click :guilabel:`OK` to close the window after you finish setting.

   .. image:: /_static/teraterm_select_serial_port.png

.. raw:: latex

   \begin{samepage}

13. Click  :menuselection:`Setup --> Serial port...`

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/teraterm_setup_serial_port.png

.. raw:: latex

   \end{samepage}

14. Please enter 115200 in the Speed field and double check if the Device Manufacturer is FTDI. If not, please select the Port again. Click the :guilabel:`OK` to close the window when the settings are completed.

   .. image:: /_static/teraterm_setup_serial_port_dialog.png

.. raw:: latex

   \begin{samepage}

15. Click :menuselection:`Setup --> Terminal...` and do the following settings in the opened window. Click :guilabel:`OK` to close the window after the settings are completed. Please note that :menuselection:`New-line --> Receive` should be changed to ``LF`` or ``Auto``.

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/teraterm_setup_terminal.png

.. raw:: latex

   \end{samepage}

16. Click :menuselection:`Setup --> Save setup...` and the dialog box will pop up, suggesting where to save the settings you just did. Select an appropriate location and click :guilabel:`Save` to save. The current settings will be adopted for all subsequent Tera Term executions.

   .. image:: /_static/teraterm_setup_save.png

.. raw:: latex

   \begin{samepage}

17. Later, if you need to save the message you received from UART, you can click :menuselection:`File --> Log...` and a dialog box will pop up to indicate where to save the received messages. If you need to record the time when each message is received, you can check the :guilabel:`Timestamp` box.

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/teraterm_save_log.png

.. raw:: latex

   \end{samepage}

.. raw:: latex

    \newpage