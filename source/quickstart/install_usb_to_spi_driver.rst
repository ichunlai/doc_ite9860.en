﻿.. _install_usb_to_spi_driver:

To install driver of USB to SPI board
=======================================

If you need to burn firmware, debug with gdb, read the registers of the chip during runtime, write image files directly to the chip's memory, etc., you will need to connect your PC to the EVB via the USB to SPI board. In order for the USB to SPI board to function properly, you need to install a driver.

.. _install_ftdi_driver:

To install FTDI driver
-------------------------------------------------

The main chip of USB to SPI board is ft2232d, produced by FTDI. Therefore, we are going to install the driver provided by the Company.

.. note::

   * If your system is Windows 10, the driver will be installed automatically and you can skip this step.
   * You can also skip this step if you do not need to receive debug data sent by EVB through uart via the Usb to SPI board.

1. First, plug the USB to SPI board into a USB slot on your PC.
2.	Find :file:`CDM20828_Setup.exe` in the folder of the unzipped :file:`Tools.7z` ( See :ref:`download_sdk` if not knowing where to download :file:`Tools.7z`) and execute it.

   .. hint::

      You can also download the FTDI driver of the latest version from their official website: https://www.ftdichip.com/Drivers/D2XX.htm

3. Click :guilabel:`Extract`.

   .. image:: /_static/ftdi_driver_install_init.png

.. raw:: latex

   \begin{samepage}

4. Click :guilabel:`Next`.

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/ftdi_driver_install_2nd.png

.. raw:: latex

   \end{samepage}

5. Installing

   .. image:: /_static/ftdi_driver_install_ing.png

.. raw:: latex

   \begin{samepage}

6. When the installation is finished, click :guilabel:`Finish` and close the window.

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/ftdi_driver_install_complete.png

.. raw:: latex

   \end{samepage}

7. Open Device Manager and you will find that there are 2 more USB Serial Ports.

   .. image:: /_static/ftdi_driver_install_device_manager.png

   .. note::

      These two USB Serial Ports are not necessarily located at COM3 or COM4, depending on your platform. In any case, the larger numbered port is usually where EVB outputs debug messages, while the smaller  numbered port is used as SPI or JTAG.


To install WinUSB driver
---------------------------------------------

Since the driver provided by FTDI is very unstable when the USB to SPI adapter is used as a JTAG interface, it often leads to accidental disconnection of gdb when using it. Therefore, ITE switched to use the WinUSB driver to drive the first interface of the ft2232d and moved the detailed control actions towards the chip to the AP layer.

To make it easier for users to install the WinUSB driver, ITE provides a program named :file:`usbtospi_driver.exe` in the folder :file:`<sdk_root>/tool/bin/` of the SDK. This program can automatically find the first interface of ft2232d and replace the original driver with WinUSB driver.

Here are the steps to install the WinUSB driver:

1. First, plug the USB to SPI board into a USB slot on your PC.
2. Execute :file:`/tool/bin/usbtospi_driver.exe`. After executing :file:`usbtospi_driver.exe`, the User Account Control dialog will pop up, click :guilabel:`Yes`.

   .. image:: /_static/winusb_user_access_control.png

3. Installing

   .. image:: /_static/winusb_install_console.png

.. raw:: latex

   \begin{samepage}

4. When the Installation is completed, the message ``[INFO] Success,Press Enter to exit this program`` will be shown. Please press :kbd:`Enter` to close this window.

   .. raw:: latex

      \nopagebreak

   .. image:: /_static/winusb_install_success.png

.. raw:: latex

   \end{samepage}

5. Open Device Manager, and you will find an additional Dual RS232 device in your system.

   .. image:: /_static/winusb_install_ite_usb_device.png

   .. note::

      If you have previously installed FTDI driver following the steps described in :ref:`install_ftdi_driver`, you will notice that one COM port (originally two COM ports) is missing from the Device Manager, and that is because the driver for the first interface of the USB to SPI board has been replaced by the WinUSB driver and renamed as Dual RS232 device.

      .. figure:: /_static/winusb_install_before.png

         The circumstance where only FTDI driver is installed

      .. figure:: /_static/winusb_install_after.png

         The circumstance where FTDI driver is installed first, and then WinUSB driver

   .. note::

      You can also use the zadig (https://zadig.akeo.ie/) to install the WinUSB driver. However, using the zadig program to install the WinUSB driver is more complicated and may cause errors. Unless you are very sure about all the steps, it is not recommended to use the zadig program.


.. raw:: latex

    \newpage