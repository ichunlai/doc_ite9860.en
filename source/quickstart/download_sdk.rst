﻿.. _download_sdk:

To Download SDK, Toolchain and Tools
===================================================

To register on ITE SoC website
-------------------------------------------

Please register on ITE SoC website (https://soc.ite.com.tw/) in order to download SDK and Toolchain.

1. Visit https://soc.ite.com.tw/en/login and click :guilabel:`+Registered`:

    .. image:: /_static/quickstart_register_new_account.png

2. Fill in required information, and click :guilabel:`+ Next`:

    .. image:: /_static/quickstart_fill_new_account_data.png

3. You will receive an email with verification code from the Website. Please enter the code on the Webssite.

4. If you pass the verification, your screen will show a notice as below, which indicates that you have successfully registered. Then, please notify your distributer to open your account.

    .. image:: /_static/quickstart_register_success.png


To Log in SoC website
----------------------

1. After your account being opened, please visit https://soc.ite.com.tw/en/login, enter account and password, and click :guilabel:`+Login`:

    .. image:: /_static/quickstart_login.png

.. raw:: latex

  \begin{samepage}

2. Your screen will show as below if you log in successfully:

    .. raw:: latex

      \nopagebreak

    .. image:: /_static/quickstart_login_success.png

.. raw:: latex

  \end{samepage}

Download SDK, Toolchain and Tools
------------------------------------

1. After logging in, click :guilabel:`Developer` label above.
2. Click :guilabel:`IT986x Series` and then :guilabel:`Download`,

    .. image:: /_static/quickstart_download_sdk.png

.. raw:: latex

  \begin{samepage}

3. Choose :guilabel:`SDK Standard` for general product development, or :guilabel:`SDK Video Intercom` for video intercom products, and then click :guilabel:`Files` icon:

    .. raw:: latex

      \nopagebreak

    .. image:: /_static/quickstart_download_standard_sdk.png

.. raw:: latex

  \end{samepage}

4. Here lists all SDK files, among which, ``ITEGCC`` is toolchain, ``V 2.4.3.0`` is SDK, ``Tools`` includes other related tools. Click :guilabel:`Download` icon to download files.

    .. image:: /_static/quickstart_download_file_list.png

.. raw:: latex

    \newpage