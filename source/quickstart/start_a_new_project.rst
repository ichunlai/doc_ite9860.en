.. _start_a_new_project:

To start a new project
=========================

Please unzip the SDK you downloaded (e.g. :file:`20200715_Standard_SDK_v2430_5f74e6_20200722.7z`) with 7-zip (https://www.developershome.com/7-zip/). Assuming you unzip it to :file:`D:\\`, you should see the following in the :file:`D:\\20200715_Standard_SDK_v2430_5f74e6_20200722` directory:

   .. image:: /_static/quickstart_sdk_file_list.png

After unzipping :file:`ite_sdk.7z` to :file:`D:\\`, you will see the following in the :file:`D:\\ite_sdk directory`:

   .. image:: /_static/quickstart_sdk_file_list_detail.png

Here is a brief introduction to each folder:

.. list-table::
    :widths: 20 80

    * - ``/build``
      - This folder contains the batch files and shell scripts needed to build the project, and the final output will be placed here also.
    * - ``/data``
      - Containing UI resource files shared by all projects, such as font files, icons, etc.
    * - ``/doc``
      - Documents related to SDK.
    * - ``/project``
      - Reference projects, and the location of any future new projects.
    * - ``/sdk``
      - Where the software driver and source code of related system are stored.
    * - ``/tool``
      - Location of the tools and software that may be used during compilation.

For a detailed introduction to the SDK, please see :ref:`sdk_architecture`.

.. attention::

   The directory of the SDK cannot contain spaces or any special characters (including Chinese).

To create a new project
--------------------------

Now we will show how to quickly create a project to run on the IT986X standard EVB. To create a new project, first execute :file:`wizard.py` program in the SDK root directory. This tool allows developers to quickly make a copy of an existing project and rename it, and later add any additional features you need based on that project.

1. When you execute :file:`wizard.py`, your screen will show as follows. Please enter the project name (e.g., ``my_project``) in the :guilabel:`Project name:` field. Check :guilabel:`Export project` in the :guilabel:`Project:` field. Select standard_project in the :guilabel:`Project name:` field. After finishing setting, please click :guilabel:`OK`.

    .. image:: /_static/quickstart_wizard_python.png

2. When finished, you will see a new :file:`my_project` directory in the :file:`project` directory under the SDK (assuming that the project name you filled in the previous step is ``my_project``).

    .. image:: /_static/quickstart_wizard_My_Indoor.png

   The contents of the generated project directory are introduced as follows:

    .. list-table::
        :widths: 35 65

        * - :file:`/itu` folder
          - Including HMI data of different resolutions, and they are available in xml and itu formats.
        * - :file:`/media` folder
          - System startup animation, and multimedia data required for the project.
        * - :file:`/sounds` folder
          - Including button click sound files.
        * - :file:`/web` folder
          - Including html pages used in Web Server, allowing users to set parameters and upgrade firmware via web pages.
        * - :file:`/CMakeLists.txt` file
          - A list of files, including projects, which is written in cmake script language.
        * - :file:`/kconfg` file
          - Providing configuration options, written in kconfig script language.
        * - Files for basic functions
          - e.g. :file:`upgrade.c`, :file:`network.c`, :file:`photo.c`, :file:`video.c`, :file:`string.c`, :file:`storage.c`, :file:`screen.c`, :file:`scne.c`.
        * - UI logic related files
          - All files that start with :file:`layer_`, which you can program and develop.
        * - :file:`function_table.c`
          - Function registration, which you can program and develop
        * - :file:`external.c`
          - Communication Thead, which you can program and develop
        * - :file:`external_process.c`
          - Custom events, which you can program and develop

   Besides, there will be two batch files in the :file:`<sdk_root>/build/openrtos` directory and one in the :file:`<sdk_root>/build/win32` directory.

   * :file:`<sdk_root>/build/openrtos/my_project.cmd`
   * :file:`<sdk_root>/build/openrtos/my_project_all.cmd`
   * :file:`<sdk_root>/build/win32/my_project_all.cmd`

   Excute :file:`<sdk_root>/build/openrtos/my_project_all.cmd` so the setting window and build tool will pop up.

.. note::

   For a detailed introduction to the wizard tool, please see :ref:`wizard`.

To set up and build projects
-------------------------------------------

Excute :file:`<sdk_root>/build/openrtos/my_project_all.cmd` to open qconf setting and build tool.

   .. image:: /_static/qconf_My_Indoor.png

Click |build_icon| to build a project.

.. |build_icon| image:: /_static/qconf_build_icon.png
  :alt: build icon
  :width: 14
  :height: 14

To extract the compiled ISO file
----------------------------------

If the compilation is successful, the resulting ISO files will be placed in :file:`<sdk_root>/build/openrtos/my_project/project/my_project` directory, and the entire build process will generate two ISO files, :file:`XXX.PKG`` and :file:`XXX.ROM`, where XXX will be different names according to the project. Take the my_project for example, the resulting ISO files will be named :file:`ITEPKG03.PKG` and :file:`ITE_NOR.ROM`.

.. note::

   The name of the generated ISO file can be changed via qconf. The actual name of the PKG file is determined by setting option of :menuselection:`Upgrade --> Upgrade package filename`, while that of the ROM file is determined by :menuselection:`Upgrade --> Generate NOR image` option.

   .. image:: /_static/qconf_my_project_pkg_filename.png

``*.PKG`` files are smaller in size and are used for firmware upgrades. This format is used when upgrading the firmware on a circuit board via USB pen drive or the web interface on EVB. If you are updating the firmware via the USB pen drive, be sure to format the USB pen drive to FAT32.

``*.ROM`` file size, the same as that of NOR flash, contains complete data such as bootloader and resource files required for UI. When the NOR flash is empty, you need to burn the firmware through nor programmer, or through the USB to SPI board, using :file:`<sdk_root>/tool/bin/usb_spi_tool.exe` under the root directory of the SDK (see :ref:`write_image_to_norflash` for details.

There is another ISO file, :file:`XXX.bin`, which contains only uncompressed raw binary code and can be written directly to memory address 0x0 to execute. Since writing NOR flash is usually much slower than writing RAM, ROM files are usually burned to NOR flash first during development, and then if the UI and other related resource files are not changed, the bin file can be written directly to memory via USB to SPI board, which will be much faster than burning NOR flash.

.. list-table:: Comparison of ISO file formats
   :widths: 20 80

   * - ``*.bin``
     - Containing only uncompressed raw binary code, which can be written directly to memory address 0x0 for execution, and used only during development.
   * - ``*.PKG``
     - Being able to include the full firmware content (excluding blank areas) or optionally only the resources that need to be updated (e.g. only UI resources).
   * - ``*.ROM``
     - Containing complete firmware components (firmware, UI resources, bootloader, etc., even including blank areas).

Next, we will do some preparation work to burn NOR flash.

.. _usb2spi_connect2pc:

To connect USB to SPI converter board to the PC
----------------------------------------------------

Please connect the USB to SPI board to PC via Mini USB.

   .. image:: /_static/usb2spi_connect2pc.png

.. note::

   If it’s connected correctly, Green/Red LED will light up.

.. _usb2spi_connect2board:

To connect the USB to SPI board to EVB
----------------------------------------------

In a power-off state, connect the 8PIN message cable of the USB to SPI board to the corresponding interface of the IT9860 Standard EVB.

   .. image:: /_static/usb2spi_connect2board.png

.. _usb2spi_switch2cooperative:

To switch EVB to Co-operative Mode (including instructions for switching to NOR Boot Mode)
-------------------------------------------------------------------------------------------------------------------

Co-operative Mode
^^^^^^^^^^^^^^^^^^^

When the board is in co-operative mode, the CPU core inside the IT986x will be in a unbooted state when powered up. At this time, users can use :file:`<sdk_root>/tool/bin/usb_spi_tool.exe` to read and write NOR/NAND flash, SoC internal memory and registers on the board via USB to SPI converter board. This mode is often used in the development phase.

To switch the EVB to co-operative mode, short-circuit the two pins in the yellow box below in a power-off state.

.. figure:: /_static/usb2spi_cooperative_jump.png
   :name: usb2spi_cooperative_jump

   Insert the two pins in the yellow box above into the jump to short, and it’s switched to Co-operative mode.

NOR Boot Mode
^^^^^^^^^^^^^^^^^^^

When the board is in NOR boot mode and powered up, the internal CPU core of IT986x will immediately retrieve instructions from the internal maskrom to execute upon. This mode is intended to be used in the final product stage.

To switch the EVB to NOR boot mode, please remove the jump from the pin in the yellow box below in the power off state.

.. figure:: /_static/usb2spi_norboot_jump.png
   :name: usb2spi_norboot_jump

   Remove the jump on the pins as shown in yellow box above, and it becomes NOR Boot Mode.

.. note::

   If you are using a circuit board other than EVB, please check with H/W staff on how to switch the BOOT_CFG setting.

.. list-table::
   :widths: 30 30 40
   :header-rows: 1

   * - BOOT_CFG1
     - BOOT_CFG0
     -
   * - 0
     - 0
     - SD Card Boot
   * - 0
     - 1
     - NOR Flash Boot
   * - 1
     - 0
     - SPI NAND Boot
   * - 1
     - 1
     - Co-operative mode

To confirm whether EVB is currently in Co-operative mode or NOR Boot mode, you can use :file:`usb_spi_tool.exe`. Here are the steps:

1. Power up the board first, then execute :file:`<sdk_root>/tool/bin/usb_spi_tool.exe`.
2. Check the usb_spi_tool title bar.

   If ``Co-operative mode`` is shown in the middle of the title bar, then the EVB is currently in Co-operative mode.

   .. image:: /_static/usb2spi_cooperative_title.png

   If ``NOR Booting mode`` is shown in the middle of the title bar, then EVB is currently in NOR Boot mode.

   .. image:: /_static/usb2spi_norboot_title.png

   .. attention::

      When the EVB is in NOR boot mode and powered on, it will immediately boot the firmware stored in NOR Flash. When the firmware is running, not all functions provided by :guilabel:`usb_spi_tool.exe` software are available. This is because some functions may be interfered with by the running firmware.

.. _write_image_to_norflash:

To burn ISO files to NOR flash
----------------------------------------

.. _usb2spi_connect:

In this section, we will explain how to burn the ISO file to NOR flash with :file:`<sdk_root>/tool/bin/usb_spi_tool.exe` in the SDK root directory. Before using usb_spi_tool, you must make sure:

- The USB to SPI board is correctly connected.

  - If you have not connected the USB to SPI board to the PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not yet connected the USB to SPI board to the EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not installed the USB to SPI board driver, please refer to :ref:`install_usb_to_spi_driver`.

- EVB has been switched to Co-operative Mode.

  .. note::

     If you are not sure if EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

After confirming that all hardware are properly connected, you can start to run the burning software. The steps are as follows:

1. After powering up the board, run :file:`<sdk_root>/tool/bin/usb_spi_tool.exe` and make sure the CHIP ID on the title bar of the window is 9860.

   .. image:: /_static/usb2spi_software_init.png

   .. note:: If the connection among the USB to SPI board and EVB or PC is not correct, the following screen will appear, indicating that the chip id cannot be read.

      .. image:: /_static/usb2spi_connect_fail.png

2. Switch to the :guilabel:`SPI Writer` tab.

   .. image:: /_static/usb2spi_spi_writer_tag.png

3. Click :guilabel:`GET ID` button to confirm that NOR flash information can be read and detected properly.

   .. image:: /_static/usb2spi_get_nor_info.png

4. Select the firmware ISO you want to burn, :file:`XXX.ROM` (select the directory of the ROM file), and click :guilabel:`Burn`` to start burning.

   .. image:: /_static/usb2spi_select_rom_path.png

   Burning:

   .. image:: /_static/usb2spi_writting_norflash.png

5. ``Spi burn Success!!`` will be displayed when the burning is completed.

   .. image:: /_static/usb2spi_write_norflash_success.png

To boot the system from NOR flash
-------------------------------------------

1. Switch the EVB to NOR boot mode in power-off state.

   .. note::

      If you are not sure if EVB has switched to NOR Boot Mode, please refer to :ref:`usb2spi_switch2cooperative`.

2. Power up.

3. If successfully booted, you should see the following message via Tera Term:

   .. image:: /_static/usb2spi_norboot_success.png

   .. note::

      If you do not know how to use Tera Term to view EVB output messages, please refer to :ref:`teraterm_show_debug_msg`.

   .. note::

      If your EVB is connected to TL068HWXH08 6.86\" MIPI panel but you cannot see anything displayed on the screen, please check the qconf setting, and make sure that :menuselection:`GPIO --> Backlight PWN Pin` option of build tool is set to 63.

      .. image:: /_static/qconf_gpio_pwm_pin_setting.png

      Backlight controls part of the corresponding hardware circuit:

      .. image:: /_static/e_circuit_backlight.png

      If your touch panel is not responding, please check the following options of qconf settings and build tools:

      1. :menuselection:`GPIO --> Touch INT Pin` must be set to 60.
      2. :menuselection:`GPIO --> Touch INT Pin Active High` option must be unchecked.
      3. :menuselection:`GPIO --> Touch Reset Pin` must be set to 59.
      4. :menuselection:`GPIO --> IIC1 CLK GPIO Pin` must be set to 62.
      5. :menuselection:`GPIO --> IIC1 DATA GPIO Pin` must be set to 61.

      .. image:: /_static/qconf_gpio_touch_panel_setting.png

      The corresponding hardware circuit to the touch panel:

      .. image:: /_static/e_circuit_touch_panel.png


To boot the system via USB to SPI board
------------------------------------------------

"Booting the system via USB to SPI board" is called SPI Boot Mode, while "booting from NOR flash" is called NOR Boot Mode. SPI Boot Mode is suitable during development because it saves some steps and time compared with NOR Boot Mode.

To use NOR Boot Mode, we must:

1. First adjust the jump on the EVB to switch to Co-operative Mode.
2. Write the new firmware to the NOR flash via the burner software.
3. Power off the EVB.
4. Adjust the jump on the EVB to switch to NOR Boot Mode.
5. Reapply power.

To use SPI Boot Mode, we just need to leave the jump at the Co-operative Mode setting, reset the EVB power, and then click :guilabel:`Run` button on the qconf setting and build tool. Here are the detailed steps of using SPI Boot Mode:

To use SPI Boot Mode, you must ensure that:

- The USB to SPI board is correctly installed.

  - If you have not connected the USB to SPI board to the PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not yet connected the USB to SPI board to your EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not yet installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

- The EVB is switched to Co-operative Mode.

  .. note::

     If you are not sure if EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

Then, please follow the steps below:

1. To use SPI Boot Mode, you need to modify some compilation settings. Please execute :file:`<sdk_root>/build/openrtos/my_project_all.cmd` to open the qconf setting and build tool.

2. Please uncheck :menuselection:`Screen --> Shows Logo on Bootloader` first, then click the |build_icon| button to create the project again.

   .. image:: /_static/qconf_uncheck_show_logo_on_bootloader.png

3. Reset the power of EVB.

4. When the program has finished building, click the |run_icon| button to boot up the system via USB to SPI board.

.. |run_icon| image:: /_static/qconf_run_icon.png
  :alt: run icon
  :width: 14
  :height: 14

.. TODO 描述點擊run後的背後執行細節。

.. _teraterm_show_debug_msg:

To display the debug message sent by EVB via UART on the PC
-----------------------------------------------------------------

To display the debug message sent by EVB via UART on the PC, you must make sure that:

- The USB to SPI board is correctly installed.

  - If you have not connected the USB to SPI board to the PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not yet connected the USB to SPI board to your EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not yet installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

Then, please follow the steps below:

1. Open Tera Term. If you have not yet installed Tera Term or have questions about the settings, please read :ref:`install_teraterm`.

   .. image:: /_static/teraterm_init.png

2. Reset or re-power the EVB. If Tera Term is set correctly, you should be able to see the debug message of the boot process on it.

   .. image:: /_static/teraterm_boot_message.png

To use GDB debugger
--------------------------------------

To use GDB debugger, you must make sure that:

- The USB to SPI board is correctly installed.

  - If you have not connected the USB to SPI board to the PC, please refer to :ref:`usb2spi_connect2pc`.
  - If you have not yet connected the USB to SPI board to your EVB, please refer to :ref:`usb2spi_connect2board`.
  - If you have not yet installed the driver of USB to SPI board, please refer to :ref:`install_usb_to_spi_driver`.

- The EVB has been switched to Co-operative Mode.

  .. note::

     If you are not sure if EVB has switched to Co-operative Mode, please refer to :ref:`usb2spi_switch2cooperative`.

Next, please follow the steps below:

1. To use the GDB debugger, the compilation settings must be modified. So you need to execute :file:`<sdk_root>/build/openrtos/my_project_all.cmd` first to open the qconf setup and build tool.

2. Switch :menuselection:`Develop Environment --> Develop Environment` to ``Develop Mode``.

   .. image:: /_static/qconf_switch_to_develop_mode.png

3. Uncheck :menuselection:`System --> Watch Dog enable`.

   .. image:: /_static/qconf_uncheck_watchdog.png

4. Uncheck :menuselection:`System --> Internal Settings --> Memory Debug enable`.

   .. image:: /_static/qconf_uncheck_memory_debug.png

5. Uncheck :menuselection:`System --> Internal Settings --> CPU write-back cache enable`.

   .. image:: /_static/qconf_uncheck_writeback.png

6. Uncheck :menuselection:`Screen --> Shows Logo on Bootloader`.

   .. image:: /_static/qconf_uncheck_show_logo_on_bootloader.png

7. Switch :menuselection:`SDK --> Build Type` to ``Debug`` or ``DebugRel``.

   .. image:: /_static/qconf_switch_to_debugrel.png

   Click the |build_icon| button to build a project.

   If you have not burned the ISO file to NOR flash before, please refer to :ref:`write_image_to_norflash` to burn the generated ISO file to NOR flash first.

8. Excute ``<sdk_root>/build/openrtos/my_project/project/my_project/jp2.cmd``.

   .. image:: /_static/gdb_jp2.png

9. Excute ``<sdk_root>/build/openrtos/my_project/project/my_project/gdb.cmd`` to open the gdb console window. You can run the project directly by typing :kbd:`c` at the ``(gdb)`` command prompt.

   .. image:: /_static/gdb_console_init.png

   .. note::

      Please refer to `Debugging with GDB <https://sourceware.org/gdb/current/onlinedocs/gdb/index.html>`_ for more information on gdb commands.

.. note::

   You can also use the GUI interface to debug the firmware by referring to the method described in :ref:`gui_debug`.

To use Visual Studio to simulate running projects on a PC
------------------------------------------------------------

To shorten the development and debugging time for software developers, ITE SDK provides the ability to simulate the project in a Windows environment using Visual Studio 2013 under Win32. This approach not only ensures the independence of the software operation, but also reduces the time for simple modifications to be downloaded to EVB/Demo Board for testing. Here is the method:

1. Please install VS2013 first, of which we will not go into the detailed steps here.
2. Execute ``<sdk_root>/build/win32/my_project.cmd`` to open the qconf setup and build tool.
3. When the qconf setup and build tool appears, click the "Save |save_icon| " button and close the window. The system will pop up the following message:

   .. |save_icon| image:: /_static/qconf_save_icon.png
     :alt: build icon
     :width: 14
     :height: 14

   .. image:: /_static/win32_build_message.png

   The message shows that the program will configure the environment needed for all runtimes. It shows that the system has detected VS 2013 and created three virtual drives A:, B:, and T: in windows, which correspond to the private, public, and temp directories under the build menu. Close this window by pressing any key if you succeeded doing so. The corresponding folder is created under :file:`<sdk_root>/build/win32`, find the :file:`my_project.sln` file in that folder and open it with visual studio 2013.

   .. image:: /_static/win32_build_sln.png

4. Compile and run it in Visaul Studio 2013. If you succeed, the following screen will appear:

   .. image:: /_static/win32_simulate.png

.. note::

   If the computer has been rebooted after the last execution of :file:`<sdk_root>/build/win32/my_project.cmd`, the previous runtime environment will no longer exist, please run :file:`<sdk_root>/build/win32/my_project.cmd` first to rebuild the runtime environment. Then use Visual Studio to open :file:`my_project.sln` file.

.. TODO 似乎沒有解釋得很清楚。
