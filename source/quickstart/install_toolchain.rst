﻿.. _install_toolchain:

To Install Toolchain
=========================

To install Toolchain in Windows
----------------------------------------------

Please directly unzip the toolchain file (e.g. :file:`ITEGCC.2018.04.13.gcc-5.4.0_windows.7z` )  that you downloaded with 7-zip(https://www.developershome.com/7-zip/) into :file:`C:\\`. Then, in the :file:`C:\\ITEGCC` folder, you will see the following content:

    .. image:: /_static/quickstart_itegcc.png

Now we have finished the installation of Toolchain in Windows.

.. TODO 若需要同時存在多個編譯器時該怎麼辦？

.. 在Linux下安裝Toolchain
.. --------------------------
..
.. TBD


.. note::

  If more than one toolchains have to exist at the same time, please see ":ref:`multiple_toolchain`"

.. raw:: latex

    \newpage