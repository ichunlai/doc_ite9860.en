.. _changelog:

Revised Record
****************************************************************
.. list-table::
   :widths: 10 10 80
   :header-rows: 1

   * - Revised date
     - Releases
     - Revised Description

   * - 2021/04/01
     - V1.0
     - Initial version V1.0.
   * - 2022/01/06
     - V1.0.1
     - Add index. Fixed typos.
   * - 2022/01/07
     - V1.1
     - Added UVC chapter.
