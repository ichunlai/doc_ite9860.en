.. _upgrade:

To update firmware
=======================================

IT986x provides several firmware update methods. This chapter introduces all the board-side firmware update methods for users to choose according to the current situation.

List of Firmware update methods
----------------------------------------------

The goal of a firmware update is to update the system by re-burning a modified :file:`.PKG` or :file:`.ROM` file built by the compiler to the Flash on the board. There are many different ways to update the firmware, and they are divided into two categories:

- Firmware update during development

  - Burning via USB2SPI tool

- Firmware update during boot-up

  - USB flash drive update
  - SD card update

Firmware update during development
----------------------------------------------

Burning via USB2SPI tool
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This method is often used for the first burn, or as a last resort when you encounter difficulty in burning with any of the updated methods. Please refer to :ref:`write_image_to_norflash` and :ref:`write_image_to_nandflash` for detailed steps.

Firmware update during boot-up
----------------------------------------------

The mechanism of firmware update during boot-up is done through the bootloader. So please follow the steps below to make sure your project has bootloader enabled.

1. Please run :file:`<sdk_root>/build/openrtos/<project_name>.cmd` or :file:`<sdk_root>/build/openrtos/<project_name>_all.cmd` to open the qconf setup and build tool.
2. Click :guilabel:`Develop Environment` and make sure that :guilabel:`Enable bootloader or not` msut be checked.

   .. image:: /_static/qconf_check_enable_bootloader.png

Then follow the instructions in each section.

USB flash drive update
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is the most commonly used and basic update method, just click :menuselection:`Peripheral --> USB# Enable` in the qconf setup and build tool to enable the corresponding USB port as USB Host Mode.

.. image:: /_static/qconf_check_enable_usb0.png

Remember to put the compiled :file:`XXX.PKG` file into the USB flash drive and insert the USB flash drive into the corresponding USB port before turning on the device.

.. note::

   The :file:`XXX.PKG` file name must be the name set by the :menuselection:`Upgrade --> Upgrade package filename` setting option in the qconf setup and build tool. In addition, the USB flash drive must be formatted to FAT format.

   .. image::  /_static/qconf_set_package_filename.png

SD card update
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The configuration is similar to the USB flash drive update, first click :menuselection:`Storage --> SD# Enable` in the qconf setup and build tool to enable the corresponding SD device.

.. image:: /_static/qconf_check_enable_sd0.png

Next, click :guilabel:`GPIO` and make sure the GPIO setting related to the SD card slot is correct. If the setting is wrong, the system will not detect the SD card slot and the update will fail.

.. image:: /_static/qconf_set_sd0_gpio.png

Remember to put the compiled :file:`XXX.PKG` file into the SD card, and insert the SD card into the corresponding card slot before turning on the device.

.. note::

   The file name must be the name set by the :menuselection:`Upgrade --> Upgrade package filename` setting option in the qconf setup and build tool. In addition, the SD Card must be formatted to FAT format.
